<?php
/**
 * The template for displaying all single posts
 */

get_header();
global $post;

$page_sidebar = $page_meta['page_sidebar'][0];

if($page_sidebar&&$page_sidebar!=="custom"&&!get_option($page_sidebar."_show")) $page_sidebar = null;

?>

<?php while ( have_posts() ) : the_post(); ?>

<?php echo tccedu_get_section_nav($post); ?>

<main id="site" class="page-standard">
	
	<div id="page-content"<?php echo $page_sidebar?" class=\"with-sidebar\"":"" ?>>
		
		<div class="wrap">
			
			<div class="page-title-wrap"><h1 class="page-title"><?php the_title(); ?></h1></div>
			
			<div class="inwrap">
				<?php
				if($page_sidebar){
					$sidebar_title = $page_sidebar=="custom"?$page_meta['custom_sidebar_title'][0]:stripslashes(get_option($page_sidebar.'_title'));
					$sidebar_copy = $page_sidebar=="custom"?$page_meta['custom_sidebar_copy'][0]:stripslashes(get_option($page_sidebar.'_copy'));
					$sidebar_link = $page_sidebar=="custom"?$page_meta['custom_sidebar_link'][0]:get_option($page_sidebar.'_link');
					$sidebar_cta = $page_sidebar=="custom"?$page_meta['custom_sidebar_cta'][0]:get_option($page_sidebar.'_cta');
					$sidebar_btn_class = $page_sidebar=="custom"?$page_meta['custom_sidebar_btn_class'][0]:get_option($page_sidebar.'_btn_class');
					echo "
				<div id=\"page-sidebar\" class=\"_".$page_sidebar.(!$page_meta['page_sidebar_not_sticky'][0]?" sticky":"")."\">
					<div class=\"panel\">
						<div class=\"sidebar-content\">";
					if($sidebar_title) echo "<label class=\"nonh2\">".$sidebar_title."</label>";
					if($sidebar_copy) echo wpautop($sidebar_copy);
					if($sidebar_link){
						echo "<a href=\"".$sidebar_link."\" class=\"btn ".
						($sidebar_btn_class?$sidebar_btn_class:"btn btn-4 btn-text btn-arrow").
						"\">".($sidebar_cta?$sidebar_cta:"Learn More")."</a>";
					}
					echo "
						</div>
					</div>
				</div>";
				} ?>
				<div class="page-copy page-col"><?php the_content(); ?></div>
			</div>
			
			
		</div>
		
	</div>
	
	<?php if($page_meta['events_mod_enable'][0]) echo tccedu_get_upcoming_events_module($post); ?>
	
	<?php if($page_meta['content_feed_mod_enable'][0]) echo tccedu_get_content_feed($post); ?>
	
</main>

<?php endwhile; ?>

<?php
get_footer();