<?php
function pardotCall($url,$data,$method='GET') {
	global $user_key,$api_key;
	if(isset($api_key)) $data = array_merge($data,array(
		'user_key'=>$user_key,
		'api_key'=>$api_key
	));
	$url .= (strpos($url,'?')!==false?'&':'?').http_build_query($data,null,'&');
	$curl_handle = curl_init($url);
	curl_setopt($curl_handle,CURLOPT_CONNECTTIMEOUT,30);
	curl_setopt($curl_handle,CURLOPT_TIMEOUT,40);
	curl_setopt($curl_handle,CURLOPT_PROTOCOLS,CURLPROTO_HTTPS);
	curl_setopt($curl_handle,CURLOPT_SSL_VERIFYHOST,2);
	curl_setopt($curl_handle,CURLOPT_RETURNTRANSFER,1);
	curl_setopt($curl_handle,CURLOPT_HTTPHEADER,array("Authorization: Pardot api_key={$api_key}, user_key={$user_key}"));
	if (strcasecmp($method,'POST') === 0) {
		curl_setopt($curl_handle,CURLOPT_POST,true);
	} elseif (strcasecmp($method,'GET') !== 0) {
		curl_setopt($curl_handle,CURLOPT_CUSTOMREQUEST,strtoupper($method));
	}
	$pardotApiResponse = curl_exec($curl_handle);
	if ($pardotApiResponse === false) {
		$humanReadableError = curl_error($curl_handle);
		$httpResponseCode = curl_getinfo($curl_handle,CURLINFO_HTTP_CODE);
		curl_close($curl_handle);
		throw new Exception("Unable to successfully complete Pardot API call to $url -- curl error: \"".
		"$humanReadableError\", HTTP response code was: $httpResponseCode");
	}
	curl_close($curl_handle);
	return $pardotApiResponse;
}

function pardotLogin(){
	global $user_key,$api_key;
	$user_key = '84686df524105855decb8370a8283c68';
	$res_api_key = pardotCall('https://pi.pardot.com/api/login/version/3',array(
		'email'=>'crm@tcc.edu',
		'password'=>'..T4!YV:X22msX9',
		'user_key'=>$user_key
	));
	$api_key = simplexml_load_string($res_api_key)->api_key;
}

function pardotUpsert($email,$fname,$lname,$phone,$campaign,$specific,$interest,$transfer,$military){
	global $res;
	if(filter_var($email,FILTER_VALIDATE_EMAIL)){
		$data = array(
			'source'=>'TCC.edu',
			'is_do_not_email'=>false
		);
		if(isset($fname)) $data['first_name'] = $fname;
		if(isset($lname)) $data['last_name'] = $lname;
		if(isset($phone)) $data['phone'] = $phone;
		if(isset($campaign)) $data['campaign_id'] = $campaign;
		if(isset($specific)) $data['Specific_Program_Interest'] = $specific;
		if(isset($interest)) $data['Program_of_Interest'] = $interest;
		if(isset($transfer)) $data['Transfer_Interest'] = $transfer;
		if(isset($military)) $data['Prospect_Military_Status'] = $military;
		$data['assigned'] = "true";
		$data['assigned_to_user'] = "tccrecruit@tcc.edu";
		$pardotresponse = pardotCall('https://pi.pardot.com/api/prospect/version/3/do/upsert/email/'.$email,$data);
		$_res = simplexml_load_string($pardotresponse);
		$_res_att = $_res->attributes();
		$_res_arr = (array)$_res;
		if($_res_att['stat']=="ok"){
			$res = array('success'=>'true','info'=>'Prospect added');
			//debug $res['prospect'] = $_res_arr['prospect']; //debug
		} elseif($_res_att['stat']=="fail") {
			$res = array('success'=>'false','info'=>$_res['err'],'in'=>'Upsert');
		} else {
			$res = array('success'=>'false','info'=>'API','in'=>'Upsert');
		}
	} else {
		$res = array('success'=>'false','info'=>'Invalid email');
	}
}

function pardotRead($email){
	global $res;
	$pardotresponse = pardotCall('https://pi.pardot.com/api/prospect/version/3/do/read/email/'.$email,array());
	$_res = simplexml_load_string($pardotresponse);
	$_res_att = $_res->attributes();
	$_res_arr = (array)$_res;
	if($_res_att['stat']=="ok"){
		$res = array('success'=>'true','info'=>'Prospect exists');
		//debug $res['prospect'] = $_res_arr['prospect']; //debug
	} elseif($_res_att['stat']=="fail") {
		$res = array('success'=>'false','info'=>$_res_arr['err'],'in'=>'Read');
	} else {
		$res = array('success'=>'false','info'=>'API','in'=>'Read');
	}
}

function pardotEmail($email,$template_id,$from_email="",$from_name="",$subject="",$text_content=""){
	global $res;
	$data = array(
		'prospect_email'=>$email,
		'campaign_id'=>14869,
		'email_template_id'=>$template_id
	);
	if(isset($template_id)){
		$data['email_template_id'] = $template_id;
	} elseif(isset($from_email)&&isset($subject)) {
		if(isset($text_content)) $data['text_content'] = $text_content;
		$data['name'] = "TCCEnroll.com Request for Information";
		$data['subject'] = $subject;
		$data['from_email'] = $from_email;
		$data['from_name'] = $from_name;
	}
	$pardotresponse = pardotCall('https://pi.pardot.com/api/email/version/3/do/send/',$data);
	$_res = simplexml_load_string($pardotresponse);
	$_res_att = $_res->attributes();
	$_res_arr = (array)$_res;
	if($_res_att['stat']=="ok"){
		$res = array('success'=>'true','info'=>'Success');
		//debug $res = array('success'=>'true','info'=>$_res_arr); //debug
		//debug $res['prospect'] = $_res_arr['prospect']; //debug
	} elseif($_res_att['stat']=="fail") {
		$res = array('success'=>'false','info'=>$_res_arr['err'],'in'=>'Email', 'data'=>$data);
	} else {
		$res = array('success'=>'false','info'=>'API','in'=>'Email');
	}
}

function sfEmbed($term){
	global $res;

	define("USERNAME", "scartwright@tcc.edu");
	define("PASSWORD", "Kdcpas2020!");
	define("SECURITY_TOKEN", "gZuQJv3GGynPT9Tj8zlcI6Ba");

	require_once ('inc/soapclient/SforcePartnerClient.php');

	$sfConnection = new SforcePartnerClient();
	$sfConnection->createConnection(get_theme_file_path()."/inc/soapclient/PartnerWSDL.xml");
	$sfConnection->login(USERNAME, PASSWORD.SECURITY_TOKEN);
	
	$query = "Select ID, Title, UrlName from Knowledge__kav WHERE PublishStatus='Online' AND Language = 'en_US' WITH DATA CATEGORY Subject__c AT ".$term." AND Tags__c AT Featured__c";
	
	foreach($sfConnection->query($query)->records as $record){
		$_rec = array(
			title=>$record->fields->Title,
			link=>"https://help.tcc.edu/s/article/".$record->fields->UrlName
		);
		if($record->fields->Body__c) $_rec['body'] = tccedu_excerpt($record->fields->Body__c,$term);
		$res['items'][] = $_rec;
	}
}

function sfQuery($term){
	global $res;
	
	$url = "https://tccapisearch.secure.force.com/apiconnect?q=".urlencode($term);
	$curl_handle = curl_init($url);
	curl_setopt($curl_handle,CURLOPT_CONNECTTIMEOUT,30);
	curl_setopt($curl_handle,CURLOPT_TIMEOUT,40);
	curl_setopt($curl_handle,CURLOPT_PROTOCOLS,CURLPROTO_HTTPS);
	curl_setopt($curl_handle,CURLOPT_SSL_VERIFYHOST,2);
	curl_setopt($curl_handle,CURLOPT_RETURNTRANSFER,1);
	curl_setopt($curl_handle,CURLOPT_CUSTOMREQUEST,"GET");
	$salesforceApiResponse = curl_exec($curl_handle);
	if ($salesforceApiResponse === false) {
		$humanReadableError = curl_error($curl_handle);
		$httpResponseCode = curl_getinfo($curl_handle,CURLINFO_HTTP_CODE);
		curl_close($curl_handle);
		throw new Exception("Unable to successfully complete Salesforce API call to $url -- curl error: \"".
		"$humanReadableError\", HTTP response code was: $httpResponseCode");
	}
	curl_close($curl_handle);
	
	$r_count = 0;
	foreach(json_decode($salesforceApiResponse) as $result){
		if($r_count<5){
			$res['items'][] = array(
				title=>$result->Title,
				excerpt=>tccedu_excerpt($result->Body__c,$term),
				link=>"https://help.tcc.edu/s/article/".$result->UrlName
			);
			$r_count += 1;
		}
	}
}



?>