<?php
/**
 * The template for displaying all single event posts
 */

get_header();
?>

<?php echo tccedu_get_section_nav($post); ?>

<main id="site" class="page-event">
	
	<div id="page-content">
		
		<div class="wrap">
			
<?php
if ( have_posts() ) {

	while ( have_posts() ) {
		the_post();
		?>
			<div class="page-title-wrap"><h1 class="page-title"><?php the_title(); ?></h1></div>
			
			<div class="inwrap">
				<div class="page-copy page-col"><?php
					
					$event_post = $post;
					$event_meta = $page_meta;
					
					$event = array(
						'link' => get_relative_permalink($event_post->ID),
						'type' => $event_post->post_type,
						'title' => $event_post->post_title,
						'content' => wpautop($event_post->post_content)
					);
					if($event_meta['event_date'][0]){
						$event_date = date_create($event_meta['event_date'][0]);
						$event['rdate'] = $event_date->format('l F jS');
						$event['day'] = $event_date->format('d');
						$event['month'] = $event_date->format('M');
					}
					if($event_meta['event_campus'][0]||$event_meta['event_campus_other'][0]){
						$event['campus'] = $event_meta['event_campus_other'][0]?$event_meta['event_campus_other'][0]:tccedu_get_campus($event_meta['event_campus'][0])['title'];
					}
					if($event_meta['event_campus'][0]){
						$event_campus = tccedu_get_campus($event_meta['event_campus'][0]);
						if($event_campus['link']) $event['campus_link'] = $event_campus['link'];
					}
					
					if($event_meta['event_time'][0]) $event['time'] = $event_meta['event_time'][0];
					if($event_meta['event_location'][0]) $event['location'] = $event_meta['event_location'][0];
					if($event_meta['event_address'][0]) $event['address'] = $event_meta['event_address'][0];
					
					echo "
					<table class=\"event\">
						<tr><td class=\"date-td\">
							<div class=\"date\"><span class=\"day\">".$event['day']."</span> ".$event['month']."</div>
						</td><td>
							<div class=\"content\">
								<a href=\"".$event['link']."\"><label class=\"title\">".$event['title']."</label></a>".
								($event['campus']?"<div class=\"meta location-general\">".($event['campus_link']?"<a href=\"".$event['campus_link']."\">".$event['campus']."</a>":$event['campus'])."</div>":"").
								($event['rdate']?"<div class=\"meta date-time\"><strong>When:</strong> ".$event['rdate'].($event['time']?", ".$event['time']:"")."</div>":"").
								($event['location']?"<div class=\"meta location-address\"><strong>Where:</strong> ".$event['location']."</div>":"").
								($event['address']?"<div class=\"meta location-address\"><strong>Address:</strong> ".$event['address']."</div>":"").
								($event['content']?"<div class=\"meta desc\"><strong>Description:</strong>
									".$event['content']."
								</div>":"")."
							</div>
						</td></tr>
					</table>
					";
				?></div>
				<div class="page-nav">
					<a href="/events/" class="btn btn-min btn-arrow-left _return">All Events</a>
				</div>
			</div>
			
		<?php
	}

} else {
	
}
?>
			
		</div>
		
	</div>
	
	<?php echo tccedu_get_content_feed($post); ?>
	
</main>

<?php
get_footer();