<?php
/**
 * Template Name: Campus Page
 */

get_header();
global $post;
?>

<?php while ( have_posts() ) : the_post(); ?>

<?php echo tccedu_get_section_nav($post); ?>

<main id="site" class="page-campus">
	
	<div id="page-content" class="with-sidebar">
		
		<div class="wrap">
			
			<div class="page-title-wrap"><h1 class="page-title"><?php echo $post->post_title; ?></h1></div>
			
			<div class="inwrap">
				<div id="page-head">
					<?php if($page_meta['campus_video_src'][0]){
						
					if($page_meta['campus_video_poster'][0]){
						$poster_id = fjarrett_get_attachment_id_by_url($page_meta['campus_video_poster'][0]);		
						if($poster_id){
							$video_poster = wp_get_attachment_image_src($poster_id,'video-poster')[0];
						} else {
							$video_poster = get_relative_link($page_meta['campus_video_poster'][0]);
						}
					}
						echo "
					<div class=\"head-video content-video\">
						<button class=\"hit\" title=\"Play Video\"></button>
						<button class=\"btn btn-play\" tabindex=\"-1\"></button>
						<div class=\"video-wrap\">
							<video width=\"100%\" height=\"auto\"".($video_poster?" poster=\"".$video_poster."\"":"").">
								<source src=\"".$page_meta['campus_video_src'][0]."\" type=\"video/mp4\">".
								($page_meta['campus_video_captions'][0]?"<track label=\"English\" kind=\"captions\" srclang=\"en\" src=\"".$page_meta['campus_video_captions'][0]."\" default>":"")."
							</video>
						</div>
					</div>";
					} else if($page_meta['campus_video_poster'][0]){
						echo "
					<div class=\"head-video poster-only\">
						<div class=\"poster-img\" style=\"background-image:url(".$page_meta['campus_video_poster'][0].");\"></div>
					</div>
					";
					} ?>
				</div>
				<div id="page-sidebar" class="_campus-page<?php echo (!$page_meta['page_sidebar_not_sticky'][0]?" sticky":""); ?>">
					<div class="panel">
						<div class="sidebar-content _campus-info">
							<?php
							
							if($page_meta['page_sidebar'][0]=="custom"){
								
								$sidebar_title = $page_meta['custom_sidebar_title'][0];
								$sidebar_copy = $page_meta['custom_sidebar_copy'][0];
								$sidebar_link = $page_meta['custom_sidebar_link'][0];
								$sidebar_cta = $page_meta['custom_sidebar_cta'][0];
								$sidebar_btn_class = $page_meta['custom_sidebar_btn_class'][0];
								if($sidebar_title) echo "<label class=\"nonh2\">".$sidebar_title."</label>";
								if($sidebar_copy) echo wpautop($sidebar_copy);
								if($sidebar_link){
									echo "<a href=\"".$sidebar_link."\" class=\"btn ".
									($sidebar_btn_class?$sidebar_btn_class:"btn btn-4 btn-text btn-arrow").
									"\">".($sidebar_cta?$sidebar_cta:"Learn More")."</a>";
								}
							} else {
								
								$campus = tccedu_get_campus(str_replace("campus_","",$page_meta['page_sidebar'][0]));
								echo "<label class=\"nonh2\">Campus Info</label>";
								echo "
								<table class=\"info\">".
									($campus['phone']?"<tr><td>
										<h5>Phone</h5>
										<p><a href=\"tel:".$campus['phone']."\">".$campus['phone']."</a></p>
									</td></tr>":"").
									($campus['address']?"<tr><td>
										<h5>Address</h5>
										<p>".$campus['address']."</p>
									</td></tr>
									<tr><td>
										<button class=\"btn btn-2 btn-min btn-arrow\" onclick=\"tcc.getDirections('".$campus['address']."');\">Get directions</button>".
										($campus['map']?"
										<a href=\"".$campus['map']."\" class=\"btn btn-2 btn-min btn-download\" target=\"_blank\">Get Campus Map</a>
									":"")."
									</td></tr>":"")."
								</table>";
								
							}
						
							?>
							
						</div>
					</div>
				</div>
				<div class="page-copy page-col"><?php the_content(); ?></div>
			</div>
			
		</div>
		
	</div>
	
	<?php if($page_meta['events_mod_enable'][0]) echo tccedu_get_upcoming_events_module($post); ?>
	
	<?php if($page_meta['map_mod_enable'][0]) echo tccedu_get_map_module($post); ?>
	
	<?php if($page_meta['content_feed_mod_enable'][0]) echo tccedu_get_content_feed($post); ?>
	
</main>

<?php endwhile; ?>

<?php
get_footer();