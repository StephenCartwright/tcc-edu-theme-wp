<?php
	global $page_meta;
	$html = "
	<div id=\"a-spot\" class=\"_slider\">
		<div class=\"slides freset\">";
	$s_count = 0;
	for($s=1;$s<=5;$s++){
		$item = "slide_".$s;
		$item_imgs = array();
		for($si=1;$si<11;$si++){
			if($page_meta[$item.'_img_'.$si][0]) array_push($item_imgs,$page_meta[$item.'_img_'.$si][0]);
		}
		$item_img = count($item_imgs)>0?$item_imgs[array_rand($item_imgs)]:$page_meta[$item.'_img'][0];
		if($page_meta[$item.'_show'][0]&&$page_meta[$item.'_title'][0]&&!empty($item_img)){
			$html .= "
			<div class=\"slide".($s==1?" on":"").($page_meta[$item.'_align'][0]=="right"?" alignright":"")."\" data-slide=\"".$s."\" style=\"background-image:url(".$item_img.");\">
				<aside class=\"trim _1\"></aside>
				<aside class=\"trim _2\"></aside>
				<div class=\"content wrap owrap\"><div class=\"vwrap\">
					<div class=\"panel\">
						<h1".(strlen($page_meta[$item.'_title'][0])>25?" class=\"long\"":"").">".$page_meta[$item.'_title'][0]."</h1>";
						if($page_meta[$item.'_copy'][0]) $html .= "
						<p>".$page_meta[$item.'_copy'][0]."</p>";
						if($page_meta[$item.'_link'][0]) $html .= "
						<a href=\"".$page_meta[$item.'_link'][0]."\" class=\"btn btn-2 btn-arrow\">".($page_meta[$item.'_cta'][0]?$page_meta[$item.'_cta'][0]:"Learn More")."</a>";
						$html .= "
					</div>
				</div></div>
			</div>";
			$s_count += 1;
		}
	}
	$html .= "</div>
		<div class=\"nav\"></div>
	</div>";
?>