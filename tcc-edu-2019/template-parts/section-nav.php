<?php
	if(is_search($post->ID)){
		$post_ancestors = array();
		$post_children = array();
	} else {
		$post_ancestors = array_reverse(get_ancestors($post->ID,'page','post_type'));
		$post_children = get_children(array(
			'post_parent'=>$post->ID,
			'post_type'=>'page',
			'order'=>'ASC',
			'orderby'=>'menu_order',
			'post_status'=>'publish',
			'meta_query'=>array(array('key'=>'page_shy','compare'=>'NOT EXISTS'))
		));
	}
	
	$html = "
	<nav id=\"section-navigation\" class=\"section-navigation\" aria-label=\"Section Menu\">
		<div class=\"wrap\">
			<table>
				<tr><td>
					<form autocomplete=\"off\"><table class=\"sn-table\">
						<tr>";
	//check for post types first, like events
	if($post->post_type=='event'){
		$html .= "
								<td class=\"_title single\">
									<div class=\"nonh2\"><a href=\"/events/\">Events</a></div>
								</td>";
	} else {
		if(count($post_ancestors)==0){// a top level page
			if(!count($post_children)>0){// top level page with no children, no section nav
				$html .= "
								<td class=\"_title single\">
									<div class=\"nonh2\"><a href=\"\\\">Home</a></div>
								</td>";
			} else {// top level page with children, needs section navigation
				$html .= "
								<td class=\"_title\">
									<div class=\"nonh2\">".$post->post_title."</div>
								</td>
								<td class=\"_second\">
									<label for=\"sn-second\" class=\"screen-reader-text\">Second Level Navigation</label>
									<select id=\"sn-second\" class=\"sn-select\">
										<option selected disabled data-role=\"placeholder\">Overview</option>";
				foreach($post_children as $c){
					$html .= "
										<option value=\"".get_relative_permalink($c->ID)."\">".$c->post_title."</option>";
				}
				$html .= "
								</select>
								</td>";
			}
		} else {// a page with ancestors
			$post_top_ancestor = get_post($post_ancestors[0]);
			if(count($post_ancestors)==1){// a second level page
				$html .= "
								<td class=\"_title\">
									<div class=\"nonh2\"><a href=\"".get_relative_permalink($post_top_ancestor->ID)."\">".$post_top_ancestor->post_title."</a></div>
								</td>";
				$post_siblings = get_children(array(
					'post_parent'=>$post_top_ancestor->ID,
					'post_type'=>'page',
					'order'=>'ASC',
					'orderby'=>'menu_order',
					'post_status'=>'publish',
					'meta_query'=>array(array('key'=>'page_shy','compare'=>'NOT EXISTS'))
				));
				$html .= "
								<td class=\"_second\">
									<label for=\"sn-second\" class=\"screen-reader-text\">Second Level Navigation</label>
									<select id=\"sn-second\" class=\"sn-select\">";
				if(get_post_meta($post->ID,'page_shy',true)){
					$html .= "
										<option selected value=\"".get_relative_permalink($post->ID)."\">".$post->post_title."</option>";
				}
				foreach($post_siblings as $s){
					$html .= "
										<option ".($post->ID==$s->ID?"selected ":"")."value=\"".get_relative_permalink($s->ID)."\">".$s->post_title."</option>";
				}
				$html .= "
									</select>
								</td>";
				if(count($post_children)>0){
					$html .= "
								<td class=\"_third\">
									<label for=\"sn-third\" class=\"screen-reader-text\">Third Level Navigation</label>
									<select id=\"sn-third\" class=\"sn-select\">
										<option selected disabled data-role=\"placeholder\">Overview</option>";
					foreach($post_children as $c){
						$html .= "
										<option value=\"".get_relative_permalink($c->ID)."\">".$c->post_title."</option>";
					}
					$html .= "
									</select>
								</td>";
				}
			} else {// a third+ level page
				$post_parent = get_post($post->post_parent);
				$post_grandparent = get_post($post_parent->post_parent);
				$post_siblings = get_children(array(
					'post_parent'=>$post_parent->ID,
					'post_type'=>'page',
					'order'=>'ASC',
					'orderby'=>'menu_order',
					'post_status'=>'publish',
					'meta_query'=>array(array('key'=>'page_shy','compare'=>'NOT EXISTS'))
				));
				if(count($post_children)>0){// with children, third level one below current page
					$html .= "
								<td class=\"_title\">
									<div class=\"nonh2\"><a href=\"".get_relative_permalink($post_parent->ID)."\">".$post_parent->post_title."</a></div>
								</td>";
					
					$html .= "
								<td class=\"_second\">
									<label for=\"sn-second\" class=\"screen-reader-text\">Second Level Navigation</label>
									<select id=\"sn-second\" class=\"sn-select\">";
					if(get_post_meta($post->ID,'page_shy',true)){
						$html .= "
										<option selected value=\"".get_relative_permalink($post_parent->ID)."\">".$post_parent->post_title."</option>";
					}
					foreach($post_siblings as $s){
						$html .= "
										<option ".($post->ID==$s->ID?"selected ":"")."value=\"".get_relative_permalink($s->ID)."\">".$s->post_title."</option>";
					}
					$html .= "
									</select>
								</td>
								<td class=\"_third\">
									<label for=\"sn-third\" class=\"screen-reader-text\">Third Level Navigation</label>
									<select id=\"sn-third\" class=\"sn-select\">
										<option selected disabled data-role=\"placeholder\">Overview</option>";
					foreach($post_children as $c){
						$html .= "
										<option value=\"".get_relative_permalink($c->ID)."\">".$c->post_title."</option>";
					}
					$html .= "
									</select>
								</td>";
				} else {// without children, third level at current page
					$html .= "
								<td class=\"_title\">
									<div class=\"nonh2\"><a href=\"".get_relative_permalink($post_grandparent->ID)."\">".$post_grandparent->post_title."</a></div>
								</td>";
					$post_uncles = get_children(array(
						'post_parent'=>$post_grandparent->ID,
						'post_type'=>'page',
						'order'=>'ASC',
						'orderby'=>'menu_order',
						'post_status'=>'publish',
						'meta_query'=>array(array('key'=>'page_shy','compare'=>'NOT EXISTS'))
					));
					$html .= "
								<td class=\"_second\">
									<label for=\"sn-second\" class=\"screen-reader-text\">Second Level Navigation</label>
									<select id=\"sn-second\" class=\"sn-select\">";
					if(get_post_meta($post_parent->ID,'page_shy',true)){
						$html .= "
										<option selected value=\"".get_relative_permalink($post_parent->ID)."\">".$post_parent->post_title."</option>";
					}
					foreach($post_uncles as $u){
						$html .= "
										<option ".($post_parent->ID==$u->ID?"selected ":"")."value=\"".get_relative_permalink($u->ID)."\">".$u->post_title."</option>";
					}
					$html .= "
									</select>
								</td>
								<td class=\"_third\">
									<label for=\"sn-third\" class=\"screen-reader-text\">Third Level Navigation</label>
									<select id=\"sn-third\" class=\"sn-select\">";
					if(get_post_meta($post->ID,'page_shy',true)){
						$html .= "
										<option selected value=\"".get_relative_permalink($post->ID)."\">".$post->post_title."</option>";
					}
					foreach($post_siblings as $s){
						$html .= "
										<option ".($post->ID==$s->ID?"selected ":"")."value=\"".get_relative_permalink($s->ID)."\">".$s->post_title."</option>";
					}
					$html .= "
									</select>
								</td>";
				}
				
			}
		}
	}
	$html .= "
						</tr>
					</table></form>
				</td><td>
					<div class=\"copy-size no-select\">
						<button class=\"set-size _1\" data-size=\"1\" title=\"Set page copy to small size\">A</button>
						<button class=\"set-size _2\" data-size=\"2\" title=\"Set page copy to normal size\">A</button>
						<button class=\"set-size _3\" data-size=\"3\" title=\"Set page copy to large size\">A</button>
					</div>
				</td></tr>
			</table>
		</div>
	</nav>";
?>