<?php
	global $page_meta;
	$style = isset($page_meta['content_feed_mod_style'][0])?$page_meta['content_feed_mod_style'][0]:"style-row";
	$tags = $page_meta['content_feed_mod_tags'][0];
	if(!$tags){
		$pagetags = array();
		foreach(get_the_tags() as $tag) $pagetags[] = $tag->slug;
		$tags = implode(",",$pagetags);
	}
	$html = "<div id=\"content-feed\" class=\"content-feed ".$style."\" data-tags=\"".$tags."\" data-match=\"".$page_meta['content_feed_mod_match'][0]."\"></div>";
?>