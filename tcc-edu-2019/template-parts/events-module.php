<?php
	global $page_meta;
	$events_title = $page_meta['events_mod_title'][0]?$page_meta['events_mod_title'][0]:"Upcoming Events";
	$events_count = intval($page_meta['events_mod_count'][0]?$page_meta['events_mod_count'][0]:3);
	$events = tccedu_get_events_in_category($page_meta['events_mod_cat'][0],$events_count)['events'];
	$html = "
	<div id=\"upcoming-events\" class=\"events-module\">
		<div class=\"wrap\">
			<h2>".$events_title."</h2>";
	if(count($events)>0){
		$html .= "
			<div class=\"events\">";
		foreach($events as $event){
			$html .= "
				<table class=\"event\">
					<tr><td class=\"date-td\">
						<div class=\"date\"><span class=\"day\">".$event['day']."</span> ".$event['month']."</div>
					</td><td>
						<div class=\"content\">
							<a href=\"".$event['link']."\" class=\"title nonh5\">".$event['title']."</a>
							".($event['campus']?"<div class=\"meta location-general\">".$event['campus']."</div>":"")."
						</div>
					</td></tr>
				</table>";
		}
		$html .= "
			</div>
			<a href=\"".($page_meta['events_mod_link'][0]?$page_meta['events_mod_link'][0]:"/events/")."\" class=\"btn btn-3 btn-text btn-arrow\">View All</a>";
	} else {
		$html .= "
			<div class=\"events empty freset\">
				<div class=\"owrap\"><div class=\"vwrap\"><div class=\"info nonh5\">No currently scheduled events, check back soon.</div></div></div>
			</div>";
	}
	$html .= "
		</div>
	</div>";
	
?>