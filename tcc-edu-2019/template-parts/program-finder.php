<?php
	global $page_meta;
	$html = "";
	if(!$page_meta['program_finder_locked'][0]){
		$html .= "
			<div class=\"pf-form-wrap\">
				<table class=\"pf-form\">
					<tr><td colspan=\"4\">
						<div class=\"pf-search-wrap\">
							<input id=\"pf-search\" class=\"search-input-large\" autocomplete=\"off\" placeholder=\"Example: Graphic Design\" />
							<div class=\"search-opts\"></div>
						</div>
					</td></tr>
					<tr><td colspan=\"4\" class=\"or\"><span>or filter by</span></td></tr>
					<tr class=\"pf-filters\">
						<td>
							<label for=\"pf-career\" class=\"screen-reader-text\">Career Pathway</label>
							<select id=\"pf-career\" class=\"pf-filter\">
								<option".(!$page_meta['program_finder_set_career'][0]?" selected":"")." disabled data-role=\"placeholder\">Career Pathway</option>
								<option value=\"\" class=\"clear\">Clear selection</option>";
								foreach(json_decode(get_option('career_options_list')) as $option){
									$html .= "
								<option value=\"".$option->id."\"".($page_meta['program_finder_set_career'][0]==$option->id?" selected":"").">".$option->name."</option>";
								}
		$html .= "
							</select>
						</td>
						<td>
							<label for=\"pf-type\" class=\"screen-reader-text\">Program Type</label>
							<select id=\"pf-type\" class=\"pf-filter\">
								<option".(!$page_meta['program_finder_set_type'][0]?" selected":"")." disabled data-role=\"placeholder\">Program Type</option>
								<option value=\"\" class=\"clear\">Clear selection</option>";
								foreach(json_decode(get_option('program_type_options_list')) as $option){
									$html .= "
								<option value=\"".$option->id."\"".($page_meta['program_finder_set_type'][0]==$option->id?" selected":"").">".$option->name."</option>";
								}
		$html .= "
							</select>
						</td>
						<td>
							<label for=\"pf-location\" class=\"screen-reader-text\">Location</label>
							<select id=\"pf-location\" class=\"pf-filter\">
								<option".(!$page_meta['program_finder_set_location'][0]?" selected":"")." disabled data-role=\"placeholder\">Location</option>
								<option value=\"\" class=\"clear\">Clear selection</option>";
								for($c=1;$c<=15;$c++){
									if(get_option('campus_'.$c.'_show')&&get_option('campus_'.$c.'_is_search')){
										$campus_id = get_option('campus_'.$c.'_id')?get_option('campus_'.$c.'_id'):sanitize_title(get_option('campus_'.$c.'_title'));
										$html .= "
								<option value=\"".$campus_id."\"".($page_meta['program_finder_set_location'][0]==$campus_id?" selected":"").">".get_option('campus_'.$c.'_title')."</option>";
									}
								}
		$html .= "
							</select>
						</td>
						<td>
							<label for=\"pf-interest\" class=\"screen-reader-text\">Interest</label>
							<select id=\"pf-interest\" class=\"pf-filter\">
								<option".(!$page_meta['program_finder_set_interest'][0]?" selected":"")." disabled data-role=\"placeholder\">Interest</option>
								<option value=\"\" class=\"clear\">Clear selection</option>";
								foreach(json_decode(get_option('interest_options_list')) as $option){
									$html .= "
								<option value=\"".$option->id."\"".($page_meta['program_finder_set_interest'][0]==$option->id?" selected":"").">".$option->name."</option>";
								}
		$html .= "
							</select>
						</td>
					</tr>";
		if($context=="module") $html .= "
					<tr><td colspan=\"4\">
						<button type=\"button\" class=\"btn btn-arrow _search\">Search</button>
					</td></tr>";
		$html .= "
				</table>
			</div>";
	}
	if($context=="program-finder"){
		if($page_meta['program_finder_locked'][0]){
		$html .= "
			<div class=\"pf-results".($page_meta['program_finder_locked'][0]?" locked":"")."\"";
			if($page_meta['program_finder_maxpage'][0]) $html .= " data-maxpage=\"".$page_meta['program_finder_maxpage'][0]."\"";
			if($page_meta['program_finder_set_career'][0]) $html .=" data-career=\"".$page_meta['program_finder_set_career'][0]."\"";
			if($page_meta['program_finder_set_degree'][0]) $html .=" data-degree=\"".$page_meta['program_finder_set_degree'][0]."\"";
			if($page_meta['program_finder_set_type'][0]) $html .=" data-type=\"".$page_meta['program_finder_set_type'][0]."\"";
			if($page_meta['program_finder_set_location'][0]) $html .=" data-location=\"".$page_meta['program_finder_set_location'][0]."\"";
			if($page_meta['program_finder_set_interest'][0]) $html .=" data-interest=\"".$page_meta['program_finder_set_interest'][0]."\"";
			$html .= ">";
		} else {
			$html .= "
			<div class=\"pf-results\"".($page_meta['program_finder_maxpage'][0]?" data-maxpage=\"".$page_meta['program_finder_maxpage'][0]."\"":"").">";
		}
		if(!$page_meta['program_finder_locked'][0]){
			$html .= "
				<div class=\"pf-info\">
					<span class=\"count\"></span>
					<button class=\"btn btn-sort _sort\">Sorting results A-Z</button>
				</div>";
		}
		$html .= "
				<div class=\"items\"></div>
				<div class=\"pf-nav\">
					<button class=\"btn btn-arrow _more\">Load more</button>
				</div>
			</div>";
	}
?>