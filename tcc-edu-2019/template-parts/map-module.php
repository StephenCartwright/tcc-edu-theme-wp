<?php
	global $page_meta;
	$map_title = $map_title?$map_title:($page_meta['map_mod_title'][0]?$page_meta['map_mod_title'][0]:"");
	$map_img = $page_meta['map_mod_img'][0];
	$map_ratio = $page_meta['map_mod_img_ratio'][0];
	$map_coords = array(preg_replace("/\s+/","",$page_meta['map_mod_coords_1'][0]),preg_replace("/\s+/","",$page_meta['map_mod_coords_2'][0]));
	$locations = unserialize($page_meta['map_mod_locations'][0]);
	if(!$locations) $locations = "";
	$html = "
	<div id=\"campuses\">".
		($map_title?"<h2>".$map_title."</h2>":"")."
		<div class=\"fwrap\">
			<div id=\"campus-map\" class=\"map\" data-locations=\"".implode(",",$locations)."\" data-map-img=\"".$map_img."\" data-map-ratio=\"".$map_ratio."\" data-map-coords=\"[[".$map_coords[0]."],[".$map_coords[1]."]]\"></div>
			<div class=\"content freset\"></div>
		</div>
	</div>";
?>