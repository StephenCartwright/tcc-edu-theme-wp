<?php
/**
 * Template Name: Program Page
 */

get_header();
global $post;
?>

<?php while ( have_posts() ) : the_post(); ?>

<?php echo tccedu_get_section_nav($post); ?>

<main id="site" class="page-program">
	
	<div id="page-content" class="with-sidebar">
		
		<div class="wrap">
			
			<div class="page-title-wrap"><h1 class="page-title"><?php echo $post->post_title; ?></h1></div>
			
			<div class="inwrap">
				<div id="page-head">
					<?php if($page_meta['program_video_src'][0]){
					echo "
					<div class=\"head-video content-video\">
						<button class=\"hit\" title=\"Play Video\"></button>
						<button class=\"btn btn-play\" tabindex=\"-1\"></button>
						<div class=\"video-wrap\">
							<video width=\"100%\" height=\"auto\"".($page_meta['program_video_poster'][0]?" poster=\"".$page_meta['program_video_poster'][0]."\"":"").">
								<source src=\"".$page_meta['program_video_src'][0]."\" type=\"video/mp4\">".
								($page_meta['program_video_captions'][0]?"<track label=\"English\" kind=\"captions\" srclang=\"en\" src=\"".$page_meta['program_video_captions'][0]."\" default>":"")."
							</video>
						</div>
					</div>";
					} else if($page_meta['program_video_poster'][0]){
						echo "
					<div class=\"head-video poster-only\">
						<div class=\"poster-img\" style=\"background-image:url(".$page_meta['program_video_poster'][0].");\"></div>
					</div>
					";
					} ?>
				</div>
				<div id="page-sidebar" class="_program-page sticky">
					<div class="panel">
						<div class="sidebar-item _form" data-guide-id="<?php echo $page_meta['program_guide'][0]; ?>">
							<button class="btn btn-close btn-2 _close disabled" disabled="disabled"></button>
							<div class="owrap"><div class="vwrap">
								<button class="hit _enter" title="Download Our Guide"></button>
								<div class="icon"></div>
								<label class="nonh2">Download Our Guide</label>
							</div></div>
							<div class="prog-form-wrap">
								<div id="prog-form" class="tcc-form compact freset">
									<div class="form-stage download-guide"></div>
								</div>
							</div>
						</div>
						<div class="sidebar-item _ask-question"><div class="owrap"><div class="vwrap">
							<a class="hit" href="https://help.tcc.edu/" title="Ask Us A Question"></a>
							<div class="icon"></div>
							<label class="nonh2">Ask Us A Question</label>
						</div></div></div>
						<div class="sidebar-item _get-started"><div class="owrap"><div class="vwrap">
							<a class="hit" href="/come-to-tcc/" title="Get Started"></a>
							<div class="icon"></div>
							<label class="nonh2">Get Started</label>
						</div></div></div>
					</div>
				</div>
				<div class="page-copy page-col"><?php the_content(); ?></div>
			</div>
			
		</div>
		
	</div>
	
	<?php if($page_meta['program_career_coach'][0]){
	echo "
	<div id=\"page-extra\">
		<div class=\"wrap\">
			<div class=\"is-this\">
				<div class=\"head\"><h3>Is this program for you?</h3></div>
				<div class=\"content page-copy freset\"><h5>Yes, if you:</h5>
					<ul>".
						(isset($page_meta['program_is_this_1'][0])?"<li>".$page_meta['program_is_this_1'][0]."</li>":"").
						(isset($page_meta['program_is_this_2'][0])?"<li>".$page_meta['program_is_this_2'][0]."</li>":"").
						(isset($page_meta['program_is_this_3'][0])?"<li>".$page_meta['program_is_this_3'][0]."</li>":"")."
					</ul>
				</div>
			</div>
			<div class=\"career-coach\">
				<script type=\"text/javascript\">
					window.onload = async function() {
						try {
							const onets = await generatePluginData(
								'tidewater',
								'".(isset($page_meta['program_career_coach_slug'][0])?$page_meta['program_career_coach_slug'][0]:"tcc-edu")."',
								'4616fd31a1d74d0da6fedc07274f06eb'
							);
							const subdomain = 'tidewater';
							const level = 'county';
							const geoids = ['37029','37053','51093','51131','51199','51550','51650','51700','51710','51735','51740','51800','51810'];

							ccWidget.render(subdomain, level, geoids, onets, false);
						} catch(e) {
							const subdomain = 'tidewater';
							const level = 'county';
							const geoids = ['37029','37053','51093','51131','51199','51550','51650','51700','51710','51735','51740','51800','51810'];
							const onets = [".implode(",",explode(',',$page_meta['program_career_coach'][0]))."];
							ccWidget.render(subdomain, level, geoids, onets, false);
						}
						
						window.career_coach_checks = 0;
						window.career_coach_tmr = setInterval(function(){
							window.career_coach_check++;
							if($(\"#cc-widget-careers .cc-content-link\").length&&!$(\"#cc-widget-careers .cc-content-link\").hasClass(\"btn\")){
								$(\"#cc-widget-careers .cc-content-link\").attr(\"class\",\"cc-content-link btn btn-2 btn-arrow\");
								$(\"#cc-widget-careers .arrow-button\")[0].className = \"arrow-button _prev btn btn-2 btn-nav btn-nav-prev\";
								$(\"#cc-widget-careers .arrow-button\")[1].className = \"arrow-button _next btn btn-2 btn-nav btn-nav-next\";
								$(\"#cc-widget-careers\").addClass(\"init\");
							}
							if($(\"#cc-widget-careers\").hasClass(\"init\")||window.career_coach_check==20){
								clearInterval(window.career_coach_tmr);
							}
						},100);
					}
				</script>
				<div id=\"cc-widget-careers\"></div>
			</div>
		</div>
	</div>";
	} ?>
	
	<?php echo tccedu_get_content_feed($post); ?>
	
</main>

<?php endwhile; ?>

<?php
get_footer();