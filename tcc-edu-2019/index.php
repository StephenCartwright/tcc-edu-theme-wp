<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage TCC_edu
 * @since 1.0.0
 */

get_header();
global $post;
?>

<?php while ( have_posts() ) : the_post(); ?>

<?php echo tccedu_get_section_nav($post); ?>

<main id="site" class="page-standard">
	
	<div id="page-content">
		
		<div class="wrap">
			
			<div class="page-title-wrap"><h1 class="page-title"><?php the_title(); ?></h1></div>
			
			<div class="inwrap">
				<div class="page-copy page-col"><?php the_content(); ?></div>
			</div>
			
		</div>
		
	</div>
	
	<?php if($page_meta['events_mod_enable'][0]) echo tccedu_get_upcoming_events_module($post); ?>
	
	<?php if($page_meta['content_feed_mod_enable'][0]) echo tccedu_get_content_feed($post); ?>
	
</main>

<?php endwhile; ?>

<?php
get_footer();