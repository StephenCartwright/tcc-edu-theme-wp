<?php
/**
 * Template Name: Events Page
 */

get_header();
global $post;
$events_query = tccedu_get_events_in_category($page_meta['events_page_cat'][0],$page_meta['events_per_page'][0],0);
?>

<?php while ( have_posts() ) : the_post(); ?>

<?php echo tccedu_get_section_nav($post); ?>

<main id="site" class="page-events">
	
	<div id="page-content">
		
		<div class="wrap">
			
			<div class="page-title-wrap"><h1 class="page-title"><?php echo $post->post_title; ?></h1></div>
			
			<div class="inwrap">
				<div class="page-copy page-col"><?php the_content(); ?></div>
			</div>
			
			<div class="search-module _events">
				<?php
				echo "
				<div id=\"tcc-events-search\" class=\"search-results event-results page-copy page-col\" data-cat=\"".$page_meta['events_page_cat'][0]."\" data-total=\"".$events_query['total']."\" data-per-page=\"".$page_meta['events_per_page'][0]."\" data-page=\"".$events_query['page']."\" data-max-pages=\"".$events_query['max_pages']."\">";
				
				if ( count($events_query['events']) ) {
					echo "
					<div class=\"info nonh5\">".($events_query['max_pages']>1?"Page ".$events_query['page']." of ".$events_query['max_pages']." for ":"").$events_query['total']." results</div>
					<div class=\"items events\">";
					foreach($events_query['events'] as $event){
						echo "
						<table class=\"event\">
							<tr><td class=\"date-td\">
								<div class=\"date\"><span class=\"day\">".$event['day']."</span> ".$event['month']."</div>
							</td><td>
								<div class=\"content\">
									<a href=\"".$event['link']."\"><label class=\"title\">".$event['title']."</label></a>".
									($event['campus']?"<div class=\"meta location-general\">".($event['campus_link']?"<a href=\"".$event['campus_link']."\">".$event['campus']."</a>":$event['campus'])."</div>":"").
									($event['rdate']?"<div class=\"meta date-time\"><strong>When:</strong> ".$event['rdate'].($event['time']?", ".$event['time']:"")."</div>":"").
									($event['location']?"<div class=\"meta location-address\"><strong>Where:</strong> ".$event['location']."</div>":"").
									($event['address']?"<div class=\"meta location-address\"><strong>Address:</strong> ".$event['address']."</div>":"").
									($event['content']?"<div class=\"meta desc\"><strong>Description:</strong>
										".$event['content']."
									</div>":"")."
									<div class=\"cta\">
										<a href=\"".$event['link']."\" class=\"btn btn-3 btn-text btn-arrow\">See Calendar Detail</a>
									</div>
								</div>
							</td></tr>
						</table>
						";
					}
					echo "
					</div>";
				} else {
					echo "
					<div class=\"info nonh5\">No currently scheduled events, check back soon.</div>
					<div class=\"items\"></div>";
				}
				
				echo "
					<div class=\"page-nav".($events_query['max_pages']<2?" _disabled":"")." ".($events_query['max_pages']>1&&$events_query['page']<2?"_closed":"")."\">
						<button class=\"btn btn-min btn-arrow _more".($events_query['max_pages']<2?" disabled\" disabled=\"disabled\"":"\"").">More results</button>
						<button class=\"btn btn-min btn-arrow-left _prev".($events_query['page']<2?" disabled\" disabled=\"disabled\"":"\"").">Previous page</button><button class=\"btn btn-min btn-arrow _next".($events_query['page']<$events_query['max_pages']?"\"":" disabled\" disabled=\"disabled\"").">Next page</button>
					</div>
				</div>";
				?>
				
			</div>
			
		</div>
		
	</div>
	
	<?php echo tccedu_get_content_feed($post); ?>
	
</main>

<?php endwhile; ?>

<?php
get_footer();