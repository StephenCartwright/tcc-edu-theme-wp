<?php
/**
 * TCC.edu functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package WordPress
 * @subpackage TCC_edu
 * @since 1.0.0
 */

/**
 * TCC.edu only works in WordPress 4.7 or later.
 */
if ( version_compare( $GLOBALS['wp_version'], '4.7', '<' ) ) {
	require get_template_directory() . '/inc/back-compat.php';
	return;
}

if ( ! function_exists( 'tccedu_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function tccedu_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on TCC.edu, use a find and replace
		 * to change 'tccedu' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'tccedu', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );
		set_post_thumbnail_size( 1568, 9999 );

		// This theme uses wp_nav_menu() in two locations.
		register_nav_menus(
			array(
				'main' => __( 'Main Menu', 'tccedu' ),
				'ribbon' => __( 'Ribbon Menu', 'tccedu' ),
				'sub-footer' => __( 'Sub Footer Menu', 'tccedu' )
			)
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'gallery',
				'caption',
			)
		);

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 190,
				'width'       => 190,
				'flex-width'  => false,
				'flex-height' => false,
			)
		);

		// Add support for Block Styles.
		add_theme_support( 'wp-block-styles' );

		// Add support for full and wide align images.
		add_theme_support( 'align-wide' );

		// Add support for editor styles.
		add_theme_support( 'editor-styles' );

		// Enqueue editor styles.
		add_editor_style( 'style-editor.css' );

		// Add custom editor font sizes.
	/*	add_theme_support(
			'editor-font-sizes',
			array(
				array(
					'name'      => __( 'Small', 'tccedu' ),
					'shortName' => __( 'S', 'tccedu' ),
					'size'      => 19.5,
					'slug'      => 'small',
				),
				array(
					'name'      => __( 'Normal', 'tccedu' ),
					'shortName' => __( 'M', 'tccedu' ),
					'size'      => 22,
					'slug'      => 'normal',
				),
				array(
					'name'      => __( 'Large', 'tccedu' ),
					'shortName' => __( 'L', 'tccedu' ),
					'size'      => 36.5,
					'slug'      => 'large',
				),
				array(
					'name'      => __( 'Huge', 'tccedu' ),
					'shortName' => __( 'XL', 'tccedu' ),
					'size'      => 49.5,
					'slug'      => 'huge',
				),
			)
		);*/

		// Editor color palette.
		/*add_theme_support(
			'editor-color-palette',
			array(
				array(
					'name'  => __( 'Primary', 'tccedu' ),
					'slug'  => 'primary',
					'color' => tccedu_hsl_hex( 'default' === get_theme_mod( 'primary_color' ) ? 199 : get_theme_mod( 'primary_color_hue', 199 ), 100, 33 ),
				),
				array(
					'name'  => __( 'Secondary', 'tccedu' ),
					'slug'  => 'secondary',
					'color' => tccedu_hsl_hex( 'default' === get_theme_mod( 'primary_color' ) ? 199 : get_theme_mod( 'primary_color_hue', 199 ), 100, 23 ),
				),
				array(
					'name'  => __( 'Dark Gray', 'tccedu' ),
					'slug'  => 'dark-gray',
					'color' => '#111',
				),
				array(
					'name'  => __( 'Light Gray', 'tccedu' ),
					'slug'  => 'light-gray',
					'color' => '#767676',
				),
				array(
					'name'  => __( 'White', 'tccedu' ),
					'slug'  => 'white',
					'color' => '#FFF',
				),
			)
		);*/

		// Add support for responsive embedded content.
		add_theme_support( 'responsive-embeds' );
	}
endif;
add_action( 'after_setup_theme', 'tccedu_setup' );


function tccedu_init(){
	
	register_taxonomy('event_category','{event}',array(
		'label'               => 'Event Categories',
		'description'         => 'Categories to filter events',
		'hierarchical'        => true,
		'public'              => true,
		'publicly_queryable'  => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_rest'        => true,
		'show_tagcloud'       => true,
		'show_in_quick_edit'  => true,
	));
	
	register_post_type('event',array(
		'label'               => 'Public Events',
		'description'         => 'Events',
		'labels'              => array(
			'name'                => _x( 'Events', 'Post Type General Name', 'tccedu' ),
			'singular_name'       => _x( 'Event', 'Post Type Singular Name', 'tccedu' ),
			'menu_name'           => 'Events',
			'all_items'           => 'All Events',
			'view_item'           => 'View Event',
			'add_new_item'        => 'Add New Event',
			'add_new'             => 'Add New',
			'edit_item'           => 'Edit Event',
			'update_item'         => 'Update Event',
			'search_items'        => 'Search Events',
			'not_found'           => 'Not Found',
			'not_found_in_trash'  => 'Not found in Trash',
		),
		'supports'            => array( 'title', 'editor', 'author', 'thumbnail', 'revisions' ),
		'taxonomies'          => array( 'event_category', 'post_tag' ),
		'hierarchical'        => false,
		'public'              => true,
		'publicly_queryable'  => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		//'show_in_rest'        => true,
		'menu_position'       => 50,
		'menu_icon'			  => 'dashicons-calendar-alt',
		'can_export'          => true,
		'has_archive'         => true,
		'rewrite'			  => array( 'slug'=>'/event' ),
		'exclude_from_search' => true,
		'capability_type'     => 'post',
	));
	
	register_post_type('feeditem',array(
		'label'               => 'Content Feed Items',
		'description'         => 'Content Feed Items',
		'labels'              => array(
			'name'                => _x( 'Feed Items', 'Post Type General Name', 'tccedu' ),
			'singular_name'       => _x( 'Feed Item', 'Post Type Singular Name', 'tccedu' ),
			'menu_name'           => 'Content Feed',
			'all_items'           => 'All Feed Items',
			'view_item'           => 'View Feed Item',
			'add_new_item'        => 'Add New Feed Item',
			'add_new'             => 'Add New',
			'edit_item'           => 'Edit Feed Item',
			'update_item'         => 'Update Feed Item',
			'search_items'        => 'Search Feed Item',
			'not_found'           => 'Not Found',
			'not_found_in_trash'  => 'Not found in Trash',
		),
		'supports'            => array( 'title', 'revisions' ),
		'taxonomies'          => array( 'post_tag', 'post_format' ),
		'hierarchical'        => false,
		'public'              => false,
		'publicly_queryable'  => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 51,
		'menu_icon'			  => 'dashicons-layout',
		'can_export'          => true,
		'has_archive'         => true,
		'rewrite'			  => array( 'slug'=>'/contentfeed' ),
		'exclude_from_search' => true,
		'capability_type'     => 'post',
	));
	
	register_post_type('program',array(
		'label'               => 'Programs',
		'description'         => 'Programs',
		'labels'              => array(
			'name'                => _x( 'Programs', 'Post Type General Name', 'tccedu' ),
			'singular_name'       => _x( 'Program', 'Post Type Singular Name', 'tccedu' ),
			'menu_name'           => 'Programs',
			'all_items'           => 'All Programs',
			'view_item'           => 'View Program',
			'add_new_item'        => 'Add New Program',
			'add_new'             => 'Add New',
			'edit_item'           => 'Edit Program',
			'update_item'         => 'Update Program',
			'search_items'        => 'Search Programs',
			'not_found'           => 'Not Found',
			'not_found_in_trash'  => 'Not found in Trash',
		),
		'supports'            => array( 'title', 'thumbnail', 'revisions' ),
		'taxonomies'          => array(),
		'hierarchical'        => true,
		'public'              => false,
		'publicly_queryable'  => false,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		//'show_in_rest'        => true,
		'menu_position'       => 52,
		'menu_icon'			  => 'dashicons-lightbulb',
		'can_export'          => true,
		'has_archive'         => true,
		//'rewrite'			  => array( 'slug'=>'/programs' ),
		'exclude_from_search' => true,
		'capability_type'     => 'post',
	));
	
	register_post_type('program-guide',array(
		'label'               => 'Program Guides',
		'description'         => 'Program Guides',
		'labels'              => array(
			'name'                => _x( 'Program Guides', 'Post Type General Name', 'tccedu' ),
			'singular_name'       => _x( 'Program Guide', 'Post Type Singular Name', 'tccedu' ),
			'menu_name'           => 'Program Guides',
			'all_items'           => 'All Program Guides',
			'view_item'           => 'View Program Guide',
			'add_new_item'        => 'Add New Program Guide',
			'add_new'             => 'Add New',
			'edit_item'           => 'Edit Program Guide',
			'update_item'         => 'Update Program Guide',
			'search_items'        => 'Search Program Guides',
			'not_found'           => 'Not Found',
			'not_found_in_trash'  => 'Not found in Trash',
		),
		'supports'            => array( 'title', 'thumbnail', 'revisions' ),
		'taxonomies'          => array(),
		'hierarchical'        => false,
		'public'              => false,
		'publicly_queryable'  => false,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		//'show_in_rest'        => true,
		'menu_position'       => 53,
		'menu_icon'			  => 'dashicons-media-document',
		'can_export'          => true,
		'has_archive'         => true,
		'rewrite'			  => array( 'slug'=>'/program-guides' ),
		'exclude_from_search' => true,
		'capability_type'     => 'post',
	));
	
	register_post_type('transfer-school',array(
		'label'               => 'Transfer Schools',
		'description'         => 'Transfer Schools',
		'labels'              => array(
			'name'                => _x( 'Transfer Schools', 'Post Type General Name', 'tccedu' ),
			'singular_name'       => _x( 'Transfer School', 'Post Type Singular Name', 'tccedu' ),
			'menu_name'           => 'Transfer Schools',
			'all_items'           => 'All Transfer Schools',
			'view_item'           => 'View Transfer School',
			'add_new_item'        => 'Add New Transfer School',
			'add_new'             => 'Add New',
			'edit_item'           => 'Edit Transfer School',
			'update_item'         => 'Update Transfer School',
			'search_items'        => 'Search Transfer Schools',
			'not_found'           => 'Not Found',
			'not_found_in_trash'  => 'Not found in Trash',
		),
		'supports'            => array( 'title', 'revisions' ),
		'taxonomies'          => array(),
		'hierarchical'        => true,
		'public'              => false,
		'publicly_queryable'  => false,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		//'show_in_rest'        => true,
		'menu_position'       => 54,
		'menu_icon'			  => 'dashicons-migrate',
		'can_export'          => true,
		'has_archive'         => true,
		//'rewrite'			  => array( 'slug'=>'/transfer-schools' ),
		'exclude_from_search' => true,
		'capability_type'     => 'post',
	));
	
	register_taxonomy_for_object_type('post_tag','page');
	
	flush_rewrite_rules();
	
	
	add_image_size('video-poster',1160,653,true);
	
	// remove front end admin bar
	add_filter('show_admin_bar','__return_false');

	remove_filter('the_content','wpautop');
	add_filter('the_content','wpautop',99);
	//add_filter('the_content','shortcode_unautop',100);
	add_filter('the_content','tccedu_clean_wp_garbage',101,1);
	
	// shortcodes
	add_shortcode('slider','tccedu_sc_slider');
	add_shortcode('slider_slide','tccedu_sc_slider_slide');
	add_shortcode('accordion','tccedu_sc_accordion');
	add_shortcode('accordion_item','tccedu_sc_accordion_item');
	add_shortcode('accordion_sorter','tccedu_sc_accordion_sorter');
	add_shortcode('sorter_item','tccedu_sc_sorter_item');
	add_shortcode('aggregate_list','tccedu_sc_aggregate_list');
	add_shortcode('program_info','tccedu_sc_program_info');
	add_shortcode('quick_tcc','tccedu_sc_quick_tcc');
	add_shortcode('sf_embed','tccedu_sc_sf_embed');
	add_shortcode('transfer_agreements','tccedu_sc_transfer_agreements');
	
	
	// blocks
	add_filter('block_categories',function($categories,$post){
		return array_merge($categories,array(array(
			'slug' => 'tcc-edu',
			'title' => 'TCC.edu Theme',
		)));
	},10,2);
	
	wp_register_script(
		'tcc-layout-image',
		get_template_directory_uri().'/admin/blocks/tcc-layout-image.js',
		array( 'wp-blocks', 'wp-element', 'wp-editor' ),
		filemtime( get_template_directory().'/admin/blocks/tcc-layout-image.js' )
	);
	wp_register_script(
		'tcc-icon-link',
		get_template_directory_uri().'/admin/blocks/tcc-icon-link.js',
		array( 'wp-blocks', 'wp-element', 'wp-editor' ),
		filemtime( get_template_directory().'/admin/blocks/tcc-icon-link.js' )
	);
	wp_register_script(
		'tcc-cta-link',
		get_template_directory_uri().'/admin/blocks/tcc-cta-link.js',
		array( 'wp-blocks', 'wp-element', 'wp-editor' ),
		filemtime( get_template_directory().'/admin/blocks/tcc-cta-link.js' )
	);
	wp_register_script(
		'tcc-button',
		get_template_directory_uri().'/admin/blocks/tcc-button.js',
		array( 'wp-blocks', 'wp-element', 'wp-editor' ),
		filemtime( get_template_directory().'/admin/blocks/tcc-button.js' )
	);
	wp_register_script(
		'tcc-accordion-wrap',
		get_template_directory_uri().'/admin/blocks/tcc-accordion-wrap.js',
		array( 'wp-blocks', 'wp-element', 'wp-editor' ),
		filemtime( get_template_directory().'/admin/blocks/tcc-accordion-wrap.js' )
	);
	wp_register_script(
		'tcc-accordion-item',
		get_template_directory_uri().'/admin/blocks/tcc-accordion-item.js',
		array( 'wp-blocks', 'wp-element', 'wp-editor' ),
		filemtime( get_template_directory().'/admin/blocks/tcc-accordion-item.js' )
	);
	
	register_block_type( 'tcc-edu/layout-image', array(
		'editor_script' => 'tcc-layout-image',
	) );
	register_block_type( 'tcc-edu/icon-link', array(
		'editor_script' => 'tcc-icon-link',
	) );
	register_block_type( 'tcc-edu/cta-link', array(
		'editor_script' => 'tcc-cta-link',
	) );
	register_block_type( 'tcc-edu/button', array(
		'editor_script' => 'tcc-button',
	) );
	register_block_type( 'tcc-edu/accordion-wrap', array(
		'editor_script' => 'tcc-accordion-wrap',
	) );
	register_block_type( 'tcc-edu/accordion-item', array(
		'editor_script' => 'tcc-accordion-item',
	) );
	
	date_default_timezone_set('America/New_York');
	
	if(!wp_next_scheduled('trending_links_event')) wp_schedule_event(time(),'daily','trending_links_event');
	
}
add_action('init','tccedu_init');

function tccedu_admin_scripts() {
	wp_enqueue_style('tccedu-fonts','https://fonts.googleapis.com/css?family=Barlow:400,400i,600|Barlow+Semi+Condensed:500,500i,600,600i,700,700i');
	wp_enqueue_style('admin-style',get_template_directory_uri().'/admin/tccedu-admin.css');
    wp_enqueue_script('admin-script',get_template_directory_uri().'/admin/tccedu-admin.js');
    wp_enqueue_media();
}
add_action('admin_enqueue_scripts','tccedu_admin_scripts');

function tccedu_admin_menu() {
	add_menu_page('TCC.edu Theme','TCC.edu Theme','manage_options','site-settings','','dashicons-admin-generic');
	add_submenu_page('site-settings','TCC.edu General Settings','General Settings','manage_options','site-settings','tccedu_settings');
	add_submenu_page('site-settings','TCC.edu Program Settings','Program Settings','manage_options','site-settings-programs','tccedu_settings_programs');
	add_submenu_page('site-settings','TCC.edu Campus Locations','Campus Locations','manage_options','site-settings-campuses','tccedu_settings_campuses');
	add_submenu_page('site-settings','TCC.edu Menu Settings','Menus','manage_options','site-settings-menus','tccedu_settings_menus');
	add_submenu_page('site-settings','TCC.edu Form Settings','Forms','manage_options','site-settings-forms','tccedu_settings_forms');
	
	remove_menu_page('edit-comments.php');
}
add_action('admin_menu','tccedu_admin_menu');

function tccedu_settings(){
	if(!current_user_can('manage_options')) wp_die('You do not have sufficient permissions to access this page.');
	$meta_state = null;
	if(isset($_POST['tccedu_meta_nonce'])&&wp_verify_nonce($_POST['tccedu_meta_nonce'],'tccedu_meta')){
		if(defined('DOING_AUTOSAVE')&&DOING_AUTOSAVE) return;
		
		$cache_outdated = false;
		if($_POST['alert_major_show']!==get_option('alert_major_show')) $cache_outdated = true;
		if($_POST['alert_major_title']!==get_option('alert_major_title')) $cache_outdated = true;
		if($_POST['alert_major_copy']!==get_option('alert_major_copy')) $cache_outdated = true;
		if($_POST['alert_major_conf']!==get_option('alert_major_conf')) $cache_outdated = true;
		if($_POST['alert_major_link']!==get_option('alert_major_link')) $cache_outdated = true;
		if($_POST['alert_major_cta']!==get_option('alert_major_cta')) $cache_outdated = true;
		if($_POST['alert_major_link_2']!==get_option('alert_major_link_2')) $cache_outdated = true;
		if($_POST['alert_major_cta_2']!==get_option('alert_major_cta_2')) $cache_outdated = true;
		if($_POST['alert_medium_show']!==get_option('alert_medium_show')) $cache_outdated = true;
		if($_POST['alert_medium_title']!==get_option('alert_medium_title')) $cache_outdated = true;
		if($_POST['alert_medium_copy']!==get_option('alert_medium_copy')) $cache_outdated = true;
		if($_POST['alert_medium_conf']!==get_option('alert_medium_conf')) $cache_outdated = true;
		if($_POST['alert_medium_link']!==get_option('alert_medium_link')) $cache_outdated = true;
		if($_POST['alert_medium_cta']!==get_option('alert_medium_cta')) $cache_outdated = true;
		if($_POST['alert_minor_show']!==get_option('alert_minor_show')) $cache_outdated = true;
		if($_POST['alert_minor_copy']!==get_option('alert_minor_copy')) $cache_outdated = true;
		if($_POST['alert_minor_cookie_days']!==get_option('alert_minor_cookie_days')) $cache_outdated = true;
		if($_POST['help_bug_show']!==get_option('help_bug_show')) $cache_outdated = true;
		if($_POST['help_bug_phone']!==get_option('help_bug_phone')) $cache_outdated = true;
		if($_POST['help_bug_cookie_days']!==get_option('help_bug_cookie_days')) $cache_outdated = true;
		if($_POST['help_bug_trigger_after']!==get_option('help_bug_trigger_after')) $cache_outdated = true;
		if($_POST['help_bug_timer']!==get_option('help_bug_timer')) $cache_outdated = true;
		if($_POST['sf_pdf_prefix']!==get_option('sf_pdf_prefix')) $cache_outdated = true;
		
		if($_POST['alert_major_show']) update_option('alert_major_show',sanitize_text_field($_POST['alert_major_show'])); else delete_option('alert_major_show');
		if($_POST['alert_major_title']) update_option('alert_major_title',sanitize_text_field($_POST['alert_major_title'])); else delete_option('alert_major_title');
		if($_POST['alert_major_copy']) update_option('alert_major_copy',$_POST['alert_major_copy']); else delete_option('alert_major_copy');
		if($_POST['alert_major_conf']) update_option('alert_major_conf',sanitize_text_field($_POST['alert_major_conf'])); else delete_option('alert_major_conf');
		if($_POST['alert_major_link']) update_option('alert_major_link',sanitize_text_field($_POST['alert_major_link'])); else delete_option('alert_major_link');
		if($_POST['alert_major_cta']) update_option('alert_major_cta',sanitize_text_field($_POST['alert_major_cta'])); else delete_option('alert_major_cta');
		if($_POST['alert_major_link_2']) update_option('alert_major_link_2',sanitize_text_field($_POST['alert_major_link_2'])); else delete_option('alert_major_link_2');
		if($_POST['alert_major_cta_2']) update_option('alert_major_cta_2',sanitize_text_field($_POST['alert_major_cta_2'])); else delete_option('alert_major_cta_2');
		
		if($_POST['alert_medium_show']) update_option('alert_medium_show',sanitize_text_field($_POST['alert_medium_show'])); else delete_option('alert_medium_show');
		if($_POST['alert_medium_title']) update_option('alert_medium_title',sanitize_text_field($_POST['alert_medium_title'])); else delete_option('alert_medium_title');
		if($_POST['alert_medium_copy']) update_option('alert_medium_copy',$_POST['alert_medium_copy']); else delete_option('alert_medium_copy');
		if($_POST['alert_medium_conf']) update_option('alert_medium_conf',sanitize_text_field($_POST['alert_medium_conf'])); else delete_option('alert_medium_conf');
		if($_POST['alert_medium_link']) update_option('alert_medium_link',sanitize_text_field($_POST['alert_medium_link'])); else delete_option('alert_medium_link');
		if($_POST['alert_medium_cta']) update_option('alert_medium_cta',sanitize_text_field($_POST['alert_medium_cta'])); else delete_option('alert_medium_cta');
		
		if($_POST['alert_minor_show']) update_option('alert_minor_show',sanitize_text_field($_POST['alert_minor_show'])); else delete_option('alert_minor_show');
		if($_POST['alert_minor_copy']) update_option('alert_minor_copy',$_POST['alert_minor_copy']); else delete_option('alert_minor_copy');
		if($_POST['alert_minor_cookie_days']) update_option('alert_minor_cookie_days',sanitize_text_field($_POST['alert_minor_cookie_days'])); else delete_option('alert_minor_cookie_days');
		
		if($_POST['help_bug_show']) update_option('help_bug_show',sanitize_text_field($_POST['help_bug_show'])); else delete_option('help_bug_show');
		if($_POST['help_bug_schedule']){
			foreach(explode("\r\n",preg_replace('/[^A-Za-z0-9\s\-\\r\\n]/','',$_POST['help_bug_schedule'])) as $day){
				$day_split = explode(" ",$day);
				if($day) $schedule_days[$day_split[0]] = explode("-",$day_split[1]);
			}
			update_option('help_bug_schedule',json_encode($schedule_days));
		} else {
			delete_option('help_bug_schedule');
		}
		if($_POST['help_bug_phone']) update_option('help_bug_phone',sanitize_text_field($_POST['help_bug_phone'])); else delete_option('help_bug_phone');
		if($_POST['help_bug_cookie_days']) update_option('help_bug_cookie_days',sanitize_text_field($_POST['help_bug_cookie_days'])); else delete_option('help_bug_cookie_days');
		if($_POST['help_bug_trigger_after']) update_option('help_bug_trigger_after',sanitize_text_field($_POST['help_bug_trigger_after'])); else delete_option('help_bug_trigger_after');
		if($_POST['help_bug_timer']) update_option('help_bug_timer',sanitize_text_field($_POST['help_bug_timer'])); else delete_option('help_bug_timer');
		
		if($_POST['sf_pdf_prefix']) update_option('sf_pdf_prefix',sanitize_text_field($_POST['sf_pdf_prefix'])); else delete_option('sf_pdf_prefix');
		
		for($p=1;$p<=5;$p++){
			
			if($_POST['promo_'.$p.'_show']!==get_option('promo_'.$p.'_show')) $cache_outdated = true;
			if($_POST['promo_'.$p.'_title']!==get_option('promo_'.$p.'_title')) $cache_outdated = true;
			if($_POST['promo_'.$p.'_copy']!==get_option('promo_'.$p.'_copy')) $cache_outdated = true;
			if($_POST['promo_'.$p.'_cta']!==get_option('promo_'.$p.'_cta')) $cache_outdated = true;
			if($_POST['promo_'.$p.'_link']!==get_option('promo_'.$p.'_link')) $cache_outdated = true;
			if($_POST['promo_'.$p.'_btn_class']!==get_option('promo_'.$p.'_btn_class')) $cache_outdated = true;
			
			if($_POST['promo_'.$p.'_show']) update_option('promo_'.$p.'_show',sanitize_text_field($_POST['promo_'.$p.'_show'])); else delete_option('promo_'.$p.'_show');
			if($_POST['promo_'.$p.'_title']) update_option('promo_'.$p.'_title',$_POST['promo_'.$p.'_title']); else delete_option('promo_'.$p.'_title');
			if($_POST['promo_'.$p.'_copy']) update_option('promo_'.$p.'_copy',$_POST['promo_'.$p.'_copy']); else delete_option('promo_'.$p.'_copy');
			if($_POST['promo_'.$p.'_cta']) update_option('promo_'.$p.'_cta',sanitize_text_field($_POST['promo_'.$p.'_cta'])); else delete_option('promo_'.$p.'_cta');
			if($_POST['promo_'.$p.'_link']) update_option('promo_'.$p.'_link',sanitize_text_field($_POST['promo_'.$p.'_link'])); else delete_option('promo_'.$p.'_link');
			if($_POST['promo_'.$p.'_btn_class']) update_option('promo_'.$p.'_btn_class',sanitize_text_field($_POST['promo_'.$p.'_btn_class'])); else delete_option('promo_'.$p.'_btn_class');
		}
		
		if($cache_outdated&&function_exists(wp_cache_clear_cache)) wp_cache_clear_cache();

		$meta_state = 'Settings saved.';
	}
	$tccedu_meta = 'settings';
	include('admin/meta-site.php');
}

function tccedu_settings_programs(){
	if(!current_user_can('manage_options')) wp_die('You do not have sufficient permissions to access this page.');
	$meta_state = null;
	if(isset($_POST['tccedu_meta_nonce'])&&wp_verify_nonce($_POST['tccedu_meta_nonce'],'tccedu_meta')){
		if(defined('DOING_AUTOSAVE')&&DOING_AUTOSAVE) return;
		
		if($_POST['career_options_list']){
			foreach(explode("\r\n",$_POST['career_options_list']) as $option){
				if($option) $career_options[] = array('id'=>sanitize_title($option),'name'=>$option);
			}
			update_option('career_options_list',json_encode($career_options));
		} else {
			delete_option('career_options_list');
		}
		if($_POST['program_type_options_list']){
			foreach(explode("\r\n",$_POST['program_type_options_list']) as $option){
				if($option) $program_type_options[] = array('id'=>sanitize_title($option),'name'=>$option);
			}
			update_option('program_type_options_list',json_encode($program_type_options));
		} else {
			delete_option('program_type_options_list');
		}
		if($_POST['degree_options_list']){
			foreach(explode("\r\n",$_POST['degree_options_list']) as $option){
				if($option) $degree_options[] = array('id'=>sanitize_title($option),'name'=>$option);
			}
			update_option('degree_options_list',json_encode($degree_options));
		} else {
			delete_option('degree_options_list');
		}
		if($_POST['interest_options_list']){
			foreach(explode("\r\n",$_POST['interest_options_list']) as $option){
				if($option) $interest_options[] = array('id'=>sanitize_title($option),'name'=>$option);
			}
			update_option('interest_options_list',json_encode($interest_options));
		} else {
			delete_option('interest_options_list');
		}
		
		tccedu_generate_tccvals();
		
		$meta_state = 'Settings saved.';
	}
	$tccedu_meta = 'settings';
	include('admin/meta-site-programs.php');
}

function tccedu_settings_campuses(){
	if(!current_user_can('manage_options')) wp_die('You do not have sufficient permissions to access this page.');
	$meta_state = null;
	if(isset($_POST['tccedu_meta_nonce'])&&wp_verify_nonce($_POST['tccedu_meta_nonce'],'tccedu_meta')){
		if(defined('DOING_AUTOSAVE')&&DOING_AUTOSAVE) return;
		
		for($c=1;$c<=15;$c++){
			
			if($_POST['campus_'.$c.'_show']) update_option('campus_'.$c.'_show',sanitize_text_field($_POST['campus_'.$c.'_show'])); else delete_option('campus_'.$c.'_show');
			if($_POST['campus_'.$c.'_title']) update_option('campus_'.$c.'_title',sanitize_text_field($_POST['campus_'.$c.'_title'])); else delete_option('campus_'.$c.'_title');
			if($_POST['campus_'.$c.'_id']) update_option('campus_'.$c.'_id',sanitize_text_field($_POST['campus_'.$c.'_id'])); else delete_option('campus_'.$c.'_id');
			if($_POST['campus_'.$c.'_location']) update_option('campus_'.$c.'_location',sanitize_text_field($_POST['campus_'.$c.'_location'])); else delete_option('campus_'.$c.'_location');
			if($_POST['campus_'.$c.'_link']) update_option('campus_'.$c.'_link',sanitize_text_field($_POST['campus_'.$c.'_link'])); else delete_option('campus_'.$c.'_link');
			if($_POST['campus_'.$c.'_phone']) update_option('campus_'.$c.'_phone',sanitize_text_field($_POST['campus_'.$c.'_phone'])); else delete_option('campus_'.$c.'_phone');
			if($_POST['campus_'.$c.'_map_pdf_id']) update_option('campus_'.$c.'_map_pdf_id',sanitize_text_field($_POST['campus_'.$c.'_map_pdf_id'])); else delete_option('campus_'.$c.'_map_pdf_id');
			if($_POST['campus_'.$c.'_map_pdf_src']) update_option('campus_'.$c.'_map_pdf_src',sanitize_text_field($_POST['campus_'.$c.'_map_pdf_src'])); else delete_option('campus_'.$c.'_map_pdf_src');
			if($_POST['campus_'.$c.'_is_search']) update_option('campus_'.$c.'_is_search',sanitize_text_field($_POST['campus_'.$c.'_is_search'])); else delete_option('campus_'.$c.'_is_search');
			if($_POST['campus_'.$c.'_type']) update_option('campus_'.$c.'_type',sanitize_text_field($_POST['campus_'.$c.'_type'])); else delete_option('campus_'.$c.'_type');
			if($_POST['campus_'.$c.'_hide']) update_option('campus_'.$c.'_hide',sanitize_text_field($_POST['campus_'.$c.'_hide'])); else delete_option('campus_'.$c.'_hide');
			if($_POST['campus_'.$c.'_coords']) update_option('campus_'.$c.'_coords',sanitize_text_field($_POST['campus_'.$c.'_coords'])); else delete_option('campus_'.$c.'_coords');
			if($_POST['campus_'.$c.'_addr_1']) update_option('campus_'.$c.'_addr_1',sanitize_text_field($_POST['campus_'.$c.'_addr_1'])); else delete_option('campus_'.$c.'_addr_1');
			if($_POST['campus_'.$c.'_addr_2']) update_option('campus_'.$c.'_addr_2',sanitize_text_field($_POST['campus_'.$c.'_addr_2'])); else delete_option('campus_'.$c.'_addr_2');
			
		}
		
		if(function_exists(wp_cache_clear_cache)) wp_cache_clear_cache();
		tccedu_generate_tccvals();
		
		$meta_state = 'Settings saved.';
	}
	$tccedu_meta = 'settings';
	include('admin/meta-site-campus.php');
}

function tccedu_settings_menus(){
	if(!current_user_can('manage_options')) wp_die('You do not have sufficient permissions to access this page.');
	$meta_state = null;
	if(isset($_POST['tccedu_meta_nonce'])&&wp_verify_nonce($_POST['tccedu_meta_nonce'],'tccedu_meta')){
		if(defined('DOING_AUTOSAVE')&&DOING_AUTOSAVE) return;
		
		for($f=1;$f<=6;$f++){
			if($_POST['menu_feature_'.$f.'_title']) update_option('menu_feature_'.$f.'_title',sanitize_text_field($_POST['menu_feature_'.$f.'_title'])); else delete_option('menu_feature_'.$f.'_title');
			if($_POST['menu_feature_'.$f.'_copy']) update_option('menu_feature_'.$f.'_copy',sanitize_text_field($_POST['menu_feature_'.$f.'_copy'])); else delete_option('menu_feature_'.$f.'_copy');
			if($_POST['menu_feature_'.$f.'_target']) update_option('menu_feature_'.$f.'_target',sanitize_text_field($_POST['menu_feature_'.$f.'_target'])); else delete_option('menu_feature_'.$f.'_target');
			if($_POST['menu_feature_'.$f.'_cta']) update_option('menu_feature_'.$f.'_cta',sanitize_text_field($_POST['menu_feature_'.$f.'_cta'])); else delete_option('menu_feature_'.$f.'_cta');
			if($_POST['menu_feature_'.$f.'_link']) update_option('menu_feature_'.$f.'_link',sanitize_text_field($_POST['menu_feature_'.$f.'_link'])); else delete_option('menu_feature_'.$f.'_link');
		}
		
		if(function_exists(wp_cache_clear_cache)) wp_cache_clear_cache();
		
		$meta_state = 'Settings saved.';
	}
	$tccedu_meta = 'settings';
	include('admin/meta-site-menus.php');
}

function tccedu_settings_forms(){
	if(!current_user_can('manage_options')) wp_die('You do not have sufficient permissions to access this page.');
	$meta_state = null;
	if(isset($_POST['tccedu_meta_nonce'])&&wp_verify_nonce($_POST['tccedu_meta_nonce'],'tccedu_meta')){
		if(defined('DOING_AUTOSAVE')&&DOING_AUTOSAVE) return;
		
		if($_POST['quicktcc_form_stages']) update_option('quicktcc_form_stages',$_POST['quicktcc_form_stages']); else delete_option('quicktcc_form_stages');
		if($_POST['sidebar_form_stages']) update_option('sidebar_form_stages',$_POST['sidebar_form_stages']); else delete_option('sidebar_form_stages');
		
		tccedu_generate_tccvals();
		
		$meta_state = 'Settings saved.';
	}
	$tccedu_meta = 'settings';
	include('admin/meta-site-forms.php');
}

add_filter('manage_event_posts_columns','tccedu_add_event_columns');
add_action('manage_event_posts_custom_column','tccedu_event_columns',10,2);

function tccedu_add_event_columns($columns){
	unset($columns['author']);
	$columns['event_date'] = __('Event Date','tccedu');
	return $columns;
}

function tccedu_event_columns($column,$post_id){
	if($column=="event_date") echo get_post_meta($post_id,'event_date',true);
}

add_filter('manage_feeditem_posts_columns','tccedu_add_feeditem_columns');
add_action('manage_feeditem_posts_custom_column','tccedu_feeditem_columns',10,2);

function tccedu_add_feeditem_columns($columns){
	unset($columns['author']);
	$columns['type'] = __('Type','tccedu');
	return $columns;
}

function tccedu_feeditem_columns($column,$post_id){
	if($column=="type") echo ucwords(get_post_meta($post_id,'feeditem_type',true));
}


function tccedu_post_meta(){
	global $post;
	
	add_meta_box('tccedu_page_settings','Event Settings','tccedu_event_meta_callback','event');
	add_meta_box('tccedu_page_settings','Feed Item Settings','tccedu_feeditem_meta_callback','feeditem');
	add_meta_box('tccedu_page_settings','Program Settings','tccedu_program_meta_callback','program');
	add_meta_box('tccedu_page_settings','Program Guide Settings','tccedu_program_guide_meta_callback','program-guide');
	add_meta_box('tccedu_page_settings','Transfer School Settings','tccedu_transfer_school_meta_callback','transfer-school');
	
	add_meta_box('tccedu_page_settings','Page Settings','tccedu_page_meta_callback','page');
	
	if($post->post_type=="event"||$post->post_type=="feeditem"){
		wp_enqueue_script('jquery-ui-core');
		wp_enqueue_script('jquery-ui-datepicker');
		wp_enqueue_style('jquery-style','//ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/themes/smoothness/jquery-ui.css');
	}
}
add_action('add_meta_boxes','tccedu_post_meta');

function tccedu_page_meta_callback($post){
	
	wp_nonce_field('tccedu_post_meta','tccedu_post_meta_nonce');
	$page_meta = get_post_meta($post->ID);
	
	if(strpos(get_page_template_slug($post->ID),"homepage.php")!==false){
		include('admin/meta-home.php');
	} else if(strpos(get_page_template_slug($post->ID),"program-page.php")!==false){
		include('admin/meta-program-page.php');
	} else if(strpos(get_page_template_slug($post->ID),"program-finder.php")!==false){
		include('admin/meta-program-finder.php');
	} else if(strpos(get_page_template_slug($post->ID),"events-page.php")!==false){
		include('admin/meta-events-page.php');
	} else if(strpos(get_page_template_slug($post->ID),"aggregate-page.php")!==false){
		include('admin/meta-aggregate-page.php');
	} else if(strpos(get_page_template_slug($post->ID),"campus-page.php")!==false){
		include('admin/meta-campus-page.php');
	} else if(strpos(get_page_template_slug($post->ID),"landing-slider-page.php")!==false){
		include('admin/meta-landing-page.php');
	} else {
		include('admin/meta-page.php');
	}
}
function tccedu_event_meta_callback($post){
	wp_nonce_field('tccedu_post_meta','tccedu_post_meta_nonce');
	$event_meta = get_post_meta($post->ID);
	include('admin/meta-event.php');
}
function tccedu_feeditem_meta_callback($post){
	wp_nonce_field('tccedu_post_meta','tccedu_post_meta_nonce');
	$feeditem_meta = get_post_meta($post->ID);
	include('admin/meta-feeditem.php');
}
function tccedu_program_meta_callback($post){
	wp_nonce_field('tccedu_post_meta','tccedu_post_meta_nonce');
	$program_meta = get_post_meta($post->ID);
	include('admin/meta-program.php');
}
function tccedu_program_guide_meta_callback($post){
	wp_nonce_field('tccedu_post_meta','tccedu_post_meta_nonce');
	$guide_meta = get_post_meta($post->ID);
	include('admin/meta-program-guide.php');
}
function tccedu_transfer_school_meta_callback($post){
	wp_nonce_field('tccedu_post_meta','tccedu_post_meta_nonce');
	$school_meta = get_post_meta($post->ID);
	include('admin/meta-transfer-school.php');
}

function tccedu_save_post($post_id){
	if(!isset($_POST['tccedu_post_meta_nonce'])) return;
	if(!wp_verify_nonce($_POST['tccedu_post_meta_nonce'],'tccedu_post_meta')) return;
	if(defined('DOING_AUTOSAVE')&&DOING_AUTOSAVE) return;
	if(!current_user_can('edit_post',$post_id)) return;
	$post = get_post($post_id);
	if($post->post_type=="event"){
		
		if(isset($_POST['event_date'])&&!empty($_POST['event_date'])) update_post_meta($post_id,'event_date',sanitize_text_field($_POST['event_date'])); else delete_post_meta($post_id,'event_date');
		if(isset($_POST['event_time'])&&!empty($_POST['event_time'])) update_post_meta($post_id,'event_time',sanitize_text_field($_POST['event_time'])); else delete_post_meta($post_id,'event_time');
		if(isset($_POST['event_campus'])&&!empty($_POST['event_campus'])) update_post_meta($post_id,'event_campus',sanitize_text_field($_POST['event_campus'])); else delete_post_meta($post_id,'event_campus');
		if(isset($_POST['event_campus_other'])&&!empty($_POST['event_campus_other'])) update_post_meta($post_id,'event_campus_other',sanitize_text_field($_POST['event_campus_other'])); else delete_post_meta($post_id,'event_campus_other');
		if(isset($_POST['event_location'])&&!empty($_POST['event_location'])) update_post_meta($post_id,'event_location',sanitize_text_field($_POST['event_location'])); else delete_post_meta($post_id,'event_location');
		if(isset($_POST['event_address'])&&!empty($_POST['event_address'])) update_post_meta($post_id,'event_address',sanitize_text_field($_POST['event_address'])); else delete_post_meta($post_id,'event_address');
		
	} else if($post->post_type=="feeditem"){
		
		if(isset($_POST['feeditem_title'])&&!empty($_POST['feeditem_title'])) update_post_meta($post_id,'feeditem_title',sanitize_text_field($_POST['feeditem_title'])); else delete_post_meta($post_id,'feeditem_title');
		if(isset($_POST['feeditem_slug'])&&!empty($_POST['feeditem_slug'])) update_post_meta($post_id,'feeditem_slug',sanitize_title($_POST['feeditem_slug'])); else update_post_meta($post_id,'feeditem_slug',$post->post_name);
		if(isset($_POST['feeditem_copy'])&&!empty($_POST['feeditem_copy'])) update_post_meta($post_id,'feeditem_copy',sanitize_text_field($_POST['feeditem_copy'])); else delete_post_meta($post_id,'feeditem_copy');
		if(isset($_POST['feeditem_img'])&&!empty($_POST['feeditem_img'])) update_post_meta($post_id,'feeditem_img',sanitize_text_field($_POST['feeditem_img'])); else delete_post_meta($post_id,'feeditem_img');
		
		if(isset($_POST['feeditem_img'])&&!empty($_POST['feeditem_img'])){
			$att_id = fjarrett_get_attachment_id_by_url($_POST['feeditem_img']);		
			if($att_id){
				update_post_meta($post_id,'feeditem_img_set',implode(",",array(
					wp_get_attachment_image_src($att_id,'thumbnail')[0],
					wp_get_attachment_image_src($att_id,'medium')[0],
					wp_get_attachment_image_src($att_id,'large')[0]
				)));
			} else {
				delete_post_meta($post_id,'feeditem_img_set');
			}
		} else {
			delete_post_meta($post_id,'feeditem_img_set');
		}
		
		if(isset($_POST['feeditem_weighted'])&&!empty($_POST['feeditem_weighted'])) update_post_meta($post_id,'feeditem_weighted',sanitize_text_field($_POST['feeditem_weighted'])); else delete_post_meta($post_id,'feeditem_weighted');
		if(isset($_POST['feeditem_type'])&&!empty($_POST['feeditem_type'])) update_post_meta($post_id,'feeditem_type',sanitize_text_field($_POST['feeditem_type'])); else delete_post_meta($post_id,'feeditem_type');
		if(isset($_POST['feeditem_video_id'])&&!empty($_POST['feeditem_video_id'])) update_post_meta($post_id,'feeditem_video_id',sanitize_text_field($_POST['feeditem_video_id'])); else delete_post_meta($post_id,'feeditem_video_id');
		if(isset($_POST['feeditem_video_src'])&&!empty($_POST['feeditem_video_src'])) update_post_meta($post_id,'feeditem_video_src',sanitize_text_field($_POST['feeditem_video_src'])); else delete_post_meta($post_id,'feeditem_video_src');
		if(isset($_POST['feeditem_video_poster'])&&!empty($_POST['feeditem_video_poster'])) update_post_meta($post_id,'feeditem_video_poster',sanitize_text_field($_POST['feeditem_video_poster'])); else delete_post_meta($post_id,'feeditem_video_poster');
		if(isset($_POST['feeditem_video_captions'])&&!empty($_POST['feeditem_video_captions'])) update_post_meta($post_id,'feeditem_video_captions',sanitize_text_field($_POST['feeditem_video_captions'])); else delete_post_meta($post_id,'feeditem_video_captions');
		if(isset($_POST['feeditem_article_author'])&&!empty($_POST['feeditem_article_author'])) update_post_meta($post_id,'feeditem_article_author',sanitize_text_field($_POST['feeditem_article_author'])); else delete_post_meta($post_id,'feeditem_article_author');
		if(isset($_POST['feeditem_article_date'])&&!empty($_POST['feeditem_article_date'])) update_post_meta($post_id,'feeditem_article_date',sanitize_text_field($_POST['feeditem_article_date'])); else delete_post_meta($post_id,'feeditem_article_date');
		if(isset($_POST['feeditem_article_excerpt'])&&!empty($_POST['feeditem_article_excerpt'])) update_post_meta($post_id,'feeditem_article_excerpt',sanitize_text_field($_POST['feeditem_article_excerpt'])); else delete_post_meta($post_id,'feeditem_article_excerpt');
		if(isset($_POST['feeditem_article_cta'])&&!empty($_POST['feeditem_article_cta'])) update_post_meta($post_id,'feeditem_article_cta',sanitize_text_field($_POST['feeditem_article_cta'])); else delete_post_meta($post_id,'feeditem_article_cta');
		if(isset($_POST['feeditem_article_link'])&&!empty($_POST['feeditem_article_link'])) update_post_meta($post_id,'feeditem_article_link',sanitize_text_field($_POST['feeditem_article_link'])); else delete_post_meta($post_id,'feeditem_article_link');
		if(isset($_POST['feeditem_link_cta'])&&!empty($_POST['feeditem_link_cta'])) update_post_meta($post_id,'feeditem_link_cta',sanitize_text_field($_POST['feeditem_link_cta'])); else delete_post_meta($post_id,'feeditem_link_cta');
		if(isset($_POST['feeditem_link_link'])&&!empty($_POST['feeditem_link_link'])) update_post_meta($post_id,'feeditem_link_link',sanitize_text_field($_POST['feeditem_link_link'])); else delete_post_meta($post_id,'feeditem_link_link');
		
	} else if($post->post_type=="program"){
		
		if(isset($_POST['program_id'])&&!empty($_POST['program_id'])) update_post_meta($post_id,'program_id',sanitize_text_field($_POST['program_id'])); else update_post_meta($post_id,'program_id',sanitize_title($post->post_title));
		if(isset($_POST['program_title'])&&!empty($_POST['program_title'])) update_post_meta($post_id,'program_title',sanitize_text_field($_POST['program_title'])); else update_post_meta($post_id,'program_title',sanitize_title($post->post_title));
		if(isset($_POST['program_desc'])&&!empty($_POST['program_desc'])) update_post_meta($post_id,'program_desc',$_POST['program_desc']); else delete_post_meta($post_id,'program_desc');
		if(isset($_POST['program_semesters'])&&!empty($_POST['program_semesters'])) update_post_meta($post_id,'program_semesters',sanitize_text_field($_POST['program_semesters'])); else delete_post_meta($post_id,'program_semesters');
		if(isset($_POST['program_credits'])&&!empty($_POST['program_credits'])) update_post_meta($post_id,'program_credits',sanitize_text_field($_POST['program_credits'])); else delete_post_meta($post_id,'program_credits');
		if(isset($_POST['program_curriculum'])&&!empty($_POST['program_curriculum'])) update_post_meta($post_id,'program_curriculum',sanitize_text_field($_POST['program_curriculum'])); else delete_post_meta($post_id,'program_curriculum');
		if(isset($_POST['program_link'])&&!empty($_POST['program_link'])) update_post_meta($post_id,'program_link',sanitize_text_field($_POST['program_link'])); else delete_post_meta($post_id,'program_link');
		
		if(isset($_POST['program_career'])&&!empty($_POST['program_career'])) update_post_meta($post_id,'program_career',sanitize_text_field($_POST['program_career'])); else delete_post_meta($post_id,'program_career');
		if(isset($_POST['program_type'])&&!empty($_POST['program_type'])) update_post_meta($post_id,'program_type',$_POST['program_type']); else delete_post_meta($post_id,'program_type');
		if(isset($_POST['program_degree'])&&!empty($_POST['program_degree'])) update_post_meta($post_id,'program_degree',$_POST['program_degree']); else delete_post_meta($post_id,'program_degree');
		if(isset($_POST['program_location'])&&!empty($_POST['program_location'])) update_post_meta($post_id,'program_location',$_POST['program_location']); else delete_post_meta($post_id,'program_location');
		if(isset($_POST['program_interest'])&&!empty($_POST['program_interest'])) update_post_meta($post_id,'program_interest',$_POST['program_interest']); else delete_post_meta($post_id,'program_interest');
		
		$program_terms = preg_replace('/[^A-Za-z0-9\-\,\?\!\.\'\"\@\#\$\%\^\&\*\(\)\s]/','',$_POST['program_terms']);
		if(isset($program_terms)&&!empty($program_terms)) update_post_meta($post_id,'program_terms',$program_terms); else delete_post_meta($post_id,'program_terms');
		
		tccedu_generate_tccvals();
		
	} else if($post->post_type=="program-guide"){
		
		if(isset($_POST['guide_id'])&&!empty($_POST['guide_id'])) update_post_meta($post_id,'guide_id',sanitize_text_field($_POST['guide_id'])); else update_post_meta($post_id,'guide_id',sanitize_title($post->post_title));
		if(isset($_POST['guide_sf_pdf'])&&!empty($_POST['guide_sf_pdf'])) update_post_meta($post_id,'guide_sf_pdf',sanitize_text_field($_POST['guide_sf_pdf'])); else delete_post_meta($post_id,'guide_sf_pdf');
		if(isset($_POST['guide_email_template'])&&!empty($_POST['guide_email_template'])) update_post_meta($post_id,'guide_email_template',sanitize_text_field($_POST['guide_email_template'])); else delete_post_meta($post_id,'guide_email_template');
		if(isset($_POST['guide_specific_interest'])&&!empty($_POST['guide_specific_interest'])) update_post_meta($post_id,'guide_specific_interest',sanitize_text_field($_POST['guide_specific_interest'])); else delete_post_meta($post_id,'guide_specific_interest');
		if(isset($_POST['guide_specific_interest_alt'])&&!empty($_POST['guide_specific_interest_alt'])) update_post_meta($post_id,'guide_specific_interest_alt',sanitize_text_field($_POST['guide_specific_interest_alt'])); else delete_post_meta($post_id,'guide_specific_interest_alt');
		if(isset($_POST['guide_program_interest'])&&!empty($_POST['guide_program_interest'])) update_post_meta($post_id,'guide_program_interest',sanitize_text_field($_POST['guide_program_interest'])); else delete_post_meta($post_id,'guide_program_interest');
		if(isset($_POST['guide_program_interest_alt'])&&!empty($_POST['guide_program_interest_alt'])) update_post_meta($post_id,'guide_program_interest_alt',sanitize_text_field($_POST['guide_program_interest_alt'])); else delete_post_meta($post_id,'guide_program_interest_alt');
		if(isset($_POST['guide_transfer_interest'])&&!empty($_POST['guide_transfer_interest'])) update_post_meta($post_id,'guide_transfer_interest',sanitize_text_field($_POST['guide_transfer_interest'])); else delete_post_meta($post_id,'guide_transfer_interest');
		if(isset($_POST['guide_military_interest'])&&!empty($_POST['guide_military_interest'])) update_post_meta($post_id,'guide_military_interest',sanitize_text_field($_POST['guide_military_interest'])); else delete_post_meta($post_id,'guide_military_interest');
		
		tccedu_generate_tccvals();
		
	} else if($post->post_type=="transfer-school"){
		
		if(isset($_POST['school_type'])&&!empty($_POST['school_type'])) update_post_meta($post_id,'school_type',sanitize_text_field($_POST['school_type'])); else delete_post_meta($post_id,'school_type');
		if(isset($_POST['school_location'])&&!empty($_POST['school_location'])) update_post_meta($post_id,'school_location',sanitize_text_field($_POST['school_location'])); else delete_post_meta($post_id,'school_location');
		if(isset($_POST['school_gpa'])&&!empty($_POST['school_gpa'])) update_post_meta($post_id,'school_gpa',sanitize_text_field($_POST['school_gpa'])); else delete_post_meta($post_id,'school_gpa');
		if(isset($_POST['school_url'])&&!empty($_POST['school_url'])) update_post_meta($post_id,'school_url',sanitize_text_field($_POST['school_url'])); else delete_post_meta($post_id,'school_url');
		if(isset($_POST['school_agreements'])&&!empty($_POST['school_agreements'])) update_post_meta($post_id,'school_agreements',$_POST['school_agreements']); else delete_post_meta($post_id,'school_agreements');
		
		tccedu_generate_tccvals();
		
	} else if($post->post_type=="page"){
		if(strpos(get_page_template_slug($post->ID),"homepage.php")!==false){
			
			if(isset($_POST['gateway_enable'])&&!empty($_POST['gateway_enable'])) update_post_meta($post_id,'gateway_enable',sanitize_text_field($_POST['gateway_enable'])); else delete_post_meta($post_id,'gateway_enable');
			if(isset($_POST['gateway_bg_img'])&&!empty($_POST['gateway_bg_img'])) update_post_meta($post_id,'gateway_bg_img',sanitize_text_field($_POST['gateway_bg_img'])); else delete_post_meta($post_id,'gateway_bg_img');
			if(isset($_POST['gateway_cookie_days'])&&!empty($_POST['gateway_cookie_days'])) update_post_meta($post_id,'gateway_cookie_days',sanitize_text_field($_POST['gateway_cookie_days'])); else delete_post_meta($post_id,'gateway_cookie_days');
			for($o=1;$o<=7;$o++){
				$item = "gateway_opt_".$o;
				if(isset($_POST[$item.'_show'])&&!empty($_POST[$item.'_show'])) update_post_meta($post_id,$item.'_show',sanitize_text_field($_POST[$item.'_show'])); else delete_post_meta($post_id,$item.'_show');
				if(isset($_POST[$item.'_title'])&&!empty($_POST[$item.'_title'])) update_post_meta($post_id,$item.'_title',sanitize_text_field($_POST[$item.'_title'])); else delete_post_meta($post_id,$item.'_title');
				if(isset($_POST[$item.'_link'])&&!empty($_POST[$item.'_link'])) update_post_meta($post_id,$item.'_link',sanitize_text_field($_POST[$item.'_link'])); else delete_post_meta($post_id,$item.'_link');
			}
			
			for($s=1;$s<=5;$s++){
				$item = "slide_".$s;
				if(isset($_POST[$item.'_show'])&&!empty($_POST[$item.'_show'])) update_post_meta($post_id,$item.'_show',sanitize_text_field($_POST[$item.'_show'])); else delete_post_meta($post_id,$item.'_show');
				if(isset($_POST[$item.'_title'])&&!empty($_POST[$item.'_title'])) update_post_meta($post_id,$item.'_title',sanitize_text_field($_POST[$item.'_title'])); else delete_post_meta($post_id,$item.'_title');
				if(isset($_POST[$item.'_copy'])&&!empty($_POST[$item.'_copy'])) update_post_meta($post_id,$item.'_copy',sanitize_text_field($_POST[$item.'_copy'])); else delete_post_meta($post_id,$item.'_copy');
				if(isset($_POST[$item.'_cta'])&&!empty($_POST[$item.'_cta'])) update_post_meta($post_id,$item.'_cta',sanitize_text_field($_POST[$item.'_cta'])); else delete_post_meta($post_id,$item.'_cta');
				if(isset($_POST[$item.'_link'])&&!empty($_POST[$item.'_link'])) update_post_meta($post_id,$item.'_link',sanitize_text_field($_POST[$item.'_link'])); else delete_post_meta($post_id,$item.'_link');
				if(isset($_POST[$item.'_align'])&&!empty($_POST[$item.'_align'])) update_post_meta($post_id,$item.'_align',sanitize_text_field($_POST[$item.'_align'])); else delete_post_meta($post_id,$item.'_align');
				if(isset($_POST[$item.'_img'])&&!empty($_POST[$item.'_img'])) update_post_meta($post_id,$item.'_img',sanitize_text_field($_POST[$item.'_img'])); else delete_post_meta($post_id,$item.'_img');
				for($si=1;$si<11;$si++){
					if(isset($_POST[$item.'_img_'.$si])&&!empty($_POST[$item.'_img_'.$si])) update_post_meta($post_id,$item.'_img_'.$si,sanitize_text_field($_POST[$item.'_img_'.$si])); else delete_post_meta($post_id,$item.'_img_'.$si);
				}
			}
			
			for($f=1;$f<=4;$f++){
				$item = "four_up_".$f;
				if(isset($_POST[$item.'_class'])&&!empty($_POST[$item.'_class'])) update_post_meta($post_id,$item.'_class',sanitize_text_field($_POST[$item.'_class'])); else delete_post_meta($post_id,$item.'_class');
				if(isset($_POST[$item.'_svg'])&&!empty($_POST[$item.'_svg'])) update_post_meta($post_id,$item.'_svg',$_POST[$item.'_svg']); else delete_post_meta($post_id,$item.'_svg');
				if(isset($_POST[$item.'_title'])&&!empty($_POST[$item.'_title'])) update_post_meta($post_id,$item.'_title',sanitize_text_field($_POST[$item.'_title'])); else delete_post_meta($post_id,$item.'_title');
				if(isset($_POST[$item.'_copy'])&&!empty($_POST[$item.'_copy'])) update_post_meta($post_id,$item.'_copy',sanitize_text_field($_POST[$item.'_copy'])); else delete_post_meta($post_id,$item.'_copy');
				if(isset($_POST[$item.'_link'])&&!empty($_POST[$item.'_link'])) update_post_meta($post_id,$item.'_link',sanitize_text_field($_POST[$item.'_link'])); else delete_post_meta($post_id,$item.'_link');
			}
			
			for($k=1;$k<=5;$k++){
				$item = "key_msg_".$k;
				if(isset($_POST[$item.'_title'])&&!empty($_POST[$item.'_title'])) update_post_meta($post_id,$item.'_title',sanitize_text_field($_POST[$item.'_title'])); else delete_post_meta($post_id,$item.'_title');
				if(isset($_POST[$item.'_link'])&&!empty($_POST[$item.'_link'])) update_post_meta($post_id,$item.'_link',sanitize_text_field($_POST[$item.'_link'])); else delete_post_meta($post_id,$item.'_link');
			}
			
		} else if(strpos(get_page_template_slug($post->ID),"program-finder.php")!==false){
			
			if(isset($_POST['program_finder_maxpage'])&&!empty($_POST['program_finder_maxpage'])) update_post_meta($post_id,'program_finder_maxpage',sanitize_text_field($_POST['program_finder_maxpage'])); else delete_post_meta($post_id,'program_finder_maxpage');
			if(isset($_POST['program_finder_locked'])&&!empty($_POST['program_finder_locked'])) update_post_meta($post_id,'program_finder_locked',sanitize_text_field($_POST['program_finder_locked'])); else delete_post_meta($post_id,'program_finder_locked');
			if(isset($_POST['program_finder_set_career'])&&!empty($_POST['program_finder_set_career'])) update_post_meta($post_id,'program_finder_set_career',sanitize_text_field($_POST['program_finder_set_career'])); else delete_post_meta($post_id,'program_finder_set_career');
			if(isset($_POST['program_finder_set_degree'])&&!empty($_POST['program_finder_set_degree'])) update_post_meta($post_id,'program_finder_set_degree',sanitize_text_field($_POST['program_finder_set_degree'])); else delete_post_meta($post_id,'program_finder_set_degree');
			if(isset($_POST['program_finder_set_location'])&&!empty($_POST['program_finder_set_location'])) update_post_meta($post_id,'program_finder_set_location',sanitize_text_field($_POST['program_finder_set_location'])); else delete_post_meta($post_id,'program_finder_set_location');
			if(isset($_POST['program_finder_set_interest'])&&!empty($_POST['program_finder_set_interest'])) update_post_meta($post_id,'program_finder_set_interest',sanitize_text_field($_POST['program_finder_set_interest'])); else delete_post_meta($post_id,'program_finder_set_interest');
			
		} else if(strpos(get_page_template_slug($post->ID),"program-page.php")!==false){
			
			if(isset($_POST['program_video_id'])&&!empty($_POST['program_video_id'])) update_post_meta($post_id,'program_video_id',sanitize_text_field($_POST['program_video_id'])); else delete_post_meta($post_id,'program_video_id');
			if(isset($_POST['program_video_src'])&&!empty($_POST['program_video_src'])) update_post_meta($post_id,'program_video_src',sanitize_text_field($_POST['program_video_src'])); else delete_post_meta($post_id,'program_video_src');
			if(isset($_POST['program_video_poster'])&&!empty($_POST['program_video_poster'])) update_post_meta($post_id,'program_video_poster',sanitize_text_field($_POST['program_video_poster'])); else delete_post_meta($post_id,'program_video_poster');
			if(isset($_POST['program_video_captions'])&&!empty($_POST['program_video_captions'])) update_post_meta($post_id,'program_video_captions',sanitize_text_field($_POST['program_video_captions'])); else delete_post_meta($post_id,'program_video_captions');
			
			if(isset($_POST['program_guide'])&&!empty($_POST['program_guide'])) update_post_meta($post_id,'program_guide',sanitize_text_field($_POST['program_guide'])); else delete_post_meta($post_id,'program_guide');
			
			if(isset($_POST['program_is_this_1'])&&!empty($_POST['program_is_this_1'])) update_post_meta($post_id,'program_is_this_1',sanitize_text_field($_POST['program_is_this_1'])); else delete_post_meta($post_id,'program_is_this_1');
			if(isset($_POST['program_is_this_2'])&&!empty($_POST['program_is_this_2'])) update_post_meta($post_id,'program_is_this_2',sanitize_text_field($_POST['program_is_this_2'])); else delete_post_meta($post_id,'program_is_this_2');
			if(isset($_POST['program_is_this_3'])&&!empty($_POST['program_is_this_3'])) update_post_meta($post_id,'program_is_this_3',sanitize_text_field($_POST['program_is_this_3'])); else delete_post_meta($post_id,'program_is_this_3');
			//if(isset($_POST['program_explore_link'])&&!empty($_POST['program_explore_link'])) update_post_meta($post_id,'program_explore_link',sanitize_text_field($_POST['program_explore_link'])); else delete_post_meta($post_id,'program_explore_link');
			if(isset($_POST['program_career_coach_slug'])&&!empty($_POST['program_career_coach_slug'])) update_post_meta($post_id,'program_career_coach_slug',sanitize_text_field($_POST['program_career_coach_slug'])); else delete_post_meta($post_id,'program_career_coach_slug');
			if(isset($_POST['program_career_coach'])&&!empty($_POST['program_career_coach'])) update_post_meta($post_id,'program_career_coach',sanitize_text_field($_POST['program_career_coach'])); else delete_post_meta($post_id,'program_career_coach');
			
		} else if(strpos(get_page_template_slug($post->ID),"events-page.php")!==false){
			
			if(isset($_POST['events_per_page'])&&!empty($_POST['events_per_page'])) update_post_meta($post_id,'events_per_page',sanitize_text_field($_POST['events_per_page'])); else delete_post_meta($post_id,'events_per_page');
			if(isset($_POST['events_page_cat'])&&!empty($_POST['events_page_cat'])) update_post_meta($post_id,'events_page_cat',sanitize_text_field($_POST['events_page_cat'])); else delete_post_meta($post_id,'events_page_cat');
			
		} else if(strpos(get_page_template_slug($post->ID),"aggregate-page.php")!==false){
			
			if(isset($_POST['quick_links_ul'])&&!empty($_POST['quick_links_ul'])) update_post_meta($post_id,'quick_links_ul',$_POST['quick_links_ul']); else delete_post_meta($post_id,'quick_links_ul');
			if(isset($_POST['trending_links_db'])&&!empty($_POST['trending_links_db'])) update_post_meta($post_id,'trending_links_db',$_POST['trending_links_db']); else delete_post_meta($post_id,'trending_links_db');
			if(isset($_POST['hide_mytcc'])&&!empty($_POST['hide_mytcc'])) update_post_meta($post_id,'hide_mytcc',$_POST['hide_mytcc']); else delete_post_meta($post_id,'hide_mytcc');
			
			for($s=1;$s<=5;$s++){
				$item = "slide_".$s;
				if(isset($_POST[$item.'_show'])&&!empty($_POST[$item.'_show'])) update_post_meta($post_id,$item.'_show',sanitize_text_field($_POST[$item.'_show'])); else delete_post_meta($post_id,$item.'_show');
				if(isset($_POST[$item.'_title'])&&!empty($_POST[$item.'_title'])) update_post_meta($post_id,$item.'_title',sanitize_text_field($_POST[$item.'_title'])); else delete_post_meta($post_id,$item.'_title');
				if(isset($_POST[$item.'_copy'])&&!empty($_POST[$item.'_copy'])) update_post_meta($post_id,$item.'_copy',sanitize_text_field($_POST[$item.'_copy'])); else delete_post_meta($post_id,$item.'_copy');
				if(isset($_POST[$item.'_cta'])&&!empty($_POST[$item.'_cta'])) update_post_meta($post_id,$item.'_cta',sanitize_text_field($_POST[$item.'_cta'])); else delete_post_meta($post_id,$item.'_cta');
				if(isset($_POST[$item.'_link'])&&!empty($_POST[$item.'_link'])) update_post_meta($post_id,$item.'_link',sanitize_text_field($_POST[$item.'_link'])); else delete_post_meta($post_id,$item.'_link');
				if(isset($_POST[$item.'_align'])&&!empty($_POST[$item.'_align'])) update_post_meta($post_id,$item.'_align',sanitize_text_field($_POST[$item.'_align'])); else delete_post_meta($post_id,$item.'_align');
				if(isset($_POST[$item.'_img'])&&!empty($_POST[$item.'_img'])) update_post_meta($post_id,$item.'_img',sanitize_text_field($_POST[$item.'_img'])); else delete_post_meta($post_id,$item.'_img');
				for($si=1;$si<11;$si++){
					if(isset($_POST[$item.'_img_'.$si])&&!empty($_POST[$item.'_img_'.$si])) update_post_meta($post_id,$item.'_img_'.$si,sanitize_text_field($_POST[$item.'_img_'.$si])); else delete_post_meta($post_id,$item.'_img_'.$si);
				}
			}
			
		} else if(strpos(get_page_template_slug($post->ID),"landing-slider-page.php")!==false){
			
			if(isset($_POST['quick_links_ul'])&&!empty($_POST['quick_links_ul'])) update_post_meta($post_id,'quick_links_ul',$_POST['quick_links_ul']); else delete_post_meta($post_id,'quick_links_ul');
			if(isset($_POST['trending_links_db'])&&!empty($_POST['trending_links_db'])) update_post_meta($post_id,'trending_links_db',$_POST['trending_links_db']); else delete_post_meta($post_id,'trending_links_db');
			if(isset($_POST['hide_mytcc'])&&!empty($_POST['hide_mytcc'])) update_post_meta($post_id,'hide_mytcc',$_POST['hide_mytcc']); else delete_post_meta($post_id,'hide_mytcc');
			
			for($s=1;$s<=5;$s++){
				$item = "slide_".$s;
				if(isset($_POST[$item.'_show'])&&!empty($_POST[$item.'_show'])) update_post_meta($post_id,$item.'_show',sanitize_text_field($_POST[$item.'_show'])); else delete_post_meta($post_id,$item.'_show');
				if(isset($_POST[$item.'_title'])&&!empty($_POST[$item.'_title'])) update_post_meta($post_id,$item.'_title',sanitize_text_field($_POST[$item.'_title'])); else delete_post_meta($post_id,$item.'_title');
				if(isset($_POST[$item.'_copy'])&&!empty($_POST[$item.'_copy'])) update_post_meta($post_id,$item.'_copy',sanitize_text_field($_POST[$item.'_copy'])); else delete_post_meta($post_id,$item.'_copy');
				if(isset($_POST[$item.'_cta'])&&!empty($_POST[$item.'_cta'])) update_post_meta($post_id,$item.'_cta',sanitize_text_field($_POST[$item.'_cta'])); else delete_post_meta($post_id,$item.'_cta');
				if(isset($_POST[$item.'_link'])&&!empty($_POST[$item.'_link'])) update_post_meta($post_id,$item.'_link',sanitize_text_field($_POST[$item.'_link'])); else delete_post_meta($post_id,$item.'_link');
				if(isset($_POST[$item.'_align'])&&!empty($_POST[$item.'_align'])) update_post_meta($post_id,$item.'_align',sanitize_text_field($_POST[$item.'_align'])); else delete_post_meta($post_id,$item.'_align');
				if(isset($_POST[$item.'_img'])&&!empty($_POST[$item.'_img'])) update_post_meta($post_id,$item.'_img',sanitize_text_field($_POST[$item.'_img'])); else delete_post_meta($post_id,$item.'_img');
				for($si=1;$si<11;$si++){
					if(isset($_POST[$item.'_img_'.$si])&&!empty($_POST[$item.'_img_'.$si])) update_post_meta($post_id,$item.'_img_'.$si,sanitize_text_field($_POST[$item.'_img_'.$si])); else delete_post_meta($post_id,$item.'_img_'.$si);
				}
			}
			
		} else if(strpos(get_page_template_slug($post->ID),"campus-page.php")!==false){
			
			if(isset($_POST['campus_video_id'])&&!empty($_POST['campus_video_id'])) update_post_meta($post_id,'campus_video_id',sanitize_text_field($_POST['campus_video_id'])); else delete_post_meta($post_id,'campus_video_id');
			if(isset($_POST['campus_video_src'])&&!empty($_POST['campus_video_src'])) update_post_meta($post_id,'campus_video_src',sanitize_text_field($_POST['campus_video_src'])); else delete_post_meta($post_id,'campus_video_src');
			if(isset($_POST['campus_video_poster'])&&!empty($_POST['campus_video_poster'])) update_post_meta($post_id,'campus_video_poster',sanitize_text_field($_POST['campus_video_poster'])); else delete_post_meta($post_id,'campus_video_poster');
			if(isset($_POST['campus_video_captions'])&&!empty($_POST['campus_video_captions'])) update_post_meta($post_id,'campus_video_captions',sanitize_text_field($_POST['campus_video_captions'])); else delete_post_meta($post_id,'campus_video_captions');
			
		}
			
		if(isset($_POST['page_redirect'])&&!empty($_POST['page_redirect'])) update_post_meta($post_id,'page_redirect',sanitize_text_field($_POST['page_redirect'])); else delete_post_meta($post_id,'page_redirect');
		if(isset($_POST['page_shy'])&&!empty($_POST['page_shy'])) update_post_meta($post_id,'page_shy',sanitize_text_field($_POST['page_shy'])); else delete_post_meta($post_id,'page_shy');
		if(isset($_POST['page_keywords'])&&!empty($_POST['page_keywords'])) update_post_meta($post_id,'page_keywords',sanitize_text_field($_POST['page_keywords'])); else delete_post_meta($post_id,'page_keywords');
		
		if(isset($_POST['page_sidebar'])&&!empty($_POST['page_sidebar'])) update_post_meta($post_id,'page_sidebar',sanitize_text_field($_POST['page_sidebar'])); else delete_post_meta($post_id,'page_sidebar');
		if(isset($_POST['page_sidebar_not_sticky'])&&!empty($_POST['page_sidebar_not_sticky'])) update_post_meta($post_id,'page_sidebar_not_sticky',sanitize_text_field($_POST['page_sidebar_not_sticky'])); else delete_post_meta($post_id,'page_sidebar_not_sticky');
		if(isset($_POST['custom_sidebar_title'])&&!empty($_POST['custom_sidebar_title'])) update_post_meta($post_id,'custom_sidebar_title',$_POST['custom_sidebar_title']); else delete_post_meta($post_id,'custom_sidebar_title');
		if(isset($_POST['custom_sidebar_copy'])&&!empty($_POST['custom_sidebar_copy'])) update_post_meta($post_id,'custom_sidebar_copy',$_POST['custom_sidebar_copy']); else delete_post_meta($post_id,'custom_sidebar_copy');
		if(isset($_POST['custom_sidebar_cta'])&&!empty($_POST['custom_sidebar_cta'])) update_post_meta($post_id,'custom_sidebar_cta',sanitize_text_field($_POST['custom_sidebar_cta'])); else delete_post_meta($post_id,'custom_sidebar_cta');
		if(isset($_POST['custom_sidebar_link'])&&!empty($_POST['custom_sidebar_link'])) update_post_meta($post_id,'custom_sidebar_link',sanitize_text_field($_POST['custom_sidebar_link'])); else delete_post_meta($post_id,'custom_sidebar_link');
		if(isset($_POST['custom_sidebar_btn_class'])&&!empty($_POST['custom_sidebar_btn_class'])) update_post_meta($post_id,'custom_sidebar_btn_class',sanitize_text_field($_POST['custom_sidebar_btn_class'])); else delete_post_meta($post_id,'custom_sidebar_btn_class');
		
		if(isset($_POST['events_mod_enable'])&&!empty($_POST['events_mod_enable'])) update_post_meta($post_id,'events_mod_enable',sanitize_text_field($_POST['events_mod_enable'])); else delete_post_meta($post_id,'events_mod_enable');
		if(isset($_POST['events_mod_count'])&&!empty($_POST['events_mod_count'])) update_post_meta($post_id,'events_mod_count',sanitize_text_field($_POST['events_mod_count'])); else delete_post_meta($post_id,'events_mod_count');
		if(isset($_POST['events_mod_cat'])&&!empty($_POST['events_mod_cat'])) update_post_meta($post_id,'events_mod_cat',sanitize_text_field($_POST['events_mod_cat'])); else delete_post_meta($post_id,'events_mod_cat');
		if(isset($_POST['events_mod_title'])&&!empty($_POST['events_mod_title'])) update_post_meta($post_id,'events_mod_title',sanitize_text_field($_POST['events_mod_title'])); else delete_post_meta($post_id,'events_mod_title');
		if(isset($_POST['events_mod_link'])&&!empty($_POST['events_mod_link'])) update_post_meta($post_id,'events_mod_link',sanitize_text_field($_POST['events_mod_link'])); else delete_post_meta($post_id,'events_mod_link');
		
		if(isset($_POST['map_mod_enable'])&&!empty($_POST['map_mod_enable'])) update_post_meta($post_id,'map_mod_enable',sanitize_text_field($_POST['map_mod_enable'])); else delete_post_meta($post_id,'map_mod_enable');
		if(isset($_POST['map_mod_title'])&&!empty($_POST['map_mod_title'])) update_post_meta($post_id,'map_mod_title',sanitize_text_field($_POST['map_mod_title'])); else delete_post_meta($post_id,'map_mod_title');
		if(isset($_POST['map_mod_img'])&&!empty($_POST['map_mod_img'])) update_post_meta($post_id,'map_mod_img',sanitize_text_field($_POST['map_mod_img'])); else delete_post_meta($post_id,'map_mod_img');
		if(isset($_POST['map_mod_img_width'])&&!empty($_POST['map_mod_img_width'])) update_post_meta($post_id,'map_mod_img_width',sanitize_text_field($_POST['map_mod_img_width'])); else delete_post_meta($post_id,'map_mod_img_width');
		if(isset($_POST['map_mod_img_height'])&&!empty($_POST['map_mod_img_height'])) update_post_meta($post_id,'map_mod_img_height',sanitize_text_field($_POST['map_mod_img_height'])); else delete_post_meta($post_id,'map_mod_img_height');
		if(isset($_POST['map_mod_img_ratio'])&&!empty($_POST['map_mod_img_ratio'])) update_post_meta($post_id,'map_mod_img_ratio',sanitize_text_field($_POST['map_mod_img_ratio'])); else delete_post_meta($post_id,'map_mod_img_ratio');
		if(isset($_POST['map_mod_coords_1'])&&!empty($_POST['map_mod_coords_1'])) update_post_meta($post_id,'map_mod_coords_1',sanitize_text_field($_POST['map_mod_coords_1'])); else delete_post_meta($post_id,'map_mod_coords_1');
		if(isset($_POST['map_mod_coords_2'])&&!empty($_POST['map_mod_coords_2'])) update_post_meta($post_id,'map_mod_coords_2',sanitize_text_field($_POST['map_mod_coords_2'])); else delete_post_meta($post_id,'map_mod_coords_2');
		if(isset($_POST['map_mod_locations'])&&!empty($_POST['map_mod_locations'])) update_post_meta($post_id,'map_mod_locations',$_POST['map_mod_locations']); else delete_post_meta($post_id,'map_mod_locations');
		
		if(isset($_POST['content_feed_mod_enable'])&&!empty($_POST['content_feed_mod_enable'])) update_post_meta($post_id,'content_feed_mod_enable',sanitize_text_field($_POST['content_feed_mod_enable'])); else delete_post_meta($post_id,'content_feed_mod_enable');
		if(isset($_POST['content_feed_mod_style'])&&!empty($_POST['content_feed_mod_style'])) update_post_meta($post_id,'content_feed_mod_style',sanitize_text_field($_POST['content_feed_mod_style'])); else delete_post_meta($post_id,'content_feed_mod_style');
		if(isset($_POST['content_feed_mod_tags'])&&!empty($_POST['content_feed_mod_tags'])) update_post_meta($post_id,'content_feed_mod_tags',sanitize_text_field($_POST['content_feed_mod_tags'])); else delete_post_meta($post_id,'content_feed_mod_tags');
		if(isset($_POST['content_feed_mod_match'])&&!empty($_POST['content_feed_mod_match'])) update_post_meta($post_id,'content_feed_mod_match',sanitize_text_field($_POST['content_feed_mod_match'])); else delete_post_meta($post_id,'content_feed_mod_match');
		
	}
	
	if(function_exists(wp_cache_clear_cache)) wp_cache_clear_cache();
}
add_action('save_post','tccedu_save_post');


function tccedu_generate_trending_links(){
	
	date_default_timezone_set('America/New_York');
	
	$db = new mysqli('localhost','tcc-edu_trending','7ec#4i3G','tcc-edu_trending');
	if($db->connect_errno>0) die('Unable to connect to database [' . $db->connect_error . ']');

	$debug_str = "";

	foreach(get_posts(array('showposts'=>-1,'post_type'=>'page')) as $post){
		if(strpos(get_page_template_slug($post->ID),"aggregate-page.php")!==false){
			$table = get_post_meta($post->ID,'trending_links_db',true);
			if(!$table) $table = sanitize_title($post->post_title);
			
			//create table
			if(!mysqli_query($db,"SELECT 1 FROM `trending_".$table."` LIMIT 1;")) tccedu_create_trending_table($db,$table);
			
			//clear old entries
			//mysqli_query($db,"DELETE FROM `trending_".$table."` WHERE `date` <= NOW() - INTERVAL 1 DAY");
			
			//CHECK if we have at least 10 items before deleting?
			//OR gather items by -1 day first, if not enough items, gather items by -7 days, delete after 7 days?
			
			//actually instead just extend records back 7 days vs 1 day
			mysqli_query($db,"DELETE FROM `trending_".$table."` WHERE `date` <= NOW() - INTERVAL 7 DAY");
			
			//gather results and build new array
			$trending_items = array();
			$result = mysqli_query($db,"select * from `trending_".$table."`");
			while($row = mysqli_fetch_array($result)){
				$item_exists = false;
				foreach($trending_items as $item_index => $item){
					if($item['href']==$row['href']&&$item['title']==$row['title']){
						$trending_items[$item_index]['count'] = $item['count']+1;
						$item_exists = true;
					}
				}
				if(!$item_exists) $trending_items[] = array('href'=>$row['href'],'title'=>$row['title'],'count'=>1);
			}
			
			//sort array by clicks, limit to 10
			usort($trending_items,function($a,$b){ return $b['count']-$a['count']; });
			$trending_items = array_slice($trending_items,0,10);
			
			//format array and save as page meta
			if(count($trending_items)){
				$trending_links_ul = "<ul>";
				foreach($trending_items as $item) $trending_links_ul .= "<li><a href=\"".$item['href']."\">".$item['title']."</a></li>";
				$trending_links_ul .= "</ul>";
			}
			
			if($trending_links_ul) update_post_meta($post->ID,'trending_links_ul',$trending_links_ul); else delete_post_meta($post->ID,'trending_links_ul');
		}
	}
	mysqli_close($db);
	echo "{\"success\":true}";
	die();
}
add_action('trending_links_event','tccedu_generate_trending_links');
if(isset($_POST['force_trending_links'])) tccedu_generate_trending_links();
if(isset($_POST['trending_link'])) tccedu_register_trending_link();

function tccedu_register_trending_link(){
	
	$post = get_post(preg_replace('/[^0-9\,]/','',$_POST['post_id']));
	if(!$post) return;
	
	$href = preg_replace('/[^A-Za-z0-9\-\.\_\~\:\/\?\#\[\]\@\!\$\&\'\(\)\*\+\,\;\=]/','',$_POST['href']);
	$title = $_POST['title'];
	$table = get_post_meta($post->ID,'trending_links_db',true);
	if(!$table) $table = sanitize_title($post->post_title);
	
	//$valid = true;
	$valid = strpos($post->post_content,$_POST['title'])!==false&&strpos($post->post_content,"href=\"".$_POST['href']."\"")!==false?true:false;
	
	if($valid&&!empty($table)&&!empty($href)&&!empty($title)){
		$db = new mysqli('localhost','tcc-edu_trending','7ec#4i3G','tcc-edu_trending');
		if($db->connect_errno>0) die('Unable to connect to database [' . $db->connect_error . ']');
		if(!mysqli_query($db,"SELECT 1 FROM `trending_".$table."` LIMIT 1;")) tccedu_create_trending_table($db,$table);
		mysqli_query($db,"INSERT INTO `trending_".$table."` (href,title) VALUES ('".$href."','".$title."')");
		mysqli_close($db);
		echo "{\"success\":true}";
	} else {
		echo "{\"success\":false}";
	}
	
	die();
}

function tccedu_create_trending_table($db,$table){
	if(!$table) return false;
	if(mysqli_query($db,"CREATE TABLE `trending_".$table."` SELECT * FROM `_trending_proto`")&&
		mysqli_query($db,"ALTER TABLE `trending_".$table."` CHANGE `date` `date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP")&&
		mysqli_query($db,"ALTER TABLE `trending_".$table."` ADD PRIMARY KEY( `date`)")) return true;
	
}

function tccedu_generate_tccvals(){
	include('admin/generate-tcc-vals.php');
}


function tccedu_ajax_att_meta() {
	$res = array();
	if(isset($_POST['att'])){
		$att_post = get_post($_POST['att']);
		$res = array(
			'url'=>wp_get_attachment_url($att_post->ID),
			'id'=>$att_post->ID,
			'title'=>$att_post->post_title,
			'desc'=>$att_post->post_content
		);
		if(strpos($att_post->post_mime_type,"video")!==false){
			$att_meta = unserialize(get_post_meta($att_post->ID)['_wp_attachment_metadata'][0]);
			$res['width'] = $att_meta['width'];
			$res['height'] = $att_meta['height'];
			$res['length'] = $att_meta['length_formatted'];
		}
	}
	wp_die(JSON_encode($res));
}
add_action('wp_ajax_get_att_meta','tccedu_ajax_att_meta');



if(isset($_POST['pardot_api'])) tccedu_pardot_api();

function tccedu_pardot_api(){
	
	if(isset($_POST['mode'])){
		global $res;
		include('pardot_sf.php');
		pardotLogin();
		if($_POST['mode']==="upsert"){
			pardotUpsert($_POST['email'],$_POST['fname'],$_POST['lname'],$_POST['phone']);
		} elseif($_POST['mode']==="request"){
			pardotUpsert($_POST['email'],$_POST['fname'],$_POST['lname'],$_POST['phone'],'5567',null,null,null,null);
			if($res['success']==true){
				if(isset($_POST['comments'])&&!empty($_POST['comments'])){
					//debug $prospect = $res['prospect']; //debug
					$res = array();
					pardotEmail("enroll@tcc.edu",null,$_POST['email'],$_POST['fname']." ".$_POST['lname'],"TCCEnroll.com Request for Information",$_POST['comments']);
					//debug pardotEmail("developers5@madebysway.com",null,$_POST['email'],$_POST['fname']." ".$_POST['lname'],"TCCEnroll.com Request for Information",$_POST['comments']); //debug
					//debug $res['prospect'] = $prospect; //debug
					//debug $res['dl'] = $_POST['dl']; //debug
					//debug $res['values'] = $values; //debug
				}
				if($res['success']==true){
					$res = array();
					pardotEmail($_POST['email'],'8938');
				}
			}
		} elseif($_POST['mode']==="guide"){
			
			$guide = get_posts(array(
				'showposts' => 1,
				'post_type' => 'program-guide',
				'post_status' => 'publish',
				'meta_query' => array(array('key'=>'guide_id', 'value'=>preg_replace('/[^a-zA-Z0-9\-]/','',$_POST['guide_id'])))
			));
			
			if($guide){
				$guide_meta = get_post_meta($guide[0]->ID);
				
				$specific_interest = $guide_meta['guide_specific_interest_alt'][0]?$guide_meta['guide_specific_interest_alt'][0]:null;
				if(!$specific_interest&&$guide_meta['guide_specific_interest'][0]){
					foreach(get_posts(array(
						'showposts'=>1, 'post_type'=>'program', 'post_status'=>'publish',
						'meta_query'=>array(array('key'=>'program_id', 'value'=>$guide_meta['guide_specific_interest'][0]))
					)) as $program){
						$specific_interest = $program->post_title;
					}
				}
				$program_interest = $guide_meta['guide_program_interest_alt'][0]?$guide_meta['guide_program_interest_alt'][0]:null;
				if(!$program_interest&&$guide_meta['guide_program_interest'][0]){
					foreach(json_decode(get_option('career_options_list')) as $career){
						if($career->id==$guide_meta['guide_program_interest'][0]) $program_interest = $career->name;
					}
				}
				
				$values = array(//["13966","Graphic Design","Arts & Humanities",null,null];
					$guide_meta['guide_email_template'][0],
					$specific_interest,
					$program_interest,
					$guide_meta['guide_transfer_interest'][0]?$guide_meta['guide_transfer_interest'][0]:null,
					$guide_meta['guide_military_interest'][0]?$guide_meta['guide_military_interest'][0]:null
				);
				
				if(isset($values)) pardotUpsert($_POST['email'],$_POST['fname'],$_POST['lname'],$_POST['phone'],null,$values[1],$values[2],$values[3],$values[4]);
				if(isset($res['success'])&&$res['success']==true){
					//debug $prospect = $res['prospect']; //debug
					$res = array();
					pardotEmail($_POST['email'],$values[0]);
					//debug $res['prospect'] = $prospect; //debug
					//debug $res['dl'] = $_POST['dl']; //debug
					//debug $res['values'] = $values; //debug
					if(isset($res['success'])&&$res['success']=="true"){
						$res['val'] = get_option('sf_pdf_prefix').$guide_meta['guide_sf_pdf'][0];
					}
				}
			} else {
				$res = array('success'=>false);
			}
		} elseif($_POST['mode']==="read"){
			pardotRead($_POST['email']);
		}
		echo json_encode($res);
	}
	
	die();
}


if(isset($_POST['salesforce_api'])) tccedu_salesforce_api();

function tccedu_salesforce_api(){
	
	if(isset($_POST['mode'])){
		global $res;
		include('pardot_sf.php');
		$res = array();
		
		if($_POST['mode']=="embed"){
			sfEmbed($_POST['term']);
		} else if($_POST['mode']=="search"){
			sfQuery($_POST['term']);
		}
		
		echo json_encode($res);
	}
	
	die();
}




function tccedu_get_events_in_category($cat="",$limit,$offset=0){
	
	if(!isset($limit)||!is_int($limit)) $limit = 99;
	
	global $wpdb;
	$all_events = $wpdb->get_results("
		SELECT wposts.*
		FROM $wpdb->posts wposts, $wpdb->postmeta wpostmeta
		WHERE wposts.ID = wpostmeta.post_id
		AND wpostmeta.meta_key = 'event_date'
		AND wposts.post_status = 'publish'
		AND wposts.post_type = 'event'
		AND STR_TO_DATE(wpostmeta.meta_value, '%m/%d/%Y') > NOW() - INTERVAL 1 DAY
		ORDER BY STR_TO_DATE(wpostmeta.meta_value, '%m/%d/%Y') ASC",OBJECT);
	
	$cat_events = array();
	if(!$cat){
		$cat_events = $all_events;
	} else {
		foreach($all_events as $event){
			$event_cats = wp_get_object_terms($event->ID,'event_category',array('fields'=>'slugs'));
			if(in_array($cat,$event_cats)) $cat_events[] = $event;
		}
	}
	$events_total = count($cat_events);
	
	$cat_events = array_slice(array_slice($cat_events,$offset),0,$limit);
	
	$results = array();
	foreach($cat_events as $event_post){
		$event_meta = get_post_meta($event_post->ID);
		$event = array(
			'link' => get_relative_permalink($event_post->ID),
			'type' => $event_post->post_type,
			'title' => $event_post->post_title,
			'content' => wpautop($event_post->post_content)
		);
		if($event_meta['event_date'][0]){
			$event_date = date_create($event_meta['event_date'][0]);
			$event['rdate'] = $event_date->format('l F jS');
			$event['day'] = $event_date->format('d');
			$event['month'] = $event_date->format('M');
		}
		if($event_meta['event_campus'][0]||$event_meta['event_campus_other'][0]){
			$event['campus'] = $event_meta['event_campus_other'][0]?$event_meta['event_campus_other'][0]:tccedu_get_campus($event_meta['event_campus'][0])['title'];
		}
		if($event_meta['event_campus'][0]){
			$event_campus = tccedu_get_campus($event_meta['event_campus'][0]);
			if($event_campus['link']) $event['campus_link'] = $event_campus['link'];
		}
		if($event_meta['event_time'][0]) $event['time'] = $event_meta['event_time'][0];
		if($event_meta['event_location'][0]) $event['location'] = $event_meta['event_location'][0];
		if($event_meta['event_address'][0]) $event['address'] = $event_meta['event_address'][0];
		
		$results[] = $event;
	}
	
	return array('events'=>$results,'total'=>$events_total,'per_page'=>$limit,'page'=>max(1,floor($offset/$limit)+1),'max_pages'=>ceil($events_total/$limit));
}



add_action('rest_api_init',function(){
	register_rest_route('rest/v1','/search/',array(
		'methods' => 'POST',
		'callback' => 'tccedu_api_search'
	));
	register_rest_route('rest/v1','/event_search/',array(
		'methods' => 'POST',
		'callback' => 'tccedu_api_event_search'
	));
	register_rest_route('rest/v1','/contentfeed/',array(
		'methods' => 'POST',
		'callback' => 'tccedu_api_content_feed_posts'
	));
	register_rest_route('rest/v1','/feeditem/',array(
		'methods' => 'POST',
		'callback' => 'tccedu_api_feed_item'
	));
});


function tccedu_api_search($data){
	
	$s = $data['s']?$data['s']:"";
	
	$search_args = array(
		's' => $s,
		'post_type' => array('page'),
		'posts_per_page' => $data['per_page']?$data['per_page']:get_option('posts_per_page'),
		'paged' => $data['page'],
		'post_status' => 'publish'
	);
	$search_query = new WP_Query();
	$search_query->parse_query($search_args);
	relevanssi_do_query($search_query);
	
	if(!$search_query->found_posts) return rest_ensure_response(false);
	
	$raw_search_term = strip_shortcodes(wp_strip_all_tags(urldecode($s)));
	
	$results = array();
	foreach($search_query->posts as $search_post){
		$results[] = array( 'link' => get_relative_permalink($search_post->ID), 'type' => $search_post->post_type, 'title' => $search_post->post_title, 'excerpt' => tccedu_excerpt($search_post,$raw_search_term) );
	}
	
	return rest_ensure_response(array(
		'posts' => $results,
		'total' => $search_query->found_posts,
		'per_page' => $search_query->query_vars['posts_per_page'],
		'page' => $search_query->query_vars['paged'],
		'max_pages' => $search_query->max_num_pages
	));
	
}

function tccedu_api_event_search($data){

	$limit = isset($data['per_page'])?$data['per_page']:3;
	$offset = $data['page']?(($data['page']-1)*$limit):0;
	
	$event_query = tccedu_get_events_in_category($data['category'],$limit,$offset);
	
	return rest_ensure_response(array(
		'posts' => $event_query['events'],
		'total' => $event_query['total'],
		'per_page' => $event_query['per_page'],
		'page' => $event_query['page'],
		'max_pages' => $event_query['max_pages']
	));
}

function tccedu_api_content_feed_posts($data){
	return rest_ensure_response(tccedu_get_content_feed_items($data));
}
function tccedu_api_feed_item($data){
	$feeditem_post = get_posts(array(
		'showposts' => 1,
		'post_type' => 'feeditem',
		'post_status' => 'publish',
		'meta_query' => array( array( 'key' => 'feeditem_slug', 'value' => $data['slug'] ) )
	))[0];
	
	if(!$feeditem_post){
		$feeditem_post = get_posts(array(
			'showposts' => 1,
			'post_type' => 'feeditem',
			'post_status' => 'publish',
			'name' => $data['name']
		))[0];
	}
	
	if(!$feeditem_post) return rest_ensure_response(false);
	
	$feeditem = array( 'post' => $feeditem_post, 'meta' => get_post_meta($feeditem_post->ID) );
	return rest_ensure_response(tccedu_parse_feed_item($feeditem));
}

function tccedu_get_content_feed_items($data){
	$feedtags = explode(',',preg_replace('/[^A-Za-z0-9\-\,]/','',$data['tag']));
	$exclude = explode(',',preg_replace('/[^0-9\,]/','',$data['exclude']));
	$limit = $data['limit']?$data['limit']:3;
	$weighted_items = array();
	$normal_items = array();
	
	//try a more strict AND relation criteria if we have multiple tags, otherwise OR
	if($data['match']=="all"&&count($feedtags)>1){
		$tax_query = array( 'relation'=>'AND' );
		foreach($feedtags as $feedtag){
			$tax_query[] = array(
				'taxonomy' => 'post_tag',
				'field' => 'slug',
				'terms' => array($feedtag)
			);
		}
	} else {
		$tax_query = array(
			array(
				'taxonomy' => 'post_tag',
				'field' => 'slug',
				'terms' => $feedtags
			)
		);
	}
	$feeditems = get_posts(array(
		'showposts' => -1,
		'post_type' => 'feeditem',
		'post_status' => 'publish',
		'tax_query' => $tax_query,
		'exclude' => $exclude
	));
	//split first result set into weighted and normal sets, ignore item weight in any later results
	foreach($feeditems as $feeditem){
		$item = array(
			'post' => $feeditem,
			'meta' => get_post_meta($feeditem->ID)
		);
		if(!$data['ignore_weight']&&$item['meta']['feeditem_weighted'][0]){
			$weighted_items[] = $item;
		} else {
			$normal_items[] = $item;
		}
	}
	//shuffle each set, recombine with weighted results first
	shuffle($normal_items);
	array_reverse($normal_items);
	shuffle($normal_items);
	shuffle($weighted_items);
	$mixed_items = array_merge($weighted_items,$normal_items);
	
	foreach($mixed_items as $item) $exclude[] = $item['post']->ID;
	
	//if not enough results and relation was AND, try OR for additional relevant results
	if(count($mixed_items)<$limit&&$data['match']=="all"&&count($feedtags)>1){
		$feeditems = get_posts(array(
			'showposts' => $limit-count($mixed_items),
			'post_type' => 'feeditem',
			'post_status' => 'publish',
			'orderby' => 'rand',
			'tax_query' => array(
				array(
					'taxonomy' => 'post_tag',
					'field' => 'slug',
					'terms' => $feedtags
				)
			),
			'exclude' => $exclude
		));
		foreach($feeditems as $feeditem){
			$mixed_items[] = array(
				'post' => $feeditem,
				'meta' => get_post_meta($feeditem->ID)
			);
			$exclude[] = $feeditem->ID;
		}
	}
	
	//if still not enough results, fill out with more random general results
	if(count($mixed_items)<$limit){
		$more_items = get_posts(array(
			'showposts' => $limit-count($mixed_items),
			'post_type' => 'feeditem',
			'post_status' => 'publish',
			'orderby' => 'rand',
			'exclude' => $exclude
		));
		foreach($more_items as $feeditem){
			$mixed_items[] = array(
				'post' => $feeditem,
				'meta' => get_post_meta($feeditem->ID)
			);
		}
	} else {
		$mixed_items = array_slice($mixed_items,0,$limit);
	}
	
	//build output objects
	foreach($mixed_items as $item){
		$parsed_items[] = tccedu_parse_feed_item($item);
	}
	
	return $parsed_items;
}

function tccedu_parse_feed_item($item){
	
	$parsed_item = array(
		'id' => $item['post']->ID,
		'slug' => $item['meta']['feeditem_slug'][0]?$item['meta']['feeditem_slug'][0]:$item['post']->post_name,
		'title' => $item['meta']['feeditem_title'][0]?$item['meta']['feeditem_title'][0]:$item['post']->post_title,
		'copy' => $item['meta']['feeditem_copy'][0],
		'type' => $item['meta']['feeditem_type'][0]
	);
	
	if($item['meta']['feeditem_img_set'][0]){
		$parsed_item['img'] = $item['meta']['feeditem_img_set'][0];
	} else {
		$att_id = fjarrett_get_attachment_id_by_url($item['meta']['feeditem_img'][0]);		
		if($att_id){
			$parsed_item['img'] = wp_get_attachment_image_src($att_id,'medium')[0];
			$img_set = implode(",",array(
				get_relative_link(wp_get_attachment_image_src($att_id,'large')[0]),
				get_relative_link(wp_get_attachment_image_src($att_id,'medium')[0])
			));
			update_post_meta($item['post']->ID,'feeditem_img_set',$img_set);
			$parsed_item['img'] = $img_set;
		} else {
			$parsed_item['img'] = get_relative_link($item['meta']['feeditem_img'][0]);
		}
	}
	
	if($parsed_item['type']=="video"){
		$parsed_item['src'] = $item['meta']['feeditem_video_src'][0];
		if($item['meta']['feeditem_video_poster'][0]){
			$poster_id = fjarrett_get_attachment_id_by_url($item['meta']['feeditem_video_poster'][0]);		
			if($poster_id){
				$parsed_item['poster'] = wp_get_attachment_image_src($poster_id,'video-poster')[0];
			} else {
				$parsed_item['poster'] = get_relative_link($item['meta']['feeditem_video_poster'][0]);
			}
		} else {
			if(!$att_id) $att_id = fjarrett_get_attachment_id_by_url($item['meta']['feeditem_img'][0]);
			if($att_id){
				$parsed_item['poster'] = wp_get_attachment_image_src($att_id,'video-poster')[0];
			} else {
				$parsed_item['poster'] = get_relative_link($item['meta']['feeditem_img'][0]);
			}
		}
		$parsed_item['captions'] = $item['meta']['feeditem_video_captions'][0];
	} else if($parsed_item['type']=="article"){
		$parsed_item['author'] = $item['meta']['feeditem_article_author'][0];
		$parsed_item['date'] = $item['meta']['feeditem_article_date'][0];
		$parsed_item['excerpt'] = $item['meta']['feeditem_article_excerpt'][0];
		$parsed_item['cta'] = $item['meta']['feeditem_article_cta'][0];
		$parsed_item['link'] = $item['meta']['feeditem_article_link'][0];
	} else if($parsed_item['type']=="link"){
		$parsed_item['cta'] = $item['meta']['feeditem_link_cta'][0];
		$parsed_item['link'] = $item['meta']['feeditem_link_link'][0];
	}
	
	$itemtags = array();
	foreach(get_the_tags($parsed_item['id']) as $tag) $itemtags[] = $tag->slug;
	
	$parsed_item['tags'] = implode(",",$itemtags);
	$parsed_item['weighted'] = $item['meta']['feeditem_weighted'][0];
	
	return $parsed_item;
}


/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
/*function tccedu_widgets_init() {

	register_sidebar(
		array(
			'name'          => __( 'Footer', 'tccedu' ),
			'id'            => 'sidebar-1',
			'description'   => __( 'Add widgets here to appear in your footer.', 'tccedu' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);

}
add_action( 'widgets_init', 'tccedu_widgets_init' );
*/
/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width Content width.
 */
function tccedu_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'tccedu_content_width', 640 );
}
add_action( 'after_setup_theme', 'tccedu_content_width', 0 );

/**
 * Enqueue scripts and styles.
 */
function tccedu_scripts() {
	
	
	wp_enqueue_style( 'tccedu-fonts', 'https://fonts.googleapis.com/css?family=Barlow:400,400i,600|Barlow+Semi+Condensed:500,500i,600,600i,700,700i');
	
	wp_enqueue_style( 'tccedu-style', get_stylesheet_uri(), array(), wp_get_theme()->get( 'Version' ) );
	
	wp_enqueue_style( 'tccedu-icons', get_template_directory_uri() . '/icons.css', array(), wp_get_theme()->get( 'Version' ) );
	
	wp_enqueue_script( 'jquery', get_theme_file_uri( '/js/jquery-3.2.1.min.js' ), array(), '3.2.1', true );
	wp_enqueue_script( 'jquery-history', get_theme_file_uri( '/js/jquery.history.js' ), array('jquery'), '', true );
	wp_enqueue_script( 'tcc-form', get_theme_file_uri( '/js/tcc-form.js' ), array('jquery'), '20190619', true );
	wp_enqueue_script( 'tcc-vals', get_theme_file_uri( '/js/tcc-vals.js' ), array(), get_option('tccvals_generated'), true );
	wp_enqueue_script( 'tcc-main', get_theme_file_uri( '/js/tcc.min.js' ), array('tcc-form','tcc-vals'), '20190619', true );
		wp_enqueue_script( 'jquery', get_theme_file_uri( '/js/tcc-internal.js' ), array(), '', true );
	//wp_enqueue_script( 'google-maps', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyBfbBd4qRp83p64FWnwg2RtUVlNGEstrbU&callback=mapReady', array('tcc-main'), '', true );
	
		if(strpos(get_page_template_slug($post->ID),"program-page.php")!==false){
		wp_enqueue_script( 'career-coach-1', 'https://s3-us-west-2.amazonaws.com/cc-widget-v2/ccWidget.js', array('tcc-main') );
		wp_enqueue_script( 'career-coach-2', 'https://s3-us-west-2.amazonaws.com/emsi-specialist-program-files/Dynamic+Plugins/dynamicPluginV2.js', array('tcc-main') );
	}
	
	wp_localize_script( 'tcc-main', 'rest_object',
		array(
			'api_nonce' => wp_create_nonce( 'wp_rest' ),
			'api_url'   => site_url('/wp-json/rest/v1/')
		)
	);

}
add_action( 'wp_enqueue_scripts', 'tccedu_scripts' );

/**
 * Fix skip link focus in IE11.
 *
 * This does not enqueue the script because it is tiny and because it is only for IE11,
 * thus it does not warrant having an entire dedicated blocking script being loaded.
 *
 * @link https://git.io/vWdr2
 */
function tccedu_skip_link_focus_fix() {
	// The following is minified via `terser --compress --mangle -- js/skip-link-focus-fix.js`.
	?>
	<script>
	/(trident|msie)/i.test(navigator.userAgent)&&document.getElementById&&window.addEventListener&&window.addEventListener("hashchange",function(){var t,e=location.hash.substring(1);/^[A-z0-9_-]+$/.test(e)&&(t=document.getElementById(e))&&(/^(?:a|select|input|button|textarea)$/i.test(t.tagName)||(t.tabIndex=-1),t.focus())},!1);
	</script>
	<?php
}
add_action( 'wp_print_footer_scripts', 'tccedu_skip_link_focus_fix' );

function tccedu_body_classes( $classes ) {
	if ( is_singular() ) {
		$classes[] = 'singular';
	} else {
		$classes[] = 'hfeed';
	}
	return $classes;
}
add_filter('body_class','tccedu_body_classes');

function tccedu_post_classes( $classes, $class, $post_id ) {
	$classes[] = 'entry';
	return $classes;
}
add_filter('post_class','tccedu_post_classes',10,3);

function tccedu_mime_types($mime_types){
	$mime_types['vtt'] = 'text/vtt';
	return $mime_types;
}
add_filter('upload_mimes','tccedu_mime_types',1,1);

function tccedu_get_section_nav($post){
	include('template-parts/section-nav.php');
	return $html;
}

function tccedu_get_a_spot_slider(){
	include('template-parts/a-spot-slider.php');
	return $html;
}

function tccedu_get_pf_search($context){
	include('template-parts/program-finder.php');
	return $html;
}

function tccedu_get_program_finder_module(){
	$html = tccedu_get_pf_search("module");
	$whtml = "
	<div id=\"program-finder\" class=\"pf-module\">
		<div class=\"wrap\">
			<div class=\"tab\">
				<h2>Looking for a specific program?</h2><button class=\"btn btn-arrow _enter\">Click Here</button><button class=\"btn btn-close _close disabled\" disabled=\"disabled\"></button>
			</div>
			".$html."
		</div>
		<div class=\"fill\"></div>
	</div>";
	return $whtml;
}

function tccedu_get_upcoming_events_module($post){
	include('template-parts/events-module.php');
	return $html;
}

function tccedu_get_map_module($post,$map_title=""){
	include('template-parts/map-module.php');
	return $html;
}

function tccedu_get_content_feed($post){
	include('template-parts/content-feed.php');
	return $html;
}

function tccedu_get_campus($id=""){
	$res = array();
	for($c=1;$c<=15;$c++){
		$campus_id = get_option('campus_'.$c.'_id')?get_option('campus_'.$c.'_id'):sanitize_title(get_option('campus_'.$c.'_title'));
		if($campus_id==$id) return array(
			'id' => $id,
			'title' => get_option('campus_'.$c.'_title'),
			'location' => get_option('campus_'.$c.'_location'),
			'address' => get_option('campus_'.$c.'_addr_1').", ".get_option('campus_'.$c.'_addr_2'),
			'link' => get_option('campus_'.$c.'_link'),
			'phone' => get_option('campus_'.$c.'_phone'),
			'type' => get_option('campus_'.$c.'_type'),
			'map' => get_option('campus_'.$c.'_map_pdf_src')
		);
	}
	return $res;
}

function tccedu_get_degrees(){
	$degrees = [];
	foreach(json_decode(get_option('degree_options_list')) as $degree){
		$degrees[$degree->id] = $degree->name;
	}
	return $degrees;
}


function tccedu_sc_slider($attr,$content=null){
	$html = "
		<div class=\"".(isset($attr['class'])?$attr['class']." ":"")."_slider\">
			<div class=\"slides\">
				".do_shortcode($content)."
			</div>
			<div class=\"nav\"></div>
			".(isset($attr['sub'])?"<p class=\"sub\">".$attr['sub']."</p>":"")."
		</div>";
	return $html;
}

function tccedu_sc_slider_slide($attr,$content=null){
	$html = "";
	if(isset($attr['type'])&&$attr['type']=="graph"){
		$html .= "
			<div class=\"slide _sgraph\" data-bar-1=\"".$attr['bar-1']."\" data-bar-2=\"".$attr['bar-2']."\" data-val-1=\"".$attr['val-1']."\" data-val-2=\"".$attr['val-2']."\" data-val-3=\"".$attr['val-3']."\" data-logo=\"".$attr['logo']."\" data-label-1=\"".$attr['label-1']."\" data-label-2=\"".$attr['label-2']."\"></div>";
	} else {
		$html .= "
			<div class=\"slide\">
				".do_shortcode($content)."
			</div>";
	}
	return $html;
}

function tccedu_sc_accordion($attr,$content=null){
	return "
		<div class=\"_accordion\">
			<div class=\"items\">
				".do_shortcode($content)."
			</div>
		</div>";
}
function tccedu_sc_accordion_item($attr,$content=null){
	return "
		<div class=\"item\" data-id=\"".$attr['id']."\">
			<div class=\"title\" tabindex=\"0\"><div><label class=\"nonh3\">".$attr['title']."</label></div></div>
			<div class=\"copy\">
				".do_shortcode($content)."
			</div>
		</div>";
}
function tccedu_sc_accordion_sorter($attr,$content=null){
	$html = "
		<div class=\"".(isset($attr['class'])?$attr['class']." ":"")."_accordion _sorter\">
			<div class=\"head\">
				<div class=\"title no-select\"><table><tr>";
	for($v=1;$v<=5;$v++) if(isset($attr['val-'.$v])) $html .= "<td><label data-val=\"".$v."\" ".(isset($attr['val-'.$v.'-sort'])&&$attr['val-'.$v.'-sort']=="alpha"?"data-sort=\"alpha\" ":"")."class=\"_".$v."\" tabindex=\"0\">".$attr['val-'.$v]."</label></td>";
	$html .= 	"</tr></table></div>
			</div>
			<div class=\"items\">
				".do_shortcode($content)."
			</div>
		</div>";
	
	return $html;
}
function tccedu_sc_sorter_item($attr,$content=null){
	$html = "
		<div class=\"item\" data-id=\"".$attr['id']."\"";
	for($v=1;$v<=5;$v++) if(isset($attr['val-'.$v])) $html .= " data-".$v."=\"".$attr['val-'.$v]."\"";
	$html .= ">
			<div class=\"title\" tabindex=\"0\"><table><tr>";
	for($v=1;$v<=5;$v++) if(isset($attr['val-'.$v])) $html .= "<td><label>".(isset($attr['val-'.$v.'-label'])?$attr['val-'.$v.'-label']:$attr['val-'.$v])."</label></td>";
	$html .= "</tr></table></div>
			<div class=\"copy\">
				".do_shortcode($content)."
			</div>
		</div>";
	return $html;
}

function tccedu_sc_aggregate_list($attr,$content=null){
	$html = "
		<div class=\"agg-list freset\">
			".($attr['title']?"<label>".$attr['title']."</label>":"")."
			".($content?$content:"")."
		</div>";
	return $html;
}

function tccedu_sc_program_info($attr){
	if(!$attr['id']) return "no id";
	$program = get_posts(array(
		'showposts' => 1,
		'post_type' => 'program',
		'post_status' => 'publish',
		'meta_query' => array( array( 'key' => 'program_id', 'value' => $attr['id'] ) )
	));
	if(!$program) return "couldn't find program";
	
	$program_meta = get_post_meta($program[0]->ID);
	
	$program_degree_titles = [];
	$degree_types = tccedu_get_degrees();
	foreach(unserialize($program_meta['program_degree'][0]) as $degree){
		$program_degree_titles[] = $degree_types[$degree];
	}
	
	$program_location_titles = [];
	foreach(unserialize($program_meta['program_location'][0]) as $location){
		$campus = tccedu_get_campus($location);
		$program_location_titles[] = $campus['link']?"<a href=\"".$campus['link']."\">".$campus['title']."</a>":$campus['title'];
	}
	$html = "
		<table class=\"program-info\">
			<tr>
				<td class=\"info\">".
				($program_meta['program_semesters'][0]?"
					<span><h5>Semesters:</h5> 
					".$program_meta['program_semesters'][0]."</span>":"").
				($program_meta['program_credits'][0]?"
					<span><h5>Credits:</h5> 
					".$program_meta['program_credits'][0]."</span>":"").
				($program_meta['program_curriculum'][0]?"
					<span><a href=\"".$program_meta['program_curriculum'][0]."\" target=\"blank\">View Curriculum</a></span>":"")."
					<h5>Locations:</h5>
					<span>".implode(", ",$program_location_titles)."</span>
				</td>
				<td>
					<h5>".($program_meta['program_title'][0]?$program_meta['program_title'][0]:$program[0]->post_title)."</h5>".
					wpautop($program_meta['program_desc'][0])."
				</td>
			</tr>
		</table>
	";
	return $html;
}

function tccedu_sc_quick_tcc($attr){
	$html = "
		<div id=\"quick-tcc\"".($attr['class']?" class=\"".$attr['class']."\"":"").">
			<div class=\"content\">
				<div id=\"quick-tcc-form\" class=\"tcc-form\">
					<div class=\"form-stage select-section\"></div>
					<div class=\"form-stage specify-interest\"></div>
					<div class=\"form-stage download-guide\"></div>
				</div>
			</div>
		</div>";
	return $html;
}

function tccedu_sc_sf_embed($attr){
	$html = "<div class=\"sf-embed\"".(isset($attr['slug'])?" data-slug=\"".$attr['slug']."\"":"").(isset($attr['title'])?" data-title=\"".$attr['title']."\"":"")."></div>";
	return $html;
}

function tccedu_sc_transfer_agreements($attr){
	$html = "<div id=\"transfer-agreements\"><table class=\"controls\"><tr><td class=\"by-school\"><h5>Search by School</h5>".(isset($attr['school_copy'])?"<p>".$attr['school_copy']."</p>":"")."</td><td class=\"by-program\"><h5>Search by Academic Program</h5>".(isset($attr['program_copy'])?"<p>".$attr['program_copy']."</p>":"")."</td></tr></table><div class=\"items\"></div></div>";
	return $html;
}


function tccedu_excerpt($post,$term=""){
	$text = "";
	if(is_object($post)){
		if(has_excerpt($post->ID)) $text = str_replace(array("\r","\n","\t"),"",get_the_excerpt($post->ID));
		if(empty($text)) $text = str_replace(array("\r","\n","\t"),"",$post->post_content);
		$text = preg_replace(array('/></','/<\/a></','/<\/a>\[/','/\<\/a\>[^\s\.\!\?\-\"\'\&\(\)]/','/\&nbsp\;/','/\s\s/'),array('> <','</a> <','</a> [','</a> ',' ',' '),$text);
	} elseif(is_string($post)){
		$text = $post;
		$text = preg_replace(array('/></','/<\/a></','/<\/a>\[/','/\&nbsp\;/','/\s\s/'),array('> <','</a> <','</a> [',' ',' '),$text);
	}
	
	$rawtext = strip_shortcodes(wp_strip_all_tags($text));
	
	if(empty($rawtext)&&is_object($post)&&isset($post->ID)) $rawtext = get_post_meta($post->ID,'_yoast_wpseo_metadesc',true);
	
	if($term){
		$term_pos = stripos($rawtext,$term);
		if($term_pos!==false){
			$ex_start = -80;
			if($term_pos>strlen($rawtext)-strlen($term)+$ex_start){
				$ex_start = -140-(strlen($rawtext)-strripos($rawtext,$term));
			}
			$rawtext = substr($rawtext,max(array(0,$term_pos+$ex_start)),300);
			$rawtext = tccedu_trim_text(250,"&hellip;".substr($rawtext,strpos($rawtext," ")+1));
			$rawtext = str_ireplace($term,"<span class=\"term\">".$term."</span>",$rawtext);
			return $rawtext;
		}
	}
	return tccedu_trim_text(250,$rawtext);
}
function tccedu_trim_text($target,$text){
	if(strlen($text)>$target) $text = substr($text,0,strrpos($text," ",($target-3)-strlen($text)))."&hellip;";
	return $text;
}
function get_relative_permalink($id){
	return get_relative_link(get_permalink($id));
}
function get_relative_link($url){
	return str_replace(home_url(),"",$url);
}

function tccedu_clean_wp_garbage($content){
	$content = force_balance_tags($content);
	$content = preg_replace('#<p>\s*+(<br\s*/*>)?\s*</p>#i','',$content);
	$content = preg_replace('~\s?<p>(\s|&nbsp;)+</p>\s?~','',$content);
	$content = strtr($content,array (
		'<p>[' => '[',
		']</p>' => ']',
		']<br />' => ']'
	));
	return $content;
}

function fjarrett_get_attachment_id_by_url( $url ) {
	$parsed_url  = explode( parse_url( WP_CONTENT_URL, PHP_URL_PATH ), $url );
	$this_host = str_ireplace( 'www.', '', parse_url( home_url(), PHP_URL_HOST ) );
	$file_host = str_ireplace( 'www.', '', parse_url( $url, PHP_URL_HOST ) );
	if ( ! isset( $parsed_url[1] ) || empty( $parsed_url[1] ) || ( $this_host != $file_host ) ) {
		return;
	}
	global $wpdb;
	$attachment = $wpdb->get_col( $wpdb->prepare( "SELECT ID FROM {$wpdb->prefix}posts WHERE guid RLIKE %s;", $parsed_url[1] ) );
	return $attachment[0];
}