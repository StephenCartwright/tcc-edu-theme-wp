<?php
/**
 * Template Name: Program Finder
 */

get_header();
global $post;
?>

<?php while ( have_posts() ) : the_post(); ?>

<?php echo tccedu_get_section_nav($post); ?>

<main id="site" class="page-program-finder">
	
	<div id="page-content">
		<div id="program-finder" class="wrap">
			<div class="page-title-wrap"><h1 class="page-title"><?php echo $post->post_title; ?></h1></div>
			
			<div class="inwrap">
				<div class="page-copy page-col"><?php the_content(); ?></div>
			</div>
			
			<?php echo tccedu_get_pf_search("program-finder"); ?>
			
		</div>
	</div>
	
	<?php if($page_meta['events_mod_enable'][0]) echo tccedu_get_upcoming_events_module($post); ?>
	
	<?php if($page_meta['content_feed_mod_enable'][0]) echo tccedu_get_content_feed($post); ?>
	
</main>

<?php endwhile; ?>

<?php
get_footer();