<?php
/**
 * The template for displaying search results pages
 */
global $wp_query;
get_header();
?>

<?php echo tccedu_get_section_nav($post); ?>

<main id="site" class="page-search">
	
	<div id="page-content">
		
		<div class="wrap">
			
			<div class="page-title-wrap"><h1 class="page-title">Site Search Results</h1></div>
			
			<div class="search">
				<form id="search-form" action="/" method="get">
					<input id="site-search" class="search-input-large" name="s" value="<?php echo get_search_query(); ?>" />
				</form>
			</div>
			
			<div class="search-module _site">
				<?php
				echo "
				<div id=\"tcc-site-search\" class=\"search-results page-copy copy-size page-col\" data-s=\"".$_GET['s']."\" data-total=\"".$wp_query->found_posts."\" data-per-page=\"".get_option('posts_per_page')."\" data-page=\"".($_GET['paged']?$_GET['paged']:1)."\" data-max-pages=\"".$wp_query->max_num_pages."\">";
				
				if ( have_posts() ) {
					echo "
					<div class=\"info nonh5\">".($wp_query->max_num_pages>1?"Page ".($_GET['paged']?$_GET['paged']:1)." of ".$wp_query->max_num_pages." for ":"").$wp_query->found_posts." results</div>
					<div class=\"items\">";
					while ( have_posts() ) {
						the_post();
						$raw_search_term = strip_shortcodes(wp_strip_all_tags($wp_query->query_vars['s']));
						echo "
						<div class=\"item\">
							<a href=\"".get_relative_permalink($post->ID)."\" class=\"nonh3\">".get_the_title()."</a>
							<p>".tccedu_excerpt($post,$raw_search_term)."</p>
						</div>";
					}
					echo "
					</div>";
				} else {
					echo "
					<div class=\"info nonh5\">No results!</div>
					<div class=\"items\"></div>";
				}
				
				echo "
					<div class=\"page-nav".($wp_query->max_num_pages<2?" _disabled":"")." _closed\">
						<button class=\"btn btn-min btn-arrow _more".($wp_query->max_num_pages<2?" disabled\" disabled=\"disabled\"":"\"").">More results</button>
						<button class=\"btn btn-min btn-arrow-left _prev disabled\" disabled=\"disabled\">Previous page</button><button class=\"btn btn-min btn-arrow _next disabled\" disabled=\"disabled\">Next page</button>
					</div>
				</div>";
				?>
				
			</div>
			
			<div class="page-title-wrap"><h1 class="page-title">Help Center Results</h1></div>
			<div class="inwrap">
				<div class="page-copy page-col">
					<p>Expand your search and access a comprehensive library of information and resources on a variety of topics from Tidewater Community College. View complete <a class="help-center-results-link" href="#">Help Center results</a>.</p>
				</div>
			</div>
			
			<div class="search-results _helpcenter">
				<div class="page-copy copy-size page-col">
					<div class="sf-search" data-term="<?php echo get_search_query(); ?>"></div>
				</div>
			</div>
			
		</div>
		
	</div>
	
</main>

<?php
get_footer();
