<?php
/**
 * Template Name: Homepage
 */

if(isset($_COOKIE["bypass-gateway"])&&!isset($_COOKIE["ignore-bypass-gateway"])){
	$redirect = $_COOKIE["bypass-gateway"];
	if($redirect!=="/"&&substr($redirect,0,2)!=="/!"){
		if(substr($redirect,0,1)=="/") $redirect = "http".($_SERVER['HTTPS']&&$_SERVER['HTTPS']!=="off"?"s":"")."://".$_SERVER['HTTP_HOST'].$redirect;
		if(filter_var($redirect,FILTER_VALIDATE_URL)!==false){
			header("Location: ".$redirect);
			die();
		}
	}
}

get_header();
?>

<?php if($page_meta['gateway_enable'][0]){ ?>
<div id="gateway" data-cookie-days="<?php echo $page_meta['gateway_cookie_days'][0]?$page_meta['gateway_cookie_days'][0]:"90"; ?>">
	<aside class="trim _1"></aside>
	<aside class="trim _2"></aside>
	<div class="gateway-form">
		<table class="inputs">
			<tr><td class="prompt">
				<label for="gateway-iam">I am</label>
			</td><td class="input">
				<select id="gateway-iam"><?php
				for($o=1;$o<=7;$o++){
					$item = "gateway_opt_".$o;
					$link = $page_meta[$item.'_link'][0];
					if($link=="/") $link = "/!".$o;
					if(isset($page_meta[$item.'_show'][0])) echo "
					<option value=\"".$link."\"".($o==1?" selected":"").">".$page_meta[$item.'_title'][0]."</option>";
				}
				?></select>
			</td></tr>
		</table>
		<div class="btns">
			<button class="btn btn-2 btn-arrow _go">Go</button><button class="btn btn-4 btn-text btn-arrow _skip">Skip</button>
		</div>
	</div>
	<div class="fill"<?php echo $page_meta['gateway_bg_img'][0]?" style=\"background-image:url(".$page_meta['gateway_bg_img'][0].");\"":"" ?>></div>
</div>
<?php } ?>

<main id="site" class="page-homepage">
	
	<?php echo tccedu_get_a_spot_slider(); ?>
	
	<div id="four-up">
		<div class="wrap">
			<table class="items">
				<tr><?php
					for($f=1;$f<=4;$f++){
						$item = "four_up_".$f;
						echo "
					<td><div class=\"item ".$page_meta[$item.'_class'][0]."\">
						<a href=\"".$page_meta[$item.'_link'][0]."\" class=\"hit\" title=\"".$page_meta[$item.'_title'][0]."\"></a>
						<div class=\"icon icon-large\">
							".$page_meta[$item.'_svg'][0]."
						</div>
						<label class=\"nonh2\">".$page_meta[$item.'_title'][0]."</label>
						<p>".$page_meta[$item.'_copy'][0]."</p>
					</div></td>";
					}
				?></tr>
			</table>
		</div>
	</div>
	
	<div id="quick-tcc">
		<div class="wrap">
			<div>
				<div class="content">
					<div id="quick-tcc-form" class="tcc-form">
						<div class="form-stage select-section"></div>
						<div class="form-stage specify-interest"></div>
						<div class="form-stage download-guide"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<?php echo tccedu_get_program_finder_module(); ?>
	
	<?php if($page_meta['events_mod_enable'][0]) echo tccedu_get_upcoming_events_module($post); ?>
	
	<div id="key-message">
		<div class="wrap">
			<table class="items">
				<tr><?php
					for($k=1;$k<=5;$k++){
						$item = "key_msg_".$k;
						echo "
					<td class=\"item\">
						<a href=\"".$page_meta[$item.'_link'][0]."\" class=\"hit\" title=\"".$page_meta[$item.'_title'][0]."\"></a>
						<label class=\"title nonh2\">".$page_meta[$item.'_title'][0]."</label>
					</td>";
					}
				?></tr>
			</table>
		</div>
	</div>
	
	<?php if($page_meta['content_feed_mod_enable'][0]) echo tccedu_get_content_feed($post); ?>
	
	<?php if($page_meta['map_mod_enable'][0]) echo tccedu_get_map_module($post); ?>
	
</main>

<?php
get_footer();