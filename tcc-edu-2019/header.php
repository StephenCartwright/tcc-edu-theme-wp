<?php
/**
 * @package WordPress
 * @subpackage TCC_edu
 * @since 1.0.0
 */
 
global $post, $page_meta;
$page_meta = get_post_meta($post->ID);

if($page_meta['page_redirect'][0]){
	$redirect = $page_meta['page_redirect'][0];
	if(substr($redirect,0,1)=="/") $redirect = "http".($_SERVER['HTTPS']&&$_SERVER['HTTPS']!=="off"?"s":"")."://".$_SERVER['HTTP_HOST'].$redirect;
	if(filter_var($redirect,FILTER_VALIDATE_URL)!==false){
		header("Location: ".$redirect);
		die();
	}
}

?><!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<link rel="profile" href="https://gmpg.org/xfn/11" />
	<?php wp_head(); ?>
	<?php echo "<script> window.theme_dir = \"".get_template_directory_uri()."/\"; </script>"; ?>
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-NX8DC8');</script>
	<!-- End Google Tag Manager -->
</head>

<body <?php body_class(); ?>>

<!-- monsido -->
<script type="text/javascript">
	var _monsido = _monsido || [];
	_monsido.push(['_setDomainToken', 'XiDyiL5H8UmEgbADGinMJw']);
	_monsido.push(['_withStatistics', 'false']);
</script>
<script async src="//cdn.monsido.com/tool/javascripts/monsido.js"></script>
<!-- monsido -->

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NX8DC8"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->


<div id="lightbox">
	<div class="owrap"><div class="vwrap">
		<div class="content">
			<div class="wrap">
				<div class="nav">
					<button class="btn btn-nav btn-nav-prev btn-2 _prev">Previous</button>
					<button class="btn btn-nav btn-nav-next btn-2 _next">Next</button>
					<button class="btn btn-close btn-2 _close"></button>
				</div>
				<div class="owrap"><div class="vwrap item"></div></div>
			</div>
		</div>
	</div></div>
	<div class="fill"></div>
</div>

<!-- begin site wrap -->
<div id="site-wrap">
<a href="#site" id="skip-nav-link">Skip navigation</a>
<header id="header">
	<!-- begin header-ribbon menu -->
	<div class="ribbon"><div class="wrap">
		<nav class="header-ribbon">
			<?php
			wp_nav_menu(array(
				'theme_location' => 'ribbon',
				'menu_class' => 'header-ribbon-menu'
			));
			?>
		</nav>
	</div></div>
	<!-- end header-ribbon menu -->
	<div id="menu-wrap" class="wrap">
		<div class="menu-features"><?php
		for($f=1;$f<=6;$f++){
			if(get_option('menu_feature_'.$f.'_target')){
				echo "
			<div class=\"sub-menu-feature freset\" data-targets=\"".get_option('menu_feature_'.$f.'_target')."\" >".
				(get_option('menu_feature_'.$f.'_title')?"
				<span class=\"title nonh2\">".get_option('menu_feature_'.$f.'_title')."</span>":"").
				(get_option('menu_feature_'.$f.'_copy')?"
				<span class=\"desc\">".get_option('menu_feature_'.$f.'_copy')."</span>":"")."
				<a href=\"".get_option('menu_feature_'.$f.'_link')."\" class=\"btn btn-2 btn-min btn-arrow\">".(get_option('menu_feature_'.$f.'_cta')?get_option('menu_feature_'.$f.'_cta'):"Learn More")."</a>
				<div class=\"vr\"></div>
			</div>";
			}
		}
		?></div>
		<a href="/" id="main-logo" class="logo" title="Tidewater Community College"></a>
		<div class="btn-mobile-menu"></div>
		<!-- begin main menu -->
		<nav id="site-navigation" class="main-navigation" aria-label="Main Menu">
			<?php
			wp_nav_menu(array(
				'theme_location' => 'main',
				'menu_class' => 'main-menu',
				'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',
			));
			?>
		</nav>
		<!-- end main menu -->
		<div id="menu-search">
			<div class="wrap">
				<button class="hit" tabindex="-1" title="Search"></button>
				<form action="/" method="get">
					<label for="search" class="screen-reader-text">Site Search</label>
					<input aria-label="search" type="text" name="s" id="search" placeholder="Search" value="">
					<button class="_search" type="submit"></button>
				</form>
			</div>
		</div>
		<div class="fill"></div>
		<div class="indicator"><div class="bit"></div></div>
		
	</div>
</header>

<?php
if(get_option('alert_major_show')){
	echo "
<div id=\"major-alert\">
	<div class=\"owrap\"><div class=\"vwrap\">
		<div class=\"panel page-copy\">
			<button class=\"btn btn-close btn-5 _close\"></button>
			<div class=\"icon\">
				<svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" width=\"69px\" height=\"62px\">
					<path fill-rule=\"evenodd\" fill=\"#fe4045\" d=\"M67.887,57.890 C66.403,60.464 63.745,62.000 60.777,62.000 L8.223,62.000 C5.255,62.000 2.597,60.464 1.113,57.891 C-0.371,55.317 -0.371,52.245 1.113,49.672 L27.390,4.110 C28.874,1.536 31.532,-0.000 34.500,-0.000 C37.468,-0.000 40.126,1.536 41.610,4.110 L67.887,49.672 C69.371,52.245 69.371,55.317 67.887,57.890 ZM31.594,51.790 C32.451,52.649 33.463,53.078 34.629,53.078 C35.829,53.078 36.858,52.649 37.715,51.790 C38.572,50.933 39.001,49.903 39.001,48.700 C39.001,47.499 38.572,46.478 37.715,45.636 C36.858,44.795 35.829,44.375 34.629,44.375 C33.463,44.375 32.451,44.795 31.594,45.636 C30.736,46.478 30.308,47.499 30.308,48.700 C30.308,49.903 30.736,50.933 31.594,51.790 ZM39.053,19.707 C39.053,19.673 39.053,19.639 39.053,19.604 C39.018,18.643 38.572,17.837 37.715,17.184 C36.858,16.532 35.829,16.205 34.629,16.205 C33.428,16.205 32.399,16.523 31.542,17.158 C30.685,17.794 30.256,18.609 30.256,19.604 C30.256,19.639 30.256,19.673 30.256,19.707 C30.256,19.742 30.273,19.776 30.308,19.810 C30.308,19.880 30.308,19.931 30.308,19.965 C30.308,19.999 30.308,20.034 30.308,20.068 L33.240,39.328 C33.343,39.912 33.505,40.324 33.729,40.564 C33.951,40.804 34.251,40.925 34.629,40.925 C35.006,40.925 35.306,40.796 35.529,40.538 C35.752,40.281 35.915,39.877 36.018,39.328 L39.001,20.068 C39.001,20.034 39.001,19.999 39.001,19.965 C39.001,19.931 39.018,19.880 39.053,19.810 C39.053,19.776 39.053,19.742 39.053,19.707 Z\"/>
				</svg>
			</div>
			<h1>".get_option('alert_major_title')."</h1>
			".(get_option('alert_major_copy')?"<p>".stripslashes(get_option('alert_major_copy'))."</p>":"")."
			".(get_option('alert_major_link')&&get_option('alert_major_cta')?"<a href=\"".get_option('alert_major_link')."\" class=\"btn btn-5 btn-text\">".get_option('alert_major_cta')."</a>":"")."
			".(get_option('alert_major_link_2')&&get_option('alert_major_cta_2')?"<a href=\"".get_option('alert_major_link_2')."\" class=\"btn btn-5 btn-text\">".get_option('alert_major_cta_2')."</a>":"")."
			<button class=\"btn btn-5 btn-arrow _conf\">".(get_option('alert_major_conf')?get_option('alert_major_conf'):"Continue")."</button>
		</div>
	</div></div>
	<div class=\"fill\"></div>
</div>";
}

if(get_option('alert_medium_show')){
	echo "
<div id=\"medium-alert\">
	<div class=\"content\">
		<button class=\"btn btn-close btn-8 _close\"></button>
		<div class=\"icon\">
			<svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" width=\"29px\" height=\"26px\">
				<path fill-rule=\"evenodd\" fill=\"#fdb81e\" d=\"M28.532,24.276 C27.909,25.356 26.791,26.000 25.544,26.000 L3.456,26.000 C2.209,26.000 1.092,25.356 0.468,24.277 C-0.156,23.197 -0.156,21.909 0.468,20.830 L11.512,1.723 C12.135,0.644 13.253,0.000 14.500,0.000 C15.748,0.000 16.865,0.644 17.488,1.723 L28.532,20.830 C29.156,21.909 29.156,23.197 28.532,24.276 ZM13.278,21.719 C13.639,22.079 14.064,22.258 14.554,22.258 C15.058,22.258 15.491,22.079 15.851,21.719 C16.211,21.359 16.392,20.927 16.392,20.423 C16.392,19.919 16.211,19.491 15.851,19.138 C15.491,18.785 15.058,18.609 14.554,18.609 C14.064,18.609 13.639,18.785 13.278,19.138 C12.918,19.491 12.738,19.919 12.738,20.423 C12.738,20.927 12.918,21.359 13.278,21.719 ZM16.413,8.264 C16.413,8.250 16.413,8.236 16.413,8.221 C16.399,7.818 16.211,7.480 15.851,7.206 C15.491,6.933 15.058,6.796 14.554,6.796 C14.049,6.796 13.617,6.929 13.257,7.195 C12.896,7.462 12.716,7.804 12.716,8.221 C12.716,8.236 12.716,8.250 12.716,8.264 C12.716,8.279 12.723,8.293 12.738,8.308 C12.738,8.337 12.738,8.358 12.738,8.372 C12.738,8.387 12.738,8.401 12.738,8.416 L13.970,16.492 C14.014,16.737 14.082,16.910 14.176,17.011 C14.269,17.111 14.395,17.162 14.554,17.162 C14.713,17.162 14.839,17.108 14.932,17.000 C15.026,16.892 15.095,16.723 15.138,16.492 L16.392,8.416 C16.392,8.401 16.392,8.387 16.392,8.372 C16.392,8.358 16.399,8.337 16.413,8.308 C16.413,8.293 16.413,8.279 16.413,8.264 Z\"/>
			</svg>
		</div>
		<span class=\"nonh2\">".get_option('alert_medium_title')."</span>
		".(get_option('alert_medium_copy')?"<p>".stripslashes(get_option('alert_medium_copy'))."</p>":"")."
		".(get_option('alert_medium_link')&&get_option('alert_medium_cta')?"<a href=\"".get_option('alert_medium_link')."\" class=\"btn btn-8 btn-text btn-arrow\">".get_option('alert_medium_cta')."</a>":"")."
	</div>
</div>";
}

if(get_option('alert_minor_show')){
	$alert_minor_cookie = get_option('alert_minor_cookie_days')?get_option('alert_minor_cookie_days'):"1";
	echo "
<div id=\"minor-alert\" data-cookie-days=\"".$alert_minor_cookie."\">
	<div class=\"content\">
		<button class=\"btn btn-close btn-7 _close\"></button>
		<div class=\"icon\">
			<svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" width=\"29px\" height=\"26px\">
				<path fill-rule=\"evenodd\" fill=\"#767b16\" d=\"M28.532,24.276 C27.909,25.356 26.791,26.000 25.544,26.000 L3.456,26.000 C2.209,26.000 1.092,25.356 0.468,24.277 C-0.156,23.197 -0.156,21.909 0.468,20.830 L11.512,1.723 C12.135,0.644 13.253,0.000 14.500,0.000 C15.748,0.000 16.865,0.644 17.488,1.723 L28.532,20.830 C29.156,21.909 29.156,23.197 28.532,24.276 ZM13.278,21.719 C13.639,22.079 14.064,22.258 14.554,22.258 C15.058,22.258 15.491,22.079 15.851,21.719 C16.211,21.359 16.392,20.927 16.392,20.423 C16.392,19.919 16.211,19.491 15.851,19.138 C15.491,18.785 15.058,18.609 14.554,18.609 C14.064,18.609 13.639,18.785 13.278,19.138 C12.918,19.491 12.738,19.919 12.738,20.423 C12.738,20.927 12.918,21.359 13.278,21.719 ZM16.413,8.264 C16.413,8.250 16.413,8.236 16.413,8.221 C16.399,7.818 16.211,7.480 15.851,7.206 C15.491,6.933 15.058,6.796 14.554,6.796 C14.049,6.796 13.617,6.929 13.257,7.195 C12.896,7.462 12.716,7.804 12.716,8.221 C12.716,8.236 12.716,8.250 12.716,8.264 C12.716,8.279 12.723,8.293 12.738,8.308 C12.738,8.337 12.738,8.358 12.738,8.372 C12.738,8.387 12.738,8.401 12.738,8.416 L13.970,16.492 C14.014,16.737 14.082,16.910 14.176,17.011 C14.269,17.111 14.395,17.162 14.554,17.162 C14.713,17.162 14.839,17.108 14.932,17.000 C15.026,16.892 15.095,16.723 15.138,16.492 L16.392,8.416 C16.392,8.401 16.392,8.387 16.392,8.372 C16.392,8.358 16.399,8.337 16.413,8.308 C16.413,8.293 16.413,8.279 16.413,8.264 Z\"/>
			</svg>
		</div>
		<p>".stripslashes(get_option('alert_minor_copy'))."</p>
	</div>
</div>";
}

if(get_option('help_bug_show')){
	$help_bug_cookie = get_option('help_bug_cookie_days')?get_option('help_bug_cookie_days'):"90";
	$help_bug_trigger = get_option('help_bug_trigger_after')?get_option('help_bug_trigger_after'):"3";
	$help_bug_timer = get_option('help_bug_timer')?get_option('help_bug_timer'):"30";
	foreach(json_decode(get_option('help_bug_schedule')) as $day_name => $day_times) $schedule_days[$day_name] = $day_times;
	$today = array( 'd'=>date("l"), 't'=>(int)date("Hi") );
	$help_bug_is_live = $schedule_days[$today['d']]&&$today['t']>=(int)$schedule_days[$today['d']][0]&&$today['t']<(int)$schedule_days[$today['d']][1];
	
	echo "
<div id=\"help-bug\" data-cookie-days=\"".$help_bug_cookie."\" data-trigger-after=\"".$help_bug_trigger."\" data-timer=\"".$help_bug_timer."\">
	<div class=\"content\">
		<button class=\"btn btn-close _close\"></button>
		";
	if($help_bug_is_live){
		echo "
		<div class=\"icon\">
			<svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" width=\"40px\" height=\"39px\">
				<path fill-rule=\"evenodd\" fill=\"rgb(0, 58, 92)\" d=\"M39.746,32.432 C39.290,31.792 38.440,31.089 37.198,30.325 C35.956,29.543 34.577,28.574 33.061,27.418 C31.527,26.262 30.322,25.693 29.445,25.711 L28.760,26.058 C28.011,26.805 27.400,27.480 26.925,28.085 C26.432,28.689 25.865,29.276 25.226,29.845 C24.843,29.916 24.477,29.889 24.130,29.765 C21.445,28.449 18.861,26.796 16.377,24.805 C13.583,22.155 11.318,19.266 9.583,16.137 C9.236,15.497 9.345,14.795 9.912,14.031 L14.459,9.924 C14.679,9.639 14.706,9.239 14.542,8.724 C12.788,6.128 11.172,3.390 9.692,0.510 C9.455,0.101 8.743,-0.059 7.556,0.030 C6.350,0.119 5.044,0.590 3.638,1.443 C2.213,2.297 1.127,3.772 0.378,5.870 C-0.061,7.417 -0.115,9.159 0.213,11.097 C0.487,12.395 0.807,13.542 1.172,14.537 C2.085,17.026 3.145,19.338 4.350,21.471 C5.501,23.196 6.697,24.840 7.939,26.405 C9.546,28.165 11.190,29.774 12.870,31.232 C14.898,32.832 16.888,34.245 18.843,35.472 C20.231,36.201 21.811,36.912 23.582,37.605 C25.372,38.299 27.628,38.761 30.349,38.992 C32.048,39.063 33.847,38.708 35.746,37.925 C37.427,37.143 38.623,36.192 39.335,35.072 C40.047,33.952 40.184,33.072 39.746,32.432 Z\"/>
			</svg>
		</div>
		<span class=\"nonh2\">Call with questions: <a class=\"nowrap\" href=\"tel:".get_option('help_bug_phone')."\">".get_option('help_bug_phone')."</a></span>";
	} else {
		echo "
		<span class=\"nonh2\">Need help? <span class=\"nowrap\">Check out the <a href=\"https://help.tcc.edu\" class=\"alt\">Help Center</a></a></span>";
	}
	echo "
	</div>
</div>";
}

?>

