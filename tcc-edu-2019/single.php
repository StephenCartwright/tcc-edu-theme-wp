<?php
/**
 * The template for displaying all single posts
 */

get_header();
?>

<?php echo tccedu_get_section_nav($post); ?>

<main id="site" class="page-standard">
	
	<div id="page-content">
		
		<div class="wrap">
			
<?php
if ( have_posts() ) {

	while ( have_posts() ) {
		the_post();
		?>
			<div class="page-title-wrap"><h1 class="page-title"><?php the_title(); ?></h1></div>
			
			<div class="inwrap">
				<div class="page-copy page-col"><?php the_content(); ?></div>
			</div>
			
		<?php
	}

} else {
	
}
?>
			
		</div>
		
	</div>
	
	<?php echo tccedu_get_content_feed($post); ?>
	
</main>

<?php
get_footer();