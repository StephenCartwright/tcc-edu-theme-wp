<?php
/**
 * Template Name: QuickTCC Page
 */

get_header();
global $post;
?>

<?php while ( have_posts() ) : the_post(); ?>

<?php echo tccedu_get_section_nav($post); ?>

<main id="site" class="page-standard">
	
	<div id="page-content">
		
		<div class="wrap">
			
			<div class="inwrap">
				<div class="page-copy page-col">
					<div id="quick-tcc">
						<div class="content">
							<div id="quick-tcc-form" class="tcc-form">
								<div class="form-stage select-section"></div>
								<div class="form-stage specify-interest"></div>
								<div class="form-stage download-guide"></div>
							</div>
						</div>
					</div>
					<?php the_content(); ?>
				</div>
			</div>
			
			
		</div>
		
	</div>
	
	<?php if($page_meta['events_mod_enable'][0]) echo tccedu_get_upcoming_events_module($post); ?>
	
	<?php if($page_meta['content_feed_mod_enable'][0]) echo tccedu_get_content_feed($post); ?>
	
</main>

<?php endwhile; ?>

<?php
get_footer();