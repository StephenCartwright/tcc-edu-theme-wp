<?php
/**
 * The template for displaying 404 pages (not found)
 */

global $wp_query;
get_header();
?>

<main id="site" class="page-standard">
	
	<div id="page-content">
		
		<div class="wrap">
			
			<div class="page-title-wrap"><h1 class="page-title">404 - Page not found</h1></div>
			<div class="inwrap">
				<div class="page-copy page-col">
					<h4>The requested content cannot be found!</h4>
				</div>
			</div>
			
			<div class="search">
				<form id="search-form" action="/" method="get">
					<input id="site-search" class="search-input-large" name="s" placeholder="Try a search instead" value="<?php echo get_search_query(); ?>" />
				</form>
			</div>
			
		</div>
		
	</div>
	
</main>

<?php
get_footer();
