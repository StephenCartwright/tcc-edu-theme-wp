/*
	TCC.edu
*/

window.TCCVals = {};

/* career pathways */
TCCVals.pf_careers = {
	"arts-humanities":"Arts & Humanities",
	"business":"Business",
	"engineering-science-mathematics":"Engineering, Science & Mathematics",
	"health-professions":"Health Professions",
	"computer-science-it":"Computer Science & IT",
	"maritime-skilled-trades":"Maritime & Skilled Trades",
	"manufacturing-transportation":"Manufacturing & Transportation",
	"public-professional-services":"Public & Professional Services",
	"social-sciences-education":"Social Sciences & Education",
};

/* campuses */
TCCVals.campuses = {
	"chesapeake":{
		id:"chesapeake",
		type:0,
		coords:"36.725210,-76.294475",
		title:"Chesapeake Campus",
		location:"Chesapeake",
		addr_1:"1428 Cedar Road",
		addr_2:"Chesapeake, VA 23322",
		phone:"757-822-5100",
		url:"/about-tcc/locations/chesapeake-campus/"
	},
	"norfolk":{
		id:"norfolk",
		type:0,
		coords:"36.850475,-76.290585",
		title:"Norfolk Campus",
		location:"Norfolk",
		addr_1:"315 Granby Street",
		addr_2:"Norfolk, VA 23510",
		phone:"757-822-1110",
		url:"/about-tcc/locations/norfolk-campus/"
	},
	"portsmouth":{
		id:"portsmouth",
		type:0,
		coords:"36.805038,-76.349346",
		title:"Portsmouth Campus",
		location:"Portsmouth",
		addr_1:"120 Campus Drive",
		addr_2:"Portsmouth, VA 23701",
		phone:"757-822-2124",
		url:"/about-tcc/locations/portsmouth-campus/"
	},
	"virginia-beach":{
		id:"virginia-beach",
		type:0,
		coords:"36.785018,-76.100371",
		title:"Virginia Beach Campus",
		location:"Virginia Beach",
		addr_1:"1700 College Crescent",
		addr_2:"Virginia Beach, VA 23453",
		phone:"757-822-7100",
		url:"/about-tcc/locations/virginia-beach-campus/"
	},
	"regional-automotive-center":{
		id:"regional-automotive-center",
		type:1,
		coords:"36.735358,-76.217751",
		title:"Regional Automotive Center",
		location:"Chesapeake",
		addr_1:"600 Innovation Drive",
		addr_2:"Chesapeake, VA 23320",
		phone:"757-822-5000",
		url:"/about-tcc/locations/regional-automotive-center/"
	},
	"center-workforce-solutions":{
		id:"center-workforce-solutions",
		type:1,
		coords:"36.905458,-76.437735",
		title:"Center for Workforce Solutions",
		location:"Suffolk",
		addr_1:"7000 College Drive",
		addr_2:"Suffolk, VA 23435",
		phone:"757-822-1234",
		url:"/about-tcc/locations/center-workforce-solutions/"
	},
	"tri-cities-center":{
		id:"tri-cities-center",
		type:1,
		coords:"36.868972,-76.417233",
		title:"Tri-Cities Center",
		location:"Portsmouth",
		addr_1:"1070 University Blvd.",
		addr_2:"Portsmouth, VA 23703",
		phone:"757-822-2623",
		url:"/about-tcc/locations/tri-cities-center/"
	},
	"visual-arts-center":{
		id:"visual-arts-center",
		type:1,
		coords:"36.835625,-76.300574",
		title:"Visual Arts Center",
		location:"Portsmouth",
		addr_1:"340 High Street",
		addr_2:"Portsmouth, VA 23704",
		phone:"757-822-1888",
		url:"/about-tcc/locations/visual-arts-center/"
	},
	/*"district-administration-building":{
		id:"district-administration-building",
		type:1,
		coords:"36.850113,-76.291316",
		title:"District Administration Building",
		location:"Norfolk",
		addr_1:"121 College Place",
		addr_2:"Norfolk, VA 23510",
		phone:"757-822-1122"
	},*/
	"skilled-trades-academy":{
		id:"skilled-trades-academy",
		type:1,
		coords:"36.805591,-76.376609",
		title:"Skilled Trades Academy",
		location:"Portsmouth",
		addr_1:"3303 Airline Blvd.",
		addr_2:"Portsmouth, VA 23701",
		phone:"757-822-2116",
		url:"/locations/skilled-trades-academy/"
	},
	"online":{
		id:"online",
		type:2,
		title:"Online",
		location:"Online"
	}
};

/* programs */
TCCVals.programs = {
	"test-program":{
		id:"test-program",
		img:"img/TEMP-IMG-1.jpg",
		title:"Test Program",
		url:"/programs/test-program/",
		career:"Public & Professional Services",
		degree:"Associate of Applied Science",
		type:["career-studies-certificate"],
		location:["virginia-beach","norfolk"],
		interest:["working-with-my-hands","helping-others"]
	},
	"american-sign-language":{
		id:"american-sign-language",
		img:"img/TEMP-IMG-1.jpg",
		title:"American Sign Language",
		url:"#item-01",
		career:"Arts & Humanities",
		degree:"Associate of Applied Science",
		type:["associate-applied-science","career-studies-certificate"],
		location:["chesapeake"],
		interest:["expressing-my-creativity","working-with-my-hands","helping-others"]
		
	},
	"graphic-design":{
		id:"graphic-design",
		img:"img/TEMP-IMG-1.jpg",
		title:"Graphic Design",
		url:"#item-02",
		career:"Arts & Humanities",
		degree:"Associate of Applied Science",
		type:["associate-applied-science","specialization"],
		location:["portsmouth"],
		interest:["expressing-my-creativity","tooling-with-tech"]
	},
	"liberal-arts":{
		id:"liberal-arts",
		img:"img/TEMP-IMG-1.jpg",
		title:"Liberal Arts",
		url:"#item-03",
		career:"Arts & Humanities",
		degree:"Associate of Applied Science",
		type:["associate-arts"],
		location:["virginia-beach","chesapeake","norfolk","portsmouth","online"],
		interest:["uncovering-our-world"]
	},
	"music":{
		id:"music",
		img:"img/TEMP-IMG-1.jpg",
		title:"Music",
		url:"#item-04",
		career:"Arts & Humanities",
		degree:"Associate of Applied Science",
		type:["associate-fine-arts","career-studies-certificate"],
		location:["norfolk"],
		interest:["expressing-my-creativity","working-with-my-hands"]
	},
	"studio-arts":{
		id:"studio-arts",
		img:"img/TEMP-IMG-1.jpg",
		title:"Studio Arts",
		url:"#item-05",
		career:"Arts & Humanities",
		degree:"Associate of Applied Science",
		type:["associate-applied-arts","specialization","career-studies-certificate"],
		location:["portsmouth"],
		interest:["expressing-my-creativity","working-with-my-hands"]
	},
	"theatre-arts":{
		id:"theatre-arts",
		img:"img/TEMP-IMG-1.jpg",
		title:"Theatre Arts",
		url:"#item-06",
		career:"Arts & Humanities",
		degree:"Associate of Applied Science",
		type:[],
		location:["chesapeake"],
		interest:["expressing-my-creativity"]
	},
	"accounting":{
		id:"accounting",
		img:"img/TEMP-IMG-1.jpg",
		title:"Accounting",
		url:"#item-07",
		career:"Business",
		degree:"Associate of Applied Science",
		type:["associate-applied-science","career-studies-certificate"],
		location:["virginia-beach","chesapeake","portsmouth","online"],
		interest:["running-a-business","crunching-numbers"]
	},
	"administrative-support-technology":{
		id:"administrative-support-technology",
		img:"img/TEMP-IMG-1.jpg",
		title:"Administrative Support Technology",
		url:"#item-08",
		career:"Business",
		degree:"Associate of Applied Science",
		type:["associate-applied-science","specialization","career-studies-certificate"],
		location:["virginia-beach","chesapeake","portsmouth"],
		interest:["running-a-business"]
	},
	"business-administration":{
		id:"business-administration",
		img:"img/TEMP-IMG-1.jpg",
		title:"Business Administration",
		url:"#item-09",
		career:"Business",
		degree:"Associate of Applied Science",
		type:["associate-arts"],
		location:["virginia-beach","chesapeake","norfolk","portsmouth","online"],
		interest:["running-a-business","crunching-numbers"]
	},
	"culinary-arts":{
		id:"culinary-arts",
		img:"img/TEMP-IMG-1.jpg",
		title:"Culinary Arts",
		url:"#item-10",
		career:"Business",
		degree:"Associate of Applied Science",
		type:["associate-applied-science","career-studies-certificate"],
		location:["norfolk"],
		interest:["expressing-my-creativity","working-with-my-hands"]
	},
	"management":{
		id:"management",
		img:"img/TEMP-IMG-1.jpg",
		title:"Management",
		url:"#item-11",
		career:"Business",
		degree:"Associate of Applied Science",
		type:["associate-applied-science","specialization","career-studies-certificate"],
		location:["virginia-beach","chesapeake","norfolk","portsmouth","online"],
		interest:["running-a-business","crunching-numbers"]
	},
	"restaurant-management":{
		id:"restaurant-management",
		img:"img/TEMP-IMG-1.jpg",
		title:"Restaurant Management",
		url:"#item-12",
		career:"Business",
		degree:"Associate of Applied Science",
		type:["career-studies-certificate"],
		location:["norfolk"],
		interest:["running-a-business","crunching-numbers"]
	},
	"computer-science":{
		id:"computer-science",
		img:"img/TEMP-IMG-1.jpg",
		title:"Computer Science",
		url:"#item-13",
		career:"Computer Science & IT",
		degree:"Associate of Applied Science",
		type:["associate-science","specialization"],
		location:["virginia-beach","chesapeake"],
		interest:["tooling-with-tech","crunching-numbers"]
	},
	"information-systems-technology":{
		id:"information-systems-technology",
		img:"img/TEMP-IMG-1.jpg",
		title:"Information Systems Technology",
		url:"#item-14",
		career:"Computer Science & IT",
		degree:"Associate of Applied Science",
		type:["associate-applied-science","specialization","career-studies-certificate"],
		location:["virginia-beach","chesapeake"],
		interest:["expressing-my-creativity","tooling-with-tech","crunching-numbers"]
	},
	"civil-engineering-technology":{
		id:"civil-engineering-technology",
		img:"img/TEMP-IMG-1.jpg",
		title:"Civil Engineering Technology",
		url:"#item-15",
		career:"Engineering, Science & Mathematics",
		degree:"Associate of Applied Science",
		type:["associate-applied-science","career-studies-certificate"],
		location:["virginia-beach","online"],
		interest:["working-with-my-hands","tooling-with-tech"]
	},
	"computer-aided-drafting-design":{
		id:"computer-aided-drafting-design",
		img:"img/TEMP-IMG-1.jpg",
		title:"Computer-Aided Drafting & Design",
		url:"#item-16",
		career:"Engineering, Science & Mathematics",
		degree:"Associate of Applied Science",
		type:["associate-applied-science","specialization"],
		location:["virginia-beach","portsmouth"],
		interest:["expressing-my-creativity","tooling-with-tech"]
	},
	"electronics-technology":{
		id:"electronics-technology",
		img:"img/TEMP-IMG-1.jpg",
		title:"Electronics Technology",
		url:"#item-17",
		career:"Engineering, Science & Mathematics",
		degree:"Associate of Applied Science",
		type:["associate-applied-science"],
		location:["virginia-beach"],
		interest:["working-with-my-hands","tooling-with-tech"]
	},
	"engineering":{
		id:"engineering",
		img:"img/TEMP-IMG-1.jpg",
		title:"Engineering",
		url:"#item-18",
		career:"Engineering, Science & Mathematics",
		degree:"Associate of Applied Science",
		type:["associate-science"],
		location:["virginia-beach","chesapeake"],
		interest:["expressing-my-creativity","tooling-with-tech","crunching-numbers"]
	},
	"horticulture":{
		id:"horticulture",
		img:"img/TEMP-IMG-1.jpg",
		title:"Horticulture",
		url:"#item-19",
		career:"Engineering, Science & Mathematics",
		degree:"Associate of Applied Science",
		type:["associate-applied-science","career-studies-certificate"],
		location:["chesapeake"],
		interest:["working-with-my-hands"]
	},
	"industrial-technology":{
		id:"industrial-technology",
		img:"img/TEMP-IMG-1.jpg",
		title:"Industrial Technology",
		url:"#item-20",
		career:"Engineering, Science & Mathematics",
		degree:"Associate of Applied Science",
		type:["associate-applied-science","specialization","career-studies-certificate"],
		location:["virginia-beach","online"],
		interest:["tooling-with-tech"]
	},
	"mechanical-engineering-technology":{
		id:"mechanical-engineering-technology",
		img:"img/TEMP-IMG-1.jpg",
		title:"Mechanical Engineering Technology",
		url:"#item-21",
		career:"Engineering, Science & Mathematics",
		degree:"Associate of Applied Science",
		type:["associate-applied-science"],
		location:["virginia-beach"],
		interest:["expressing-my-creativity","tooling-with-tech"]
	},
	"science":{
		id:"science",
		img:"img/TEMP-IMG-1.jpg",
		title:"Science",
		url:"#item-22",
		career:"Engineering, Science & Mathematics",
		degree:"Associate of Applied Science",
		type:["associate-science"],
		location:["virginia-beach","chesapeake","norfolk","portsmouth"],
		interest:["tooling-with-tech","crunching-numbers","uncovering-our-world"]
	},
	"veterinary-technology":{
		id:"veterinary-technology",
		img:"img/TEMP-IMG-1.jpg",
		title:"Veterinary Technology",
		url:"#item-23",
		career:"Engineering, Science & Mathematics",
		degree:"Associate of Applied Science",
		type:["associate-applied-science","career-studies-certificate"],
		location:["virginia-beach"],
		interest:["working-with-my-hands","tooling-with-tech"]
	},
	"diagnostic-medical-sonography":{
		id:"diagnostic-medical-sonography",
		img:"img/TEMP-IMG-1.jpg",
		title:"Diagnostic Medical Sonography",
		url:"#item-24",
		career:"Health Professions",
		degree:"Associate of Applied Science",
		type:["associate-applied-science"],
		location:["virginia-beach"],
		interest:["helping-others"]
	},
	"emergency-medical-services":{
		id:"emergency-medical-services",
		img:"img/TEMP-IMG-1.jpg",
		title:"Emergency Medical Services",
		url:"#item-25",
		career:"Health Professions",
		degree:"Associate of Applied Science",
		type:["associate-applied-science","career-studies-certificate"],
		location:["virginia-beach"],
		interest:["helping-others"]
	},
	"health-information-management":{
		id:"health-information-management",
		img:"img/TEMP-IMG-1.jpg",
		title:"Health Information Management",
		url:"#item-26",
		career:"Health Professions",
		degree:"Associate of Applied Science",
		type:["associate-applied-science"],
		location:["virginia-beach"],
		interest:["helping-others"]
	},
	"medical-laboratory-technology":{
		id:"medical-laboratory-technology",
		img:"img/TEMP-IMG-1.jpg",
		title:"Medical Laboratory Technology",
		url:"#item-27",
		career:"Health Professions",
		degree:"Associate of Applied Science",
		type:["associate-applied-science"],
		location:["virginia-beach"],
		interest:["helping-others"]
	},
	"nurse-aide":{
		id:"nurse-aide",
		img:"img/TEMP-IMG-1.jpg",
		title:"Nurse Aide",
		url:"#item-28",
		career:"Health Professions",
		degree:"Associate of Applied Science",
		type:["career-studies-certificate"],
		location:["portsmouth"],
		interest:["helping-others"]
	},
	"nursing":{
		id:"nursing",
		img:"img/TEMP-IMG-1.jpg",
		title:"Nursing",
		url:"#item-29",
		career:"Health Professions",
		degree:"Associate of Applied Science",
		type:["associate-applied-science"],
		location:["portsmouth"],
		interest:["helping-others"]
	},
	"occupational-therapy":{
		id:"occupational-therapy",
		img:"img/TEMP-IMG-1.jpg",
		title:"Occupational Therapy",
		url:"#item-30",
		career:"Health Professions",
		degree:"Associate of Applied Science",
		type:["associate-applied-science"],
		location:["virginia-beach"],
		interest:["helping-others"]
	},
	"pharmacy-technology":{
		id:"pharmacy-technology",
		img:"img/TEMP-IMG-1.jpg",
		title:"Pharmacy Technology",
		url:"#item-31",
		career:"Health Professions",
		degree:"Associate of Applied Science",
		type:["career-studies-certificate"],
		location:["portsmouth"],
		interest:["helping-others"]
	},
	"phlebotomy":{
		id:"phlebotomy",
		img:"img/TEMP-IMG-1.jpg",
		title:"Phlebotomy",
		url:"#item-32",
		career:"Health Professions",
		degree:"Associate of Applied Science",
		type:["career-studies-certificate"],
		location:["virginia-beach"],
		interest:["helping-others"]
	},
	"physical-therapy":{
		id:"physical-therapy",
		img:"img/TEMP-IMG-1.jpg",
		title:"Physical Therapy",
		url:"#item-33",
		career:"Health Professions",
		degree:"Associate of Applied Science",
		type:["associate-applied-science"],
		location:["virginia-beach"],
		interest:["helping-others"]
	},
	"radiography":{
		id:"radiography",
		img:"img/TEMP-IMG-1.jpg",
		title:"Radiography",
		url:"#item-34",
		career:"Health Professions",
		degree:"Associate of Applied Science",
		type:["associate-applied-science","career-studies-certificate"],
		location:["virginia-beach"],
		interest:["helping-others"]
	},
	"respiratory-therapy":{
		id:"respiratory-therapy",
		img:"img/TEMP-IMG-1.jpg",
		title:"Respiratory Therapy",
		url:"#item-35",
		career:"Health Professions",
		degree:"Associate of Applied Science",
		type:["associate-applied-science","career-studies-certificate"],
		location:["virginia-beach"],
		interest:["helping-others"]
	},
	"automotive":{
		id:"automotive",
		img:"img/TEMP-IMG-1.jpg",
		title:"Automotive",
		url:"#item-36",
		career:"Manufacturing & Transportation",
		degree:"Associate of Applied Science",
		type:["associate-applied-science","career-studies-certificate"],
		location:["chesapeake"],
		interest:["working-with-my-hands","tooling-with-tech"]
	},
	"collision-repair":{
		id:"collision-repair",
		img:"img/TEMP-IMG-1.jpg",
		title:"Collision Repair",
		url:"#item-37",
		career:"Manufacturing & Transportation",
		degree:"Associate of Applied Science",
		type:["career-studies-certificate"],
		location:["chesapeake"],
		interest:["working-with-my-hands"]
	},
	"diesel-technology":{
		id:"diesel-technology",
		img:"img/TEMP-IMG-1.jpg",
		title:"Diesel Technology",
		url:"#item-38",
		career:"Manufacturing & Transportation",
		degree:"Associate of Applied Science",
		type:["associate-applied-science","career-studies-certificate"],
		location:["chesapeake"],
		interest:["working-with-my-hands","tooling-with-tech"]
	},
	"machine-technology":{
		id:"machine-technology",
		img:"img/TEMP-IMG-1.jpg",
		title:"Machine Technology",
		url:"#item-39",
		career:"Manufacturing & Transportation",
		degree:"Associate of Applied Science",
		type:["career-studies-certificate"],
		location:["chesapeake"],
		interest:["working-with-my-hands","tooling-with-tech"]
	},
	"marine-gasoline-engine-technology":{
		id:"marine-gasoline-engine-technology",
		img:"img/TEMP-IMG-1.jpg",
		title:"Marine Gasoline Engine Technology",
		url:"#item-40",
		career:"Manufacturing & Transportation",
		degree:"Associate of Applied Science",
		type:["career-studies-certificate"],
		location:["chesapeake"],
		interest:["working-with-my-hands"]
	},
	"mechatronics":{
		id:"mechatronics",
		img:"img/TEMP-IMG-1.jpg",
		title:"Mechatronics",
		url:"#item-41",
		career:"Manufacturing & Transportation",
		degree:"Associate of Applied Science",
		type:["associate-applied-science","career-studies-certificate"],
		location:["chesapeake"],
		interest:["working-with-my-hands","tooling-with-tech"]
	},
	"electrical-technology":{
		id:"electrical-technology",
		img:"img/TEMP-IMG-1.jpg",
		title:"Electrical Technology",
		url:"#item-42",
		career:"Maritime & Skilled Trades",
		degree:"Associate of Applied Science",
		type:["associate-applied-science","career-studies-certificate"],
		location:["chesapeake"],
		interest:["working-with-my-hands","tooling-with-tech"]
	},
	"heating-ventilation-air-conditioning-refrigeration":{
		id:"heating-ventilation-air-conditioning-refrigeration",
		img:"img/TEMP-IMG-1.jpg",
		title:"Heating, Ventilation, Air Conditioning & Refrigeration",
		url:"#item-43",
		career:"Maritime & Skilled Trades",
		degree:"Associate of Applied Science",
		type:["associate-applied-science"],
		location:["portsmouth"],
		interest:["working-with-my-hands","tooling-with-tech"]
	},
	"maritime-technologies":{
		id:"maritime-technologies",
		img:"img/TEMP-IMG-1.jpg",
		title:"Maritime Techologies",
		url:"#item-44",
		career:"Maritime & Skilled Trades",
		degree:"Associate of Applied Science",
		type:["associate-applied-science","career-studies-certificate"],
		location:["chesapeake","portsmouth"],
		interest:["working-with-my-hands","tooling-with-tech"]
	},
	"technical-studies":{
		id:"technical-studies",
		img:"img/TEMP-IMG-1.jpg",
		title:"Technical Studies",
		url:"#item-45",
		career:"Maritime & Skilled Trades",
		degree:"Associate of Applied Science",
		type:["associate-applied-science","specialization"],
		location:["portsmouth"],
		interest:["running-a-business"]
	},
	"truck-driving":{
		id:"truck-driving",
		img:"img/TEMP-IMG-1.jpg",
		title:"Truck Driving",
		url:"#item-46",
		career:"Maritime & Skilled Trades",
		degree:"Associate of Applied Science",
		type:["career-studies-certificate"],
		location:["portsmouth"],
		interest:["working-with-my-hands"]
	},
	"welding":{
		id:"welding",
		img:"img/TEMP-IMG-1.jpg",
		title:"Welding",
		url:"#item-47",
		career:"Maritime & Skilled Trades",
		degree:"Associate of Applied Science",
		type:["career-studies-certificate"],
		location:["portsmouth"],
		interest:["working-with-my-hands"]
	},
	"criminal-justice":{
		id:"criminal-justice",
		img:"img/TEMP-IMG-1.jpg",
		title:"Criminal Justice",
		url:"#item-48",
		career:"Public & Professional Services",
		degree:"Associate of Applied Science",
		type:["associate-applied-science","specialization","career-studies-certificate"],
		location:["virginia-beach","chesapeake","norfolk","portsmouth","online"],
		interest:["helping-others"]
	},
	"early-childhood-development":{
		id:"early-childhood-development",
		img:"img/TEMP-IMG-1.jpg",
		title:"Early Childhood Development",
		url:"#item-49",
		career:"Public & Professional Services",
		degree:"Associate of Applied Science",
		type:["associate-applied-science","career-studies-certificate"],
		location:["virginia-beach","chesapeake","norfolk","portsmouth"],
		interest:["helping-others"]
	},
	"fire-science":{
		id:"fire-science",
		img:"img/TEMP-IMG-1.jpg",
		title:"Fire Science",
		url:"#item-50",
		career:"Public & Professional Services",
		degree:"Associate of Applied Science",
		type:["associate-applied-science","career-studies-certificate"],
		location:["virginia-beach"],
		interest:["working-with-my-hands","helping-others"]
	},
	"funeral-service":{
		id:"funeral-service",
		img:"img/TEMP-IMG-1.jpg",
		title:"Funeral Service",
		url:"#item-51",
		career:"Public & Professional Services",
		degree:"Associate of Applied Science",
		type:["associate-applied-science"],
		location:["virginia-beach"],
		interest:["working-with-my-hands","helping-others"]
	},
	"hospitality-management":{
		id:"hospitality-management",
		img:"img/TEMP-IMG-1.jpg",
		title:"Hospitality Management",
		url:"#item-52",
		career:"Public & Professional Services",
		degree:"Associate of Applied Science",
		type:["associate-applied-science","specialization","career-studies-certificate"],
		location:["virginia-beach"],
		interest:["running-a-business"]
	},
	"interior-design":{
		id:"interior-design",
		img:"img/TEMP-IMG-1.jpg",
		title:"Interior Design",
		url:"#item-53",
		career:"Public & Professional Services",
		degree:"Associate of Applied Science",
		type:["associate-applied-science","career-studies-certificate"],
		location:["chesapeake"],
		interest:["expressing-my-creativity"]
	},
	"paralegal-studies":{
		id:"paralegal-studies",
		img:"img/TEMP-IMG-1.jpg",
		title:"Paralegal Studies",
		url:"#item-54",
		career:"Public & Professional Services",
		degree:"Associate of Applied Science",
		type:["associate-applied-science","career-studies-certificate"],
		location:["virginia-beach"],
		interest:["helping-others"]
	},
	"personal-training-fitness":{
		id:"personal-training-fitness",
		img:"img/TEMP-IMG-1.jpg",
		title:"Personal Training & Fitness",
		url:"#item-55",
		career:"Public & Professional Services",
		degree:"Associate of Applied Science",
		type:["career-studies-certificate"],
		location:["virginia-beach","norfolk"],
		interest:["working-with-my-hands","helping-others"]
	},
	"general-studies":{
		id:"general-studies",
		img:"img/TEMP-IMG-1.jpg",
		title:"General Studies",
		url:"#item-56",
		career:"Social Sciences & Education",
		degree:"Associate of Applied Science",
		type:["associate-science","specialization"],
		location:["virginia-beach","chesapeake","norfolk","portsmouth","online"],
		interest:["uncovering-our-world"]
	},
	"human-services":{
		id:"human-services",
		img:"img/TEMP-IMG-1.jpg",
		title:"Human Services",
		url:"#item-57",
		career:"Social Sciences & Education",
		degree:"Associate of Applied Science",
		type:["associate-applied-science"],
		location:["norfolk","portsmouth"],
		interest:["helping-others"]
	},
	"social-sciences":{
		id:"social-sciences",
		img:"img/TEMP-IMG-1.jpg",
		title:"Social Sciences",
		url:"#item-58",
		career:"Social Sciences & Education",
		degree:"Associate of Applied Science",
		type:["associate-science"],
		location:["virginia-beach","chesapeake","norfolk","portsmouth","online"],
		interest:["uncovering-our-world"]
	}
};

/* program finder predictive search associative keys */
TCCVals.pf_keys = [
	
	{key:"test program",item:"test-program"},
	{key:"test",item:"test-program"},
	
	{key:"American Sign Language",item:"american-sign-language"},
	{key:"ASL",item:"american-sign-language"},
	{key:"sign language",item:"american-sign-language"},
	{key:"deaf interpreter",item:"american-sign-language"},
	{key:"deaf",item:"american-sign-language"},

	{key:"Graphic Design",item:"graphic-design"},
	{key:"graphic designer",item:"graphic-design"},
	{key:"photoshop",item:"graphic-design"},
	{key:"designer",item:"graphic-design"},

	{key:"Liberal Arts",item:"liberal-arts"},

	{key:"Music",item:"music"},
	{key:"musician",item:"music"},
	{key:"performance",item:"music"},
	{key:"performing",item:"music"},

	{key:"Studio Arts",item:"studio-arts"},
	{key:"glass",item:"studio-arts"},
	{key:"glassblowing",item:"studio-arts"},
	{key:"photography",item:"studio-arts"},
	{key:"art therapy",item:"studio-arts"},
	{key:"art therapist",item:"studio-arts"},
	{key:"photographer",item:"studio-arts"},
	{key:"glassblower",item:"studio-arts"},
	{key:"artist",item:"studio-arts"},
	{key:"painting",item:"studio-arts"},
	{key:"ceramics",item:"studio-arts"},

	{key:"Theatre Arts",item:"theatre-arts"},
	{key:"theater arts",item:"theatre-arts"},
	{key:"theater",item:"theatre-arts"},
	{key:"performance art",item:"theatre-arts"},
	{key:"performing",item:"theatre-arts"},
	{key:"performer",item:"theatre-arts"},
	{key:"acting",item:"theatre-arts"},
	{key:"actor",item:"theatre-arts"},

	{key:"Accounting",item:"accounting"},
	{key:"accountant",item:"accounting"},

	{key:"Administrative Support Technology",item:"administrative-support-technology"},
	{key:"executive assistant",item:"administrative-support-technology"},
	{key:"admin assistant",item:"administrative-support-technology"},
	{key:"medical office assistant",item:"administrative-support-technology"},

	{key:"Business Administration",item:"business-administration"},

	{key:"Culinary Arts",item:"culinary-arts"},
	{key:"cooking classes",item:"culinary-arts"},
	{key:"culinary classes",item:"culinary-arts"},
	{key:"chef",item:"culinary-arts"},
	{key:"cook",item:"culinary-arts"},

	{key:"Management",item:"management"},
	{key:"small business owner",item:"management"},

	{key:"Restaurant Management",item:"restaurant-management"},

	{key:"Computer Science",item:"computer-science"},

	{key:"Information Systems Technology",item:"information-systems-technology"},
	{key:"IT",item:"information-systems-technology"},
	{key:"cybersecurity",item:"information-systems-technology"},
	{key:"development",item:"information-systems-technology"},
	{key:"developer",item:"information-systems-technology"},
	{key:"programmer",item:"information-systems-technology"},
	{key:"computing",item:"information-systems-technology"},

	{key:"Civil Engineering Technology",item:"civil-engineering-technology"},
	{key:"surveyor",item:"civil-engineering-technology"},
	{key:"CET",item:"civil-engineering-technology"},

	{key:"Computer-Aided Drafting & Design",item:"computer-aided-drafting-design"},
	{key:"CAD",item:"computer-aided-drafting-design"},
	{key:"CADD",item:"computer-aided-drafting-design"},
	{key:"drafting",item:"computer-aided-drafting-design"},
	{key:"draftsman",item:"computer-aided-drafting-design"},

	{key:"Electronics Technology",item:"electronics-technology"},

	{key:"Engineering",item:"engineering"},
	{key:"engineer",item:"engineering"},

	{key:"Horticulture",item:"horticulture"},
	{key:"landscaper",item:"horticulture"},
	{key:"horticulturalist",item:"horticulture"},

	{key:"Industrial Technology",item:"industrial-technology"},

	{key:"Mechanical Engineering Technology",item:"mechanical-engineering-technology"},

	{key:"Science",item:"science"},

	{key:"Veterinary Technology",item:"veterinary-technology"},
	{key:"animals",item:"veterinary-technology"},
	{key:"vet",item:"veterinary-technology"},
	{key:"veterinarian",item:"veterinary-technology"},

	{key:"Diagnostic Medical Sonography",item:"diagnostic-medical-sonography"},
	{key:"ultrasound",item:"diagnostic-medical-sonography"},
	{key:"sonography",item:"diagnostic-medical-sonography"},
	{key:"sonographer",item:"diagnostic-medical-sonography"},

	{key:"Emergency Medical Services",item:"emergency-medical-services"},
	{key:"EMS",item:"emergency-medical-services"},
	{key:"EMT",item:"emergency-medical-services"},

	{key:"Health Information Management",item:"health-information-management"},

	{key:"Medical Laboratory Technology",item:"medical-laboratory-technology"},
	{key:"lab technician",item:"medical-laboratory-technology"},

	{key:"Nurse Aide",item:"nurse-aide"},
	{key:"CNA",item:"nurse-aide"},

	{key:"Nursing",item:"nursing"},
	{key:"nurse",item:"nursing"},
	{key:"RN",item:"nursing"},
	{key:"LPN",item:"nursing"},

	{key:"Occupational Therapy",item:"occupational-therapy"},
	{key:"therapist",item:"occupational-therapy"},

	{key:"Pharmacy Technology",item:"pharmacy-technology"},

	{key:"Phlebotomy",item:"phlebotomy"},
	{key:"blood drawing",item:"phlebotomy"},
	{key:"blood",item:"phlebotomy"},
	{key:"IV technician",item:"phlebotomy"},

	{key:"Physical Therapy",item:"physical-therapy"},
	{key:"therapist",item:"physical-therapy"},

	{key:"Radiography",item:"radiography"},
	{key:"x-ray tech",item:"radiography"},
	{key:"xray",item:"radiography"},
	{key:"radiologic",item:"radiography"},

	{key:"Respiratory Therapy",item:"respiratory-therapy"},

	{key:"Automotive",item:"automotive"},
	{key:"mechanic",item:"automotive"},

	{key:"Collision Repair",item:"collision-repair"},
	{key:"mechanic",item:"collision-repair"},
	{key:"body work",item:"collision-repair"},

	{key:"Diesel Technology",item:"diesel-technology"},
	{key:"mechanic",item:"diesel-technology"},

	{key:"Machine Technology",item:"machine-technology"},
	{key:"CNC operator",item:"machine-technology"},

	{key:"Marine Gasoline Engine Technology",item:"marine-gasoline-engine-technology"},
	{key:"mechanic",item:"marine-gasoline-engine-technology"},
	{key:"boat mechanic",item:"marine-gasoline-engine-technology"},

	{key:"Mechatronics",item:"mechatronics"},
	{key:"factory",item:"mechatronics"},
	{key:"manufacturing",item:"mechatronics"},

	{key:"Electrical Technology",item:"electrical-technology"},
	{key:"electrician",item:"electrical-technology"},
	{key:"solar power",item:"electrical-technology"},

	{key:"Heating, Ventilation, Air Conditioning & Refrigeration",item:"heating-ventilation-air-conditioning-refrigeration"},
	{key:"HVAC",item:"heating-ventilation-air-conditioning-refrigeration"},

	{key:"Maritime Techologies",item:"maritime-technologies"},
	{key:"journeymans",item:"maritime-technologies"},
	{key:"journeyman",item:"maritime-technologies"},
	{key:"apprenticeship",item:"maritime-technologies"},
	{key:"shipyard",item:"maritime-technologies"},

	{key:"Technical Studies",item:"technical-studies"},

	{key:"Truck Driving",item:"truck-driving"},
	{key:"trucker",item:"truck-driving"},
	{key:"long haul",item:"truck-driving"},
	{key:"CDL",item:"truck-driving"},

	{key:"Welding",item:"welding"},
	{key:"welder",item:"welding"},
	{key:"apprenticeship",item:"welding"},

	{key:"Criminal Justice",item:"criminal-justice"},
	{key:"police officer",item:"criminal-justice"},
	{key:"law enforcement",item:"criminal-justice"},
	{key:"security guard",item:"criminal-justice"},
	{key:"administration of justice,",item:"criminal-justice"},

	{key:"Early Childhood Development",item:"early-childhood-development"},
	{key:"CDA equivalent",item:"early-childhood-development"},
	{key:"preschool teacher",item:"early-childhood-development"},

	{key:"Fire Science",item:"fire-science"},
	{key:"firefighter",item:"fire-science"},
	{key:"fire academy",item:"fire-science"},

	{key:"Funeral Service",item:"funeral-service"},
	{key:"undertaker",item:"funeral-service"},

	{key:"Hospitality Management",item:"hospitality-management"},
	{key:"hotel manager",item:"hospitality-management"},
	{key:"hotel",item:"hospitality-management"},
	{key:"guest services",item:"hospitality-management"},

	{key:"Interior Design",item:"interior-design"},
	{key:"interior designer",item:"interior-design"},
	{key:"interior decorating",item:"interior-design"},
	{key:"decorating",item:"interior-design"},

	{key:"Paralegal Studies",item:"paralegal-studies"},
	{key:"legal assistant",item:"paralegal-studies"},

	{key:"Personal Training & Fitness",item:"personal-training-fitness"},
	{key:"trainor",item:"personal-training-fitness"},
	{key:"personal trainer",item:"personal-training-fitness"},
	{key:"athletic trainer",item:"personal-training-fitness"},
	{key:"fitness instructor",item:"personal-training-fitness"},

	{key:"General Studies",item:"general-studies"},

	{key:"Human Services",item:"human-services"},
	{key:"social work",item:"human-services"},
	{key:"human services",item:"human-services"},

	{key:"Social Sciences",item:"social-sciences"}
];

/* quick tcc program guides */
TCCVals.program_guides = {

	
	"test-program":{label:"Test Program Guide",pdf:"transfer-uva.pdf"},
	
	
	/* specify-transfer */
	"guaranteed-transfer-uva":{label:"Guaranteed Transfer to UVA",pdf:"transfer-uva.pdf"},
	"guaranteed-transfer-vt":{label:"Guaranteed Transfer to Virginia Tech",pdf:"transfer-vt.pdf"},
	"guaranteed-transfer-jmu":{label:"Guaranteed Transfer to JMU",pdf:"transfer-jmu.pdf"},
	"guaranteed-transfer-william-mary":{label:"Guaranteed Transfer to William & Mary",pdf:"transfer-william-mary.pdf"},
	"guaranteed-transfer-nsu":{label:"Guaranteed Transfer to NSU",pdf:"transfer-nsu.pdf"},
	"guaranteed-transfer-odu":{label:"Guaranteed Transfer to ODU",pdf:"transfer-odu.pdf"},
	"guaranteed-transfer-vcu":{label:"Guaranteed Transfer to VCU",pdf:"transfer-vcu.pdf"},
	
	/* specify-skills-arts-humanities */
	"career-pathway-american-sign-language":{label:"American Sign Language",pdf:"american-sign-language.pdf"},
	"career-pathway-graphic-design":{label:"Graphic Design",pdf:"graphic-design.pdf"},
	"career-pathway-music":{label:"Music",pdf:"music.pdf"},
	"career-pathway-studio-arts":{label:"Studio Arts",pdf:"studio-arts.pdf"},
	"career-pathway-theatre-arts":{label:"Theatre Arts",pdf:"theatre-arts.pdf"},
	"career-pathway-arts-humanities":{label:"Arts & Humanities",pdf:"arts-humanities.pdf"},

	/* specify-skills-business */
	"career-pathway-accounting":{label:"Accounting",pdf:"accounting.pdf"},
	"career-pathway-administrative-support-technology":{label:"Administrative Support Technology",pdf:"administrative-support-technology.pdf"},
	"career-pathway-business-admin-management":{label:"Business Admin & Management",pdf:"business-admin-management.pdf"},
	"career-pathway-culinary-arts":{label:"Culinary Arts",pdf:"culinary-arts.pdf"},
	"career-pathway-hospitality-management":{label:"Hospitality Management",pdf:"hospitality-management.pdf"},
	"career-pathway-business":{label:"Business",pdf:"business.pdf"},

	/* specify-skills-engineering-science-math */
	"career-pathway-civil-engineering-technology":{label:"Civil Engineering Technology",pdf:"civil-engineering-technology.pdf"},
	"career-pathway-cadd":{label:"CADD",pdf:"cadd.pdf"},
	"career-pathway-electronics-technology":{label:"Electronics Technology",pdf:"electronics-technology.pdf"},
	"career-pathway-engineering-transfer":{label:"Engineering (transfer)",pdf:"engineering-transfer.pdf"},
	"career-pathway-industrial-technology":{label:"Industrial Technology",pdf:"industrial-technology.pdf"},
	"career-pathway-mechanical-engineering-technology":{label:"Mechanical Engineering Technology",pdf:"mechanical-engineering-technology.pdf"},
	"career-pathway-veterinary-technology":{label:"Veterinary Technology",pdf:"veterinary-technology.pdf"},
	"career-pathway-engineering-science-math":{label:"Engineering, Science & Math",pdf:"engineering-science-math.pdf"},

	/* specify-skills-health-professions */
	"career-pathway-diagnostic-medical-sonography":{label:"Diagnostic Medical Sonography",pdf:"diagnostic-medical-sonography.pdf"},
	"career-pathway-ems":{label:"EMS",pdf:"ems.pdf"},
	"career-pathway-health-information-management":{label:"Health Information Management",pdf:"health-information-management.pdf"},
	"career-pathway-medical-laboratory-technology":{label:"Medical Laboratory Technology",pdf:"medical-laboratory-technology.pdf"},
	"career-pathway-phlebotomy":{label:"Phlebotomy",pdf:"phlebotomy.pdf"},
	"career-pathway-nursing":{label:"Nursing",pdf:"nursing.pdf"},
	"career-pathway-nurse-aide":{label:"Nurse Aide",pdf:"nurse-aide.pdf"},
	"career-pathway-occupational-therapy":{label:"Occupational Therapy",pdf:"occupational-therapy.pdf"},
	"career-pathway-pharmacy-technology":{label:"Pharmacy Technology",pdf:"pharmacy-technology.pdf"},
	"career-pathway-physical-therapy":{label:"Physical Therapy",pdf:"physical-therapy.pdf"},
	"career-pathway-radiography":{label:"Radiography",pdf:"radiography.pdf"},
	"career-pathway-respiratory-therapy":{label:"Respiratory Therapy",pdf:"respiratory-therapy.pdf"},
	"career-pathway-health-professions":{label:"Health Professions",pdf:"health-professions.pdf"},

	/* specify-skills-computer-science-it */
	"career-pathway-computer-science-ist":{label:"Computer Science & IST",pdf:"computer-science-ist.pdf"},
	"career-pathway-cyber-security":{label:"Cyber Security",pdf:"cyber-security.pdf"},
	"career-pathway-computer-science-it":{label:"Computer Science & IT",pdf:"computer-science-it.pdf"},

	/* specify-skills-manufacturing-transportation */
	"career-pathway-automotive-technology":{label:"Automotive Technology",pdf:"automotive-technology.pdf"},
	"career-pathway-collision-repair":{label:"Collision Repair",pdf:"collision-repair.pdf"},
	"career-pathway-diesel-technology":{label:"Diesel Technology",pdf:"diesel-technology.pdf"},
	"career-pathway-machine-technology":{label:"Machine Technology",pdf:"machine-technology.pdf"},
	"career-pathway-marine-gasoline-engine-technology":{label:"Marine Gasoline Engine Technology",pdf:"marine-gasoline-engine-technology.pdf"},
	"career-pathway-mechatronics":{label:"Mechatronics",pdf:"mechatronics.pdf"},
	"career-pathway-truck-driving":{label:"Truck Driving",pdf:"truck-driving.pdf"},
	"career-pathway-manufacturing-transportation":{label:"Manufacturing & Transportation",pdf:"manufacturing-transportation.pdf"},

	/* specify-skills-maritime-skilled-trades */
	"career-pathway-electrical-technology":{label:"Electrical Technology",pdf:"electrical-technology.pdf"},
	"career-pathway-hvac-r":{label:"HVAC/R",pdf:"hvac-r.pdf"},
	"career-pathway-maritime-technologies":{label:"Maritime Technologies",pdf:"maritime-technologies.pdf"},
	"career-pathway-technical-studies":{label:"Technical Studies",pdf:"technical-studies.pdf"},
	"career-pathway-welding":{label:"Welding",pdf:"welding.pdf"},
	"career-pathway-maritime-skilled-trades":{label:"Maritime & Skilled Trades",pdf:"maritime-skilled-trades.pdf"},

	/* specify-skills-public-professional-services */
	"career-pathway-criminal-justice":{label:"Criminal Justice",pdf:"criminal-justice.pdf"},
	"career-pathway-early-childhood-education":{label:"Early Childhood Education",pdf:"early-childhood-education.pdf"},
	"career-pathway-fire-science":{label:"Fire Science",pdf:"fire-science.pdf"},
	"career-pathway-funeral-service":{label:"Funeral Service",pdf:"funeral-service.pdf"},
	"career-pathway-horticulture":{label:"Horticulture",pdf:"horticulture.pdf"},
	"career-pathway-interior-design":{label:"Interior Design",pdf:"interior-design.pdf"},
	"career-pathway-paralegal-studies":{label:"Paralegal Studies",pdf:"paralegal-studies.pdf"},
	"career-pathway-personal-training-fitness":{label:"Personal Training & Fitness",pdf:"personal-training-fitness.pdf"},
	"career-pathway-public-professional-services":{label:"Public & Professional Services",pdf:"public-professional-services.pdf"},
	
	/* specify-skills-social-sciences-education */
	"career-pathway-social-sciences":{label:"Social Sciences",pdf:"social-sciences.pdf"},
	"career-pathway-human-services":{label:"Human Services",pdf:"human-services.pdf"},
	
	/* specify-online */
	"online-transfer":{label:"Online Transfer",pdf:"online-transfer.pdf"},
	"online-degrees":{label:"Online Degrees",pdf:"online-degrees.pdf"},
	
	/* specify-paying */
	"paying-military-benefits":{label:"Using Military Benefits",pdf:"military-benefits.pdf"},
	"paying-financial-aid":{label:"Financial Aid",pdf:"financial-aid.pdf"},
	"paying-scholarships":{label:"Scholarships",pdf:"scholarships.pdf"},

	/* specify-just-class */
	"just-class-enrichment":{label:"Enrichment",pdf:"enrichment.pdf"},
	"just-class-career-advancement":{label:"Career Advancement",pdf:"career-advancement.pdf"},

	/*   */
	"hot-jobs":{title:"<span class=\"alt\">Download our</span> Guide to Hot Jobs",copy:"TCC has a variety of short term programs designed for quick job placement.",pdf:"hot-jobs.pdf"},
	"visiting-students":{title:"<span class=\"alt\">Download our</span> Visiting Students Guide",copy:"Want to take a class (and save some money) while you’re home for the summer - you’re in luck.",pdf:"visiting-students.pdf"},
	"guide-transfer":{title:"<span class=\"alt\">Download our</span> Guide To Transfer",copy:"TCC has guaranteed transfer agreements with Virginia’s best 4-year schools.",pdf:"guide-transfer.pdf"},
	
	/*  */
	"regional-automotive-center":{title:"Download our Guide",pdf:"rac.pdf"},
	
};

/* quick tcc form stages */
TCCVals.qt_stages = {
	"select-section":{
		el:".select-section",
//		callback:function(){
//			if(this.val) _self.to_section = this.val;
//		},
		title:"<span class=\"alt\">I'm most</span> interested in:",
		options:[
			{label:"Transfer",val:"guaranteed-transfer",stage:"specify-transfer",icon:"img/icons/select-transfer.png"},
			{label:"Skills for a job",val:"short-term-programs",stage:"specify-skills",icon:"img/icons/select-skills.png"},
			{label:"Online Classes",val:"online-classes",stage:"specify-online",icon:"img/icons/select-online.png"},
			{label:"Paying for college",val:"paying-for-college",stage:"specify-paying",icon:"img/icons/select-paying.png"},
			{label:"Taking a class or two",val:"individual-classes",stage:"specify-just-class",icon:"img/icons/select-just-class.png"},
			{label:"One on one help",val:"individual-classes",stage:"forward-to-page",icon:"img/icons/select-one-on-one.png"}
		],
		btns:{next:""},
		wrap_class:"on-form on-select-section"
	},
	/*----*/
	"specify-transfer":{
		el:".specify-interest",
		title:"<span class=\"alt\">Where do you want to</span> transfer?",
		options:[
			{label:"I know where I want to go",stage:"specify-transfer-decided",icon:"img/icons/transfer-decided.png"},
			{label:"Not sure, what are my options?",val:"guide-transfer",stage:"forward-to-page",icon:"img/icons/unsure.png"},
			{label:"I am a currently enrolled 4-year student",val:"visiting-students",stage:"forward-to-page",icon:"img/icons/transfer-enrolled.png"}
		],
//		btns:{prev:"select-section",next:"",skip:"hide-form"},
		btns:{prev:"select-section",next:""},
		wrap_class:"on-form on-specify-interest"
	},
	"specify-transfer-decided":{
		el:".specify-interest",
		title:"<span class=\"alt\">Where do you want to</span> transfer?",
		options:[
			{val:"guaranteed-transfer-uva",stage:"forward-to-page",icon:"img/icons/uva.png",css:"logo"},
			{val:"guaranteed-transfer-vt",stage:"forward-to-page",icon:"img/icons/vt.png",css:"logo"},
			{val:"guaranteed-transfer-odu",stage:"forward-to-page",icon:"img/icons/odu.png",css:"logo"},
			{val:"guaranteed-transfer-nsu",stage:"forward-to-page",icon:"img/icons/nsu.png",css:"logo"},
			{val:"guaranteed-transfer-jmu",stage:"forward-to-page",icon:"img/icons/jmu.png",css:"logo"},
			{val:"guaranteed-transfer-vcu",stage:"forward-to-page",icon:"img/icons/vcu.png",css:"logo"},
			{val:"guaranteed-transfer-william-mary",stage:"forward-to-page",icon:"img/icons/wm.png",css:"logo"},
			{label:"Other / Not Listed",val:"guide-transfer",stage:"forward-to-page",css:"logo"},
		],
//		btns:{prev:"specify-transfer",next:"",skip:"hide-form"},
		btns:{prev:"specify-transfer",next:""},
		wrap_class:"on-form on-specify-interest large"
	},
	/*----*/
	"specify-skills":{
		el:".specify-interest",
		title:"<span class=\"alt\">What Career Pathway are you</span> interested in?",
		options:[
			{label:"Arts & Humanities",stage:"specify-skills-arts-humanities",icon:"img/icons/skills-arts-humanities.png"},
			{label:"Business",stage:"specify-skills-business",icon:"img/icons/skills-business.png"},
			{label:"Computer Science & IT",stage:"specify-skills-computer-science-it",icon:"img/icons/skills-computer-science-it.png"},
			{label:"Engineering, Science & Math",stage:"specify-skills-engineering-science-math",icon:"img/icons/skills-engineering-science-math.png"},
			{label:"Health Professions",stage:"specify-skills-health-professions",icon:"img/icons/skills-health-professions.png"},
			{label:"Manufacturing & Transportation",stage:"specify-skills-manufacturing-transportation",icon:"img/icons/skills-manufacturing-transportation.png"},
			{label:"Maritime & Skilled Trades",stage:"specify-skills-maritime-skilled-trades",icon:"img/icons/skills-maritime-skilled-trades.png"},
			{label:"Public & Professional Services",stage:"specify-skills-public-professional-services",icon:"img/icons/skills-public-professional-services.png"},
			{label:"Social Sciences & Education",stage:"specify-skills-social-sciences-education",icon:"img/icons/skills-social-sciences-education.png"},
			{label:"Not sure, what are my options?",val:"hot-jobs",stage:"forward-to-page",icon:"img/icons/unsure.png"}
		],
//		btns:{prev:"select-section",next:"",skip:"hide-form"},
		btns:{prev:"select-section",next:""},
		wrap_class:"on-form on-specify-interest"
	},
	"specify-skills-arts-humanities":{
		el:".specify-interest",
		title:"<span class=\"alt\">What program are you</span> interested in?",
		options:[
			{label:"American Sign Language",val:"career-pathway-american-sign-language",stage:"forward-to-page",icon:"img/icons/american-sign-language.png"},
			{label:"Graphic Design",val:"career-pathway-graphic-design",stage:"forward-to-page",icon:"img/icons/graphic-design.png"},
			{label:"Music",val:"career-pathway-music",stage:"forward-to-page",icon:"img/icons/music.png"},
			{label:"Studio Arts",val:"career-pathway-studio-arts",stage:"forward-to-page",icon:"img/icons/studio-arts.png"},
			{label:"Theatre Arts",val:"career-pathway-theatre-arts",stage:"forward-to-page",icon:"img/icons/theatre-arts.png"},
			{label:"Not Sure?",val:"career-pathway-arts-humanities",stage:"forward-to-page",icon:"img/icons/unsure.png"},
		],
//		btns:{prev:"specify-skills",next:"",skip:"hide-form"},
		btns:{prev:"specify-skills",next:""},
		wrap_class:"on-form on-specify-interest"
	},
	"specify-skills-business":{
		el:".specify-interest",
		title:"<span class=\"alt\">What program are you</span> interested in?",
		options:[
			{label:"Accounting",val:"career-pathway-accounting",stage:"forward-to-page",icon:"img/icons/accounting.png"},
			{label:"Administrative Support Technology",val:"career-pathway-administrative-support-technology",stage:"forward-to-page",icon:"img/icons/administrative-support-technology.png"},
			{label:"Business Admin & Management",val:"career-pathway-business-admin-management",stage:"forward-to-page",icon:"img/icons/business-admin-management.png"},
			{label:"Culinary Arts",val:"career-pathway-culinary-arts",stage:"forward-to-page",icon:"img/icons/culinary-arts.png"},
			{label:"Hospitality Management",val:"career-pathway-hospitality-management",stage:"forward-to-page",icon:"img/icons/hospitality-management.png"},
			{label:"Not Sure?",val:"career-pathway-business",stage:"forward-to-page",icon:"img/icons/unsure.png"},
		],
//		btns:{prev:"specify-skills",next:"",skip:"hide-form"},
		btns:{prev:"specify-skills",next:""},
		wrap_class:"on-form on-specify-interest"
	},
	"specify-skills-computer-science-it":{
		el:".specify-interest",
		title:"<span class=\"alt\">What program are you</span> interested in?",
		options:[
			{label:"Computer Science & IST",val:"career-pathway-computer-science-ist",stage:"forward-to-page",icon:"img/icons/computer-science-ist.png"},
			{label:"Cyber Security",val:"career-pathway-cyber-security",stage:"forward-to-page",icon:"img/icons/cyber-security.png"},
			{label:"Not Sure?",val:"career-pathway-computer-science-it",stage:"forward-to-page",icon:"img/icons/unsure.png"},
		],
//		btns:{prev:"specify-skills",next:"",skip:"hide-form"},
		btns:{prev:"specify-skills",next:""},
		wrap_class:"on-form on-specify-interest"
	},
	"specify-skills-engineering-science-math":{
		el:".specify-interest",
		title:"<span class=\"alt\">What program are you</span> interested in?",
		options:[
			{label:"Civil Engineering Technology",val:"career-pathway-civil-engineering-technology",stage:"forward-to-page",icon:"img/icons/civil-engineering-technology.png"},
			{label:"CADD",val:"career-pathway-cadd",stage:"forward-to-page",icon:"img/icons/cadd.png"},
			{label:"Electronics Technology",val:"career-pathway-electronics-technology",stage:"forward-to-page",icon:"img/icons/electronics-technology.png"},
			{label:"Engineering (transfer)",val:"career-pathway-engineering-transfer",stage:"forward-to-page",icon:"img/icons/engineering-transfer.png"},
			{label:"Industrial Technology",val:"career-pathway-industrial-technology",stage:"forward-to-page",icon:"img/icons/industrial-technology.png"},
			{label:"Mechanical Engineering Technology",val:"career-pathway-mechanical-engineering-technology",stage:"forward-to-page",icon:"img/icons/mechanical-engineering-technology.png"},
			{label:"Veterinary Technology",val:"career-pathway-veterinary-technology",stage:"forward-to-page",icon:"img/icons/veterinary-technology.png"},
			{label:"Not Sure?",val:"career-pathway-engineering-science-math",stage:"forward-to-page",icon:"img/icons/unsure.png"},
		],
//		btns:{prev:"specify-skills",next:"",skip:"hide-form"},
		btns:{prev:"specify-skills",next:""},
		wrap_class:"on-form on-specify-interest"
	},
	"specify-skills-health-professions":{
		el:".specify-interest",
		title:"<span class=\"alt\">What program are you</span> interested in?",
		options:[
			{label:"Diagnostic Medical Sonography",val:"career-pathway-diagnostic-medical-sonography",stage:"forward-to-page",icon:"img/icons/diagnostic-medical-sonography.png"},
			{label:"EMS",val:"career-pathway-ems",stage:"forward-to-page",icon:"img/icons/ems.png"},
			{label:"Health Information Management",val:"career-pathway-health-information-management",stage:"forward-to-page",icon:"img/icons/health-information-management.png"},
			{label:"Medical Laboratory Technology",val:"career-pathway-medical-laboratory-technology",stage:"forward-to-page",icon:"img/icons/medical-laboratory-technology.png"},
			{label:"Nursing",val:"career-pathway-nursing",stage:"forward-to-page",icon:"img/icons/nursing.png"},
			{label:"Nurse Aide",val:"career-pathway-nurse-aide",stage:"forward-to-page",icon:"img/icons/nurse-aide.png"},
			{label:"Occupational Therapy",val:"career-pathway-occupational-therapy",stage:"forward-to-page",icon:"img/icons/occupational-therapy.png"},
			{label:"Pharmacy Technology",val:"career-pathway-pharmacy-technology",stage:"forward-to-page",icon:"img/icons/pharmacy-technology.png"},
			{label:"Phlebotomy",val:"career-pathway-phlebotomy",stage:"forward-to-page",icon:"img/icons/phlebotomy.png"},
			{label:"Physical Therapy",val:"career-pathway-physical-therapy",stage:"forward-to-page",icon:"img/icons/physical-therapy.png"},
			{label:"Radiography",val:"career-pathway-radiography",stage:"forward-to-page",icon:"img/icons/radiography.png"},
			{label:"Respiratory Therapy",val:"career-pathway-respiratory-therapy",stage:"forward-to-page",icon:"img/icons/respiratory-therapy.png"},
			{label:"Not Sure?",val:"career-pathway-health-professions",stage:"forward-to-page",icon:"img/icons/unsure.png"},
		],
//		btns:{prev:"specify-skills",next:"",skip:"hide-form"},
		btns:{prev:"specify-skills",next:""},
		wrap_class:"on-form on-specify-interest"
	},
	"specify-skills-manufacturing-transportation":{
		el:".specify-interest",
		title:"<span class=\"alt\">What program are you</span> interested in?",
		options:[
			{label:"Automotive Technology",val:"career-pathway-automotive-technology",stage:"forward-to-page",icon:"img/icons/automotive-technology.png"},
			{label:"Collision Repair",val:"career-pathway-collision-repair",stage:"forward-to-page",icon:"img/icons/collision-repair.png"},
			{label:"Diesel Technology",val:"career-pathway-diesel-technology",stage:"forward-to-page",icon:"img/icons/diesel-technology.png"},
			{label:"Machine Technology",val:"career-pathway-machine-technology",stage:"forward-to-page",icon:"img/icons/machine-technology.png"},
			{label:"Marine Gasoline Engine Technology",val:"career-pathway-marine-gasoline-engine-technology",stage:"forward-to-page",icon:"img/icons/marine-gasoline-engine-technology.png"},
			{label:"Mechatronics",val:"career-pathway-mechatronics",stage:"forward-to-page",icon:"img/icons/mechatronics.png"},
			{label:"Not Sure?",val:"career-pathway-manufacturing-transportation",stage:"forward-to-page",icon:"img/icons/unsure.png"},
		],
//		btns:{prev:"specify-skills",next:"",skip:"hide-form"},
		btns:{prev:"specify-skills",next:""},
		wrap_class:"on-form on-specify-interest"
	},
	"specify-skills-maritime-skilled-trades":{
		el:".specify-interest",
		title:"<span class=\"alt\">What program are you</span> interested in?",
		options:[
			{label:"Electrical Technology",val:"career-pathway-electrical-technology",stage:"forward-to-page",icon:"img/icons/electrical-technology.png"},
			{label:"HVAC/R",val:"career-pathway-hvac-r",stage:"forward-to-page",icon:"img/icons/hvac-r.png"},
			{label:"Maritime Technologies",val:"career-pathway-maritime-technologies",stage:"forward-to-page",icon:"img/icons/maritime-technologies.png"},
			{label:"Technical Studies",val:"career-pathway-technical-studies",stage:"forward-to-page",icon:"img/icons/technical-studies.png"},
			{label:"Truck Driving",val:"career-pathway-truck-driving",stage:"forward-to-page",icon:"img/icons/truck-driving.png"},
			{label:"Welding",val:"career-pathway-welding",stage:"forward-to-page",icon:"img/icons/welding.png"},
			{label:"Not Sure?",val:"career-pathway-maritime-skilled-trades",stage:"forward-to-page",icon:"img/icons/unsure.png"},
		],
//		btns:{prev:"specify-skills",next:"",skip:"hide-form"},
		btns:{prev:"specify-skills",next:""},
		wrap_class:"on-form on-specify-interest"
	},
	"specify-skills-public-professional-services":{
		el:".specify-interest",
		title:"<span class=\"alt\">What program are you</span> interested in?",
		options:[
			{label:"Criminal Justice",val:"career-pathway-criminal-justice",stage:"forward-to-page",icon:"img/icons/criminal-justice.png"},
			{label:"Early Childhood Education",val:"career-pathway-early-childhood-education",stage:"forward-to-page",icon:"img/icons/early-childhood-education.png"},
			{label:"Fire Science",val:"career-pathway-fire-science",stage:"forward-to-page",icon:"img/icons/fire-science.png"},
			{label:"Funeral Service",val:"career-pathway-funeral-service",stage:"forward-to-page",icon:"img/icons/funeral-service.png"},
			{label:"Horticulture",val:"career-pathway-horticulture",stage:"forward-to-page",icon:"img/icons/horticulture.png"},
			{label:"Interior Design",val:"career-pathway-interior-design",stage:"forward-to-page",icon:"img/icons/interior-design.png"},
			{label:"Paralegal Studies",val:"career-pathway-paralegal-studies",stage:"forward-to-page",icon:"img/icons/paralegal-studies.png"},
			{label:"Personal Training & Fitness",val:"career-pathway-personal-training-fitness",stage:"forward-to-page",icon:"img/icons/personal-training-fitness.png"},
			{label:"Not Sure?",val:"career-pathway-public-professional-services",stage:"forward-to-page",icon:"img/icons/unsure.png"},
		],
//		btns:{prev:"specify-skills",next:"",skip:"hide-form"},
		btns:{prev:"specify-skills",next:""},
		wrap_class:"on-form on-specify-interest"
	},
	"specify-skills-social-sciences-education":{
		el:".specify-interest",
		title:"<span class=\"alt\">What program are you</span> interested in?",
		options:[
			{label:"Human Services",val:"career-pathway-human-services",stage:"forward-to-page",icon:"img/icons/human-services.png"},
			{label:"Social Sciences",val:"career-pathway-social-sciences",stage:"forward-to-page",icon:"img/icons/skills-social-sciences-education.png"},
		],
//		btns:{prev:"specify-skills",next:"",skip:"hide-form"},
		btns:{prev:"specify-skills",next:""},
		wrap_class:"on-form on-specify-interest"
	},
	/*----*/
	"specify-online":{
		el:".specify-interest",
		title:"I’d like to take online classes to:",
		options:[
			{label:"Earn Credits and eventually transfer to another College or University",val:"online-transfer",stage:"forward-to-page",icon:"img/icons/online-transfer.png"},
			{label:"Earn my degree or certificate at TCC",val:"online-degrees",stage:"forward-to-page",icon:"img/icons/online-degrees.png"},
		],
//		btns:{prev:"select-section",next:"",skip:"hide-form"},
		btns:{prev:"select-section",next:""},
		wrap_class:"on-form on-specify-interest"
	},
	/*----*/
	"specify-paying":{
		el:".specify-interest",
		title:"I’d like to learn more about:",
		options:[
			{label:"Using Military Benefits",val:"paying-military-benefits",stage:"forward-to-page",icon:"img/icons/paying-military-benefits.png"},
			{label:"Applying for Financial Aid",val:"paying-financial-aid",stage:"forward-to-page",icon:"img/icons/paying-financial-aid.png"},
			{label:"Scholarships",val:"paying-scholarships",stage:"forward-to-page",icon:"img/icons/paying-scholarships.png"},
		],
//		btns:{prev:"select-section",next:"",skip:"hide-form"},
		btns:{prev:"select-section",next:""},
		wrap_class:"on-form on-specify-interest"
	},
	/*----*/
	"specify-just-class":{
		el:".specify-interest",
		title:"<span class=\"alt\">What are you</span> interested in?",
		options:[
			{label:"Fun/Enrichment",val:"just-class-enrichment",stage:"forward-to-page",icon:"img/icons/just-class-enrichment.png"},
			{label:"Career Advancement",val:"just-class-career-advancement",stage:"forward-to-page",icon:"img/icons/just-class-career-advancement.png"},
			{label:"Pick Up A Class To Transfer",val:"visiting-students",stage:"forward-to-page",icon:"img/icons/just-class-visiting-students.png"},
		],
//		btns:{prev:"select-section",next:"",skip:"hide-form"},
		btns:{prev:"select-section",next:""},
		wrap_class:"on-form on-specify-interest"
	},
	/*----* /
	"download-rac-guide":{
		el:".download-guide",
		init:function(){
//			var _guide = _self.guides["regional-automotive-center"];
//			$(this.el).removeClass("show-errors").find(".title").html(_guide.title?_guide.title:"Download our guide on <span class=\"item alt\">"+_guide.label+"</span>");
//			if(_guide.copy) $(this.el).find(".title").after("<p>"+_guide.copy+"</p>");
//			$(this.el).find(".btns .btn._download").val(_guide.pdf).data("val","regional-automotive-center");
		},
		title:" ",
		inputs:[
			//{label:"Email Address",type:"email",name:"guide_addr",required:true,validate:"email",autocomplete:"email"}
			{label:"First Name",name:"guide_fname",required:true,validate:"empty",autocomplete:"given-name"},
			{label:"Last Name",name:"guide_lname",required:true,validate:"empty",autocomplete:"family-name"},
			{label:"Email Address",type:"email",name:"guide_email",required:true,validate:"email",autocomplete:"email"},
			{label:"Phone Number",type:"tel",name:"guide_phone",required:true,validate:"phone",autocomplete:"tel"}
		],
//		btns:{download:"",skip:"hide-form"},
		btns:{download:""},
		wrap_class:"on-form on-download-guide"
	},
	/ *----*/
	"forward-to-page":{
		halt:function(_form,_item){
			tcc.toPage(_form.on_stage.val);
		}
	},
/*	"download-guide":{
		el:".download-guide",
		init:function(_form,_item){
//			var _guide = _self.guides[_item];
//			$(this.el).removeClass("show-errors").find(".title").html(_guide.title?_guide.title:"Download our guide on <span class=\"item alt\">"+_guide.label+"</span>");
//			if(_guide.copy) $(this.el).find(".title").after("<p>"+_guide.copy+"</p>");
//			$(this.el).find(".btns .btn._download").val(_guide.pdf).data("val",_item);
			var _guide = TCCVals.program_guides[_item];
			_form.el.find(this.el).removeClass("show-errors").find(".title").html(_guide.title?_guide.title:"<span class=\"alt\">Download our guide on</span> "+_guide.label);
			if(_guide.copy) _form.el.find(this.el+" .title").after("<p>"+_guide.copy+"</p>");
			_form.el.find(this.el+" .btns .btn._download").val(_guide.pdf).data("val",_item);
			
		},
		title:" ",
		inputs:[
			{label:"Email Address",type:"email",name:"guide_email",required:true,validate:"email",autocomplete:"email"}
		],
//		btns:{download:"",skip:"hide-form"},
		btns:{prev:"select-section",download:""},
		wrap_class:"on-form on-download-guide"
	},
	"form-disable":{
		el:".download-guide",
		halt:function(_form){
//			$(this.el).addClass("disabled");
//			$(this.el).find(".inputs .input").addClass("disabled").attr("disabled","disabled");
//			$(this.el).find(".btns .btn._download").addClass("disabled waiting").attr("disabled","disabled");
			_form.el.find(this.el).addClass("disabled");
			_form.el.find(this.el+" .inputs .input").addClass("disabled").attr("disabled","disabled");
			_form.el.find(this.el+" .btns .btn._download").addClass("disabled waiting").attr("disabled","disabled");
		}
	},
	"form-error":{
		el:".download-guide",
		halt:function(_form){
//			$(this.el).addClass("show-errors");
			_form.el.find(this.el).addClass("show-errors");
		}
	},
	"form-retry":{
		el:".download-guide",
		halt:function(_form){
//			$(this.el).removeClass("disabled");
//			$(this.el).find(".inputs .input").removeClass("disabled").removeAttr("disabled");
//			$(this.el).find(".btns .btn._download").removeClass("disabled waiting").removeAttr("disabled");
			_form.el.find(this.el).removeClass("disabled");
			_form.el.find(this.el).find(".inputs .input").removeClass("disabled").removeAttr("disabled");
			_form.el.find(this.el).find(".btns .btn._download").removeClass("disabled waiting").removeAttr("disabled");
		}
	},
	"form-thanks":{
		el:".download-guide",
		init:function(_form){
//			$(this.el).removeClass("disabled");
			_form.el.find(this.el).removeClass("disabled");
		},
//		title:"<span class=\"alt\">Thank you</span>",
		title:"Thank you",
		copy:"Thank you for your interest in TCC and downloading this guide!</p><p>If you have additional questions, please contact our New Student Support Team at <a href=\"mailto:enroll@tcc.edu\">enroll@tcc.edu</a> or by calling <a href=\"tel:757-822-1111\">757-822-1111</a> – we are here to help!",
		btns:{back:"select-section",go:"hide-form"},
		wrap_class:"on-form on-download-guide"
	},
	"request-info":{
		halt:function(_form){
//			_self.showModal();
			//alert("request-info");
			tcc.toPage("contact");
		}
	},
	"hide-form":{
		halt:function(_form){
//			if(_self.to_section) _self.setSection(_self.to_section,true);
//			if($("body").hasClass("mobile")){
//				_self.hideForm();
//			}
			alert("hide-form");
		}
	}//,*/
	//"section-page":{}
};


TCCVals.sidebar_stages = {
	"sidebar-download":{
		el:".download-guide",
		init:function(_form,_item){
			
			console.log("init sidebar form download guide");
			console.log(_form);
			console.log(_item);
			
			var _guide = TCCVals.program_guides[_item];
			//$(this.el).removeClass("show-errors").find(".title").html(_guide.title?_guide.title:"Download our guide on <span class=\"item alt\">"+_guide.label+"</span>");
			$(this.el).removeClass("show-errors");
			//if(_guide.copy) $(this.el).find(".title").after("<p>"+_guide.copy+"</p>");
			$(this.el).find(".btns .btn._download").val(_guide.pdf);
			
		},
		//title:" ",
		inputs:[
			//{label:"Email Address",type:"email",name:"guide_email",required:true,validate:"email",autocomplete:"email"}
			{label:"First Name",name:"guide_fname",required:true,validate:"empty",autocomplete:"given-name"},
			{label:"Last Name",name:"guide_lname",required:true,validate:"empty",autocomplete:"family-name"},
			{label:"Email Address",type:"email",name:"guide_email",required:true,validate:"email",autocomplete:"email"},
			{label:"Phone Number",type:"tel",name:"guide_phone",required:true,validate:"phone",autocomplete:"tel"}
		],
		btns:{download:""},
		btn_class:"btn-2 btn-min"
	},
	"form-disable":{
		el:".download-guide",
		halt:function(){
			$(this.el).addClass("disabled");
			$(this.el).find(".inputs .input").addClass("disabled").attr("disabled","disabled");
			$(this.el).find(".btns .btn._download").addClass("disabled waiting").attr("disabled","disabled");
		}
	},
	"form-error":{
		el:".download-guide",
		halt:function(){
			$(this.el).addClass("show-errors");
		}
	},
	"form-retry":{
		el:".download-guide",
		halt:function(){
			$(this.el).removeClass("disabled");
			$(this.el).find(".inputs .input").removeClass("disabled").removeAttr("disabled");
			$(this.el).find(".btns .btn._download").removeClass("disabled waiting").removeAttr("disabled");
		}
	},
	"form-thanks":{
		el:".download-guide",
		init:function(){
			$(this.el).removeClass("disabled");
		},
		title:"<span class=\"alt\">Thank you</span>",
		copy:"Thank you for your interest in TCC and downloading this guide!</p><p>If you have additional questions, please contact our New Student Support Team at <a href=\"mailto:enroll@tcc.edu\">enroll@tcc.edu</a> or by calling <a href=\"tel:757-822-1111\">757-822-1111</a> – we are here to help!",
	}
};