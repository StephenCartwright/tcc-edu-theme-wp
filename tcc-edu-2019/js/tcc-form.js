/*
	TCC.edu
*/

function TCCForm(_id,_el,_stages,_submit){
	console.log("new TCCForm");
	this.id = _id||"tcc-form";
	this.el = _el;
	this.stages = _stages||{};
	this.on_stage;
	this.submit = _submit;
	
	this.setStage = function(_stage,_vals){
		console.log("tccform "+this.id+" setStage: "+_stage);
		console.log(this.stages[_stage]);
		console.log(_vals?_vals:"no vals");
		var _stage = this.stages[_stage];
		var _stage_el = this.el.find(_stage.el);
		var _vals = _vals||{};
		if(_stage.halt) return _stage.halt(this,_vals);
		if(_stage.el&&_stage_el.length){
			var _html = _stage.title?"<div class=\"page-title-wrap\"><div class=\"title page-title nonh1\">"+_stage.title+"</div></div>":"";
			_html += _stage.title_2?"<div class=\"title nonh2\">"+_stage.title_2+"</div>":"";
			_html += _stage.copy?"<p>"+_stage.copy+"</p>":"";
			if(_stage.options) _html += this.buildStageOptions(_stage);
			if(_stage.inputs) _html += this.buildStageInputs(_stage);
			if(_stage.btns) _html += this.buildStageButtons(_stage);
			if(_stage.inputs) _html += "<div class=\"disclaimer\"><a href=\"https://www.tcc.edu/about-tcc/website-privacy-statement\" target=\"_blank\" tabindex=\"0\">We value your privacy</a></a>";
			if(_stage.captcha) _html += "<div class=\"g-recaptcha\" data-sitekey=\"6LeC5lYUAAAAAEWjsSDmzlMnyk0qmpviKFVNcY-i\" data-callback=\"captchaCB\" data-size=\"invisible\" tabindex=\"0\"></div>";
			_stage_el.html(_html);
			this.bindStage(_stage);
			_stage_el.stop(false,false).css({opacity:0}).animate({opacity:1},300);
			if(_stage.options) setTimeout(function(){ this.setOptionsHeight(_stage); }.bind(this),10);
		}
		if(_stage.init) _stage.init(this,_vals.val?_vals.val:(this.on_stage?this.on_stage.val:null));
		if(this.on_stage&&this.on_stage.wrap_class) this.el.removeClass(this.on_stage.wrap_class)
		if(this.on_stage&&this.on_stage.callback) this.on_stage.callback();
		this.el.addClass(_stage.wrap_class);
		this.on_stage = _stage;
		this.on_stage_el = _stage_el;
	};
	
	this.buildStageOptions = function(_stage){
		var _html = "<div class=\"form-wrap list"+(_stage.options.length>9?" long":"")+"\"><div class=\"options\">";
		for(var _o in _stage.options){
			var _opt = _stage.options[_o];
			_html += "<div class=\"o_wrap"+(_opt.css?" "+_opt.css:"")+"\"><div class=\"i_wrap\"><div class=\"option no-select\""+(_opt.stage?" data-stage=\""+_opt.stage+"\"":"")+(_opt.val?" data-val=\""+_opt.val+"\"":"")+" title=\""+_opt.label+"\" tabindex=\"0\">"+(_opt.icon?"<img class=\"icon\" src=\""+window.theme_dir+_opt.icon+"\" alt=\"icon\"/>":"")+(_opt.label?"<label class=\"nonh5\">"+_opt.label+"</label>":"")+"</div></div></div>";
		}
		_html += "</div></div>";
		setTimeout(function(){ this.setOptionsHeight(_stage); }.bind(this),10);
		return _html;
	};
	
	this.buildStageInputs = function(_stage){
		console.log("tccForm buildStageInputs");
		console.log(_stage);
		var _rq;
		var _html = "<div class=\"form-wrap\"><div class=\"inputs\">";
		for(var _i in _stage.inputs){
			var _inp = _stage.inputs[_i];
			var _id = this.id+"-"+(_inp.name?_inp.name:_i);
			_html += "<div class=\"input_wrap\"><label class=\"_comp\" for=\""+_id+"\">"+_inp.label+"</label>";
			if(_inp.required) _rq = true;
			if(_inp.type=="textarea"){
				_html += "<textarea id=\""+_id+"\" class=\"input"+(_rq?" required":"")+"\""+(_inp.name?" name=\""+_inp.name+"\"":"")+(_inp.label?" aria-label=\""+_inp.label+"\" placeholder=\""+_inp.label+(_rq&&_stage.inputs.length>1?" *":"")+"\"":"")+(_rq?" aria-required=\"true\" aria-invalid=\"false\" aria-describedby=\""+_id+"-error\" data-required=\"true\"":"")+(_inp.validate?" data-validate=\""+_inp.validate+"\"":"")+" maxlength=\"500\" tabindex=\"0\">"+(_inp.icon?"<img class=\"icon\" src=\""+window.theme_dir+_inp.icon+"\" alt=\"icon\"/>":"")+(_inp.value?_inp.value:"")+"</textarea>";
			} else {
				_html += "<input id=\""+_id+"\""+(_inp.name?" name=\""+_inp.name+"\"":"")+(_inp.type?" type=\""+_inp.type+"\"":"")+" class=\"input"+(_rq?" required":"")+"\""+(_inp.name?" name=\""+_inp.name+"\"":"")+(_inp.label?" aria-label=\""+_inp.label+"\" placeholder=\""+_inp.label+(_rq&&_stage.inputs.length>1?" *":"")+"\"":"")+(_rq?" aria-required=\"true\" aria-invalid=\"false\" aria-describedby=\""+_id+"-error\" data-required=\"true\"":"")+(_inp.validate?" data-validate=\""+_inp.validate+"\"":"")+(_inp.autocomplete?" autocomplete=\""+_inp.autocomplete+"\"":"")+(_inp.type=="tel"?" maxlength=\"12\"":"")+" tabindex=\"0\">"+(_inp.icon?"<img class=\"icon\" src=\""+window.theme_dir+_inp.icon+"\" alt=\"icon\"/>":"")+(_inp.value?_inp.value:"")+"</input>";
			}
			_html += "</div>";
		}
		if(_rq&&_stage.inputs.length>1) _html += "<p class=\"sub req\"><em>* Required Fields</em></p>";
		_html += "</div></div>";
		return _html;
	};
	
	this.buildStageButtons = function(_stage){
		var _html = "<div class=\"btns\">";
		var _btn_class = _stage.btn_class+" "||"";
		for(var _b in _stage.btns){
			if(_b=="prev"){
				_html += "<button class=\"btn btn-3 basic btn-arrow-left _prev\" value=\""+_stage.btns[_b]+"\" tabindex=\"0\">Go Back</button>";
			} else if(_b=="next"){
				_html += "<button class=\"btn "+_btn_class+"btn-arrow _next disabled\" value=\""+_stage.btns[_b]+"\" tabindex=\"0\">Next Step</button>";
			} else if(_b=="skip"){
				_html += "<button class=\"btn basic btn-3 btn-arrow slim _skip\" value=\""+_stage.btns[_b]+"\" tabindex=\"0\">Skip</button>";
			} else if(_b=="back"){
				_html += "<button class=\"btn basic btn-3 btn-arrow-left slim _back\" value=\""+_stage.btns[_b]+"\" tabindex=\"0\">Back</button>";
			} else if(_b=="submit"){
				_html += "<button class=\"btn "+_btn_class+"btn-arrow _submit disabled\" value=\""+_stage.btns[_b]+"\" tabindex=\"0\">Submit</button>";
			} else if(_b=="download"){
				_html += "<button class=\"btn "+_btn_class+"btn-download _download disabled\" value=\""+_stage.btns[_b]+"\" tabindex=\"0\">Download</button>";
			} else if(_b=="go"){
				_html += "<button class=\"btn basic btn-3 btn-arrow _go\" value=\""+_stage.btns[_b]+"\" tabindex=\"0\">Continue</button>";
			} else {
				_html += "<button class=\"btn\" value=\""+_stage.btns[_b]+"\" tabindex=\"0\">"+_b+"</button>";
			}
		}
		_html += "</div>";
		return _html;
	};
	
	this.bindStage = function(_stage){
		console.log("tccform "+this.id+" bindStage");
		var _form = this;
		var _stage_el = this.el.find(_stage.el);
		_stage_el.find(".options .option").on("click",function(){
			_form.selOption($(this));
		}).on("keydown",this.handleSimpleKey);
		_stage_el.find(".inputs .input.required[type=tel]").on("keyup change",function(_e){
			_form.formatPhone($(this),_e);
		});
		_stage_el.find(".inputs .input").on("keyup change",function(_e){
			_form.validate();
			if(_e.originalEvent.keyCode==13) _stage_el.find(".btns .btn._submit, .btns .btn._download").trigger("click");
		});
		_stage_el.find(".btns .btn._next").on("click",function(){
			if(!$(this).hasClass("disabled")) _form.advStage();
		});
		_stage_el.find(".btns .btn._download").on("click",function(){
			if($(this).hasClass("disabled")){
				var _err = _form.validate();
				if(_err.length) $(_form.on_stage.el).find(".inputs [name="+_err[0].name+"]").focus();
				_form.setStage("form-error");
				return;
			}
			var _guide_dl = $(this).val();
			_form.submit(_form.getGuide,function(){
				console.log("first level fallback, reattempt");
				_form.submit(_form.getGuide,_form.submitFallback);
			});
		});
		_stage_el.find(".btns .btn._submit").on("click",function(){
			if($(this).hasClass("disabled")){
				var _err = _form.validate();
				if(_err.length) $(_form.on_stage.el).find(".inputs [name="+_err[0].name+"]").focus();
				_form.setStage("form-error");
				return;
			}
			_form.submit(null,function(){
				console.log("first level fallback, reattempt");
				_form.submit(null,_form.submitFallback,true);
			});
		});
		_stage_el.find(".btns .btn.basic").on("click",function(){
			_form.on_stage.val = null;
			_form.on_stage.next_stage = null;
			_form.setStage($(this).val());
		});
		_stage_el.find(".g-recaptcha").each(function(){
			grecaptcha.render($(this)[0],{"sitekey":$(this).data("sitekey")});
		});
	};

	this.setOptionsHeight = function(_stage){
		console.log("tccform "+this.id+" setOptionsHeight");
		_stage = _stage||this.on_stage;
		var _stage_el = this.el.find(_stage.el);
		_stage_el.find(".options .o_wrap").css({height:"auto"});
		var _h = 0;
		_stage_el.find(".options .o_wrap").each(function(){
			_h = Math.max(_h,$(this).height());
		}).css({height:_h});
	};
	
	this.unbindStage = function(_stage){
		console.log("tccform "+this.id+" unbindStage");
		this.on_stage_el.find(".options .option, .btns .btn").off("click");
		this.on_stage_el.find(".inputs .input").off("keyup change");
	};
	
	this.selOption = function(_option){
		console.log("tccform "+this.id+" selOption "+$(_option).text());
		if(_option.hasClass("selected")){
			this.unselOption(_option);
		} else {
			var _form = this;
			_option.closest(".options").find(".option.selected").each(function(){
				_form.unselOption($(this),true);
			});
			_option.addClass("selected");
			if(_option.data("val")) this.on_stage.val = _option.data("val");
			if(_option.data("stage")) this.next_stage = _option.data("stage");
			this.validate();
		}
	};
	
	this.unselOption = function(_option,_skipvalidation){
		console.log("tccform "+this.id+" unselOption: "+$(_option).text());
		_option.removeClass("selected");
		this.on_stage.val = null;
		this.next_stage = null;
		if(!_skipvalidation) this.validate();
	};
	
	this.validate = function(){
		console.log("tccform "+this.id+" validate");
		var _err = [];
		var _required = false;
		
		this.on_stage_el.find(".error_msg").remove();
				
		if(this.on_stage.inputs){
			for(var _i in this.on_stage.inputs){
				var _inp = this.on_stage.inputs[_i];
				if(_inp.required){
					_required = true;
					var _el = this.on_stage_el.find(".inputs [name="+_inp.name+"]");
					var _val = _el.val();
					var _e = false;
					if(_inp.validate=="empty"&&this.isEmpty(_val)) _e = true;
					if(_inp.validate=="email"&&!this.validateEmail(_val)) _e = true;
					if(_inp.validate=="phone"&&!this.validatePhone(_val)) _e = true;
					if(_e){
						_err.push(_inp);
						var _err_msg = _inp.label;
						if(_inp.validate=="empty"){
							_err_msg += " must not be empty";
						} else if(_inp.validate=="email"){
							_err_msg += " must be a valid email address";
						} else if(_inp.validate=="phone"){
							_err_msg += " must be in the format <span class=\"ph\">###-###-####</span>";
						}
						_el.after("<div id=\""+_el.attr("id")+"-error\" class=\"error_msg\" aria-role=\"alert\">"+_err_msg+"</div>");
					} else {
						$(_el.attr("id")+"-error").remove();
					}
					_el.attr("aria-invalid",_e).toggleClass("error",_e);
				}
			}
		}
		var _pass = _required?!_err.length:false;
		if(!_required&&this.next_stage) _pass = true;
		if(this.on_stage.btns){
			for(var _b in this.on_stage.btns){
				if(_b=="next"||_b=="submit"||_b=="download") this.on_stage_el.find(".btns .btn._"+_b).toggleClass("disabled",!_pass);
			}
		}
		return _err;
	};
	
	this.handleSimpleKey = function(_e){
		if(_e.originalEvent.keyCode==13||_e.originalEvent.keyCode==32){
			$(this).trigger("click");
			_e.preventDefault();
		}
	};
	
	this.validateEmail = function(_email){
		return new RegExp(/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/).test(_email);
	};
	
	this.formatPhone = function(_el,_e){
		var _val = _el.val();
		var _nval = _val.replace(/\D/g,'');
		if(_e.originalEvent.keyCode!==8&&_e.originalEvent.keyCode!==46) if(_nval.length==3||_nval.length==6) _nval += "-";
		if(_nval.length>3&&_nval.substr(3,1)!=="-") _nval = _nval.slice(0,3)+"-"+_nval.slice(3);
		if(_nval.length>7&&_nval.substr(7,1)!=="-") _nval = _nval.slice(0,7)+"-"+_nval.slice(7);
		if(_nval.length>12) _nval = _nval.slice(0,12);
		_el.val(_nval);
	};
	
	this.validatePhone = function(_phone){
		return new RegExp(/^([0-9]{3})+\-+([0-9]{3})+\-([0-9]{4})+$/).test(_phone);
	};
	
	this.isEmpty = function(_str){
		_str = _str||"";
		return (_str.replace(/\s+/g,'')==="");
	};
	
	this.advStage = function(){
		console.log("tccform "+this.id+" advStage, "+this.next_stage);
		if(this.stages[this.next_stage].halt){
			this.setStage(this.next_stage);
		} else {
			this.unbindStage(this.on_stage);
			this.on_stage_el.stop(false,false).animate({opacity:0},300);
			this.stage_tmr = setTimeout(this.nextStage.bind(this),300);
		}
	};
	
	this.nextStage = function(){
		console.log("tccform "+this.id+" nextStage");
		if(this.next_stage) this.setStage(this.next_stage);
	};
	
	this.submitDone = function(_res){
		console.log("tccform "+this.id+" submitDone, response:");
		console.log(_res);
		if(_res.success&&_res.success!=="false"){
			this.setStage("form-thanks");
			if(this.cb) this.cb(_res.val);
		} else {
			if(this.fb) this.fb(_res.val);
		}
	};
	
	this.submitFail = function(_res){
		console.log("tccform "+this.id+" submitFail --- PARDOT API FAILURE"+(this.fb?", trying fallback":""));
		console.log(_res);
		if(this.fb) this.fb(_res.val);
	};
	
	this.submitFallback = function(){
		alert("Sorry, there's been an error! Please try again.");
		this.setStage("form-retry");
	};
	this.getGuide = function(_guide){
		console.log("tccform "+this.id+" getGuide: "+_guide);
		window.location.href = _guide;
		//window.open(_guide,"_guide");
	};
	
};