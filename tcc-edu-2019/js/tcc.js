/*
	TCC.edu
*/

window.getCookie = function(_key){
	return decodeURIComponent(document.cookie.replace(new RegExp("(?:(?:^|.*;)\\s*" + encodeURIComponent(_key).replace(/[\-\.\+\*]/g, "\\$&") + "\\s*\\=\\s*([^;]*).*$)|^.*$"), "$1")) || null;
};
window.setCookie = function(_key,_val,_end){
	if(!_key||/^(?:expires|max\-age|path|domain|secure)$/i.test(_key))	return false;
	document.cookie = encodeURIComponent(_key)+"="+encodeURIComponent(_val)+(_end?"; expires="+_end.toUTCString():"; expires=Fri, 31 Dec 9999 23:59:59 GMT")+"; domain="+document.domain+"; path=/;";
	return true;
};

(function($){
	window.$ = $;
	window.tcc = new TCC();
})(jQuery);

function TCC(){
	
	this.widgets = [];
	this.sliders = [];
	this.graphs = [];
	this.sorters = [];
	this.hs = window.location.hash.substr(1);
	
	this.getQuery = function(){
		//console.log("TCC.getQuery");
		var _qs = this.qs = {};
		window.location.search.replace(new RegExp("([^?=&]+)(=([^&]*))?","g"),function($0,$1,$2,$3){ _qs[$1] = $3; });
		return this.qs;
	};
	this.qs = this.getQuery();
	//console.log(this.qs);
	
	this.toPage = function(_url,_target){
		//console.log("TCC.toPage: "+_url);
		_url = (_url.substr(0,1)=="/"?window.location.origin+"/":"")+_url;
		if(_target){
			window.open(_url,_target);
		} else {
			window.location.href = _url;
		}
	};
	this.setUrl = function(_url,_title,_replace,_usebase){
		//console.log("TCC.setUrl: "+_url+", _replace: "+_replace);
		if(!History.pushState) return;
		if(_usebase) _url = window.location.origin+window.location.pathname+_url;
		if(_replace){
			History.replaceState({},(_title?_title:document.title),_url);
		} else {
			History.pushState({},(_title?_title:document.title),_url);
		}
	};
	this.setHash = function(_hash,_replace){
		//console.log("TCC.setHash: "+_hash+", "+(_replace?"replacing":"not replacing"));
		if(_replace){
			if(!_hash) console.log("empty hash");
			this.setUrl(window.location.search+(_hash?"#"+_hash:" "),false,_replace,true);
		} else {
			if(_hash){
				window.location.hash = _hash;
			} else {
				var _top = document.body.scrollTop+document.documentElement.scrollTop;
				if(!_top&&this.device.scrolltop) _top = this.device.scrolltop*-1;
				window.location.hash = "";
				$("body,html").stop(false,false).animate({scrollTop:_top},0);
			}
		}
	};
	
	this.handleSimpleKey = function(_e){
		if(_e.originalEvent.keyCode==13||_e.originalEvent.keyCode==32){
			$(this).trigger("click");
			_e.preventDefault();
		}
	};
	
	this.init = function(){
		//console.log("TCC.init");
		
		//if($("#campus-map").length) this.checkMapReady();
		this.campus_map = $("#campus-map").length?new TCCMap():null;
		
		if($("#quick-tcc").length) this.initQuickTCC();
		
		if($("#key-message").length) this.initKeyMsg();
		
		this.sidebar = $("#page-sidebar").length?new TCCSidebar():null;
		
		this.programfinder = $("#program-finder").length?new TCCProgramFinder():null;
		
		this.lightbox = new TCCLightbox();
		
		this.search = $(".search-module").length?new TCCSearch():null;
		
		this.contentfeed = $("#content-feed").length?new TCCContentFeed():null;
		
		if($("._track-trending").length) this.initTrendingLinks();
		
		if($(".sf-embed").length) this.initSFEmbed();
		if($(".sf-search").length) this.initSFSearch();
		
		if($("#transfer-agreements").length) this.initTransferAgreements();
		
		this.initUI();
		
		$(window).trigger("hashchange");
		
	};
	this.initUI = function(){
		//console.log("TCC.initUI");
		
		$("#menu-wrap .menu-features .sub-menu-feature").each(function(){
			if($(this).data("targets")){
				$("#site-navigation .menu-item:contains('"+$(this).data("targets")+"') .sub-menu").before($(this));
			}
		});
		
		this.moveMenuIndicator();
		this.fitMainMenu();
		setTimeout(function(){
			tcc.moveMenuIndicator();
			tcc.fitMainMenu();
		},100);
		
		$("#section-navigation .sn-select").on("change",function(_e){ tcc.toPage($(this).val()); });
		
		$("#main-logo, #menu-main-menu > .menu-item > a").on("mouseenter focus",this.hoverMenuItem);
		$("#menu-main-menu > .menu-item").on("mouseleave focusout",this.leaveMenuItem);
		$("#menu-search .hit, #menu-search input, #menu-search button").on("mouseenter focus",this.enterMenuSearch.bind(this));
		$("#menu-search input").on("keyup change focus mouseenter",this.resetMenuSearchTmr.bind(this));
		
		$("#header .btn-mobile-menu").on("click",function(){ $("#header").toggleClass("mobile-on"); });
		
		//alerts
		
		if($("#major-alert").length) tcc.showAlert("major");
		if($("#medium-alert").length) tcc.showAlert("medium");
		if($("#minor-alert").length&&!getCookie("seen-minor-alert")) tcc.showAlert("minor");
		
		if($("#help-bug").length) this.checkHelpBug();
		
		//homepage
		if($("#gateway").length){
			if(getCookie("bypass-gateway")){
				if(!getCookie("ignore-bypass-gateway")){
					var _redir = getCookie("bypass-gateway");
					if(_redir!=="/"&&_redir.substr(0,2)!=="/!") this.bypassIntroGateway(_redir);
				}
			} else {
				this.openIntroGateway();
			}
		}
		
		$("a").each(function(){
			var _href = $(this).attr("href");
			var _matches = [
				"/",window.location.origin,window.location.origin+"/",
				"http://"+window.location.host,"http://"+window.location.host+"/",
				"https://"+window.location.host,"https://"+window.location.host+"/"
			];
			if(_matches.indexOf(_href)>=0){
				$(this).on("click",function(){ setCookie("ignore-bypass-gateway","ignore",new Date(new Date().getTime()+6000)); });
			}
		});
		
		$("#gateway-iam").on("change",function(){ tcc.closeIntroGateway($("#gateway-iam").val(),$("#gateway-iam option:selected").text()); });
		
		$("#gateway .btn._go").on("click",function(){ tcc.closeIntroGateway($("#gateway-iam").val(),$("#gateway-iam option:selected").text()); });
		$("#gateway .btn._skip").on("click",function(){ tcc.closeIntroGateway("/","skip"); });
		
		$("#four-up .item._quick-tcc .hit").on("click",this.openQuickTCCModule.bind(this));
		$("#four-up #quick-tcc .btn._close").on("click",this.closeQuickTCCModule.bind(this));
		
		$("#program-finder .tab .btn._enter").on("click",this.openProgramFinderModule.bind(this));
		$("#program-finder .tab .btn._close").on("click",this.closeProgramFinderModule.bind(this));
		
		//etc
		
		$("select").each(function(){ tcc.widgets.push(new TCCSelect(this)); });
		
		$("#page-content .content-video").each(function(){ tcc.widgets.push(new TCCContentVideo($(this))); });
		
		if($("._accordion").length) this.initAccordions();
		
		$("._slider").not("._sl_init").each(function(){ tcc.sliders.push(new TCCSlider($(this))); });
		$("._sorter").not("._so_init").each(function(){ tcc.sorters.push(new TCCSorter($(this))); });
		$("._slider").each(function(){ for(var _s in tcc.sliders) if(tcc.sliders[_s].el[0]==$(this)[0]) tcc.sliders[_s].activate(); });
		
		$(".hit").on("mouseenter focus",function(){
			$(this).parent().find(".btn").addClass("over");
		}).on("mouseleave focusout",function(){
			$(this).parent().find(".btn").removeClass("over");
		});
		
		$("a").each(function(){ if($(this).attr("href").indexOf("://help.tcc.edu")>-1) $(this).attr("target","helpcenter"); });
		
		$(".icon-link").each(function(){ if(!$(this).find(".hit").attr("title")) $(this).find(".hit").attr("title",$(this).find("label").text()); });
		
		tcc.setCopySize(getCookie("copy-size"));
		$("#section-navigation .copy-size .set-size").on("click",function(_e){ tcc.setCopySize($(this).data("size")); });
		
		$(window).on("resize",this.handleResize.bind(this)).trigger("resize");
		
	};
	
	this.showAlert = function(_alert){
		//console.log("TCC.showAlert: "+_alert);
		$("body").addClass("on-"+_alert+"-alert");
		if(_alert=="major"){
			$("#major-alert .btn._close, #major-alert .btn._conf").on("click",this.closeMajorAlert);
		} else if(_alert=="medium"){
			$("#medium-alert .btn._close").on("click",this.closeMediumAlert);
		} else if(_alert=="minor"){
			$("#minor-alert .btn._close").on("click",this.closeMinorAlert);
		}
	};
	this.closeMajorAlert = function(_e){
		//console.log("TCC.closeMajorAlert");
		_e.preventDefault();
		$("#major-alert .btn._close, #major-alert .btn._conf").off("click",tcc.closeMajorAlert);
		$("body").addClass("from-major-alert");
		setTimeout(function(){
			$("body").removeClass("from-major-alert on-major-alert");
		},500);
	};
	this.closeMediumAlert = function(_e){
		//console.log("TCC.closeMediumAlert");
		_e.preventDefault();
		$("#medium-alert .btn._close").off("click",tcc.closeMediumAlert);
		$("body").addClass("from-medium-alert");
		setTimeout(function(){
			$("body").removeClass("from-medium-alert on-medium-alert");
		},250);
	};
	this.closeMinorAlert = function(_e){
		//console.log("TCC.closeMinorAlert");
		_e.preventDefault();
		$("#minor-alert .btn._close").off("click",tcc.closeMinorAlert);
		$("body").addClass("from-minor-alert");
		setTimeout(function(){
			$("body").removeClass("from-minor-alert on-minor-alert");
		},250);
		setCookie("seen-minor-alert",1,new Date(new Date().getTime()+(864e5*parseInt($("#minor-alert").data("cookie-days")))));
	};
	
	this.checkHelpBug = function(){
		//console.log("TCC.checkHelpBug");
		var _state = parseInt(getCookie("help-bug"));
		if(_state!==0){
			var _count = _state?parseInt(_state)+1:1;
			if(_count<=$("#help-bug").data("trigger-after")){
				setCookie("help-bug",_count,new Date(new Date().getTime()+864e5));
			} else {
				this.queueHelpBug();
			}
		}
	};
	this.queueHelpBug = function(){
		//console.log("TCC.queueHelpBug");
		//check exclusion conditions like page type or whatever
		if($("#site.page-aggregate").length) return;
		$("body").addClass("queue-help-bug");
		setTimeout(tcc.showHelpBug,(1000*parseInt($("#help-bug").data("timer"))));
	};
	this.showHelpBug = function(){
		//console.log("TCC.showHelpBug");
		if($("#gateway").is(":visible")) return;
		$("#help-bug .content a").on("click",function(){
			tcc.setHelpBugDismissCookie();
			tcc.gaEvent({category:"General",action:"Click Help Bug link",label:$(this).text()});
		});
		$("#help-bug .btn._close").on("click",tcc.closeHelpBug);
		$("body").addClass("on-help-bug").removeClass("queue-help-bug");
	};
	this.closeHelpBug = function(_e){
		//console.log("TCC.closeHelpBug");
		_e.preventDefault();
		$("#help-bug .btn._close").off("click",tcc.closeMinorAlert);
		$("body").addClass("from-help-bug");
		setTimeout(function(){
			$("body").removeClass("from-help-bug on-help-bug");
		},250);
		tcc.setHelpBugDismissCookie();
		tcc.gaEvent({category:"General",action:"Close Help Bug"});
	};
	this.setHelpBugDismissCookie = function(){
		//console.log("TCC.setHelpBugDismissCookie");
		setCookie("help-bug",0,new Date(new Date().getTime()+(864e5*parseInt($("#help-bug").data("cookie-days")))));
	};
	
	this.openIntroGateway = function(){
		//console.log("TCC.openIntroGateway");
		if(!$("#gateway").length) return;
		$("body").addClass("on-gateway");
		$("#main-logo").on("click",function(_e){
			_e.preventDefault();
			tcc.closeIntroGateway("/","logo");
		});
	};
	this.closeIntroGateway = function(_target,_label){
		//console.log("TCC.closeIntroGateway: "+_label);
		setCookie("bypass-gateway",_target,new Date(new Date().getTime()+(864e5*parseInt($("#gateway").data("cookie-days")))));
		
		tcc.gaEvent({category:"Homepage",action:"Close Intro Gateway",label:_label});
		
		$("#main-logo").off("click");
		
		if(_target=="/"||_target.substr(0,2)=="/!"){
			$("body").addClass("from-gateway");
			setTimeout(function(){
				$("body").removeClass("from-gateway on-gateway");
			},500);
		} else {
			tcc.toPage(_target);
		}
	};
	this.bypassIntroGateway = function(_target){
		//console.log("TCC.bypassIntroGateway");
		_target = _target||"/";
		if(_target=="/"||_target.substr(0,2)=="/!"){
			$("body").removeClass("on-gateway");
		} else {
			tcc.toPage(_target);
		}
	};
	
	this.setCopySize = function(_size){
		//console.log("TCC.setCopySize: "+_size);
		if(!_size||_size==undefined){
			$("#section-navigation .copy-size .set-size._2").addClass("on");
		} else if(_size==this.copy_size){
			this.unsetCopySize();
		} else {
			this.copy_size = _size;
			$("#section-navigation .copy-size .set-size.on").removeClass("on");
			$("#section-navigation .copy-size .set-size._"+_size).addClass("on");
			$("body").removeClass("copy-size-1 copy-size-2 copy-size-3").addClass("copy-size-"+_size);
			setCookie("copy-size",_size,new Date(new Date().getTime()+2592e6));//864e5 = 1 day, 2592e6 = 30 days
			
			tcc.gaEvent({category:"General",action:"Set Copy Size",label:_size});
		}
	};
	this.unsetCopySize = function(){
		//console.log("TCC.unsetCopySize");
		this.copy_size = null;
		$("#section-navigation .copy-size .set-size.on").removeClass("on");
		$("body").removeClass("copy-size-1 copy-size-2 copy-size-3");
		setCookie("copy-size",0,new Date(new Date().getTime()));
		
		tcc.gaEvent({category:"General",action:"Unset Copy Size"});
		
		this.setCopySize();
	};
	
	this.hoverMenuItem = function(){
		//console.log("TCC.hoverMenuItem");
		tcc.moveMenuIndicator($(this));
		$("#menu-main-menu > .menu-item.is-focused").removeClass("is-focused");
		$(this).parent().addClass("is-focused");
		tcc.fitMainMenu($(this).siblings(".sub-menu").length?$(this).parent():null);
	};
	this.leaveMenuItem = function(_e){
		//console.log("TCC.leaveMenuItem");
		if($(_e.relatedTarget).parent().is(".menu-item.is-focused")||!$(_e.relatedTarget).parents(".menu-item.is-focused").length){
			$("#menu-main-menu > .menu-item").removeClass("is-focused");
			tcc.moveMenuIndicator();
		}
		tcc.fitMainMenu();
	};
	
	this.moveMenuIndicator = function(_el){
		//console.log("TCC.moveMenuIndicator");
		_el = _el||$("#site-navigation .current-menu-item, #site-navigation .current-menu-ancestor, #site-navigation .current-page-ancestor")[0]||$("#header .logo");
		_el = $(_el);
		var _w = Math.floor(_el.position().left+_el.outerWidth());
		$("#header .indicator").css({width:_w});
		$("#header .indicator .bit").css({right:(_el.outerWidth()*.5)}).toggleClass("on",_w!==$("#header .logo").outerWidth());
	};
	this.fitMainMenu = function(_el){
		//console.log("TCC.fitMainMenu");
		_el = _el||$("#site-navigation .sub-menu:visible").parent();
		var _h = 0;
		var _w = 0;
		var _l = 0;
		var _t = $("#menu-wrap").outerHeight()-10;
		if(_el.length){
			var _sm_h = _el.find(".sub-menu").outerHeight();
			var _f_h = _el.find(".sub-menu-feature").outerHeight()||0;
			_h = Math.max(_f_h+50,_sm_h);
			_w = this.device.width;
			_l = (_w-$("#menu-wrap").outerWidth())*-.5;
			_t = $("#menu-wrap").outerHeight();
		}
		$("#header .fill").css({height:_h,width:_w,top:_t,left:_l});
		_el.find(".sub-menu-feature .vr").css({height:_h-70});
	};
	
	this.enterMenuSearch = function(){
		$("#menu-search").addClass("on");
		$(window).on("click",this.clickOffMenuSearch);
		$("#header .main-navigation .menu-item, #header .main-navigation .menu-item > a").on("mouseenter focus",tcc.leaveMenuSearch);
		this.resetMenuSearchTmr();
	};
	this.resetMenuSearchTmr = function(){
		clearTimeout(this.search_reset_tmr);
		this.search_reset_tmr = setTimeout(this.leaveMenuSearch.bind(this),5000);
	};
	this.leaveMenuSearch = function(){
		$("#menu-search").removeClass("on");
		$("#menu-search input").blur();
		$(window).off("click",this.clickOffMenuSearch);
		$("#header .main-navigation .menu-item, #header .main-navigation .menu-item > a").off("mouseenter focus",tcc.leaveMenuSearch);
	};
	this.clickOffMenuSearch = function(_e){
		//console.log("TCC.clickOffMenuSearch");
		if(_e.target!==$("#menu-search")[0]&&!$(_e.target).parents("#menu-search").length) tcc.leaveMenuSearch();
	};
	
	this.initQuickTCC = function(){
		//console.log("TCC.initQuickTCC");
		this.qt_form = new TCCForm("qt-form",$("#quick-tcc-form"),TCCVals.qt_stages);
		this.qt_form.setStage("select-section");
	};
	this.openQuickTCCModule = function(_e){
		_e.preventDefault();
		$("#four-up").removeClass("from-quick-tcc").addClass("to-quick-tcc on-quick-tcc");
		this.qt_form.setStage("select-section");
		clearTimeout(this.qt_tmr);
		this.qt_tmr = setTimeout(function(){
			$("#four-up").removeClass("to-quick-tcc");
		},500);
		
		tcc.gaEvent({category:"Homepage",action:"Open QuickTCC Module"});
		
	};
	this.closeQuickTCCModule = function(){
		$("#four-up").removeClass("to-quick-tcc").addClass("from-quick-tcc");
		clearTimeout(this.qt_tmr);
		this.qt_tmr = setTimeout(function(){
			$("#four-up").removeClass("from-quick-tcc on-quick-tcc");
		},500);
	};
	
	this.openProgramFinderModule = function(){
		$("#program-finder").removeClass("out").addClass("in on");
		$("#program-finder .tab .btn._enter").addClass("disabled").attr("disabled","disabled");
		$("#program-finder .tab .btn._close").removeClass("disabled").removeAttr("disabled");
		if(this.pf_transit_tmr) clearTimeout(this.pf_transit_tmr);
		this.pf_transit_tmr = setTimeout(function(){
			$("#program-finder").removeClass("in");
		},400);
		
		tcc.gaEvent({category:"Homepage",action:"Open Program Finder Module"});
		
	};
	this.closeProgramFinderModule = function(){
		$("#program-finder").removeClass("in on").addClass("out");
		$("#program-finder .tab .btn._close").addClass("disabled").attr("disabled","disabled");
		$("#program-finder .tab .btn._enter").removeClass("disabled").removeAttr("disabled");
		if(this.pf_transit_tmr) clearTimeout(this.pf_transit_tmr);
		this.pf_transit_tmr = setTimeout(function(){
			$("#program-finder").removeClass("out");
		},400);
	};
	
	this.initKeyMsg = function(){
		$("#key-message .item a").on("mouseenter focus",function(){
			$("#key-message").addClass("focus");
			$(this).parent().addClass("focus");
		}).on("mouseleave focusout",function(){
			if(!$("#key-message .item a:focus").length) $("#key-message").removeClass("focus");
			if(!$(this).is(":focus")) $(this).parent().removeClass("focus");
		});
		this.cycleKeyMsg();
	};
	this.cycleKeyMsg = function(){
		//console.log("TCC.cycleKeyMsg");
		var _item = $("#key-message .item.on")[0]||$("#key-message .item")[0];
		var _next = $(_item).is(".on")?($(_item).next().length?$(_item).next():$($(_item).siblings().get(0))):$(_item);
		$("#key-message .item.on").removeClass("on");
		_next.addClass("on");
		clearTimeout(tcc.key_msg_tmr);
		tcc.key_msg_tmr = setTimeout(tcc.cycleKeyMsg,3000);
	};
	
	this.initAccordions = function(_el){
		//console.log("TCC.initAccordions");
		_el = _el||$("#site");
		var _accordions = _el.is("._accordion")?_el:_el.find("._accordion");
		_accordions.each(function(){
			$(this).find(".item .title").off("click").on("click",function(){
				var _item = $(this).parent();
				_item.toggleClass("on");
				var _nexthash = _item.is(".on")?_item.data("id"):"";
				if(!_item.is(".on")&&_item.siblings(".on").length){
					var _items = _item.parent().children();
					var _sp = _sa = _in = _items.index(_item);
					var _nearest;
					while(!_nearest){
						if(_sp>0) _sp--;
						if(_sa<_items.length) _sa++;
						if(_sp>=0&&$(_items.get(_sp)).hasClass("on")) _nearest = _items.get(_sp);
						if(_sa<=_items.length&&$(_items.get(_sa)).hasClass("on")) _nearest = _items.get(_sa);
						if(_sp==0&&_sa==_items.length) break;
					}
					_nexthash = $(_nearest).data("id");
				}
				tcc.setHash(_nexthash,false);
				if(tcc.sidebar&&tcc.sidebar.sticky) tcc.sidebar.track(300);
			}).off("keydown").on("keydown",tcc.handleSimpleKey);
		});
		$(window).off("hashchange",tcc.checkAccordionsHash).on("hashchange",tcc.checkAccordionsHash);
	};
	this.checkAccordionsHash = function(){
		//console.log("TCC.checkAccordionsHash");
		var _hash = window.location.hash.substr(1);
		var _item = _hash?$("._accordion .item[data-id="+_hash+"]"):false;
		if(_item&&_item.length){
			_item.addClass("on");
			if(tcc.sidebar&&tcc.sidebar.sticky) tcc.sidebar.track(200);
			clearTimeout(tcc.acc_hash_tmr);
			var _mod = document.body.scrollTop+document.documentElement.scrollTop==0?3:1;
			tcc.acc_hash_tmr = setTimeout(function(){ tcc.scrollTo(_item,_item.find(".title").outerHeight()+_item.find(".copy").outerHeight(),_mod); },200);
		}
	};
	
	this.cancelScrollTo = function(){
		//console.log("TCC.cancelScrollTo");
		$("body,html").stop(false,false);
		clearTimeout(this.scrollTo_cancel_tmr);
		$(window).off("wheel",tcc.cancelScrollTo);
	};
	this.scrollTo = function(_to,_height,_time_mod){
		//console.log("TCC.scrollTo: "+(typeof _to=="number"?_to:_to.id||_to.data("id")||_to.className));
		var _top = _to;
		if(typeof _to!=="number"){
			var _h = _height?_height:_to.outerHeight();
			_top = _h<this.device.height?_to.offset().top-(tcc.device.height*.5)+(_h*.5):_to.offset().top;
		}
		var _time_mod = _time_mod||1;
		var _time = Math.max(250,Math.min(1000,parseInt(Math.abs(document.body.scrollTop+document.documentElement.scrollTop-_top)*.2)))*_time_mod;
		$("body,html").stop(false,false).animate({scrollTop:_top},_time);
		if(tcc.sidebar&&tcc.sidebar.sticky) tcc.sidebar.track(_time);
		
		$(window).on("wheel",tcc.cancelScrollTo);
		clearTimeout(this.scrollTo_cancel_tmr);
		this.scrollTo_cancel_tmr = setTimeout(function(){
			$(window).off("wheel",tcc.cancelScrollTo);
		},_time);
	};
	
	this.initTrendingLinks = function(){
		//console.log("TCC.initTrendingLinks");
		$("._track-trending a").on("click",function(_e){
			var _pid = $(this).parents("._track-trending").data("pid");
			$.ajax({url:"/",type:"POST",data:{"trending_link":true,"post_id":_pid,"href":$(this).attr("href"),"title":$(this).text()},dataType:"json"});
		});
	};
	
	this.initSFEmbed = function(){
		//console.log("TCC.initSFEmbed");
		$(".sf-embed").each(function(){
			var _data = {
				salesforce_api:true,
				mode:"embed",
				term:$(this).data("slug")
			};
			var _self = $(this);
			var _html = "<div class=\"icon\"></div><h3>"+($(this).data("title")?$(this).data("title"):"Related Help Center Topics")+"</h3>";
			$.ajax({url:"/",type:"POST",data:_data,dataType:"json"}).done(function(_res){
				_html += "<ul>";
				for(var _r in _res.items) _html += "<li><a href=\""+_res.items[_r].link+"\">"+_res.items[_r].title+"</a></li>";
				_html += "</ul>";
				_self.addClass("init").html(_html);
			}).fail(function(_res){
				//console.log("sf embed fail!");
				//console.log(_res);
			});
		});
	};
	
	this.initSFSearch = function(){
		//console.log("TCC.initSFSearch");
		$(".sf-search").each(function(){
			
			var _data = {
				salesforce_api:true,
				mode:"search",
				term:$(this).data("term")
			};
			
			var _self = $(this);
			var _html = "";
			
			_self.html("<div class=\"info nonh5\">Searching...</div><div class=\"items\"></div><a href=\"https://help.tcc.edu/s/global-search/"+encodeURIComponent($(this).data("term"))+"\" class=\"btn btn-arrow btn-6\">More Help Center Results</a>");
			
			$(".help-center-results-link").attr("href","https://help.tcc.edu/s/global-search/"+encodeURIComponent($(this).data("term")));
			
			
			$.ajax({url:"/",type:"POST",data:_data,dataType:"json"}).done(function(_res){
				var _html = "";
				if(!_res.items||!_res.items.length){
					_self.addClass("init").find(".info").html("No results!");
				} else {
					for(var _r in _res.items){
						_html += "<div class=\"item\"><a href=\""+_res.items[_r].link+"\" class=\"nonh3\">"+_res.items[_r].title+"</a>";
						if(_res.items[_r].excerpt) _html += "<p>"+_res.items[_r].excerpt+"</p>";
						_html += "</div>";
					}
					_self.addClass("init").find(".info").remove();
					_self.find(".items").html(_html);
				}
			}).fail(function(_res){
				//console.log("sf search fail!");
				//console.log(_res);
			});
			
			
		});
	};
	
	this.initTransferAgreements = function(){
		//console.log("TCC.initTransferAgreements");
		var _html = "";
		var _items = "";
		var _programs = [];
		
		_html = "<label for=\"transfer-school\" class=\"screen-reader-text\">Select School</label><select id=\"transfer-school\" data-type=\"school\"><option selected disabled data-role=\"placeholder\">Select School</option><option value=\"\" class=\"clear\">Clear selection</option>";
		for(var _s in TCCVals.transfer_schools){
			var _school = TCCVals.transfer_schools[_s];
			_html += "<option value=\""+_school.id+"\">"+_school.name+"</option>";
			_items += "<div class=\"item\" data-id=\""+_school.id+"\"><h3>"+_school.name+"</h3>";
			if(_school.location) _items += "<p>Location: "+_school.location+"</p>";
			if(_school.type) _items += "<p>Type of school: "+_school.type+"</p>";
			_items += "<p>Minimum GPA accepted: "+(_school.gpa?_school.gpa:"Not specified")+"</p>";
			if(_school.url) _items += "<a class=\"btn btn-3 btn-text btn-arrow\" href=\""+_school.url+"\">View "+_school.name+" agreements</a>";
			_items += "</div>";
			
			for(var _a in _school.agreements) if(_programs.indexOf(_school.agreements[_a])<0) _programs.push(_school.agreements[_a]);
		}
		_html += "</select>";
		$("#transfer-agreements .by-school").append(_html);
		
		_html = "<label for=\"transfer-program\" class=\"screen-reader-text\">Select Academic Program</label><select id=\"transfer-program\" data-type=\"program\"><option selected disabled data-role=\"placeholder\">Select Academic Program</option><option value=\"\" class=\"clear\">Clear selection</option>";
		_programs.sort(function(a,b){
			if(a<b) return -1;
			if(a>b) return 1;
			return 0;
		});
		for(var _p in _programs) _html += "<option value=\""+_programs[_p]+"\">"+TCCVals.programs[_programs[_p]].title+"</option>";
		_html += "</select>";
		$("#transfer-agreements .by-program").append(_html);
		$("#transfer-agreements .items").append(_items);
		
		$("#transfer-agreements select").on("change",function(){
			if($(this).hasClass("override")) return;
			$("#transfer-agreements select").not($(this)).val("").addClass("override").trigger("change").removeClass("override");;
			tcc.filterTransferAgreements($(this).val(),$(this).data("type"));
		});
		
		
	};
	this.filterTransferAgreements = function(_id,_type){
		//console.log("TCC.filterTransferAgreements: "+_id+", "+_type);
		$("#transfer-agreements .items .item").removeClass("on");
		if(_type=="school"){
			if(_id) $("#transfer-agreements .items .item[data-id="+_id+"]").addClass("on");
		} else if(_type=="program"){
			var _schools = [];
			for(var _s in TCCVals.transfer_schools){
				if(TCCVals.transfer_schools[_s].agreements.indexOf(_id)>=0) _schools.push(_s);
			}
			for(var _s in _schools){
				$("#transfer-agreements .items .item[data-id="+_schools[_s]+"]").addClass("on");
			}
		}
	};
	
	
	this.device = {
		mobile:$(window).width()<960?true:false,
		width:$(window).width(),
		height:$(window).height(),
		responsive:0,
		scrolltop:null
	};
	
	this.responsive_breaks = [519,719,959,1259,1599];
	this.getResponsiveMode = function(_w){
		_w = _w?_w:this.device.width;
		var _mode = 0;
		for(var _m in this.responsive_breaks) if(_w>this.responsive_breaks[_m]) _mode = parseInt(_m)+1;
		return _mode;
	};
	this.handleResize = function(){
		//console.log("TCC.handleResize");
		this.device.mobile = $(window).width()<960?true:false;
		this.device.width = $("body").prop("clientWidth")||$(window).width();
		this.device.height = $("body").prop("clientHeight");
		if(this.getResponsiveMode()!==this.device.responsive){
			this.device.responsive = this.getResponsiveMode();
			this.moveMenuIndicator();
			this.fitMainMenu();
		}
		if(this.qt_form) this.qt_form.setOptionsHeight();
		if(this.campus_map) this.campus_map.resize();
		if(this.contentfeed) this.contentfeed.resize();
		
		$("body.on-lightbox #site-wrap").css({height:this.device.scrolltop+this.device.height});
		
	};
	
	
	this.gaEvent = function(_vals){
		//console.log("TCC.gaEvent: "+JSON.stringify(_vals));
		if(window.ga){
			var _data = {hitType:"event"};
			if(_vals&&_vals.category) _data.eventCategory = _vals.category;
			if(_vals&&_vals.action) _data.eventAction = _vals.action;
			if(_vals&&_vals.label) _data.eventLabel = _vals.label;
			window.ga("send",_data);
		}
	};
	
	$(document).ready(function(){
		tcc.init();
	});
	return this;
}

function TCCSidebar(){
	
	this.sticky = false;
	this.pad
	this.w_h;
	this.w_p;
	this.h;
	this.form;
	var _self = this;
	
	this.init = function(){
		//console.log("TCCSidebar.init");
		if($("#page-sidebar.sticky").length) this.initSticky();
		if($("#page-sidebar ._form").length) this.initForm();
		return this;
	};
	
	this.initForm = function(){
		this.form = new TCCForm("sidebar-form",$("#page-sidebar ._form"),TCCVals.sidebar_stages,function(_cb,_fb){
			this.setStage("form-disable",{scroll:false});
			var _form = this;
			var _data = {
				pardot_api:true,
				mode:"guide",
				guide_id:$("#prog-form .btn._download").val(),
				email:$("#prog-form input[name=guide_email]").val(),
				fname:$("#prog-form .input[name=guide_fname]").val(),
				lname:$("#prog-form .input[name=guide_lname]").val(),
				phone:$("#prog-form .input[name=guide_phone]").val()
			};
			this.cb = _cb;
			this.fb = _fb;
			
			tcc.gaEvent({category:"Guide Download",action:"Form Submit",label:_data.guide_id});
			
			$.ajax({url:"/",type:"POST",data:_data,dataType:"json"}).done(this.submitDone.bind(this)).fail(this.submitFail.bind(this));
		});
	
		this.form.setStage("sidebar-download",{val:$("#page-sidebar ._form").data("guide-id")});
		
		$("#page-sidebar .sidebar-item._form ._enter").on("click",this.openForm);
		$("#page-sidebar .sidebar-item._form ._close").on("click",this.closeForm);
	};
	
	this.initSticky = function(){
		//console.log("TCCSidebar.initSticky");
		this.sticky = true;
		$(window).on("resize",this.resize.bind(this)).trigger("resize");
		$(window).on("load",$(window).trigger.bind($(window),"resize"));
		$(window).on("scroll",this.scroll.bind(this)).trigger("scroll");
	};
	this.resize = function(_e){
		//console.log("TCCSidebar.resize");
		this.pad = $("#page-sidebar").offset().top;
		this.w_h = $("#page-content").outerHeight()-$("#page-sidebar").parent().position().top;
		this.w_p = parseInt($("#page-content").css("padding-top"))+parseInt($("#page-content").css("padding-bottom"));
		this.h = $("#page-sidebar .panel").outerHeight();
		$(window).trigger("scroll");
	};
	this.track = function(_len){
		//console.log("TCCSidebar.track, len: "+_len);
		if(tcc.device.responsive<3) return;
		
		_len = _len+10||260;
		clearTimeout(this.size_tmt);
		this.size_tmt = setTimeout(function(){
			clearInterval(_self.size_tmr);
			clearTimeout(_self.size_tmt);
		},_len);
		clearInterval(this.size_tmr);
		this.size_tmr = setInterval(this.resize.bind(this),50);
	};
	this.scroll = function(_e){
		//console.log("TCCSidebar.scroll");
		if(!$("#page-sidebar").is(":visible")) return;
		
		if(tcc.device.responsive<3){
			$("#page-sidebar .panel").css({top:0});
			return;
		}
		
		var _offset = Math.max(0,document.body.scrollTop+document.documentElement.scrollTop-this.pad);
		if(_offset+this.h+this.w_p>this.w_h){
			$("#page-sidebar .panel").css({top:this.w_h-this.h-this.w_p});
		} else {
			$("#page-sidebar .panel").css({top:_offset});
			if(_offset>0) var _pad = this.pad;
		}
		clearTimeout(this.scroll_tmr);
		this.scroll_tmr = setTimeout(function(){
			if(_pad==undefined) return
			var _offset = Math.max(0,document.body.scrollTop+document.documentElement.scrollTop-_pad);
			$("#page-sidebar .panel").css({top:_offset});
		},30);
	};
	
	this.openForm = function(_e){
		_e.preventDefault();
		$("#page-sidebar").addClass("on-form");
		$("#page-sidebar .sidebar-item._form ._enter").addClass("disabled").attr("disabled","disabled");
		$("#page-sidebar .sidebar-item._form ._close").removeClass("disabled").removeAttr("disabled");
		$("#page-sidebar .sidebar-item._form").addClass("on");
		
		tcc.gaEvent({category:"Guide Download",action:"Open Form",label:$("#page-sidebar .sidebar-item._form").data("guideId")});
		
	};
	this.closeForm = function(_e){
		_e.preventDefault();
		$("#page-sidebar").removeClass("on-form");
		$("#page-sidebar .sidebar-item._form ._enter").removeClass("disabled").removeAttr("disabled");
		$("#page-sidebar .sidebar-item._form ._close").addClass("disabled").attr("disabled","disabled");
		$("#page-sidebar .sidebar-item._form").removeClass("on");
		
		tcc.gaEvent({category:"Guide Download",action:"Close Form",label:$("#page-sidebar .sidebar-item._form").data("guideId")});
		
	};
	
	return this.init();
}


function TCCSelect(_el){
	//console.log("new TCCSelect: "+$(_el).attr("id"));
	this.el = $(_el);
	this.id = this.el.attr("id")||"tcc-select-"+tcc.widgets.length;
	this.selected;
	this.label;
	this.close_tmr;
	this.wrap = this.el.wrap("<div class=\"select-wrap\"></div>").parent();
	var _self = this;
	
	this.init = function(){
		
		this.wrap.append("<div class=\"tcc-select\"><button type=\"button\" class=\"sel-val\" /><div class=\"sel-opts\"></div></div>");
		
		this.wrap.find("select option").each(function(){
			if($(this).data("role")&&$(this).data("role")=="placeholder"){
				_self.wrap.find(".sel-val").data("placeholder",$(this).text());
			} else {
				var _html = "<div class=\"opt"+($(this).attr("class")?" "+$(this).attr("class")+"":"")+"\" data-val=\""+$(this).attr("value")+"\">"+$(this).html()+($(this).is(".clear")?"<span></span>":"")+"</div>";
				_self.wrap.find(".sel-opts").append(_html);
			}
		});
		this.wrap.find(".opt").on("click",function(){
			_self.close(true);
			_self.selectVal($(this).data("val"));
		}).on("mouseenter",function(){
			clearTimeout(_self.close_tmr);
			_self.wrap.find(".sel-opts .opt").removeClass("sel");
			if(_self.selected) _self.wrap.find(".sel-opts .opt[data-val=\""+_self.selected+"\"]").addClass("sel-set");
			$(this).addClass("sel");
		}).on("mouseleave",function(){
			_self.setCloseTimeout();
			$(this).removeClass("sel");
			if(_self.selected) _self.wrap.find(".sel-opts .opt[data-val=\""+_self.selected+"\"]").addClass("sel");
		});
		
		this.el.on("change",this.sync.bind(this));
		this.sync();
		this.wrap.find(".sel-val").on("click focus",this.open.bind(this));
		this.wrap.find(".sel-val").on("keydown keyup",this.handleKey);
		this.wrap.on("focusout",function(_e){ if(_e.relatedTarget) _self.close(true); });
		
		return this;
	};
	
	this.selectVal = function(_val){
		//console.log("TCCSelect "+this.id+".selectVal: "+_val);
		_val = _val==undefined?this.selected:_val;
		_self.wrap.find("select").val(_val).trigger("change");
	};
	
	this.handleKey = function(_e){
		//console.log("TCCSelect "+_self.id+".handleKey: "+_e.keyCode);
		_self.setCloseTimeout();
		
		if(_e.keyCode==40||_e.keyCode==39||_e.keyCode==38||_e.keyCode==37){//down, right, up, left
			_e.preventDefault();
			if(_e.type=="keydown") return;
			var _opt = _self.selected?_self.wrap.find(".sel-opts .opt[data-val=\""+_self.selected+"\"]"):_self.wrap.find(".sel-opts .opt.clear");
			if(_e.keyCode==40||_e.keyCode==39){//down, right
				if(!_opt.is(":last-child")) _self.selected = _opt.next().data("val");
				if(!_self.selected) _self.selected = _self.wrap.find(".sel-opts .opt:first-child").data("val");
			} else if(_e.keyCode==38||_e.keyCode==37){//up, left
				if(!_opt.is(":first-child")) _self.selected = _opt.prev().data("val");
			}
			var _new_opt = _self.selected?_self.wrap.find(".sel-opts .opt[data-val=\""+_self.selected+"\"]"):_self.wrap.find(".sel-opts .opt.clear");
			if(_new_opt.length){
				var _opt_top = _new_opt.position().top;
				var _ps = _self.wrap.find(".sel-opts").scrollTop();
				var _ph = _self.wrap.find(".sel-opts").outerHeight();
				if(_opt_top<0){
					_self.wrap.find(".sel-opts").stop(false,false).animate({scrollTop:_ps+_opt_top},200);
				} else if(_opt_top+_new_opt.outerHeight()>_ph){
					_self.wrap.find(".sel-opts").stop(false,false).animate({scrollTop:_ps+(_opt_top+_new_opt.outerHeight()-_ph)},200);
				}
			}
			_self.wrap.find(".sel-opts .opt").removeClass("sel sel-set");
			_new_opt.addClass("sel sel-set");
		} else if(_e.keyCode==13||_e.keyCode==32){//enter, space
			_e.preventDefault();
			_self.close();
			_self.selectVal();
		}
	};
	
	this.sync = function(){
		//console.log("TCCSelect "+this.id+".sync");
		this.selected = this.el.val();
		if(!this.selected){
			this.label = this.wrap.find(".sel-val").data("placeholder");
			this.wrap.find(".sel-opts .opt").removeClass("sel sel-set");
		} else {
			var _opt = this.el.find("option[value=\""+this.selected+"\"]");
			this.label = _opt.length?_opt.text():this.selected;
			this.wrap.find(".sel-opts .opt").removeClass("sel sel-set");
			this.wrap.find(".sel-opts .opt[data-val=\""+this.selected+"\"]").addClass("sel sel-set");
		}
		this.wrap.toggleClass("_selected",_self.selected?true:false);
		
		clearTimeout(this.selected_tmr);
		this.selected_tmr = setTimeout(function(){
			_self.wrap.removeClass("_selected").toggleClass("selected",_self.selected?true:false);
		},300);
		
		this.wrap.find(".sel-val").data("val",this.selected).html(this.label);
	};
	this.setCloseTimeout = function(){
		clearTimeout(this.close_tmr);
		this.close_tmr = setTimeout(this.close.bind(this,true),10000);
	};
	this.open = function(){
		//console.log("TCCSelect "+this.id+".open");
		if(this.wrap.hasClass("on")) return;
		$(".select-wrap.latest").removeClass("latest");
		this.wrap.removeClass("out").addClass("on latest");
		this.setCloseTimeout();
		
		if(this.wrap.find(".sel-opts .opt.set-sel").length){
			this.wrap.find(".sel-opts .opt.set-sel").focus();
		} else {
			this.wrap.find(".sel-opts").focus();
		}
		
		$(window).on("click",this.clickOff);
	};
	this.close = function(_nodelay){
		//console.log("TCCSelect "+this.id+".close, _nodelay: "+_nodelay);
		clearTimeout(this.close_tmr);
		var _self = this;
		this.close_tmr = setTimeout(function(){
			_self.wrap.addClass("out").removeClass("on");
			_self.close_tmr = setTimeout(_self.wrap.removeClass.bind(_self.wrap,"out"),300);
		},_nodelay?10:100);
		
		this.wrap.find(".sel-val").blur();
		
		$(window).off("click",this.clickOff);
	};
	this.clickOff = function(_e){
		//console.log("TCCSelect "+_self.id+".clickOff");
		if(_e.target!==_self.wrap[0]&&!$(_e.target).parents().is(_self.wrap)) _self.close(true);
	};
	
	return this.init();
}


function TCCSlider(_el){
	//console.log("new TCCSlider");
	this.el = _el||null;
	this.slide_els = _el.find(".slide")||[];
	this.slides = [];
	this.on_slide = 0;
	this.slide_time = 8000;
	this.tmr = null;
	this.tmr_cnt = 0;
	this.graph = null;
	
	this.init = function(){
		//console.log("TCCSlider.init");
		for(var _s=0;_s<this.slide_els.length;_s++){
			var _s_el = $(this.slide_els[_s])
			var _slide = { el:_s_el };
			if(_s_el.hasClass("_sgraph")){
				_slide.graph = new TCCCostGraph(_s_el);
				tcc.graphs.push(_slide.graph);
			}
			this.slides.push(_slide);
		}
		if(this.slides.length>1) {
			this.el.find(".slide").each(function(_i){ $(this).attr("data-slide",_i); });
			if(!this.el.closest("#a-spot").length){
				this.el.find(".nav").append("<div class=\"nav-btns\"><button class=\"btn btn-3 btn-arrow-left btn-text _prev\" tabindex=\"0\">Previous</button><button class=\"btn btn-3 btn-arrow btn-text _next\" tabindex=\"0\">Next</button></div>");
				this.el.find(".nav .btn._prev").off("click").on("click",this.prev.bind(this)).off("keydown").on("keydown",this.handleKey.bind(this));
				this.el.find(".nav .btn._next").off("click").on("click",this.next.bind(this)).off("keydown").on("keydown",this.handleKey.bind(this));
			}
			this.el.find(".nav").append("<div class=\"nav-dots\"></div>");
			var _self = this;
			for(var _s in this.slides) this.el.find(".nav-dots").append("<div class=\"ind\" data-slide=\""+this.slides[_s].el.data("slide")+"\" tabindex=\"0\"></div>");
			this.el.find(".nav .ind").off("click").on("click",function(){
				if($(this).attr("data-slide")!==_self.slides[_self.on_slide]) _self.show($(this).attr("data-slide"),true);
				_self.cycle();
			}).off("keydown").on("keydown",this.handleKey.bind(this));
		}
		this.show(this.on_slide,true,true);
		this.el.addClass("_sl_init");
		return this;
	};
	this.activate = function(){
		this.el.addClass("active");
		this.cycle();
	};
	this.deactivate = function(){
		this.el.removeClass("active");
		clearInterval(this.tmr);
	};
	this.auto = function(){
		if(this.el.hasClass("active")){
			this.tmr_cnt++;
			this.show(this.on_slide+1);
			if(this.tmr_cnt>=100) clearInterval(this.tmr);
		}
	};
	this.show = function(_slide,_reset,_skip){
		_slide = parseInt(_slide);
		if(_slide===this.on_slide&&!_skip) return;
		if(!_skip) this.slides[this.on_slide].el.removeClass("on").addClass("out").stop(false,false).animate({opacity:0},1000,function(){ $(this).removeClass("out"); });
		this.on_slide = _slide>this.slides.length-1?0:(_slide<0?this.slides.length-1:_slide);
		this.slides[this.on_slide].el.addClass("on").css({opacity:_skip?1:0}).stop(false,false).animate({opacity:1},750);
		this.el.find(".nav .ind").removeClass("on");
		this.el.find(".nav .ind[data-slide="+this.on_slide+"]").addClass("on");
		if(this.slides[this.on_slide].graph) this.slides[this.on_slide].graph.animIn();
		if(_reset) this.tmr_cnt = 0;
		this.cycle();
	};
	this.cycle = function(){
		clearInterval(this.tmr);
		if(this.slides.length>1) this.tmr = setInterval(this.auto.bind(this),this.slide_time);
	};
	this.handleKey = function(_e){
		if(_e.originalEvent.keyCode==39){
			this.next();
		} else if(_e.originalEvent.keyCode==37){
			this.prev();
		} else if(_e.originalEvent.keyCode==13||_e.originalEvent.keyCode==32){
			if($(_e.target).hasClass("next")){
				this.next();
			} else if($(_e.target).hasClass("prev")){
				this.prev();
			} else if($(_e.target).hasClass("ind")){
				if($(_e.target).attr("data-slide")!==this.slides[this.on_slide]) this.show($(_e.target).attr("data-slide"),true);
				_self.cycle();
			} else {
				this.next();
			}
			_e.preventDefault();
		}
	};
	this.next = function(_e){
		this.show(this.on_slide+1,true);
	};
	this.prev = function(_e){
		this.show(this.on_slide-1,true);
	};
	
	return this.init();
}


function TCCCostGraph(_el){
	//console.log("new TCCCostGraph");
	this.el = _el||null;
	this.bar_1 = _el.data("bar-1")||0;
	this.bar_2 = _el.data("bar-2")||0;
	this.val_1 = _el.data("val-1")||0;
	this.val_2 = _el.data("val-2")||0;
	this.val_3 = _el.data("val-3")||0;
	this.disp_1;
	this.disp_2;
	this.disp_val_1;
	this.disp_val_2;
	this.disp_val_3;
	this.logo = _el.data("logo");
	this.label_1 = _el.data("label-1");
	this.label_2 = _el.data("label-2");
	
	this.init = function(){
		//console.log("TCCCostGraph.init");
		this.el.append("\
			<table><tr>\
				<td class=\"slide-graph\"><div class=\"graph no-select\"><div class=\"logo\"></div><div class=\"bar _1\"><div class=\"fill\"></div><div class=\"diff\"></div></div><div class=\"bar _2\"><div class=\"fill\"></div></div></div></td>\
				<td class=\"slide-data\"><table>\
					<tr><td class=\"_1\"><label>"+this.label_1+"</label><span class=\"val nonh5 _1\"></span></td></tr>\
					<tr><td class=\"_2\"><label>"+this.label_2+"</label><span class=\"val nonh5 _2\"></span></td></tr>\
					<tr><td class=\"_3\"><label>Total savings</label><span class=\"val nonh5 _3\"></span></td></tr>\
				</table></td>\
			</tr></table>\
		");
		this.el.find(".logo").css({"background-image":"url(\""+window.theme_dir+this.logo+"\")"});
		this.el.find(".val._1").text("0");
		this.el.find(".val._2").text("0");
		this.el.find(".val._3").text();
		
		this.el.find(".bar._1.fill").css({width:"0%"});
		this.el.find(".bar._1.diff").css({width:"0%"});
		this.el.find(".bar._2.fill").css({width:"0%"});
		
		return this;
	};
	
	this.formatValue = function(_val){
		return "$"+parseInt(_val).toLocaleString("en");
	};
	
	this.animIn = function(){
		this.disp_1 = 0;
		this.disp_2 = 0;
		this.disp_val_1 = 0;
		this.disp_val_2 = 0;
		this.disp_val_3 = 0;
		this.el.find(".bar._1 .fill, .bar._1 .diff, .bar._2 .fill").css({left:"-100%"});
		this.el.find(".val._1, .val._2, .val._3").text("");
		this.el.find("td._3").css({opacity:0});
		$(this).stop(false,false).animate({
			disp_1:this.bar_1,
			disp_2:this.bar_2,
			disp_val_1:this.val_1,
			disp_val_2:this.val_2
		},{
			duration:800,
			start:function(){
				this.el.find(".val._1, .val_2").stop(false,false).css({opacity:0}).animate({opacity:1},250);
				this.el.find(".bar._1 .fill").animate({left:((100-this.bar_1)*-1)+"%"},800);
				this.el.find(".bar._2 .fill").animate({left:((100-this.bar_2)*-1)+"%"},800);
			},
			step:function(){
				this.el.find(".val._1").text(this.formatValue(Math.floor(this.disp_val_1)));
				this.el.find(".val._2").text(this.formatValue(Math.floor(this.disp_val_2)));
			},
			done:function(){
				this.el.find(".val._1").text(this.formatValue(this.val_1));
				this.el.find(".val._2").text(this.formatValue(this.val_2));
				this.disp_2 = this.disp_1;
			}
		}).delay(200).animate({disp_2:this.bar_2,disp_val_3:this.val_3},{
			duration:1000,
			start:function(){
				this.el.find("td._3").stop(false,false).animate({opacity:1},500);
				this.el.find(".bar._1 .diff").css({opacity:0,left:((100-this.bar_1)*-1)+"%"}).animate({opacity:1.5,left:((100-this.bar_2)*-1)+"%"},1000);
			},
			step:function(){
				this.el.find(".val._3").text(this.formatValue(Math.floor(this.disp_val_3)));
			},
			done:function(){
				this.el.find(".val._3").text(this.formatValue(this.val_3));
			}
		});
		
	};
	
	return this.init();
}


function TCCSorter(_el){
	//console.log("new TCCSorter");
	this.el = _el||null;
	this.sorting = {};
	this.items = [];
	
	this.init = function(){
		//console.log("TCCSorter.init");
		var _self = this;
		this.el.find(".head label").off("click").on("click",function(){
			if(_self.sorting.val==$(this).data("val")) $(this).toggleClass("_d");
			_self.sort($(this).data("val"),!$(this).hasClass("_d"),$(this).data("sort"));
		}).off("keydown").on("keydown",tcc.handleSimpleKey);
		this.el.find(".items .item").each(function(){
			_self.items.push($(this));
		});
		this.sort("1",true);
		this.el.addClass("_so_init");
		return this;
	};
	
	this.sort = function(_val,_asc,_type){
		//console.log("TCCSorter.sort: "+_val+(_asc?", ascending":"")+(_type?", type: "+_type:""));
		_val = String(_val);
		this.sorting = {val:_val,type:_type?_type:null,asc:_asc};
		this.el.find(".head label").removeClass("_on");
		this.el.find(".head label._"+_val).addClass("_on");
		this.el.find(".items").empty();
		this.items.sort(function(a,b){ return _type=="alpha"?(_asc?(a.data(_val)<b.data(_val)?-1:(a.data(_val)>b.data(_val)?1:0)):(a.data(_val)>b.data(_val)?-1:(a.data(_val)<b.data(_val)?1:0))):(_asc?a.data(_val)-b.data(_val):b.data(_val)-a.data(_val)); });
		
		for(var _i in this.items) this.el.find(".items").append(this.items[_i]);
		tcc.initAccordions(this.el);
	};
	
	return this.init();
}


function TCCProgramFinder(){
	
	this.el = $("#program-finder");
	this.input = this.el.find("#pf-search");
	this.selected;
	this.search_val;
	this.search_sel;
	this.search_label;
	this.filters = {};
	this.sort = "a-z";
	this.mode = $("#site").is(".page-homepage")?"module":"page";
	this.locked = this.el.find(".pf-results").is(".locked");
	var _self = this;
	
	this.init = function(){
		//console.log("TCCProgramFinder.init");
		
		if(this.locked){
			if(this.el.find(".pf-results").data("career")) this.filters["pf-career"] = this.el.find(".pf-results").data("career");
			if(this.el.find(".pf-results").data("type")) this.filters["pf-type"] = this.el.find(".pf-results").data("type");
			if(this.el.find(".pf-results").data("location")) this.filters["pf-location"] = this.el.find(".pf-results").data("location");
			if(this.el.find(".pf-results").data("interest")) this.filters["pf-interest"] = this.el.find(".pf-results").data("interest");
			this.submit();
		} else {
			if(this.mode=="module"){
				$("#pf-career").val("").trigger("change");
				$("#pf-type").val("").trigger("change");
				$("#pf-location").val("").trigger("change");
				$("#pf-interest").val("").trigger("change");
			}
			this.input.on("keyup change",this.handleSearch.bind(this)).on("click focus",function(){
				if($(this).val()!=="") _self.openResults();
			});
			this.el.find(".pf-search-wrap").on("focusout",function(_e){
				if(_e.relatedTarget) _self.closeResults(true);
			});
			this.el.find(".pf-filters select").on("change",this.handleFilterChange);
			this.el.find(".pf-info .btn._sort").on("click",this.toggleSort.bind(this));
			this.el.find(".btn._search").on("click",this.submit.bind(this));
			if(this.mode!=="module"){
				$(window).on("popstate",function(_e){
					tcc.getQuery();
					_self.syncQueryFilters();
				}).trigger("popstate");
			}
			
			this.checkQuery();
		}
		this.el.find(".pf-nav .btn._more").on("click",this.showPendingResults.bind(this));
		
		return this;
	};
	
	this.checkQuery = function(){
		//console.log("TCCProgramFinder.checkQuery");
		tcc.getQuery();
		if(tcc.qs["pf-career"]||tcc.qs["pf-type"]||tcc.qs["pf-location"]||tcc.qs["pf-interest"]){
			this.filterResults(tcc.qs);
		}
	};
	
	this.syncQueryFilters = function(){
		//console.log("TCCProgramFinder.syncQueryFilters");
		
		this.el.find(".pf-filters select").off("change",this.handleFilterChange);
		if(!$("#pf-career").val()||tcc.qs["pf-career"]) $("#pf-career").val(tcc.qs["pf-career"]).trigger("change");
		if(!$("#pf-type").val()||tcc.qs["pf-type"]) $("#pf-type").val(tcc.qs["pf-type"]).trigger("change");
		if(!$("#pf-location").val()||tcc.qs["pf-location"]) $("#pf-location").val(tcc.qs["pf-location"]).trigger("change");
		if(!$("#pf-interest").val()||tcc.qs["pf-interest"]) $("#pf-interest").val(tcc.qs["pf-interest"]).trigger("change");
		this.el.find(".pf-filters select").on("change",this.handleFilterChange);
		
		this.parseFilters();
		
		this.submit();
	};
	
	this.parseFilters = function(){
		//console.log("TCCProgramFinder.parseFilters");
		this.filters = {};
		if($("#pf-career").val()) this.filters["pf-career"] = $("#pf-career").val();
		if($("#pf-type").val()) this.filters["pf-type"] = $("#pf-type").val();
		if($("#pf-location").val()) this.filters["pf-location"] = $("#pf-location").val();
		if($("#pf-interest").val()) this.filters["pf-interest"] = $("#pf-interest").val();
	};
	
	this.handleSearch = function(_e){
		//console.log("TCCProgramFinder.handleSearch: "+_e.type+", "+_e.keyCode);
		this.search_val = this.input.val();
		if(_e.keyCode==40||_e.keyCode==39||_e.keyCode==38||_e.keyCode==37){//up, down
			var _opt = this.el.find(".search-opts .opt[data-item="+this.selected+"]");
			if(_e.keyCode==40||_e.keyCode==39){//down
				if(!_opt.is(":last-child")) this.selected = _opt.next().data("item");
			} else if(_e.keyCode==38||_e.keyCode==37){//up
				if(!_opt.is(":first-child")) this.selected = _opt.prev().data("item");
			}
			var _new_opt = this.el.find(".search-opts .opt[data-item="+this.selected+"]");
			if(_new_opt.length){
				var _opt_top = _new_opt.position().top;
				var _ps = this.el.find(".search-opts").scrollTop();
				var _ph = this.el.find(".search-opts").outerHeight();
				if(_opt_top<0){
					this.el.find(".search-opts").stop(false,false).animate({scrollTop:_ps+_opt_top},200);
				} else if(_opt_top+_new_opt.outerHeight()>_ph){
					this.el.find(".search-opts").stop(false,false).animate({scrollTop:_ps+(_opt_top+_new_opt.outerHeight()-_ph)},200);
				}
			}
		} else if(_e.keyCode==13){//enter
			this.selectResult(this.selected);
		}
		var _opts = [];
		if(this.search_val){
			for(var _t in TCCVals.pf_terms){
	//			if(TCCVals.pf_terms[_t].key.toLowerCase().indexOf(this.search_val.toLowerCase())>=0){ //loose match any part, ness = fitness/business
				if(TCCVals.pf_terms[_t].key.toLowerCase().indexOf(this.search_val.toLowerCase())==0){ //strict matching from start of string
					if(!_opts[TCCVals.pf_terms[_t].item]) _opts[TCCVals.pf_terms[_t].item] = TCCVals.programs[TCCVals.pf_terms[_t].item].id;
				}
			}
		}
		$(window).off("click",this.clickOff);
		this.el.find(".search-opts").empty();
		if(Object.keys(_opts).length){
			for(_prop in _opts) this.el.find(".search-opts").append("<div class=\"opt\" data-item=\""+_opts[_prop]+"\">"+TCCVals.programs[_prop].title+"</div>");
			this.el.find(".search-opts .opt").on("click",function(_e){
				_self.selectResult($(this).data("item"));
			});
			this.el.find(".search-opts .opt").on("mouseenter",function(){
				_self.el.find(".search-opts .opt").removeClass("sel");
				$(this).addClass("sel");
				_self.input.blur();
			}).on("mouseleave",function(){
				$(this).removeClass("sel");
			});
		} else {
			this.el.find(".search-opts .opt").off("click");
			if(this.search_val!=="") this.el.find(".search-opts").html("<div class=\"empty\">No matches!</div>");
		}
		this.openResults();
		if(!this.el.find(".search-opts .opt[data-item="+this.selected+"]").length||!this.selected) this.selected = this.el.find(".search-opts .opt:first-child").data("item");
		$(window).on("click",this.clickOff);
		this.el.find(".search-opts .opt").removeClass("sel");
		this.el.find(".search-opts .opt[data-item="+this.selected+"]").addClass("sel");
	};
	
	this.openResults = function(){
		//console.log("TCCProgramFinder.openResults");
		this.el.find(".pf-search-wrap").addClass("on");
	};
	this.closeResults = function(_nodelay){
		//console.log("TCCProgramFinder.closeResults");
		var _pf = this.el.find(".pf-search-wrap");
		setTimeout(_pf.removeClass.bind(_pf,"on"),_nodelay?10:200);
		$(window).off("click",this.clickOff);
	};
	this.clickOff = function(_e){
		//console.log("TCCProgramFinder.clickOff");
		if(_e.target!==_self.el.find(".pf-search-wrap")[0]&&!$(_e.target).parents("#program-finder .pf-search-wrap").length) _self.closeResults(true);
	};
	this.selectResult = function(_item){
		//console.log("TCCProgramFinder.selectResult, _item: "+_item);
		this.search_sel = _item;
		this.search_label = this.el.find(".search-opts .opt[data-item="+_item+"]").text();
		this.input.val(this.search_label).trigger("change");
		var _program = TCCVals.programs[_item];
		
		tcc.gaEvent({category:"Program Finder",action:"Suggested "+_program.title,label:this.search_val});
		
		tcc.toPage(_program.link);
	};
	
	this.handleFilterChange = function(_e){
		//console.log("TCCProgramFinder.handleFilterChange: "+$(this).attr("id"));
		_self.handleFilter(this);
	};
	
	this.handleFilter = function(_sel){
		//console.log("TCCProgramFinder.handleFilter: "+$(_sel).attr("id")+", "+$(_sel).val());
		this.parseFilters();
		if(this.mode!=="module"){
			var _params = this.getParams();
			tcc.setUrl("?"+_params.join("&"),false,false,true);
		}
		this.submit();
	};
	
	this.getParams = function(_filters){
		_filters = _filters||this.filters;
		var _params = [];
		for(var _k in _filters) _params.push(encodeURIComponent(_k)+"="+encodeURIComponent(_filters[_k]));
		return _params;
	};
	
	this.submit = function(){
		//console.log("TCCProgramFinder.submit: "+(this.search_sel?this.search_sel+" ("+this.search_label+")":JSON.stringify(this.filters)));
		
		tcc.gaEvent({category:"Program Finder",action:"Filter"+(this.mode=="module"?" (module)":""),label:JSON.stringify(this.filters)});
		
		if(this.mode=="module"){
			var _params = this.getParams();
			tcc.toPage("/programs/"+(_params.length?"?"+_params.join("&"):""));
		} else {
			this.filterResults();
		}
	};
	
	this.filterResults = function(){
		//console.log("TCCProgramFinder.filterResults: "+JSON.stringify(this.filters));
		this.filtered_progs = [];
		for(var _p in TCCVals.programs){
			var _prog = TCCVals.programs[_p];
			var _push = true;
			if(this.filters["pf-career"]&&_prog.career!==this.filters["pf-career"]) _push = false;
			if(this.filters["pf-type"]&&_prog.type.indexOf(this.filters["pf-type"])<0) _push = false;
			if(this.filters["pf-location"]&&_prog.location.indexOf(this.filters["pf-location"])<0) _push = false;
			if(this.filters["pf-interest"]&&_prog.interest.indexOf(this.filters["pf-interest"])<0) _push = false;
			if(_push) this.filtered_progs.push(_prog);
		}
		this.sortResults();
	};
	
	this.sortResults = function(){
		//console.log("TCCProgramFinder.sortResults");
		var _sorted = [];
		for(var _p in this.filtered_progs) _sorted.push(this.filtered_progs[_p]);
		if(this.sort=="a-z"){
			_sorted.sort(function(a,b){
				if(a.title<b.title) return -1;
				if(a.title>b.title) return 1;
				return 0;
			});
		} else if(this.sort=="z-a"){
			_sorted.sort(function(a,b){
				if(a.title>b.title) return -1;
				if(a.title<b.title) return 1;
				return 0;
			});
		}
		this.sorted_progs = _sorted;
		this.buildResults();
	};
	
	this.buildResults = function(){
		//console.log("TCCProgramFinder.buildResults");
		
		var _maxpage = parseInt(this.el.find(".pf-results").data("maxpage"))||12;
		if(_maxpage==-1) _maxpage = 1e3;
		
		this.el.find(".pf-results .pf-info .count").html(this.filtered_progs.length+" results");
		this.el.find(".pf-results .items").html("<div class=\"first\" /><div class=\"paged\" />");
		
		this.el.find(".pf-nav").toggleClass("on",this.filtered_progs.length>_maxpage);
		
		if(!this.sorted_progs.length){
			this.el.find(".pf-results .items").append("<div class=\"empty\">No matches!</div>");
		} else {
			var _p_i = 0;
			for(var _p in this.sorted_progs){
				_p_i++;
				var _cont = this.el.find(".pf-results .items").find(this.sorted_progs.length<_maxpage+1||(this.sorted_progs.length>_maxpage&&_p_i<_maxpage+1)?".first":".paged");
				var _prog = this.sorted_progs[_p];				
				
				var _html = "\
					<div class=\"item freset out\" data-id=\""+_prog.id+"\">\
						<a href=\""+_prog.link+"\" class=\"img\" title=\""+_prog.title+"\" style=\"background-image:url('"+_prog.img+"');\"></a>\
						<div class=\"content\">\
							<a href=\""+_prog.link+"\" class=\"nonh2\">"+_prog.title+"</a>\
							<ul>\
								<li><strong>Career Pathway:</strong><br />"+TCCVals.careers[_prog.career]+"</li>\
								<li><strong>Degree Type:</strong><br />"+_prog.degree.map(function(_i){ return TCCVals.degrees[_i]; }).join(", ")+"</li>\
								<li><strong>Location:</strong><br />"+_prog.location.map(function(_i){ return TCCVals.campuses[_i].type=="secondary"?TCCVals.campuses[_i].title:TCCVals.campuses[_i].location; }).join(", ")+"</li>\
							</ul>\
						</div>\
					</div>";
				
				_cont.append(_html);
			}
			this.staggerResults("first");
		}
	};
	
	this.toggleSort = function(){
		//console.log("TCCProgramFinder.toggleSort");
		this.sort = this.sort=="a-z"?"z-a":"a-z";
		this.el.find(".pf-info .btn._sort").text("Sorting results "+this.sort.toUpperCase());
		this.sortResults();
	};
	
	this.showPendingResults = function(){
		//console.log("TCCProgramFinder.showPendingResults");
		this.el.find(".pf-results .items .paged").addClass("in");
		this.el.find(".pf-nav").removeClass("on");
		this.staggerResults("paged");
		this.el.find(".pf-results .items .paged .item:first-child .img").focus();
		
		tcc.gaEvent({category:"Program Finder",action:"Show More Results",label:JSON.stringify(this.filters)});
		
	};
	
	this.resultIn = function(_cont){ _cont.find(".item.out").eq(0).removeClass("out"); };
	this.staggerResults = function(_which){
		//console.log("TCCProgramFinder.staggerResults");
		var _cont = this.el.find(".pf-results ."+_which);
		this.resultIn(_cont);
		clearInterval(this.results_in_tmr);
		this.results_in_tmr = setInterval(function(){
			if(!_cont.find(".item.out").length) return clearInterval(_self.results_in_tmr);
			_self.resultIn(_cont);
		},50);
	};
	
	return this.init();
}


function TCCSearch(){
	
	this.str = "";
	this.cat = "";
	this.type = "site";
	this.el;
	this.page;
	this.per_page;
	this.max_pages;
	this.total;
	
	var _self = this;
	
	this.init = function(){
		//console.log("TCCSearch.init");
		
		this.el = $(".search-module .search-results");
		this.str = this.el.data("s")||"";
		this.cat = this.el.data("cat")||"";
		this.page = this.el.data("page")||1;
		this.per_page = this.el.data("per-page");
		this.max_pages = this.el.data("max-pages");
		this.total = this.el.data("total");
		
		this.el.find(".page-nav .btn._next, .page-nav .btn._more").on("click",function(){
			if(!$(this).hasClass("disabled")) _self.navigateResults(1);
		});
		this.el.find(".page-nav .btn._prev").on("click",function(){
			if(!$(this).hasClass("disabled")) _self.navigateResults(-1);
		});
		if(this.el.is("#tcc-site-search")){
			this.per_page = this.per_page||5;
			$("#search-form").on("submit",function(_e){
				_e.preventDefault();
				_self.str = $("#site-search").val();
				_self.page = 0;
				_self.per_page = 0;
				_self.max_pages = 0;
				_self.total = 0;
				tcc.setUrl("?s="+_self.encodeString(_self.str),false,false,true);
				_self.submit();
				
				$(".sf-search").data("term",_self.str);
				tcc.initSFSearch();
			});
			$(window).on("popstate",function(_e){
				tcc.getQuery();
				_self.checkQuery();
			}).trigger("popstate");
			this.checkQuery();
		}
		if(this.el.is("#tcc-events-search")){
			this.type = "events";
			this.per_page = this.per_page||10;
		}
		return this;
	};
	
	this.encodeString = function(_str){
		//console.log("TCCSearch.encodeString: "+_str);
		return encodeURIComponent(_str).replace(/!/g,"%21").replace(/'/g,"%27").replace(/\(/g,"%28").replace(/\)/g,"%29").replace(/\%20/g,"+");
	};
	
	this.checkQuery = function(){
		//console.log("TCCSearch.checkQuery");
		if(tcc.qs.s&&tcc.qs.s!==this.encodeString($("#site-search").val())){
			$("#site-search").val(tcc.qs.s.replace(/\+/g," "));
			$("#search-form").submit();
		}
	};
	
	this.submit = function(){
		//console.log("TCCSearch.submit: "+this.type+", "+this.str);
		this.el.find(".info").html("Searching...");
		this.el.addClass("searching");
		this.el.find(".page-nav .btn._more").addClass("disabled").attr("disabled","disabled");
		this.el.find(".page-nav .btn._prev").addClass("disabled").attr("disabled","disabled");
		this.el.find(".page-nav .btn._next").addClass("disabled").attr("disabled","disabled");
		var _call = {
			method:'POST',
			beforeSend:function(_xhr){ _xhr.setRequestHeader('X-WP-Nonce',rest_object.api_nonce);},
			success:this.handleResults.bind(this)
		};
		if(this.type=="site"){
			_call.url = rest_object.api_url+'search/';
			_call.data = { s:this.str, per_page:this.per_page, page:this.page };
		} else if(this.type=="events"){
			_call.url = rest_object.api_url+'event_search/';
			_call.data = { category:this.cat, per_page:this.per_page, page:this.page };
		}
		$.ajax(_call);
	};
	
	this.navigateResults = function(_dir){
		//console.log("TCCSearch.navigateResults: "+_dir);
		_self.page = Math.min(_self.max_pages,Math.max(0,_self.page+_dir));
		this.submit();
	};
	
	this.handleResults = function(_res){
		//console.log("TCCSearch.handleResults");
		//console.log(_res);
		if(!_res){
			this.total = 0;
			this.per_page = 0;
			this.page = 0;
			this.max_pages = 0;
			this.el.find(".items").html("");
			this.el.find(".info").html("No results!");
			this.el.find(".page-nav").addClass("_disabled");
			this.el.removeClass("searching");
		} else {
			this.total = parseInt(_res.total);
			this.per_page = _res.per_page;
			this.page = _res.page?_res.page:1;
			this.max_pages = _res.max_pages;
			this.el.find(".page-nav").toggleClass("_disabled",this.max_pages<2);
			if(this.total>this.per_page) this.el.find(".page-nav .btn._more").removeClass("disabled").removeAttr("disabled","disabled");
			if(this.page>1){
				this.el.find(".page-nav._closed").removeClass("_closed");
				this.el.find(".page-nav .btn._prev.disabled").removeClass("disabled").removeAttr("disabled","disabled");
			}
			if(this.page<this.max_pages){
				this.el.find(".page-nav .btn._next.disabled").removeClass("disabled").removeAttr("disabled","disabled");
			}
			this.buildResults(_res);
			tcc.scrollTo(tcc.search.el.offset().top);
		}
	};
	
	this.buildResults = function(_res){
		//console.log("TCCSearch.buildResults");
		//console.log(_res);
		var _html = "";
		for(var _i in _res.posts){
			var _item = _res.posts[_i];
			if(this.type=="site"){
				_html += this.buildSiteResult(_item);
			} else if(this.type=="events"){
				_html += this.buildEventsResult(_item);
			}
		}
		this.el.find(".items").html(_html);
		this.el.find(".info").html((this.max_pages>1?"Page "+this.page+" of "+this.max_pages+" for ":"")+this.total+" results");
		this.el.removeClass("searching");
	};
	
	this.buildSiteResult = function(_item){
		return "\
		<div class=\"item _"+_item.type+"\">\
			<a href=\""+_item.link+"\" class=\"nonh3\">"+_item.title+"</a>\
			<p>"+_item.excerpt+"</p>\
		</div>";
	};
	
	this.buildEventsResult = function(_item){
		return "\
		<table class=\"event\">\
			<tr><td class=\"date-td\">\
				<div class=\"date\"><span class=\"day\">"+_item.day+"</span> "+_item.month+"</div>\
			</td><td>\
				<div class=\"content\">\
					<a href=\""+_item.link+"\"><label class=\"title\">"+_item.title+"</label></a>"+
					(_item.campus?"<div class=\"meta location-general\">"+_item.campus+"</div>":"")+
					(_item.rdate?"<div class=\"meta date-time\"><strong>When:</strong> "+_item.rdate+(_item.time?", "+_item.time:"")+"</div>":"")+
					(_item.location?"<div class=\"meta location-address\"><strong>Where:</strong> "+_item.location+"</div>":"")+
					(_item.address?"<div class=\"meta location-address\"><strong>Address:</strong> "+_item.address+"</div>":"")+
					(_item.content?"<div class=\"meta desc\"><strong>Description:</strong>\
						"+_item.content+"\
					</div>":"")+"\
					<div class=\"cta\">\
						<a href=\""+_item.link+"\" class=\"btn btn-3 btn-text btn-arrow\">See Calendar Detail</a>\
					</div>\
				</div>\
			</td></tr>\
		</table>";
	};
	
	return this.init();
	
}


function TCCMap(){
	var _self = this;
	this.map_markers = [];
	this.init = function(){
		//console.log("TCCMap.init");
		this.mod_el = $("#campuses");
		this.map_el = this.mod_el.find(".map");
		this.map_el.append("<div class=\"wrap\"></div>");
		this.map_wrap = this.map_el.find(".wrap");
		this.map_wrap.css({"background-image":"url('"+this.map_el.data("map-img")+"')"});
		this.map_wrap.append("<div class=\"markers\"></div><div class=\"info freset\"><button class=\"btn btn-close btn-1 _close\"></button></div>");
		
		this.map_locations = this.map_el.data("locations")?this.map_el.data("locations").split(","):[];
		this.map_corners = this.map_el.data("map-coords"); // top left, bottom right
		
		this.map_spans = [this.map_corners[0][0]-this.map_corners[1][0],this.map_corners[0][1]-this.map_corners[1][1]];
		
		for(var _c in TCCVals.campuses){
			var _campus = TCCVals.campuses[_c];
			if((!this.map_locations.length&&_campus.type=="primary")||(this.map_locations.length&&this.map_locations.indexOf(_campus.id)>=0)){
				var _html = "\
					<a href=\"#\" class=\"item\" data-id=\""+_campus.id+"\">\
						<div class=\"icon\"></div>\
						<label>"+_campus.title+"</label>\
						<p>"+_campus.addr_1+", "+_campus.addr_2+"</p>\
					</div>\
				";
				this.mod_el.find(".content").append(_html);
			}
			if(!this.map_locations.length||(this.map_locations.indexOf(TCCVals.campuses[_c].id)>=0)){
				if(_campus.coords){
					var _coords = _campus.coords.split(",");
					var _perc_x = (((this.map_corners[0][1]-_coords[1])/this.map_spans[1])*100).toFixed(1);
					var _perc_y = (((this.map_corners[0][0]-_coords[0])/this.map_spans[0])*100).toFixed(1);
					var _marker_html = "<button class=\"marker "+_campus.type+"\" title=\""+_campus.title+"\" data-id=\""+_campus.id+"\" style=\"top:"+_perc_y+"%;left:"+_perc_x+"%\"></button>";
					this.map_wrap.find(".markers").append(_marker_html);
				}
				this.map_wrap.find(".info").append(this.getMarkerContent(_campus));
			}
		}
		
		this.mod_el.find(".content .item, .markers .marker").on("click",function(_e){
			_self.openInfo($(this).data("id"));
			_e.preventDefault();
		});
		this.mod_el.find(".map .info .btn._close").on("click",this.closeInfo.bind(this));
		
		this.map_el.addClass("init");
		
		this.resize();
		
		return this;
		
	};
	this.getMarkerContent = function(_campus){
		//console.log("TCCMap.getMarkerContent: "+_campus.id);
		var _src = "<div class=\"campus-info\" data-id=\""+_campus.id+"\"><div class=\"campus-title-wrap\"><h5 class=\"campus-title\">"+_campus.title+"</h5></div><table><tr><td>"+_campus.addr_1+"<br />"+_campus.addr_2+"</td><td>Phone: <a href=\"tel:"+_campus.phone+"\">"+_campus.phone+"</a></td></tr></table><div class=\"btns\"><button class=\"btn btn-3 btn-min _directions\" onclick=\"tcc.campus_map.getDirections('"+_campus.addr_1+", "+_campus.addr_2+"');\">Get directions</button><button class=\"btn btn-arrow btn-min _more\" onclick=\"tcc.toPage('"+_campus.link+"');\">Learn more</button></div></div>";
		return _src;
	};
	this.openInfo = function(_id){
		//console.log("TCCMap.openInfo: "+_id);
		clearTimeout(this.closeInfo_tmr);
		this.map_el.find(".info").addClass("on");
		this.map_el.find(".info .campus-info.on").removeClass("on");
		this.map_el.find(".info .campus-info[data-id="+_id+"]").addClass("on");
		this.resize();
		tcc.scrollTo(this.map_el);
	};
	this.closeInfo = function(){
		//console.log("TCCMap.closeInfo");
		this.map_el.find(".info").addClass("out");
		clearTimeout(this.closeInfo_tmr);
		this.closeInfo_tmr = setTimeout(function(){
			_self.map_el.find(".info").removeClass("on out");
			_self.map_el.find(".info .campus-info.on").removeClass("on");
		},250);
	};
	this.getDirections = function(_addr){
		//console.log("TCCMap.getMapDirections: "+_addr);
		tcc.toPage("https://maps.google.com/?"+$.param({daddr:_addr}),"_directions");
	};
	this.resize = function(){
		//console.log("TCCMap.resize");
		if(this.map_wrap){
			var _map_size = [];
			if((this.map_el.outerWidth()/this.map_el.data("map-ratio"))>=this.map_el.outerHeight()){//scale based on width
				_map_size[0] = this.map_el.outerWidth();
				_map_size[1] = Math.ceil(_map_size[0]/this.map_el.data("map-ratio"));
			} else {//scale based on height
				_map_size[1] = this.map_el.outerHeight();
				_map_size[0] = Math.ceil(_map_size[1]*this.map_el.data("map-ratio"));
			}
			this.map_wrap.css({width:Math.ceil(_map_size[0]),height:Math.ceil(_map_size[1]),marginTop:Math.ceil(_map_size[1]*-.5),marginLeft:Math.ceil(_map_size[0]*-.5)});
			this.map_wrap.find(".info").css({marginTop:this.map_wrap.find(".info").outerHeight()*-.5});
		}
	};
	
	return this.init();
}


function TCCContentFeed(){
	
	this.el = $("#content-feed");
	this.tags = this.el.data("tags")||"";
	this.match = this.el.data("match")||"any";
	this.queue = [];
	this.items = [];
	this.index = 0;
	
	var _self = this;
	
	this.init = function(){
		//console.log("TCCContentFeed.init");
		
		this.pullItems(function(_res){
			//console.log("contentfeed init pull");
			//console.log(_res);
			var _html = "";
			for(var _i in _res){
				var _item = _res[_i];
				_html += "<div class=\"item _"+_i+"\" data-img=\""+_item.img+"\" data-id=\""+_item.id+"\" data-slug=\""+_item.slug+"\" data-type=\""+_item.type+"\" data-title=\""+_item.title+"\" data-copy=\""+_item.copy+"\"";
				if(_item.type=="video"){
					_html += " data-src=\""+_item.src+"\" data-poster=\""+_item.poster+"\""+(_item.captions?" data-captions=\""+_item.captions+"\"":"");
				} else if(_item.type=="article"){
					_html += " data-cta=\""+_item.cta+"\" data-link=\""+_item.link+"\" data-author=\""+_item.author+"\" data-date=\""+_item.date+"\" data-excerpt=\""+_item.excerpt+"\"";
				} else if(_item.type=="link"){
					_html += " data-cta=\""+_item.cta+"\" data-link=\""+_item.link+"\"";
				}
				_html += ">\
					<button class=\"hit\"></button>\
					<div class=\"wrap\">\
						"+(_item.type=="video"?"<div class=\"btn btn-play\"></div>":"")+"\
						"+(_item.title||_item.copy?"<div class=\"copy\">\
							"+(_item.title?"<label class=\"title nonh5\">"+_item.title+"</label>":"")+"\
							"+(_item.copy?"<p>"+_item.copy+"</p>":"")+"\
						</div>":"")+"\
						<div class=\"img\"></div>\
					</div>\
				</div>";
			}
			
			_self.el.append(_html);
			_self.resize();
			
			for(var _i=0;_i<Math.min(_self.items.length,9);_i++){
				$("#content-feed .item[data-slug="+_self.items[_i].slug+"]").on("click",function(_e){
					_e.preventDefault();
					if($(this).data("type")=="link"){
						tcc.toPage($(this).data("link"));
					} else {
						tcc.lightbox.open($(this).data("slug"));
					}
				});
			}
		});
		
		return this;
	};
	
	this.pullItems = function(_cb,_fb){
		//console.log("TCCContentFeed.pullItems");
		$.ajax({
			method:'POST',
			url:rest_object.api_url+'contentfeed/',
			data:{
				limit:9,
				exclude:this.queue.join(","),
				tag:this.tags,
				match:this.match
			},
			beforeSend:function(_xhr){
				_xhr.setRequestHeader('X-WP-Nonce',rest_object.api_nonce);
			},
			success:function(_res){
				if(_res&&_res.length){
					_self.parseItems(_res);
					if(_cb) _cb(_res);
				} else {
					if(_fb) _fb(_res);
				}
			}
		});
	};
	this.parseItems = function(_items){
		//console.log("TCCContentFeed.parseItems");
		for(var _i in _items){
			var _item = _items[_i];
			this.items.push(_item);
			this.queue.push(_item.id);
		}
	};
	this.getItem = function(_id){
		//console.log("TCCContentFeed.getItem: "+_id);
		if(typeof _id=="string"){
			for(var _i in this.items) if(this.items[_i].slug==_id) return this.items[_i];
		} else if(typeof _id=="number"){
			for(var _i in this.items) if(this.items[_i].id==_id) return this.items[_i];
		}
	};
	this.resize = function(){
		//console.log("TCCContentFeed.resize, mode: "+tcc.device.responsive);
		
		var _w = tcc.device.width;
		var _h = this.el.outerHeight();
		var _w1, _w2, _w3, _w4, _h1, _h2;
		var _img_srcs = [];
		
		var _1 = tcc.device.width>400?1:0;//for true mobile, step size down
		var _2 = tcc.device.width>400?2:1;
		
		if(this.el.hasClass("style-large")){
			if(tcc.device.responsive>4){
				_w1 = Math.ceil(_w*.333);
				_w2 = Math.ceil(_w1*.5);
				_w3 = _w-(_w1*2);
				_w4 = _w-(_w1*2+_w2);
				_h1 = Math.ceil(_h*.333);
				_h2 = _h-_h1;
				this.el.find(".item._0").css({width:_w1,height:_h2,top:0,left:0});
				this.el.find(".item._1").css({width:_w2,height:_h1,top:_h2,left:0});
				this.el.find(".item._2").css({width:_w2,height:_h1,top:_h2,left:_w2});
				this.el.find(".item._3").css({width:_w2,height:_h1,top:0,left:_w1});
				this.el.find(".item._4").css({width:_w2,height:_h1,top:0,left:_w1+_w2});
				this.el.find(".item._5").css({width:_w1,height:_h2,top:_h1,left:_w1});
				this.el.find(".item._6").css({width:_w3,height:_h2,top:0,left:_w1*2});
				this.el.find(".item._7").css({width:_w2,height:_h1,top:_h2,left:_w1*2});
				this.el.find(".item._8").css({width:_w4,height:_h1,top:_h2,left:(_w1*2)+_w2});
				_img_srcs = [_2,_1,_1,_1,_1,_2,_2,_1,_1];
			} else if(tcc.device.responsive==4){
				_w1 = Math.ceil(_w*.5);
				_w2 = Math.ceil(_w1*.5);
				_w3 = _w-_w1;
				_w4 = _w3-_w2;
				_h1 = Math.ceil(_h*.333);
				_h2 = _h-_h1;
				this.el.find(".item._0").css({width:_w1,height:_h2,top:0,left:0});
				this.el.find(".item._1").css({width:_w2,height:_h1,top:_h2,left:0});
				this.el.find(".item._2").css({width:_w2,height:_h1,top:_h2,left:_w2});
				this.el.find(".item._3").css({width:_w2,height:_h1,top:0,left:_w1});
				this.el.find(".item._4").css({width:_w4,height:_h1,top:0,left:_w1+_w2});
				this.el.find(".item._5").css({width:_w3,height:_h2,top:_h1,left:_w1});
				_img_srcs = [_2,_1,_1,_1,_1,_2,null,null,null];
			} else if(tcc.device.responsive==3||tcc.device.responsive==2){
				_w1 = Math.ceil(_w*.5);
				_w2 = _w-_w1;
				_h1 = Math.ceil(_h*.5);
				_h2 = _h-_h1;
				this.el.find(".item._0").css({width:_w1,height:_h,top:0,left:0});
				this.el.find(".item._1").css({width:_w2,height:_h1,top:0,left:_w2});
				this.el.find(".item._2").css({width:_w2,height:_h2,top:_h2,left:_w2});
				_img_srcs = [_2,_1,_1,null,null,null,null,null,null];
			} else if(tcc.device.responsive<2){
				_h1 = Math.ceil(_h*.33);
				_h2 = _h-(_h1*2);
				this.el.find(".item._0").css({width:_w,height:_h1,top:0,left:0});
				this.el.find(".item._1").css({width:_w,height:_h1,top:_h1,left:0});
				this.el.find(".item._2").css({width:_w,height:_h2,top:_h1*2,left:0});
				_img_srcs = [_1,_1,_1,null,null,null,null,null,null];
			}
		} else if(this.el.hasClass("style-row")){
			if(tcc.device.responsive>3){
				_w1 = Math.ceil(_w*.2);
				_w2 = _w-(_w1*4);
				this.el.find(".item._0").css({width:_w1,height:_h,top:0,left:0});
				this.el.find(".item._1").css({width:_w1,height:_h,top:0,left:_w1});
				this.el.find(".item._2").css({width:_w1,height:_h,top:0,left:_w1*2});
				this.el.find(".item._3").css({width:_w1,height:_h,top:0,left:_w1*3});
				this.el.find(".item._4").css({width:_w2,height:_h,top:0,left:_w1*4});
				_img_srcs = [_1,_1,_1,_1,_1,null,null,null,null];
			} else if(tcc.device.responsive==3){
				_w1 = Math.ceil(_w*.25);
				_w2 = _w-(_w1*3);
				this.el.find(".item._0").css({width:_w1,height:_h,top:0,left:0});
				this.el.find(".item._1").css({width:_w1,height:_h,top:0,left:_w1});
				this.el.find(".item._2").css({width:_w1,height:_h,top:0,left:_w1*2});
				this.el.find(".item._3").css({width:_w2,height:_h,top:0,left:_w1*3});
				_img_srcs = [_1,_1,_1,_1,null,null,null,null,null];
			} else if(tcc.device.responsive==2){
				_w1 = Math.ceil(_w*.33);
				_w2 = _w-(_w1*2);
				this.el.find(".item._0").css({width:_w1,height:_h,top:0,left:0});
				this.el.find(".item._1").css({width:_w1,height:_h,top:0,left:_w1});
				this.el.find(".item._2").css({width:_w2,height:_h,top:0,left:_w1*2});
				_img_srcs = [_1,_1,_1,null,null,null,null,null,null];
			} else if(tcc.device.responsive<2){
				_h1 = Math.ceil(_h*.5);
				_h2 = Math.ceil((_h-_h1)*.5);
				_h3 = _h-_h1-_h2;
				this.el.find(".item._0").css({width:_w,height:_h1,top:0,left:0});
				this.el.find(".item._1").css({width:_w,height:_h2,top:_h1,left:0});
				this.el.find(".item._2").css({width:_w,height:_h3,top:_h1+_h2,left:0});
				_img_srcs = [_1,_1,_1,null,null,null,null,null,null];
			}
		}
		
		for(var _i in _img_srcs){
			var _item = this.el.find(".item._"+_i);
			if(_item.data("img")){
				var _img_set = _item.data("img").split(",");
				if(_img_srcs[_i]!==null){
					_item.find(".img").css({"background-image":"url('"+(_img_set.length>1?_img_set[_img_srcs[_i]]:_img_set[0])+"')"});
					_item.find(".img").css({height:_item.outerHeight()-_item.find(".copy").outerHeight()});
				} else {
					_item.find(".img").css({"background-image":"none"});
				}
			}
		}
		
		this.el.find(".item .btn-play").each(function(){
			$(this).css({top:(($(this).parent().outerHeight()-$(this).siblings(".copy").outerHeight())*.5)});
		});
		
		this.el.addClass("js");
	};
	
	return this.init();
	
}


function TCCLightbox(){
	
	var _self = this;
	this.feed_total = false;
	
	this.init = function(){
		//console.log("TCCLightbox.init");
		
		$("#lightbox .fill").on("mouseenter",function(){
			$("#lightbox .btn-close").addClass("over");
		}).on("mouseleave",function(){
			$("#lightbox .btn-close").removeClass("over");
		});
		
		$("#lightbox .btn._close, #lightbox .fill").on("click",this.close);
		$("#lightbox .btn._prev, #lightbox .btn._next").on("click",function(){
			_self.navigate($(this).hasClass("_prev")?"prev":"next");
		});
		
		$(window).on("hashchange",function(){
			if(window.location.hash.indexOf("#focus-")==0){
				var _item = window.location.hash.substr(("#focus-").length);
				if(_item){
					if(!$("#lightbox").data("id")){
						_self.open(_item);
					} else if($("#lightbox").data("id")!==_item){
						_self.cycle(_item);
					}
				}
			} else if(!window.location.hash||window.location.hash=="#"||(window.location.hash.substr(0,("#focus-").length)!=="#focus-")){
				if($("#lightbox").data("id")) _self.close();
			}
		});
		
		return this;
	};
	
	this.build = function(_item){
		var _html = "";
		var _img_set = _item.img.split(",");
		if(_item.type=="video"){
			_html += "\
			<div class=\"type-video\">\
				<div class=\"content-video\">\
					<button class=\"hit\"></button>\
					<button class=\"btn btn-play\" tabindex=\"-1\"></button>\
					<div class=\"video-wrap\">\
						<video width=\"100%\" height=\"auto\""+(_item.poster?" poster=\""+_item.poster+"\"":"")+" preload=\"metadata\">\
							<source src=\""+_item.src+"\" type=\""+(_item.src_type?_item.src_type:"video/mp4")+"\">"+
							(_item.captions?"<track label=\"English\" kind=\"captions\" srclang=\"en\" src=\""+_item.captions+"\" default>":"")+"\
						</video>\
					</div>\
				</div>";
			if(_item.title||_item.copy){
				_html += "\
				<div class=\"caption\">"+
				(_item.title?"<label class=\"title nonh3\">"+_item.title+"</label>":"")+
				(_item.copy?"<p>"+_item.copy+"</p>":"")+"\
				</div>";
			}
			_html += "</div>";
		} else if(_item.type=="article"){
			_html += "\
			<div class=\"type-article\">\
				<div class=\"image\" style=\"background-image:url('"+_img_set[1]+"')\"></div>\
				<div class=\"article-copy\">\
					"+(_item.title?"<label class=\"title nonh2\">"+_item.title+"</label>":"")+
					(_item.author?"<span class=\"article-author\">"+_item.author+"</span> ":"")+
					(_item.date?"<span class=\"article-date\">"+_item.date+"</span>":"")+
					(_item.excerpt?"<div class=\"article-excerpt\">"+_item.excerpt+"</div>":"")+
					(_item.link?"\
					<div class=\"link-out\">\
						<a href=\""+_item.link+"\" class=\"btn btn-2 btn-min btn-arrow\">"+(_item.cta?_item.cta:"Read More")+"</a>\
					</div>":"")+"\
				</div>\
			</div>";
		} else if(_item.type=="image"){
			_html += "\
			<div class=\"type-image\">\
				<img class=\"image\" src=\""+_img_set[2]+"\" />";
			if(_item.title||_item.copy){
				_html += "\
				<div class=\"caption\">"+
				(_item.title?"<label class=\"title nonh3\">"+_item.title+"</label>":"")+
				(_item.copy?"<p>"+_item.copy+"</p>":"")+"\
				</div>";
			}
			_html += "</div>";
		} else if(_item.type=="link"){
			_html += "\
			<div class=\"type-link\" style=\"background-image:url('"+_img_set[1]+"')\">\
				<div class=\"caption\">"+
				(_item.title?"<label class=\"title nonh2\">"+_item.title+"</label>":"")+
				(_item.copy?"<p>"+_item.copy+"</p>":"")+
				(_item.link?"\
					<div class=\"link-out\">\
						<a href=\""+_item.link+"\" class=\"btn btn-2 btn-min btn-arrow\">"+(_item.cta?_item.cta:"Learn More")+"</a>\
					</div>":"")+"\
				</div>\
				<div class=\"fill\"></div>\
			</div>";
		}
		_html += "\
		<div class=\"catch\"></div>";
		
		return _html;
	};
	
	this.open = function(_item){
		//console.log("TCCLightbox.open: "+(_item.slug?_item.slug:_item));
		
		if($("#gateway").is(":visible")) return tcc.setHash("");
		
		_item = tcc.contentfeed.getItem(_item)||_item;
		
		if(typeof _item=="string"){
			
			$("#lightbox .btn._prev, #lightbox .btn._next").addClass("disabled").attr("disabled","disabled");
			$("#lightbox").data("id","loading");
			$("#lightbox .content .item").html("<div class=\"loading\"><span class=\"nonh2\">Loading</span></div>");
			
			$(window).off("keydown keyup",_self.handleKey);
			
			tcc.gaEvent({category:"Lightbox",action:"Load specific item",label:_item});
			
			$.ajax({
				method:'POST',
				url:rest_object.api_url+'feeditem/',
				data:{ slug:_item },
				beforeSend:function(_xhr){ _xhr.setRequestHeader('X-WP-Nonce',rest_object.api_nonce); },
				success:function(_res){
					if(_res&&_res.id){
						tcc.contentfeed.items.push(_res);
						tcc.contentfeed.queue.splice(0,0,_res.id);
					}
					tcc.contentfeed.index = -1;
					_self.navigate("next");
				},
				fail:function(_res){
					tcc.contentfeed.index = -1;
					_self.navigate("next");
				}
			});
			
		} else {
			
			tcc.setHash("focus-"+_item.slug);
			
			tcc.contentfeed.index = tcc.contentfeed.queue.indexOf(_item.id);
			
			$("#lightbox .btn._next").removeClass("disabled").removeAttr("disabled");
			
			if(tcc.contentfeed.index==0){
				$("#lightbox .btn._prev").addClass("disabled").attr("disabled","disabled");
			} else {
				$("#lightbox .btn._prev").removeClass("disabled").removeAttr("disabled");
			}
			
			$("#lightbox").data("id",_item.slug);
			$("#lightbox .content .item").html(this.build(_item));
			
			$("#lightbox .catch").on("mouseenter",function(){
				$("#lightbox .btn-close").addClass("over");
			}).on("mouseleave",function(){
				$("#lightbox .btn-close").removeClass("over");
			}).on("click",this.close);
			
			$(window).on("keydown keyup",_self.handleKey);
			
			tcc.gaEvent({category:"Lightbox",action:"View item",label:_item.slug});//view content feed item
			
			if(_item.type=="video"){
				this.video = new TCCContentVideo($("#lightbox .type-video .content-video"));
				this.video.play();
			}
		}
		
		tcc.device.scrolltop = document.body.scrollTop+document.documentElement.scrollTop||parseInt($("#site-wrap").css("margin-top"))*-1;
		
		$("body").addClass("on-lightbox");
		$("body,html").scrollTop(0);
		$("#site-wrap").css({"margin-top":tcc.device.scrolltop*-1,height:tcc.device.scrolltop+tcc.device.height});
		
		if(!$("#lightbox :focus").length) $("#lightbox .content .btn-close").focus();
	};
	
	this.handleKey = function(_e){
		//console.log("TCCLightbox.handleKey: "+_e.keyCode);
		if(_e.keyCode==27){
			$("#lightbox .btn._close").trigger("click");
		} else if(_e.keyCode==40||_e.keyCode==39||_e.keyCode==38||_e.keyCode==37){
			_e.preventDefault();
			if(_e.type=="keydown") return;
			if(_e.keyCode==40||_e.keyCode==39){
				$("#lightbox .btn._next").trigger("click");
			} else if(_e.keyCode==38||_e.keyCode==37){
				$("#lightbox .btn._prev").trigger("click");
			}
		}
	};
	
	this.navigate = function(_dir){
		if(!_dir) _dir = "next";
		//console.log("TCCLightbox.navigate: "+_dir);
		
		if(_dir=="prev"&&tcc.contentfeed.index==0) return;
		var _next_item = tcc.contentfeed.queue[tcc.contentfeed.index+(_dir=="prev"?-1:1)];
		
		tcc.gaEvent({category:"Lightbox",action:"Navigate",label:_dir});
		
		$(window).off("keydown keyup",_self.handleKey);
		
		if(!_next_item){
			if(!this.feed_total){
				$("#lightbox .btn._prev, #lightbox .btn._next").addClass("disabled").attr("disabled","disabled");
				$("body").addClass("cycle-lightbox-out");
				clearTimeout(tcc.lightbox_tmr);
				
				tcc.gaEvent({category:"Lightbox",action:"Load more items"});
				
				tcc.contentfeed.pullItems(this.navigate.bind(this),this.loop.bind(this));
			} else {
				this.loop();
			}
		} else {
			this.cycle(_next_item);
		}
	};
	
	this.cycle = function(_item){
		//console.log("TCCLightbox.cycle: "+(_item.slug?_item.slug:_item));
		$("body").addClass("cycle-lightbox-out");
		clearTimeout(tcc.lightbox_tmr);
		tcc.lightbox_tmr = setTimeout(function(){
			$("body").removeClass("cycle-lightbox-out");
			_self.open(_item);
		},250);
		
	};
	
	this.loop = function(){
		//console.log("TCCLightbox.loop");
		tcc.contentfeed.index = -1;
		this.feed_total = true;
		
		tcc.gaEvent({category:"Lightbox",action:"Looped!"});
		
		this.navigate("next");
	};
	
	this.close = function(){
		//console.log("TCCLightbox.close");
		
		$(window).off("keydown keyup",_self.handleKey);
		
		if(window.location.hash=="#focus-"+$("#lightbox").data("id")) tcc.setHash("");
		
		$("body").addClass("off-lightbox");
		clearTimeout(tcc.lightbox_tmr);
		tcc.lightbox_tmr = setTimeout(function(){
			$("body").removeClass("on-lightbox off-lightbox");
			$("#site-wrap").css({"margin-top":0,height:"auto"});
			$("body,html").stop(false,false).animate({scrollTop:tcc.device.scrolltop},0);
			tcc.device.scrolltop = null;
			$("#lightbox").data("id","");
			if(_self.video) _self.video.reset();
		},200);
	};
	
	return this.init();
}


function TCCContentVideo(_el){
	
	this.el = _el;
	var _self = this;
	
	this.init = function(_vid){
		//console.log("TCCContentVideo.init");
		this.el.find(".hit").on("click",this.play.bind(this));
		this.el.find("video")[0].addEventListener("ended",this.reset.bind(this));
		if(this.el.find("video")[0]&&this.el.find("video")[0].textTracks[0]) this.el.find("video")[0].textTracks[0].mode = "hidden";
	};
	this.play = function(){
		//console.log("TCCContentVideo.play");
		this.el.addClass("on");
		this.el.find("video").attr("controls",true)[0].currentTime = 0;
		this.el.find("video")[0].play();
		
		tcc.gaEvent({category:"Video",action:"Play",label:this.el.find("video").find("source").attr("src")});//play content video
		
	};
	this.reset = function(){
		//console.log("TCCContentVideo.reset");
		_v = this.el.find("video")[0];
		_v.currentTime = 0;
		_v.pause();
		_v.load();
		this.el.removeClass("on");
		$(_v).removeAttr("controls");
	};
	
	return this.init();
}