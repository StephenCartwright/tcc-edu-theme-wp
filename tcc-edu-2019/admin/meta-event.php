<div id="page_settings" class="meta-settings">

<table id="page_main" class="group">
	<tr><td>
		
		<table class="control solo" name="event_options">
			<tr>
				<td><label for="event_date">Date</label></td>
				<td><input type="text" class="_date" id="event_date" name="event_date" placeholder="Select date" value="<?php echo esc_attr($event_meta['event_date'][0]); ?>" /></td>
			</tr>
			<tr>
				<td><label for="event_time">Time</label><sub><em>"12:00 pm - 2:00 pm"</em></sub></td>
				<td><input type="text" id="event_time" name="event_time" value="<?php echo esc_attr($event_meta['event_time'][0]); ?>" /></td>
			</tr>
			<tr>
				<td><label for="event_campus">Campus</label><sub>The custom campus overrides the selected campus title, but where links are used the custom campus uses the selected campus page url</sub></td>
				<td>
					<select id="event_campus" name="event_campus"><?php
						echo "<option".(!$event_meta['event_campus'][0]?" selected":"")."></option>";
						for($c=1;$c<=15;$c++){
							if(get_option('campus_'.$c.'_show')){
								$campus_id = get_option('campus_'.$c.'_id')?get_option('campus_'.$c.'_id'):sanitize_title(get_option('campus_'.$c.'_title'));
								echo "<option value=\"".$campus_id."\"".($event_meta['event_campus'][0]==$campus_id?" selected":"").">".get_option('campus_'.$c.'_title')."</option>";
							}
						}
					?></select>
					<input type="text" id="event_campus_other" name="event_campus_other" placeholder="Custom campus" value="<?php echo esc_attr($event_meta['event_campus_other'][0]); ?>" size="25" />
				</td>
			</tr>
			<tr>
				<td><label for="event_location">Specific Location</label><sub><em>"Martin Building, Room 2610"</em></td>
				<td><input type="text" id="event_location" name="event_location" value="<?php echo esc_attr($event_meta['event_location'][0]); ?>" size="25" /></td>
			</tr>
			<tr>
				<td><label for="event_address">Address</label><sub><em>"300 Granby Street, Norfolk, VA 23510"</em></td>
				<td><input type="text" id="event_address" name="event_address" value="<?php echo esc_attr($event_meta['event_address'][0]); ?>" size="25" /></td>
			</tr>
		</table>
		
	</td></tr>
</table>

</div>