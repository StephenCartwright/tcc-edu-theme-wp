<div id="page_settings" class="meta-settings">

<table id="page_main" class="group">
	<tr><td>
		
		<table class="control solo" name="program_options">
			<tr>
				<td><label for="program_id">ID</label></td>
				<td><input type="text" class="_autoslug _format-slug" data-slugs="#title" id="program_id" name="program_id" value="<?php echo esc_attr($program_meta['program_id'][0]); ?>" /></td>
			</tr>
			<tr>
				<td><label for="program_title">Long Title</label><sub>A long title that's displayed in the <em>[program_info]</em> shortcode.</sub></td>
				<td><input type="text" id="program_title" name="program_title" value="<?php echo esc_attr($program_meta['program_title'][0]); ?>" /></td>
			</tr>
			<tr>
				<td><label for="program_desc">Description</label><sub>A text description that's displayed in the <em>[program_info]</em> shortcode. This field supports html.</sub></td>
				<td><textarea id="program_desc" class="long" name="program_desc"><?php echo stripslashes(esc_attr($program_meta['program_desc'][0])); ?></textarea></td>
			</tr>
			<tr>
				<td><label for="program_semesters">Semesters</label></td>
				<td><input type="text" id="program_semesters" name="program_semesters" value="<?php echo esc_attr($program_meta['program_semesters'][0]); ?>" /></td>
			</tr>
			<tr>
				<td><label for="program_credits">Credits</label></td>
				<td><input type="text" id="program_credits" name="program_credits" value="<?php echo esc_attr($program_meta['program_credits'][0]); ?>" /></td>
			</tr>
			<tr>
				<td><label for="program_curriculum">Curriculum Link</label></td>
				<td><input type="text" id="program_curriculum" name="program_curriculum" value="<?php echo esc_attr($program_meta['program_curriculum'][0]); ?>" /></td>
			</tr>
			<tr>
				<td><label for="program_link">Page Link</label></td>
				<td><input type="text" id="program_link" name="program_link" value="<?php echo esc_attr($program_meta['program_link'][0]); ?>" /></td>
			</tr>
			<tr>
				<td><label for="program_career">Career Pathway</label></td>
				<td>
					<select id="program_career" name="program_career"><?php
						echo "<option value=\"\"".(!$program_meta['program_career'][0]?" selected":"")."></option>";
						foreach(json_decode(get_option('career_options_list')) as $option){
							echo "<option value=\"".$option->id."\"".($program_meta['program_career'][0]==$option->id?" selected":"").">".$option->name."</option>";
						}
					?></select>
				</td>
			</tr>
			<tr>
				<td><label for="program_type">Program Types</label></td>
				<td>
					<div class="checkbox-group"><?php
						foreach(json_decode(get_option('program_type_options_list')) as $option){
							echo "<div class=\"option\"><label><input type=\"checkbox\" name=\"program_type[]\" value=\"".$option->id."\"".(in_array($option->id,unserialize($program_meta['program_type'][0]))?" checked=\"checked\"":"")." />".$option->name."</label></div>";
						}
					?></div>
				</td>
			</tr>
			<tr>
				<td><label for="program_degree">Degree Types</label></td>
				<td>
					<div class="checkbox-group"><?php
						foreach(json_decode(get_option('degree_options_list')) as $option){
							echo "<div class=\"option\"><label><input type=\"checkbox\" name=\"program_degree[]\" value=\"".$option->id."\"".(in_array($option->id,unserialize($program_meta['program_degree'][0]))?" checked=\"checked\"":"")." />".$option->name."</label></div>";
						}
					?></div>
				</td>
			</tr>
			<tr>
				<td><label for="program_location">Locations</label></td>
				<td>
					<div class="checkbox-group"><?php
						for($c=1;$c<=15;$c++){
							if(get_option('campus_'.$c.'_show')){
								$campus_id = get_option('campus_'.$c.'_id')?get_option('campus_'.$c.'_id'):sanitize_title(get_option('campus_'.$c.'_title'));
								echo "<div class=\"option\"><label><input type=\"checkbox\" name=\"program_location[]\" value=\"".$campus_id."\"".(in_array($campus_id,unserialize($program_meta['program_location'][0]))?" checked=\"checked\"":"")." />".get_option('campus_'.$c.'_title')."</label></div>";
							}
						}
					?></div>
				</td>
			</tr>
			<tr>
				<td><label for="program_interest">Interest Types</label></td>
				<td>
					<div class="checkbox-group"><?php
						foreach(json_decode(get_option('interest_options_list')) as $option){
							echo "<div class=\"option\"><label><input type=\"checkbox\" name=\"program_interest[]\" value=\"".$option->id."\"".(in_array($option->id,unserialize($program_meta['program_interest'][0]))?" checked=\"checked\"":"")." />".$option->name."</label></div>";
						}
					?></div>
				</td>
			</tr>
			<tr>
				<td><label for="program_terms">Associated Terms</label><sub>A list of alternate terms to associate with this program in the Program Finder open search. Case insensitive. Add each term on a new line.</sub></td>
				<td><textarea id="program_terms" class="medium" name="program_terms"><?php echo esc_attr($program_meta['program_terms'][0]); ?></textarea></td>
			</tr>
		</table>
		
	</td></tr>
</table>

</div>