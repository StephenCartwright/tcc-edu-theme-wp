<?php if(!$tccedu_meta) return; ?>

<div class="wrap">
<h2>TCC.edu Theme Settings</h2>
<?php if($meta_state) echo '<div id="setting-error-settings_updated" class="updated settings-error"><p><strong>'.$meta_state.'</strong></p></div>'; ?>
<form method="post" action="admin.php?page=site-settings">
<?php wp_nonce_field('tccedu_meta','tccedu_meta_nonce'); ?>

<div id="tccedu_settings" class="meta-settings">
	<table class="form-table">
		<tr>
			<td>
		
		<table class="section"><tr><td>
			<h2>Alerts</h2>
			<div id="alerts_options" class="control-wrap">
				<table class="control-group" name="alert_major_options">
					<tr><td><label for="alert_major_show" class="_for-toggler"><input type="checkbox" class="_toggler" data-toggles="#alert_major_options" name="alert_major_show" id="alert_major_show" value="1"<?php echo get_option('alert_major_show')=="1"?" checked=\"checked\"":""; ?> /> Enable Major Alert</label></td></tr>
					<tr><td>
						<div id="alert_major_options">
							<table class="group">
								<tr>
									<td><label for="alert_major_title">Title</label></td>
									<td><input type="text" id="alert_major_title" name="alert_major_title" value="<?php echo get_option('alert_major_title'); ?>" /></td>
								</tr>
								<tr>
									<td><label for="alert_major_copy">Copy</label><sub>Messages should be kept minimal with further details in a link. This field supports html.</sub></td>
									<td><textarea id="alert_major_copy" name="alert_major_copy"><?php echo stripslashes(get_option('alert_major_copy')); ?></textarea></td>
								</tr>
								<tr>
									<td><label for="alert_major_conf">Continue Button Label</label><sub></sub></td>
									<td><input type="text" id="alert_major_conf" name="alert_major_conf" placeholder="Continue" value="<?php echo get_option('alert_major_conf'); ?>" /></td>
								</tr>
								<tr>
									<td><label for="alert_major_link">Custom Button Link</label></td>
									<td><input type="text" id="alert_major_link" name="alert_major_link" value="<?php echo get_option('alert_major_link'); ?>" /></td>
								</tr>
								<tr>
									<td><label for="alert_major_cta">Custom Button Label</label></td>
									<td><input type="text" id="alert_major_cta" name="alert_major_cta" value="<?php echo get_option('alert_major_cta'); ?>" /></td>
								</tr>
								<tr>
									<td><label for="alert_major_link_2">Custom Button 2 Link</label></td>
									<td><input type="text" id="alert_major_link_2" name="alert_major_link_2" value="<?php echo get_option('alert_major_link_2'); ?>" /></td>
								</tr>
								<tr>
									<td><label for="alert_major_cta_2">Custom Button 2 Label</label></td>
									<td><input type="text" id="alert_major_cta_2" name="alert_major_cta_2" value="<?php echo get_option('alert_major_cta_2'); ?>" /></td>
								</tr>
							</table>
						</div>
					</td></tr>
				</table>
				
				<table class="control-group" name="alert_medium_options">
					<tr><td><label for="alert_medium_show" class="_for-toggler"><input type="checkbox" class="_toggler" data-toggles="#alert_medium_options" name="alert_medium_show" id="alert_medium_show" value="1"<?php echo get_option('alert_medium_show')=="1"?" checked=\"checked\"":""; ?> /> Enable Medium Alert</label></td></tr>
					<tr><td>
						<div id="alert_medium_options">
							<table class="group">
								<tr>
									<td><label for="alert_medium_title">Title</label></td>
									<td><input type="text" id="alert_medium_title" name="alert_medium_title" value="<?php echo get_option('alert_medium_title'); ?>" /></td>
								</tr>
								<tr>
									<td><label for="alert_medium_copy">Copy</label><sub>Messages should be kept minimal with further details in a link. This field supports html.</sub></td>
									<td><textarea id="alert_medium_copy" name="alert_medium_copy"><?php echo stripslashes(get_option('alert_medium_copy')); ?></textarea></td>
								</tr>
								<tr>
									<td><label for="alert_medium_link">Custom Button Link</label></td>
									<td><input type="text" id="alert_medium_link" name="alert_medium_link" value="<?php echo get_option('alert_medium_link'); ?>" /></td>
								</tr>
								<tr>
									<td><label for="alert_medium_cta">Custom Button Label</label></td>
									<td><input type="text" id="alert_medium_cta" name="alert_medium_cta" value="<?php echo get_option('alert_medium_cta'); ?>" /></td>
								</tr>
							</table>
						</div>
					</td></tr>
				</table>
				
				<table class="control-group" name="alert_minor_options">
					<tr><td><label for="alert_minor_show" class="_for-toggler"><input type="checkbox" class="_toggler" data-toggles="#alert_minor_options" name="alert_minor_show" id="alert_minor_show" value="1"<?php echo get_option('alert_minor_show')=="1"?" checked=\"checked\"":""; ?> /> Enable Minor Alert</label></td></tr>
					<tr><td>
						<div id="alert_minor_options">
							<table class="group">
								<tr>
									<td><label for="alert_minor_copy">Copy</label><sub>Messages should be kept minimal with further details in a link. This field supports html.</sub></td>
									<td><textarea id="alert_minor_copy" name="alert_minor_copy"><?php echo stripslashes(get_option('alert_minor_copy')); ?></textarea></td>
								</tr>
								<tr>
									<td><label for="alert_minor_cookie_days">Cookie Length</label><sub>Prevent the Minor Alert from showing again for # days</sub></td>
									<td><input type="number" min="0" id="alert_minor_cookie_days" name="alert_minor_cookie_days" placeholder="1" value="<?php echo get_option('alert_minor_cookie_days'); ?>" /></td>
								</tr>
							</table>
						</div>
					</td></tr>
				</table>
			</div>
			
		</td></tr></table>
		
		<table class="section"><tr><td>
			<h2>Help Bug</h2>
			<p>Settings for the pop-up urging site visitors to contact TCC. After # number of page views in one day, the Help Bug is triggered which begins a countdown of # seconds before the Help Bug is displayed on the page. The timer is triggered this way on each page load until the Help Bug is clicked or dismissed.</p>
			<div id="help_bug_options" class="control-wrap">
				
				<table class="control-group" name="help_bug_options">
					<tr><td><label for="help_bug_show" class="_for-toggler"><input type="checkbox" class="_toggler" data-toggles="#help_bug_options" name="help_bug_show" id="help_bug_show" value="1"<?php echo get_option('help_bug_show')=="1"?" checked=\"checked\"":""; ?> /> Enable Help Bug</label></td></tr>
					<tr><td>
						<div id="help_bug_options">
							<table class="group">
								<tr>
									<td><label for="help_bug_schedule">Call Schedule</label><sub>A simple weekly schedule for when to display the phone cta vs help center cta. Omit days with no schedule. Add each scheduled day on a new line in this format, as 24-hour EST time: <em>"Monday&nbsp;0900-1700"</em>.</sub></td>
									<td>
									<?php
										$schedule_text = array();
										foreach(json_decode(get_option('help_bug_schedule')) as $schedule_day => $schedule_time){
											$schedule_text[] = $schedule_day." ".implode("-",$schedule_time);
										}
									?>
									
									<textarea class="medium" id="help_bug_schedule" name="help_bug_schedule" placeholder="Monday 0900-1700
Tuesday 0900-1700
Wednesday 0900-1700
Thursday 0900-1700
Friday 0900-1700
Saturday 0900-1300"><?php echo implode("\r\n",$schedule_text); ?></textarea></td>
								</tr>
								<tr>
									<td><label for="help_bug_phone">Phone Number</label><sub>Phone number when showing the phone cta</sub></td>
									<td><input type="text" id="help_bug_phone" name="help_bug_phone" value="<?php echo get_option('help_bug_phone'); ?>" /></td>
								</tr>
								<tr>
									<td><label for="help_bug_cookie_days">Cookie Length</label><sub>When clicked or dismissed, prevent the Help Bug from showing again for # days</sub></td>
									<td><input type="number" min="0" id="help_bug_cookie_days" name="help_bug_cookie_days" placeholder="90" value="<?php echo get_option('help_bug_cookie_days'); ?>" /></td>
								</tr>
								<tr>
									<td><label for="help_bug_trigger_after">Trigger After</label><sub>How many page views should be counted before triggering the Help Bug timer</sub></td>
									<td><input type="number" min="0" id="help_bug_trigger_after" name="help_bug_trigger_after" placeholder="3" value="<?php echo get_option('help_bug_trigger_after'); ?>" /></td>
								</tr>
								<tr>
									<td><label for="help_bug_timer">Timer Length</label><sub>How many seconds to wait before displaying the Help Bug after it's been triggered</sub></td>
									<td><input type="number" min="0" id="help_bug_timer" name="help_bug_timer" placeholder="30" value="<?php echo get_option('help_bug_timer'); ?>" /></td>
								</tr>
							</table>
						</div>
					</td></tr>
				</table>
				
			</div>
		</td></tr></table>
		
		<table class="section"><tr><td>
			<h2>Salesforce</h2>
			<p>Settings for Salesforce integration.</p>
			<div id="salesforce_options" class="control-wrap">
				
				<table class="control-group" name="salesforce_options">
					<tr><td>
						<table class="group">
							<tr>
								<td><label for="sf_pdf_prefix">Download Prefix</label><sub>URL prefix for Salesforce Download IDs</sub></td>
								<td><input type="text" id="sf_pdf_prefix" name="sf_pdf_prefix" value="<?php echo get_option('sf_pdf_prefix'); ?>" /></td>
							</tr>
						</table>
					</td></tr>
				</table>
				
			</div>
		</td></tr></table>
		
		<table class="section"><tr><td>
			<h2>Promos</h2>
			<p>Preset promo blocks used as page sidebars and in page content. Buttons are optional.</p>
			<div id="promos_options" class="control-wrap"><?php
			for($p=1;$p<=5;$p++){
				echo "
				<table class=\"control-group\" name=\"promo_".$p."_options\">
					<tr><td><label for=\"promo_".$p."_show\" class=\"_for-toggler\"><input type=\"checkbox\" class=\"_toggler\" data-toggles=\"#promo_".$p."_options\" name=\"promo_".$p."_show\" id=\"promo_".$p."_show\" value=\"1\"".(get_option('promo_'.$p.'_show')=="1"?" checked=\"checked\"":"")."/> Enable Promo ".$p."</label></td></tr>
					<tr><td>
						<div id=\"promo_".$p."_options\">
							<table class=\"group\">
								<tr>
									<td><label for=\"promo_".$p."_title\">Title</label><sub>Supports html</sub></td>
									<td><textarea id=\"promo_".$p."_title\" name=\"promo_".$p."_title\" maxlength=\"1250\">".stripslashes(get_option('promo_'.$p.'_title'))."</textarea></td>
								</tr>
								<tr>
									<td><label for=\"promo_".$p."_copy\">Copy</label><sub>Supports html</sub></td>
									<td><textarea class=\"medium\" id=\"promo_".$p."_copy\" name=\"promo_".$p."_copy\" maxlength=\"2500\">".stripslashes(get_option('promo_'.$p.'_copy'))."</textarea></td>
								</tr>
								<tr>
									<td><label for=\"promo_".$p."_cta\">Button CTA</label></td>
									<td><input type=\"text\" id=\"promo_".$p."_cta\" name=\"promo_".$p."_cta\" placeholder=\"Learn More\" value=\"".get_option('promo_'.$p.'_cta')."\" /></td>
								</tr>
								<tr>
									<td><label for=\"promo_".$p."_link\">Button Link</label></td>
									<td><input type=\"text\" id=\"promo_".$p."_link\" name=\"promo_".$p."_link\" value=\"".get_option('promo_'.$p.'_link')."\" /></td>
								</tr>
								<tr>
									<td><label for=\"promo_".$p."_btn_class\">Button Class</label></td>
									<td><input type=\"text\" id=\"promo_".$p."_btn_class\" name=\"promo_".$p."_btn_class\" placeholder=\"btn-4 btn-text btn-arrow\" value=\"".get_option('promo_'.$p.'_btn_class')."\" /></td>
								</tr>
							</table>
						</div>
					</td></tr>
				</table>
				";
			}
			?></div>
			
		</td></tr></table>
		
		
			</td>
		</tr>
	</table>
</div>

<?php submit_button('Save Changes','primary','submit-form'); ?>
	
</form>
</div>