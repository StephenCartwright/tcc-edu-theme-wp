<?php if(!$tccedu_meta) return; ?>

<div class="wrap">
<h2>TCC.edu Theme Settings</h2>
<?php if($meta_state) echo '<div id="setting-error-settings_updated" class="updated settings-error"><p><strong>'.$meta_state.'</strong></p></div>'; ?>
<form method="post" action="admin.php?page=site-settings-campuses">
<?php wp_nonce_field('tccedu_meta','tccedu_meta_nonce'); ?>

<div id="tccedu_settings" class="meta-settings">
	<table class="form-table">
		<tr>
			<td>
		
		<table class="section"><tr><td>
			<h2>Campus Locations</h2>
			<p>These values represent options for program locations, map markers, etc.</p>
			<div id="campuses_options" class="control-wrap"><?php
			for($c=1;$c<=15;$c++){
				echo "
				<table class=\"control-group\" name=\"campus_".$c."_options\">
					<tr><td><label for=\"campus_".$c."_show\" class=\"_for-toggler\"><input type=\"checkbox\" class=\"_toggler\" data-toggles=\"#campus_".$c."_options\" name=\"campus_".$c."_show\" id=\"campus_".$c."_show\" value=\"1\"".(get_option('campus_'.$c.'_show')=="1"?" checked=\"checked\"":"")."/> Enable Campus</label></td></tr>
					<tr><td>
						<div id=\"campus_".$c."_options\">
							<table class=\"group\">
								<tr>
									<td><label for=\"campus_".$c."_title\">Title</label></td>
									<td><input type=\"text\" class=\"large-text\" id=\"campus_".$c."_title\" name=\"campus_".$c."_title\" value=\"".get_option('campus_'.$c.'_title')."\" /></td>
								</tr>
								<tr>
									<td><label for=\"campus_".$c."_id\">ID</label></td>
									<td><input type=\"text\" class=\"_autoslug _format-slug\" data-slugs=\"#campus_".$c."_title\" id=\"campus_".$c."_id\" name=\"campus_".$c."_id\" value=\"".get_option('campus_'.$c.'_id')."\" /></td>
								</tr>
								<tr>
									<td><label for=\"campus_".$c."_location\">General Location</label><sub><em>\"Norfolk\"</em>, <em>\"Portsmouth\"</em>, etc</sub></td>
									<td><input type=\"text\" id=\"campus_".$c."_location\" name=\"campus_".$c."_location\" value=\"".get_option('campus_'.$c.'_location')."\" /></td>
								</tr>
								<tr>
									<td><label for=\"campus_".$c."_link\">Page Link</label></td>
									<td><input type=\"text\" id=\"campus_".$c."_link\" name=\"campus_".$c."_link\" value=\"".get_option('campus_'.$c.'_link')."\" /></td>
								</tr>
								<tr>
									<td><label for=\"campus_".$c."_phone\">Phone Number</label></td>
									<td><input type=\"text\" id=\"campus_".$c."_phone\" name=\"campus_".$c."_phone\" value=\"".get_option('campus_'.$c.'_phone')."\" /></td>
								</tr>
								<tr>
									<td><label for=\"campus_".$c."_map_pdf\">Campus Map PDF</label></td>
									<td>
									
										<div class=\"editor\">
											<div class=\"editor-control\">
												<input type=\"button\" class=\"button _media-pdf-select\" data-id=\"#campus_".$c."_map_pdf_id\" value=\"Choose PDF\" />
												<input type=\"button\" class=\"button _media-clear\" data-id=\"#campus_".$c."_map_pdf_id, #campus_".$c."_map_pdf_src\" value=\"Clear\" />
												<input type=\"text\" id=\"campus_".$c."_map_pdf_id\" class=\"entry _pdf-previews\" data-pre=\"#campus_".$c."_map_pdf_pre\" data-simple-src=\"#campus_".$c."_map_pdf_src\" name=\"campus_".$c."_map_pdf_id\" value=\"".get_option('campus_'.$c.'_map_pdf_id')."\" />
												<input type=\"text\" id=\"campus_".$c."_map_pdf_src\" class=\"_pdf-simple-previews\" data-pre=\"#campus_".$c."_map_pdf_pre\" data-id-src=\"#campus_".$c."_map_pdf_id\" name=\"campus_".$c."_map_pdf_src\" value=\"".get_option('campus_'.$c.'_map_pdf_src')."\" />
											</div>
											<div class=\"summary\" id=\"campus_".$c."_map_pdf_pre\">
												<div class=\"info\"></div>
												<div class=\"link empty\"></div>
											</div>
										</div>
									</td>
								</tr>
								<tr>
									<td><label for=\"campus_".$c."_is_search\">Search Option</label><sub>If checked, this is a search option in the Program Finder</sub></td>
									<td><input type=\"checkbox\" name=\"campus_".$c."_is_search\" id=\"campus_".$c."_is_search\" value=\"1\"".(get_option('campus_'.$c.'_is_search')=="1"?" checked=\"checked\"":"")."/></td>
								</tr>
								<tr>
									<td><label for=\"campus_".$c."_type\">Campus Type</label></td>
									<td>
										<select id=\"campus_".$c."_type\" class=\"_tabber\" data-tabs=\".campus_".$c."_type\"  name=\"campus_".$c."_type\">
											<option value=\"primary\"".(get_option('campus_'.$c.'_type')=="primary"?" selected":"").">Primary Campus</option>
											<option value=\"secondary\"".(get_option('campus_'.$c.'_type')=="secondary"?" selected":"").">Secondary Campus</option>
											<option value=\"other\"".(get_option('campus_'.$c.'_type')=="other"?" selected":"").">Other/Unmappable</option>
										</select>
									</td>
								</tr>
								<tr>
									<td colspan=\"2\" class=\"campus_".$c."_type _tabbed\">
										<div class=\"tab\" data-tab-id=\"primary,secondary\">
											<h4>Location Settings</h4>
											<table class=\"group\">
												<tr>
													<td><label for=\"campus_".$c."_hide\">Hide on Campus Maps</label></td>
													<td><input type=\"checkbox\" name=\"campus_".$c."_hide\" id=\"campus_".$c."_hide\" value=\"1\"".(get_option('campus_'.$c.'_hide')=="1"?" checked=\"checked\"":"")."/></td>
												</tr>
												<tr>
													<td><label for=\"campus_".$c."_coords\">Map Pin Coordinates</label></td>
													<td><input type=\"text\" id=\"campus_".$c."_coords\" name=\"campus_".$c."_coords\" value=\"".get_option('campus_'.$c.'_coords')."\" /></td>
												</tr>
												<tr>
													<td><label for=\"campus_".$c."_addr_1\">Street Address</label></td>
													<td><input type=\"text\" id=\"campus_".$c."_addr_1\" name=\"campus_".$c."_addr_1\" value=\"".get_option('campus_'.$c.'_addr_1')."\" /></td>
												</tr>
												<tr>
													<td><label for=\"campus_".$c."_addr_2\">City, State Zip</label></td>
													<td><input type=\"text\" id=\"campus_".$c."_addr_2\" name=\"campus_".$c."_addr_2\" value=\"".get_option('campus_'.$c.'_addr_2')."\" /></td>
												</tr>
											</table>
											
										</div>
									</td>
								</tr>
							</table>
						</div>
					</td></tr>
				</table>
				";
			}
			?></div>
			
		</td></tr></table>
		
			</td>
		</tr>
	</table>
</div>

<?php submit_button('Save Changes','primary','submit-form'); ?>
	
</form>
</div>