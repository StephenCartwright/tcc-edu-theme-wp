<?php
date_default_timezone_set('America/New_York');

$tccvals = "/*
	TCC.edu
	generated ".date("Y-m-d H:i:s")."
*/

window.TCCVals = {};

/* career pathways */
TCCVals.careers = {";
foreach(json_decode(get_option('career_options_list')) as $option){
	$tccvals .= "
	\"".$option->id."\":\"".$option->name."\",";
}
$tccvals .= "
};

/* degree types */
TCCVals.degrees = {";
foreach(json_decode(get_option('degree_options_list')) as $option){
	$tccvals .= "
	\"".$option->id."\":\"".$option->name."\",";
}
$tccvals .= "
};

/* program types */
TCCVals.program_types = {";
foreach(json_decode(get_option('program_type_options_list')) as $option){
	$tccvals .= "
	\"".$option->id."\":\"".$option->name."\",";
}
$tccvals .= "
};

/* campuses */
TCCVals.campuses = {";
for($c=1;$c<=15;$c++){
	if(get_option('campus_'.$c.'_show')){
		$campus_id = get_option('campus_'.$c.'_id')?get_option('campus_'.$c.'_id'):sanitize_title(get_option('campus_'.$c.'_title'));
		$campus_type = get_option('campus_'.$c.'_type');
		$tccvals .= "
		\"".$campus_id."\":{
			id:\"".$campus_id."\",
			title:\"".get_option('campus_'.$c.'_title')."\",
			type:\"".get_option('campus_'.$c.'_type')."\",
			link:\"".get_option('campus_'.$c.'_link')."\",
			phone:\"".get_option('campus_'.$c.'_phone')."\",
			location:\"".get_option('campus_'.$c.'_location')."\",".
			(get_option('campus_'.$c.'_map_pdf')?"
			map_pdf:\"".get_option('campus_'.$c.'_map_pdf')."\",":"").
			($campus_type!=="other"?"
			hide:\"".get_option('campus_'.$c.'_hide')."\",
			coords:\"".get_option('campus_'.$c.'_coords')."\",
			addr_1:\"".get_option('campus_'.$c.'_addr_1')."\",
			addr_2:\"".get_option('campus_'.$c.'_addr_2')."\",":"")."
		},";
	}
}
$tccvals .= "
};

/* programs */
TCCVals.programs = {";
$pf_terms = array();
foreach(get_posts(array('showposts'=>-1,'post_type'=>'program')) as $program){
	$program_meta = get_post_meta($program->ID);
	$program_id = $program_meta['program_id'][0]?$program_meta['program_id'][0]:sanitize_title($program->post_title);
	$degree_ids = array();
	$type_ids = array();
	$location_ids = array();
	$interest_ids = array();
	foreach(unserialize($program_meta['program_degree'][0]) as $degree) $degree_ids[] = $degree;
	foreach(unserialize($program_meta['program_type'][0]) as $type) $type_ids[] = $type;
	foreach(unserialize($program_meta['program_location'][0]) as $location) $location_ids[] = $location;
	foreach(unserialize($program_meta['program_interest'][0]) as $interest) $interest_ids[] = $interest;
	$tccvals .= "
	\"".$program_id."\":{
		id:\"".$program_id."\",
		img:\"".get_the_post_thumbnail_url($program->ID,'medium')."\",
		title:\"".$program->post_title."\",
		link:\"".$program_meta['program_link'][0]."\",
		career:\"".$program_meta['program_career'][0]."\",
		degree:".json_encode($degree_ids).",
		type:".json_encode($type_ids).",
		location:".json_encode($location_ids).",
		interest:".json_encode($interest_ids)."
	},";
	$pf_terms[] = "{key:\"".addslashes($program->post_title)."\",item:\"".$program_id."\"}";
	foreach(explode("\r\n",$program_meta['program_terms'][0]) as $term){
		$pf_terms[] = "{key:\"".addslashes($term)."\",item:\"".$program_id."\"}";
	}
}
$tccvals .= "
};

/* program finder predictive search associative terms */
TCCVals.pf_terms = [
	".implode(",\r\n\t",$pf_terms). "
];

/* transfer schools */
TCCVals.transfer_schools = {";
$pf_terms = array();
foreach(get_posts(array('showposts'=>-1,'post_type'=>'transfer-school','orderby'=>'post_title','order'=>'asc')) as $school){
	$school_meta = get_post_meta($school->ID);
	$school_id = sanitize_title($school->post_title);
	$agreements_ids = array();
	foreach(unserialize($school_meta['school_agreements'][0]) as $agreement){
		$program = get_posts(array('name'=>$agreement,'post_type'=>'program','post_status'=>'publish','numberposts'=>1))[0];
		if($program) $agreements_ids[] = get_post_meta($program->ID,'program_id',true);
	}
	$tccvals .= "
	\"".$school_id."\":{
		id:\"".$school_id."\",
		name:\"".$school->post_title."\",
		type:\"".$school_meta['school_type'][0]."\",
		location:\"".$school_meta['school_location'][0]."\",
		gpa:\"".$school_meta['school_gpa'][0]."\",
		url:\"".$school_meta['school_url'][0]."\",
		agreements:".json_encode($agreements_ids)."
	},";
}
$tccvals .= "
};

/* quick tcc form stages */
TCCVals.qt_stages = ".(get_option('quicktcc_form_stages')?stripslashes(get_option('quicktcc_form_stages')):"{}").";

/* program sidebar form stages */
TCCVals.sidebar_stages = ".(get_option('sidebar_form_stages')?stripslashes(get_option('sidebar_form_stages')):"{}").";

";

file_put_contents(get_template_directory()."/js/tcc-vals.js",$tccvals);

update_option('tccvals_generated',time());

?>