<div id="page_settings" class="meta-settings">

<table id="page_main" class="group">
	<tr><td>
		
		<table class="control solo" name="page_options">
			<tr>
				<td><label for="events_per_page">Events Per Page</label></td>
				<td><input type="number" min="0" id="events_per_page" name="events_per_page" placeholder="10" value="<?php echo esc_attr($page_meta['events_per_page'][0]); ?>" /></td>
			</tr>
			<tr>
				<td><label for="events_page_cat">Events Category</label></td>
				<td>
					<select id="events_page_cat" name="events_page_cat">
					<?php
					echo "<option value=\"\"".(!$page_meta['events_page_cat'][0]?" selected":"").">All Events</option>";
					$evt_cats = get_terms('event_category','orderby=count');
					$html = "";
					foreach($evt_cats as $evt_cat){
						$html .= "
						<option value=\"".$evt_cat->slug."\"".(isset($page_meta['events_page_cat'][0])&&$page_meta['events_page_cat'][0]==$evt_cat->slug?" selected":"").">".$evt_cat->name."</option>";
					}
					echo $html;
					?>
					</select>
				</td>
			</tr>
			<tr>
				<td><label for="page_keywords">Page Keywords</label><sub>Additional search terms that don't already exist in the page content, repeated terms will increase relevance</sub></td>
				<td>
				<textarea id="page_keywords" name="page_keywords" maxlength="1250"><?php echo stripslashes(esc_attr($page_meta['page_keywords'][0])); ?></textarea>
				</td>
			</tr>
		</table>
		
		<?php include('inc/meta-content-feed.php'); ?>
		
	</td></tr>
</table>

</div>