
		<table class="section"><tr><td>
			<h2>Map Module</h2>
			
			<div id="map_mod_options" class="control-wrap">
				<table class="control" name="event_mod_options">
					<tr><td><label for="map_mod_enable" class="_for-toggler"><input type="checkbox" class="_toggler" data-toggles="#map_mod_enabled_options" name="map_mod_enable" id="map_mod_enable" value="1" <?php echo $page_meta['map_mod_enable'][0]=="1"?"checked=\"checked\" ":""; ?>/> Enable Map Module</label></td></tr>
					<tr>
						<td>
							<div id="map_mod_enabled_options">
				
				<table class="control" name="map_mod_options">
					<tr>
						<td><label for="map_mod_title">Title</label></td>
						<td><input type="text" class="large-text" id="map_mod_title" name="map_mod_title" placeholder="" value="<?php echo esc_attr($page_meta['map_mod_title'][0]); ?>" /></td>
					</tr>
					<tr>
						<td><label for="map_mod_img">Map Image</label></td>
						<td>
							<div class="editor">
								<div class="editor-control">
									<input type="button" class="button _media-select _ratios" data-id="#map_mod_img" value="Choose" />
									<input type="button" class="button _media-clear" data-id="#map_mod_img" value="Clear" />
									<input type="text" id="map_mod_img" class="_previews" data-pre="#map_mod_img_pre" name="map_mod_img" value="<?php echo esc_attr($page_meta['map_mod_img'][0]); ?>" />
								</div>
								<div class="summary image-preview empty" id="campus_video_poster_pre"></div>
							</div>
						</td>
					</tr>
					<tr>
						<td><label for="map_mod_img_ratio">Map Image Ratio</label><sub>Width/height, e.g. <em>1.42</em></sub></td>
						<td>
							<table class="control">
								<tr>
									<td><label for="map_mod_img_width">Width</label></td><td><input type="text" id="map_mod_img_width" name="map_mod_img_width" placeholder="" value="<?php echo esc_attr($page_meta['map_mod_img_width'][0]); ?>" /></td>
									<td><label for="map_mod_img_height">Height</label></td><td><input type="text" id="map_mod_img_height" name="map_mod_img_height" placeholder="" value="<?php echo esc_attr($page_meta['map_mod_img_height'][0]); ?>" /></td>
								</tr>
								<tr>
									<td><label for="map_mod_img_ratio">Ratio</label></td><td colspan="4"><input type="text" id="map_mod_img_ratio" name="map_mod_img_ratio" placeholder="Width/height" value="<?php echo esc_attr($page_meta['map_mod_img_ratio'][0]); ?>" /></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td><label>Map Coordinates</label><sub>Latitude and Longitude, e.g. <em>37.00,-76.50</em></sub></td>
						<td>
							<table class="control">
								<tr>
									<td><label for="map_mod_coords_1">Top Left</label></td><td><input type="text" id="map_mod_coords_1" name="map_mod_coords_1" placeholder="" value="<?php echo esc_attr($page_meta['map_mod_coords_1'][0]); ?>" /></td>
									<td><label for="map_mod_coords_2">Bottom Right</label></td><td><input type="text" id="map_mod_coords_2" name="map_mod_coords_2" placeholder="" value="<?php echo esc_attr($page_meta['map_mod_coords_2'][0]); ?>" /></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td><label for="map_mod_locations">Locations</label><sub>Maximum of 4. Select none to display all location map markers and only list primary campuses, like the homepage.</sub></td>
						<td>
							<div class="checkbox-group"><?php
								for($c=1;$c<=15;$c++){
									if(get_option('campus_'.$c.'_show')){
										$campus_id = get_option('campus_'.$c.'_id')?get_option('campus_'.$c.'_id'):sanitize_title(get_option('campus_'.$c.'_title'));
										if(get_option('campus_'.$c.'_type')!=="other") echo "<div class=\"option\"><label><input type=\"checkbox\" name=\"map_mod_locations[]\" value=\"".$campus_id."\"".(in_array($campus_id,unserialize($page_meta['map_mod_locations'][0]))?" checked=\"checked\"":"")." />".get_option('campus_'.$c.'_title')."</label></div>";
									}
								}
							?></div>
						</td>
					</tr>
				</table>
				
							</div>
						</td>
					</tr>
				</table>
			</div>
			
		</td></tr></table>
		