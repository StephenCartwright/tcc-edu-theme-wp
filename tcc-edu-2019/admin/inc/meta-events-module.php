
		<table class="section"><tr><td>
			<h2>Events Module</h2>
			
			<div id="events_mod_options" class="control-wrap">
				<table class="control" name="event_mod_options">
					<tr><td><label for="events_mod_enable" class="_for-toggler"><input type="checkbox" class="_toggler" data-toggles="#events_mod_enabled_options" name="events_mod_enable" id="events_mod_enable" value="1" <?php echo $page_meta['events_mod_enable'][0]=="1"?"checked=\"checked\" ":""; ?>/> Enable Events Module</label></td></tr>
					<tr>
						<td>
							<div id="events_mod_enabled_options">
				
				<table class="control" name="events_mod_options">
					<tr>
						<td><label for="events_mod_title">Title</label></td>
						<td><input type="text" class="large-text" id="events_mod_title" name="events_mod_title" placeholder="Upcoming Events" value="<?php echo esc_attr($page_meta['events_mod_title'][0]); ?>" /></td>
					</tr>
					<tr>
						<td><label for="events_mod_count">Item Count</label><sub>Multiples of 3 work best</sub></td>
						<td><input type="number" min="0" id="events_mod_count" name="events_mod_count" placeholder="3" value="<?php echo esc_attr($page_meta['events_mod_count'][0]); ?>" /></td>
					</tr>
					<tr>
						<td><label for="events_mod_cat">Events Category</label></td>
						<td>
							<select id="events_mod_cat" name="events_mod_cat">
							<?php
							echo "<option value=\"\"".(!$page_meta['events_mod_cat'][0]?" selected":"").">All Events</option>";
							$evt_cats = get_terms('event_category','orderby=count');
							$html = "";
							foreach($evt_cats as $evt_cat){
								$html .= "
								<option value=\"".$evt_cat->slug."\"".(isset($page_meta['events_mod_cat'][0])&&$page_meta['events_mod_cat'][0]==$evt_cat->slug?" selected":"").">".$evt_cat->name."</option>";
							}
							echo $html;
							?>
							</select>
						</td>
					</tr>
					<tr>
						<td><label for="events_mod_link">View All Link</label></td>
						<td><input type="text" id="events_mod_link" name="events_mod_link" placeholder="/events/" value="<?php echo esc_attr($page_meta['events_mod_link'][0]); ?>" /></td>
					</tr>
				</table>
				
							</div>
						</td>
					</tr>
				</table>
			</div>
			
		</td></tr></table>
		