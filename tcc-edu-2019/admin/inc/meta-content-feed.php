
		<table class="section"><tr><td>
			<h2>Content Feed Module</h2>
			
			<div id="content_feed_options" class="control-wrap">
				<table class="control" name="content_feed_options">
					<tr><td><label for="content_feed_mod_enable" class="_for-toggler"><input type="checkbox" class="_toggler" data-toggles="#content_feed_mod_enabled_options" name="content_feed_mod_enable" id="content_feed_mod_enable" value="1" <?php echo $page_meta['content_feed_mod_enable'][0]=="1"?"checked=\"checked\" ":""; ?>/> Enable Content Feed</label></td></tr>
					<tr>
						<td>
							<div id="content_feed_mod_enabled_options">
			
				<table class="control" name="content_feed_mod_options">
					<tr>
						<td><label for="content_feed_mod_style">Module Style</label></td>
						<td>
							<select id="content_feed_mod_style" name="content_feed_mod_style">
								<option value="style-row" <?php if($page_meta['content_feed_mod_style'][0]=="style-row") echo " selected"; ?>>Row style</option>
								<option value="style-large" <?php if($page_meta['content_feed_mod_style'][0]=="style-large") echo " selected"; ?>>Large style</option>
							</select>
						</td>
					</tr>
					<tr>
						<td><label for="content_feed_mod_tags">Tags Override</label><sub>Comma-separated list of tag slugs, otherwise page tags are used</sub></td>
						<td><textarea id="content_feed_mod_tags" name="content_feed_mod_tags" maxlength="250"><?php echo esc_attr($page_meta['content_feed_mod_tags'][0]); ?></textarea></td>
					</tr>
					<tr>
						<td><label for="content_feed_mod_match">Search Matching</label><sub><em>"Match All"</em> selects weighted results matching ALL tags, best with small groups of tags. <em>"Match Any"</em> selects weighted results matching ANY of the tags, best with large groups of tags.</sub></td>
						<td>
							<select id="content_feed_mod_match" name="content_feed_mod_match">
								<option value="all" <?php if($page_meta['content_feed_mod_match'][0]=="all") echo " selected"; ?>>Match All</option>
								<option value="any" <?php if($page_meta['content_feed_mod_match'][0]=="any") echo " selected"; ?>>Match Any</option>
							</select>
						</td>
					</tr>
				</table>
				
							</div>
						</td>
					</tr>
				</table>
			</div>
			
		</td></tr></table>
		