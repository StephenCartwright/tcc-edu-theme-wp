
		<table class="section"><tr><td>
			<h2>Slides</h2>
			
			<div id="slides_options" class="control-wrap"><?php
			for($s=1;$s<=5;$s++){
				echo "
				<table class=\"control-group\" name=\"slide_".$s."_options\">
					<tr><td><label for=\"slide_".$s."_show\" class=\"_for-toggler\"><input type=\"checkbox\" class=\"_toggler\" data-toggles=\"#slide_".$s."_options\" name=\"slide_".$s."_show\" id=\"slide_".$s."_show\" value=\"1\"".($page_meta['slide_'.$s.'_show'][0]=="1"?" checked=\"checked\"":"")."/> Enable Slide</label></td></tr>
					<tr><td>
						<div id=\"slide_".$s."_options\">
							<table class=\"group\">
								<tr>
									<td><label for=\"slide_".$s."_title\">Title</label></td>
									<td><input type=\"text\" class=\"large-text\" id=\"slide_".$s."_title\" name=\"slide_".$s."_title\" value=\"".esc_attr($page_meta['slide_'.$s.'_title'][0])."\" /></td>
								</tr>
								<tr>
									<td><label for=\"slide_".$s."_copy\">Copy</label></td>
									<td><textarea id=\"slide_".$s."_copy\" name=\"slide_".$s."_copy\" maxlength=\"250\">".esc_attr($page_meta['slide_'.$s.'_copy'][0])."</textarea></td>
								</tr>
								<tr>
									<td><label for=\"slide_".$s."_cta\">Button CTA</label></td>
									<td><input type=\"text\" id=\"slide_".$s."_cta\" name=\"slide_".$s."_cta\" placeholder=\"Learn More\" value=\"".esc_attr($page_meta['slide_'.$s.'_cta'][0])."\" /></td>
								</tr>
								<tr>
									<td><label for=\"slide_".$s."_link\">Button Link</label></td>
									<td><input type=\"text\" id=\"slide_".$s."_link\" name=\"slide_".$s."_link\" value=\"".esc_attr($page_meta['slide_'.$s.'_link'][0])."\" /></td>
								</tr>
								<tr>
									<td><label for=\"slide_".$s."_align\">Alignment</label></td>
									<td>
										<select id=\"slide_".$s."_align\" name=\"slide_".$s."_align\">
											<option value=\"left\"".(!$page_meta['slide_'.$s.'_align'][0]||$page_meta['slide_'.$s.'_align'][0]=="left"?" selected":"").">Left</option>
											<option value=\"right\"".($page_meta['slide_'.$s.'_align'][0]=="right"?" selected":"").">Right</option>
										</select>
									</td>
								</tr>
								<tr>
									<td><label>Image bank</label><sub>A random image from this group will be displayed on each page load</sub></td>
									<td>
										<table class=\"control group\">
											<tr>
												<td>
													<div class=\"editor\">
														<div class=\"editor-control\">
															<input type=\"button\" class=\"button _media-select\" data-id=\"#slide_".$s."_img_1\" value=\"Choose\" />
															<input type=\"button\" class=\"button _media-clear\" data-id=\"#slide_".$s."_img_1\" value=\"Clear\" />
															<input type=\"text\" id=\"slide_".$s."_img_1\" class=\"_previews\" data-pre=\"#slide_".$s."_img_1_pre\" name=\"slide_".$s."_img_1\" value=\"".esc_attr($page_meta['slide_'.$s.'_img_1'][0])."\" />
														</div>
														<div class=\"summary image-preview empty\" id=\"slide_".$s."_img_1_pre\"></div>
													</div>
												</td>
											</tr>
											<tr>
												<td>
													<div class=\"editor\">
														<div class=\"editor-control\">
															<input type=\"button\" class=\"button _media-select\" data-id=\"#slide_".$s."_img_2\" value=\"Choose\" />
															<input type=\"button\" class=\"button _media-clear\" data-id=\"#slide_".$s."_img_2\" value=\"Clear\" />
															<input type=\"text\" id=\"slide_".$s."_img_2\" class=\"_previews\" data-pre=\"#slide_".$s."_img_2_pre\" name=\"slide_".$s."_img_2\" value=\"".esc_attr($page_meta['slide_'.$s.'_img_2'][0])."\" />
														</div>
														<div class=\"summary image-preview empty\" id=\"slide_".$s."_img_2_pre\"></div>
													</div>
												</td>
											</tr>
											<tr>
												<td>
													<div class=\"editor\">
														<div class=\"editor-control\">
															<input type=\"button\" class=\"button _media-select\" data-id=\"#slide_".$s."_img_3\" value=\"Choose\" />
															<input type=\"button\" class=\"button _media-clear\" data-id=\"#slide_".$s."_img_3\" value=\"Clear\" />
															<input type=\"text\" id=\"slide_".$s."_img_3\" class=\"_previews\" data-pre=\"#slide_".$s."_img_3_pre\" name=\"slide_".$s."_img_3\" value=\"".esc_attr($page_meta['slide_'.$s.'_img_3'][0])."\" />
														</div>
														<div class=\"summary image-preview empty\" id=\"slide_".$s."_img_3_pre\"></div>
													</div>
												</td>
											</tr>
											<tr>
												<td>
													<div class=\"editor\">
														<div class=\"editor-control\">
															<input type=\"button\" class=\"button _media-select\" data-id=\"#slide_".$s."_img_4\" value=\"Choose\" />
															<input type=\"button\" class=\"button _media-clear\" data-id=\"#slide_".$s."_img_4\" value=\"Clear\" />
															<input type=\"text\" id=\"slide_".$s."_img_4\" class=\"_previews\" data-pre=\"#slide_".$s."_img_4_pre\" name=\"slide_".$s."_img_4\" value=\"".esc_attr($page_meta['slide_'.$s.'_img_4'][0])."\" />
														</div>
														<div class=\"summary image-preview empty\" id=\"slide_".$s."_img_4_pre\"></div>
													</div>
												</td>
											</tr>
											<tr>
												<td>
													<div class=\"editor\">
														<div class=\"editor-control\">
															<input type=\"button\" class=\"button _media-select\" data-id=\"#slide_".$s."_img_5\" value=\"Choose\" />
															<input type=\"button\" class=\"button _media-clear\" data-id=\"#slide_".$s."_img_5\" value=\"Clear\" />
															<input type=\"text\" id=\"slide_".$s."_img_5\" class=\"_previews\" data-pre=\"#slide_".$s."_img_5_pre\" name=\"slide_".$s."_img_5\" value=\"".esc_attr($page_meta['slide_'.$s.'_img_5'][0])."\" />
														</div>
														<div class=\"summary image-preview empty\" id=\"slide_".$s."_img_5_pre\"></div>
													</div>
												</td>
											</tr>
											<tr>
												<td>
													<div class=\"editor\">
														<div class=\"editor-control\">
															<input type=\"button\" class=\"button _media-select\" data-id=\"#slide_".$s."_img_6\" value=\"Choose\" />
															<input type=\"button\" class=\"button _media-clear\" data-id=\"#slide_".$s."_img_6\" value=\"Clear\" />
															<input type=\"text\" id=\"slide_".$s."_img_6\" class=\"_previews\" data-pre=\"#slide_".$s."_img_6_pre\" name=\"slide_".$s."_img_6\" value=\"".esc_attr($page_meta['slide_'.$s.'_img_6'][0])."\" />
														</div>
														<div class=\"summary image-preview empty\" id=\"slide_".$s."_img_6_pre\"></div>
													</div>
												</td>
											</tr>
											<tr>
												<td>
													<div class=\"editor\">
														<div class=\"editor-control\">
															<input type=\"button\" class=\"button _media-select\" data-id=\"#slide_".$s."_img_7\" value=\"Choose\" />
															<input type=\"button\" class=\"button _media-clear\" data-id=\"#slide_".$s."_img_7\" value=\"Clear\" />
															<input type=\"text\" id=\"slide_".$s."_img_7\" class=\"_previews\" data-pre=\"#slide_".$s."_img_7_pre\" name=\"slide_".$s."_img_7\" value=\"".esc_attr($page_meta['slide_'.$s.'_img_7'][0])."\" />
														</div>
														<div class=\"summary image-preview empty\" id=\"slide_".$s."_img_7_pre\"></div>
													</div>
												</td>
											</tr>
											<tr>
												<td>
													<div class=\"editor\">
														<div class=\"editor-control\">
															<input type=\"button\" class=\"button _media-select\" data-id=\"#slide_".$s."_img_8\" value=\"Choose\" />
															<input type=\"button\" class=\"button _media-clear\" data-id=\"#slide_".$s."_img_8\" value=\"Clear\" />
															<input type=\"text\" id=\"slide_".$s."_img_8\" class=\"_previews\" data-pre=\"#slide_".$s."_img_8_pre\" name=\"slide_".$s."_img_8\" value=\"".esc_attr($page_meta['slide_'.$s.'_img_8'][0])."\" />
														</div>
														<div class=\"summary image-preview empty\" id=\"slide_".$s."_img_8_pre\"></div>
													</div>
												</td>
											</tr>
											<tr>
												<td>
													<div class=\"editor\">
														<div class=\"editor-control\">
															<input type=\"button\" class=\"button _media-select\" data-id=\"#slide_".$s."_img_9\" value=\"Choose\" />
															<input type=\"button\" class=\"button _media-clear\" data-id=\"#slide_".$s."_img_9\" value=\"Clear\" />
															<input type=\"text\" id=\"slide_".$s."_img_9\" class=\"_previews\" data-pre=\"#slide_".$s."_img_9_pre\" name=\"slide_".$s."_img_9\" value=\"".esc_attr($page_meta['slide_'.$s.'_img_9'][0])."\" />
														</div>
														<div class=\"summary image-preview empty\" id=\"slide_".$s."_img_9_pre\"></div>
													</div>
												</td>
											</tr>
											<tr>
												<td>
													<div class=\"editor\">
														<div class=\"editor-control\">
															<input type=\"button\" class=\"button _media-select\" data-id=\"#slide_".$s."_img_10\" value=\"Choose\" />
															<input type=\"button\" class=\"button _media-clear\" data-id=\"#slide_".$s."_img_10\" value=\"Clear\" />
															<input type=\"text\" id=\"slide_".$s."_img_10\" class=\"_previews\" data-pre=\"#slide_".$s."_img_10_pre\" name=\"slide_".$s."_img_10\" value=\"".esc_attr($page_meta['slide_'.$s.'_img_10'][0])."\" />
														</div>
														<div class=\"summary image-preview empty\" id=\"slide_".$s."_img_10_pre\"></div>
													</div>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</div>
					</td></tr>
				</table>
				";
			}
			?></div>
			
		</td></tr></table>
		