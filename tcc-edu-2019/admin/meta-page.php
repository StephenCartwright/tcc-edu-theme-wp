<div id="page_settings" class="meta-settings">

<table id="page_main" class="group">
	<tr><td>
		
		<table class="control solo" name="page_options">
			<tr>
				<td><label for="page_redirect">Page Redirect</label><sub>Forward visitors to a url instead of loading the page. Use a relative path or a fully qualified url, <em>"/about-tcc/"</em> or <em>"http://www.tcc.edu/"</em></sub></td>
				<td><input type="text" id="page_redirect" name="page_redirect" value="<?php echo esc_attr($page_meta['page_redirect'][0]); ?>" /></td>
			</tr>
			<tr>
				<td><label for="page_shy">Hide in Section Nav</label></td>
				<td><input type="checkbox" name="page_shy" id="page_shy" value="1"<?php echo $page_meta['page_shy'][0]=="1"?" checked=\"checked\"":""; ?>/></td>
			</tr>
			<tr>
				<td><label for="page_keywords">Page Keywords</label><sub>Additional search terms that don't already exist in the page content, repeated terms will increase relevance</sub></td>
				<td>
				<textarea id="page_keywords" name="page_keywords" maxlength="1250"><?php echo stripslashes(esc_attr($page_meta['page_keywords'][0])); ?></textarea>
				</td>
			</tr>
		</table>
		
		
		<table class="section"><tr><td>
			<h2>Page Sidebar</h2>
			
			<div id="page_sidebar_options" class="control-wrap">
				<table class="control" name="page_sidebar_options">
					<tr>
						<td><label for="page_sidebar">Sidebar</label></td>
						<td>
							<select id="page_sidebar" class="_tabber" data-tabs="#page_sidebar_types" name="page_sidebar"><?php
								echo "<option value=\"\"".(!$page_meta['page_sidebar'][0]?" selected":"").">No sidebar</option>";
								for($p=1;$p<=5;$p++){
									echo "<option value=\"promo_".$p."\"".($page_meta['page_sidebar'][0]=="promo_".$p?" selected":"").">Promo ".$p."</option>";
								}
								echo "<option value=\"custom\"".($page_meta['page_sidebar'][0]=="custom"?" selected":"").">Custom</option>";
							?></select>
						</td>
					</tr>
					<tr>
						<td><label for="page_sidebar_not_sticky">Disable Sticky Effect</label></td>
						<td><input type="checkbox" name="page_sidebar_not_sticky" id="page_sidebar_not_sticky" value="1"<?php echo $page_meta['page_sidebar_not_sticky'][0]=="1"?" checked=\"checked\"":""; ?>/></td>
					</tr>
					<tr id="page_sidebar_types" class="_tabbed">
						<td colspan="2">
						<?php
						for($p=1;$p<=5;$p++){
							$promo = "promo_".$p;
							$promo_title = stripslashes(get_option($promo.'_title'));
							$promo_copy = stripslashes(get_option($promo.'_copy'));
							$promo_link = get_option($promo.'_link');
							$promo_cta = get_option($promo.'_cta');
							$promo_btn_class = get_option($promo.'_btn_class');
							echo "
							<div class=\"tab editor-styles-wrapper\" data-tab-id=\"promo_".$p."\">
								".(!get_option($promo.'_show')?"<span class=\"error\">This promo is currently disabled, and won't display on the page!</span>":"")."
								<div class=\"promo-preview\">
									<div class=\"panel\">
										<div class=\"promo-content\">";
									if($promo_title) echo "<label class=\"nonh2\">".$promo_title."</label>";
									if($promo_copy) echo wpautop($promo_copy);
									if($promo_link){
										echo "<a href=\"".$promo_link."\" class=\"btn ".
										($promo_btn_class?$promo_btn_class:"btn btn-4 btn-text btn-arrow").
										"\">".($promo_cta?$promo_cta:"Learn More")."</a>";
									}
									echo "
										</div>
									</div>
								</div>
							</div>";
						}
						?>
							<div class="tab" data-tab-id="custom">
					
								<div id="custom_sidebar_options">
									<table class="group">
										<tr>
											<td><label for="custom_sidebar_title">Title</label><sub>Supports html</sub></td>
											<td><textarea id="custom_sidebar_title" name="custom_sidebar_title" maxlength="1250"><?php echo stripslashes(esc_attr($page_meta['custom_sidebar_title'][0])); ?></textarea></td>
										</tr>
										<tr>
											<td><label for="custom_sidebar_copy">Copy</label><sub>Supports html</sub></td>
											<td><textarea class="medium" id="custom_sidebar_copy" name="custom_sidebar_copy" maxlength="2500"><?php echo stripslashes(esc_attr($page_meta['custom_sidebar_copy'][0])); ?></textarea></td>
										</tr>
										<tr>
											<td><label for="custom_sidebar_cta">Button CTA</label></td>
											<td><input type="text" id="custom_sidebar_cta" name="custom_sidebar_cta" placeholder="Learn More" value="<?php echo esc_attr($page_meta['custom_sidebar_cta'][0]); ?>" /></td>
										</tr>
										<tr>
											<td><label for="custom_sidebar_link">Button Link</label></td>
											<td><input type="text" id="custom_sidebar_link" name="custom_sidebar_link" value="<?php echo esc_attr($page_meta['custom_sidebar_link'][0]); ?>" /></td>
										</tr>
										<tr>
											<td><label for="custom_sidebar_btn_class">Button Class</label></td>
											<td><input type="text" id="custom_sidebar_btn_class" name="custom_sidebar_btn_class" placeholder="btn-4 btn-text btn-arrow" value="<?php echo esc_attr($page_meta['custom_sidebar_btn_class'][0]); ?>" /></td>
										</tr>
									</table>
								</div>
					
							</div>
						</td>
					</tr>
				</table>
			</div>
			
		</td></tr></table>

		<?php include('inc/meta-events-module.php'); ?>
		
		<?php include('inc/meta-content-feed.php'); ?>
		
	</td></tr>
</table>

</div>