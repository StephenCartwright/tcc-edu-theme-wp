<div id="page_settings" class="meta-settings">

<table id="page_main" class="group">
	<tr><td>
		
		<table class="section"><tr><td>
			<h2>Gateway</h2>
			
			<div id="gateway_options" class="control-wrap">
				<table class="control" name="gateway_options">
					<tr><td><label for="gateway_enable" class="_for-toggler"><input type="checkbox" class="_toggler" data-toggles="#gateway_enabled_options" name="gateway_enable" id="gateway_enable" value="1" <?php echo $page_meta['gateway_enable'][0]=="1"?"checked=\"checked\" ":""; ?>/> Enable Gateway</label></td></tr>
					<tr>
						<td>
							<div id="gateway_enabled_options">
				<table class="group">
					<tr>
						<td><label for="gateway_bg_img">Background Image</label></td>
						<td>
							<div class="editor">
								<div class="editor-control">
									<input type="button" class="button _media-select" data-id="#gateway_bg_img" value="Choose" />
									<input type="button" class="button _media-clear" data-id="#gateway_bg_img" value="Clear" />
									<input type="text" id="gateway_bg_img" class="_previews" data-pre="#gateway_bg_img_pre" name="gateway_bg_img" value="<?php echo esc_attr($page_meta['gateway_bg_img'][0]); ?>" />
								</div>
								<div class="summary image-preview empty" id="gateway_bg_img_pre"></div>
							</div>
						</td>
					</tr>
					<tr>
						<td><label for="gateway_cookie_days">Cookie Length</label><sub>Prevent the gateway from showing again for # days</sub></td>
						<td><input type="number" min="0" id="gateway_cookie_days" name="gateway_cookie_days" placeholder="90" value="<?php echo esc_attr($page_meta['gateway_cookie_days'][0]); ?>" /></td>
					</tr>
					<tr>
						<td><label for="gateway_user_options">User Options</label><sub>Links should be relative, e.g. <em>"/come-to-tcc/get-started/"</em>. Using the link <em>"/"</em> will trigger the gateway-to-homepage transition. Enabling more than 5 options may create display issues.</sub></td>
						<td><?php
						for($o=1;$o<=7;$o++){
							echo "
							<table class=\"control wide\" name=\"gateway_opt_".$o."_options\">
								<tr><td><label for=\"gateway_opt_".$o."_show\" class=\"_for-toggler\"><input type=\"checkbox\" class=\"_toggler\" data-toggles=\"#gateway_opt_".$o."_options\" name=\"gateway_opt_".$o."_show\" id=\"gateway_opt_".$o."_show\" value=\"1\"".($page_meta['gateway_opt_'.$o.'_show'][0]=="1"?" checked=\"checked\"":"")."/> Enable Option</label></td></tr>
								<tr><td>
									<div id=\"gateway_opt_".$o."_options\">
										<table class=\"group\">
											<tr>
												<td><label for=\"gateway_opt_".$o."_title\">Title</label></td>
												<td><input type=\"text\" id=\"gateway_opt_".$o."_title\" name=\"gateway_opt_".$o."_title\" value=\"".esc_attr($page_meta['gateway_opt_'.$o.'_title'][0])."\" /></td>
											</tr>
											<tr>
												<td><label for=\"gateway_opt_".$o."_link\">Link</label></td>
												<td><input type=\"text\" id=\"gateway_opt_".$o."_link\" name=\"gateway_opt_".$o."_link\" value=\"".esc_attr($page_meta['gateway_opt_'.$o.'_link'][0])."\" /></td>
											</tr>
										</table>
									</div>
								</td></tr>
							</table>
							";
						}
						?></td>
					</tr>
				</table>
							</div>
						</td>
					</tr>
				</table>
			</div>
			
		</td></tr></table>
		
		<?php include('inc/meta-slides.php'); ?>
		
		<table class="section"><tr><td>
			<h2>Four-Up Module</h2>
			<p>Class name primarily used for styling. The class name <em>"_quick-tcc"</em> will launch Quick TCC module on click.</p>
			
			<div id="four_up_options" class="control-wrap"><?php
			for($f=1;$f<=4;$f++){
				echo "
				<table class=\"control inline\" name=\"four_up_".$f."_options\">
					<tr><td>
						<h3>Item ".$f."</h3>
						<div id=\"four_up_".$f."_options\">
							<table class=\"group\">
								<tr>
									<td><label for=\"four_up_".$f."_class\">Class Name</label></td>
									<td><input type=\"text\" id=\"four_up_".$f."_class\" name=\"four_up_".$f."_class\" value=\"".esc_attr($page_meta['four_up_'.$f.'_class'][0])."\" /></td>
								</tr>
								<tr>
									<td><label for=\"four_up_".$f."_svg\">SVG Icon</label></td>
									<td><textarea id=\"four_up_".$f."_svg\" name=\"four_up_".$f."_svg\" class=\"_svg-previews\" data-pre=\"#four_up_".$f."_svg_pre\">".esc_attr($page_meta['four_up_'.$f.'_svg'][0])."</textarea></td>
								</tr>
								<tr>
									<td colspan=\"2\"><div class=\"svg-preview empty\" id=\"four_up_".$f."_svg_pre\"></div></td>
								</tr>
								<tr>
									<td><label for=\"four_up_".$f."_title\">Title</label></td>
									<td><input type=\"text\" class=\"large-text\" id=\"four_up_".$f."_title\" name=\"four_up_".$f."_title\" value=\"".esc_attr($page_meta['four_up_'.$f.'_title'][0])."\" /></td>
								</tr>
								<tr>
									<td><label for=\"four_up_".$f."_copy\">Copy</label></td>
									<td><textarea id=\"four_up_".$f."_copy\" name=\"four_up_".$f."_copy\" maxlength=\"250\">".esc_attr($page_meta['four_up_'.$f.'_copy'][0])."</textarea></td>
								</tr>
								<tr>
									<td><label for=\"four_up_".$f."_link\">Link</label></td>
									<td><input type=\"text\" id=\"four_up_".$f."_link\" name=\"four_up_".$f."_link\" value=\"".esc_attr($page_meta['four_up_'.$f.'_link'][0])."\" /></td>
								</tr>
							</table>
						</div>
					</td></tr>
				</table>
				";
			}
			?></div>
			
		</td></tr></table>
		
		<?php include('inc/meta-events-module.php'); ?>
		
		<table class="section"><tr><td>
			<h2>Key Message Module</h2>
			
			<div id="key_msg_options" class="control-wrap"><?php
			for($k=1;$k<=5;$k++){
				echo "
				<table class=\"control inline\" name=\"key_msg_".$k."_options\">
					<tr><td>
						<h3>Item ".$k."</h3>
						<div id=\"key_msg_".$k."_options\">
							<table class=\"group\">
								<tr>
									<td><label for=\"key_msg_".$k."_title\">Title</label></td>
									<td><input type=\"text\" class=\"large-text\" id=\"key_msg_".$k."_title\" name=\"key_msg_".$k."_title\" value=\"".esc_attr($page_meta['key_msg_'.$k.'_title'][0])."\" /></td>
								</tr>
								<tr>
									<td><label for=\"key_msg_".$k."_link\">Link</label></td>
									<td><input type=\"text\" id=\"key_msg_".$k."_link\" name=\"key_msg_".$k."_link\" value=\"".esc_attr($page_meta['key_msg_'.$k.'_link'][0])."\" /></td>
								</tr>
							</table>
						</div>
					</td></tr>
				</table>
				";
			}
			?></div>
			
		</td></tr></table>
		
		<?php include('inc/meta-content-feed.php'); ?>
		
		<?php include('inc/meta-map-module.php'); ?>
		
	</td></tr>
</table>

</div>