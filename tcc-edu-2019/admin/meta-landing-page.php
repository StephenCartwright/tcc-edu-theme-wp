<div id="page_settings" class="meta-settings">

<table id="page_main" class="group">
	<tr><td>
		
		<table class="control solo" name="page_options">
			<tr>
				<td><label for="quick_links_ul">Quick Links</label><sub>Html for the Quick Links at the top of this page. Should use <em>ul</em>, <em>li</em>, and <em>a</em> tags.</sub></td>
				<td><textarea class="wide long" id="quick_links_ul" name="quick_links_ul" maxlength="5000"><?php echo stripslashes(esc_attr($page_meta['quick_links_ul'][0])); ?></textarea></td>
			</tr>
			<tr>
				<td><label for="trending_links_db">Trending Links DB</label><sub>Table name to track trending links. Can be shared across multiple pages.</sub></td>
				<td><input type="text" class="_autoslug _format-slug" data-slugs="#post-title-0" id="trending_links_db" name="trending_links_db" value="<?php echo esc_attr($page_meta['trending_links_db'][0]); ?>" /></td>
			</tr>
			<tr>
				<td><label for="trending_links_force">Force List Generation</label><sub>Skip the 24-hour wait and immediately generate Trending Links for the last 24 hours</sub></td>
				<td><button id="trending_links_force" class="_ajaxer" data-path="/" data-key="force_trending_links" data-val="1">Generate Trending Links</button></td>
			</tr>
			<tr>
				<td><label for="hide_mytcc">Hide MyTCC</label></td>
				<td><input type="checkbox" name="hide_mytcc" id="hide_mytcc" value="1"<?php echo $page_meta['hide_mytcc'][0]=="1"?" checked=\"checked\"":""; ?>/></td>
			</tr>
			<tr>
				<td><label for="page_keywords">Page Keywords</label><sub>Additional search terms that don't already exist in the page content, repeated terms will increase relevance</sub></td>
				<td>
				<textarea id="page_keywords" name="page_keywords" maxlength="1250"><?php echo stripslashes(esc_attr($page_meta['page_keywords'][0])); ?></textarea>
				</td>
			</tr>
		</table>
		
		<?php include('inc/meta-slides.php'); ?>
		
		<?php include('inc/meta-events-module.php'); ?>
		
		<?php include('inc/meta-content-feed.php'); ?>
		
	</td></tr>
</table>

</div>