<div id="page_settings" class="meta-settings">

<table id="page_main" class="group">
	<tr><td>
		
		<table class="control solo" name="page_options">
			<tr>
				<td><label for="campus_video_src">Video Source</label><sub>Format should be h264 encoded mp4</sub></td>
				<td>
					<div class="editor">
						<div class="editor-control">
							<input type="button" class="button _media-video-select" data-id="#campus_video_id" value="Choose" />
							<input type="button" class="button _media-clear" data-id="#campus_video_id, #campus_video_src" value="Clear" />
							<input type="text" id="campus_video_id" class="vid entry _vid-previews" data-pre="#campus_video_src_pre" data-simple-src="#campus_video_src" name="campus_video_id" value="<?php echo esc_attr($page_meta['campus_video_id'][0]); ?>" />
							<input type="text" id="campus_video_src" class="vid _vid-simple-previews" data-pre="#campus_video_src_pre" data-id-src="#campus_video_id" name="campus_video_src" value="<?php echo esc_attr($page_meta['campus_video_src'][0]); ?>" />
						</div>
						<div class="summary video-preview" id="campus_video_src_pre">
							<div class="info"></div>
							<div class="video empty"></div>
						</div>
					</div>
				</td>
			</tr>
			<tr>
				<td><label for="campus_video_poster">Poster Image</label><sub>Override the default first video frame. Should be 16:9 aspect ratio.</sub></td>
				<td>
					<div class="editor">
						<div class="editor-control">
							<input type="button" class="button _media-select" data-id="#campus_video_poster" value="Choose" />
							<input type="button" class="button _media-clear" data-id="#campus_video_poster" value="Clear" />
							<input type="text" id="campus_video_poster" class="_previews" data-pre="#campus_video_poster_pre" name="campus_video_poster" value="<?php echo esc_attr($page_meta['campus_video_poster'][0]); ?>" />
						</div>
						<div class="summary image-preview empty" id="campus_video_poster_pre"></div>
					</div>
				</td>
			</tr>
			<tr>
				<td><label for="campus_video_captions">Video Captions</label><sub>A WebVTT (.vtt) captions file</sub></td>
				<td>
					<div class="editor">
						<div class="editor-control">
							<input type="button" class="button _media-vtt-select" data-id="#campus_video_captions" value="Choose" />
							<input type="button" class="button _media-clear" data-id="#campus_video_captions" value="Clear" />
							<input type="text" id="campus_video_captions" name="campus_video_captions" value="<?php echo esc_attr($page_meta['campus_video_captions'][0]); ?>" />
						</div>
					</div>
				</td>
			</tr>
			<tr>
				<td><label for="page_keywords">Page Keywords</label><sub>Additional search terms that don't already exist in the page content, repeated terms will increase relevance</sub></td>
				<td>
				<textarea id="page_keywords" name="page_keywords" maxlength="1250"><?php echo stripslashes(esc_attr($page_meta['page_keywords'][0])); ?></textarea>
				</td>
			</tr>
		</table>
		
		<table class="section"><tr><td>
			<h2>Page Sidebar</h2>
			
			<div id="page_sidebar_options" class="control-wrap">
				<table class="control" name="page_sidebar_options">
					<tr>
						<td><label for="page_sidebar">Sidebar</label></td>
						<td>
							<select id="page_sidebar" class="_tabber" data-tabs="#page_sidebar_types" name="page_sidebar"><?php
								echo "<option value=\"custom\"".(!$page_meta['page_sidebar'][0]||$page_meta['page_sidebar'][0]=="custom"?" selected":"").">Custom</option>";
								
								for($c=1;$c<=15;$c++){
									if(get_option('campus_'.$c.'_show')){
										$campus_id = get_option('campus_'.$c.'_id')?get_option('campus_'.$c.'_id'):sanitize_title(get_option('campus_'.$c.'_title'));
										$campus = tccedu_get_campus($campus_id);
										echo "<option value=\"campus_".$campus_id."\"".($page_meta['page_sidebar'][0]=="campus_".$campus_id?" selected":"").">Campus Info: ".$campus['title']."</option>";
									}
								}
							?></select>
						</td>
					</tr>
					<tr>
						<td><label for="page_sidebar_not_sticky">Disable Sticky Effect</label></td>
						<td><input type="checkbox" name="page_sidebar_not_sticky" id="page_sidebar_not_sticky" value="1"<?php echo $page_meta['page_sidebar_not_sticky'][0]=="1"?" checked=\"checked\"":""; ?>/></td>
					</tr>
					<tr id="page_sidebar_types" class="_tabbed">
						<td colspan="2"><?php
						for($c=1;$c<=15;$c++){
							if(get_option('campus_'.$c.'_show')){
								$campus_id = get_option('campus_'.$c.'_id')?get_option('campus_'.$c.'_id'):sanitize_title(get_option('campus_'.$c.'_title'));
								$campus = tccedu_get_campus($campus_id);
								echo "
							<div class=\"tab editor-styles-wrapper\" data-tab-id=\"campus_".$campus_id."\">
								(Campus info for: ".$campus['title'].")
							</div>";
							}
						}
						?>
							<div class="tab" data-tab-id="custom">
					
								<div id="custom_sidebar_options">
									<table class="group">
										<tr>
											<td><label for="custom_sidebar_title">Title</label><sub>Supports html</sub></td>
											<td><textarea id="custom_sidebar_title" name="custom_sidebar_title" maxlength="1250"><?php echo stripslashes(esc_attr($page_meta['custom_sidebar_title'][0])); ?></textarea></td>
										</tr>
										<tr>
											<td><label for="custom_sidebar_copy">Copy</label><sub>Supports html</sub></td>
											<td><textarea class="medium" id="custom_sidebar_copy" name="custom_sidebar_copy" maxlength="2500"><?php echo stripslashes(esc_attr($page_meta['custom_sidebar_copy'][0])); ?></textarea></td>
										</tr>
										<tr>
											<td><label for="custom_sidebar_cta">Button CTA</label></td>
											<td><input type="text" id="custom_sidebar_cta" name="custom_sidebar_cta" placeholder="Learn More" value="<?php echo esc_attr($page_meta['custom_sidebar_cta'][0]); ?>" /></td>
										</tr>
										<tr>
											<td><label for="custom_sidebar_link">Button Link</label></td>
											<td><input type="text" id="custom_sidebar_link" name="custom_sidebar_link" value="<?php echo esc_attr($page_meta['custom_sidebar_link'][0]); ?>" /></td>
										</tr>
										<tr>
											<td><label for="custom_sidebar_btn_class">Button Class</label></td>
											<td><input type="text" id="custom_sidebar_btn_class" name="custom_sidebar_btn_class" placeholder="btn-4 btn-text btn-arrow" value="<?php echo esc_attr($page_meta['custom_sidebar_btn_class'][0]); ?>" /></td>
										</tr>
									</table>
								</div>
					
							</div>
						</td>
					</tr>
				</table>
			</div>
			
		</td></tr></table>
		
		<?php include('inc/meta-events-module.php'); ?>
		
		<?php include('inc/meta-map-module.php'); ?>
		
		<?php include('inc/meta-content-feed.php'); ?>
		
	</td></tr>
</table>

</div>