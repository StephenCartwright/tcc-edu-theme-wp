var el = wp.element.createElement;
var BlockControls = wp.editor.BlockControls;
var InspectorControls = wp.editor.InspectorControls;
var PlainText = wp.editor.PlainText;
var PanelBody = wp.components.PanelBody;
var TextControl = wp.components.TextControl;
var SelectControl = wp.components.SelectControl;
var CheckboxControl = wp.components.CheckboxControl;

wp.blocks.registerBlockType('tcc-edu/button',{
	title:'TCC Button',
	icon:'marker',
	category:'tcc-edu',
	attributes:{
		type:{
			type:'string',
			source:'attribute',
			selector:'a.btn',
			attribute:'data-type',
			default:"1"
		},
		icon:{
			type:'string',
			source:'attribute',
			selector:'a.btn',
			attribute:'data-icon',
			default:""
		},
		var_min:{
			type:'string',
			source:'attribute',
			selector:'a.btn',
			attribute:'data-min',
			default:0
		},
		var_text:{
			type:'string',
			source:'attribute',
			selector:'a.btn',
			attribute:'data-text',
			default:0
		},
		target:{
			type:'string',
			source:'attribute',
			selector:'a.btn',
			attribute:'target',
			default:''
		},
		title:{
			type:'array',
			source:'children',
			selector:'label'
		},
		link:{
			type:'url'
		},
		label:{
			type:'string',
			selector:'label'
		}
	},

	edit:function(props){
		var attributes = props.attributes

		return [
			el(BlockControls,{
					key:'controls'
				},
				el('div',{
						className:'components-toolbar'
					},
					el(SelectControl,{
						onChange:function(_type){
							props.setAttributes({
								type:_type
							})
						},
						type:'select',
						options:[{
							label:"Style 1",
							value:"1"
						},{
							label:"Style 2",
							value:"2"
						},{
							label:"Style 3",
							value:"3"
						},{
							label:"Style 4",
							value:"4"
						},{
							label:"Style 5",
							value:"5"
						},{
							label:"Style 6",
							value:"6"
						},{
							label:"Style 7",
							value:"7"
						}],
						value:attributes.type
					}),
					el(SelectControl,{
						onChange:function(_icon){
							props.setAttributes({
								icon:_icon
							})
						},
						type:'select',
						options:[{
							label:"No icon",
							value:""
						},{
							label:"Right Arrow",
							value:"arrow"
						},{
							label:"Left Arrow",
							value:"arrow-left"
						},{
							label:"Download",
							value:"download"
						}],
						value:attributes.icon
					}),
					el(CheckboxControl,{
						label:'Small',
						checked:attributes.var_min=="true",
						onChange:function(_min){
							props.setAttributes({
								var_min:_min?"true":""
							})
						},
						value:true
					}),
					el(CheckboxControl,{
						label:'Borderless',
						checked:attributes.var_text=="true",
						onChange:function(_text){
							props.setAttributes({
								var_text:_text?"true":""
							})
						},
						value:true
					}),
					el(CheckboxControl,{
						label:'Opens New Window',
						checked:attributes.target,
						onChange:function(_blank){
							props.setAttributes({
								target:_blank?"_blank":""
							})
						},
						value:true
					}),
					el('button',{
						className:'components-button components-icon-button components-toolbar__control btn-get-html',
						onClick:function(){
							var _html = "<a"+(attributes.link?" href=\""+attributes.link+"\"":"")+" class=\""+("btn"+(attributes.type!=="1"?" btn-"+attributes.type:"")+(attributes.icon?" btn-"+attributes.icon:"")+(attributes.var_text=="true"?" btn-text":"")+(attributes.var_min=="true"?" btn-min":""))+"\""+(attributes.target?" target=\"_blank\"":"")+">"+attributes.label+"</a>";
							if(prompt("Copy HTML to clipboard?",_html)){
								if(!$("#block-copy-html").length) $("body").prepend("<input id=\"block-copy-html\" style=\"position:absolute;top:-200%;\"></input>");
								$("#block-copy-html").val(_html).select();
								document.execCommand("copy");
							}
						}
					},"Copy HTML")
				)
			),

			el(InspectorControls,{
					key:'inspector'
				},
				el(PanelBody,{
						title:'TCC Button Settings',
						className:'block-tcc-btn',
						initialOpen:true
					},

					el(SelectControl,{
						onChange:function(_type){
							props.setAttributes({
								type:_type
							})
						},
						type:'select',
						options:[{
							label:"Style 1",
							value:"1"
						},{
							label:"Style 2",
							value:"2"
						},{
							label:"Style 3",
							value:"3"
						},{
							label:"Style 4",
							value:"4"
						},{
							label:"Style 5",
							value:"5"
						},{
							label:"Style 6",
							value:"6"
						},{
							label:"Style 7",
							value:"7"
						}],
						value:attributes.type
					}),
					el(SelectControl,{
						onChange:function(_icon){
							props.setAttributes({
								icon:_icon
							})
						},
						type:'select',
						options:[{
							label:"No icon",
							value:""
						},{
							label:"Right Arrow",
							value:"arrow"
						},{
							label:"Left Arrow",
							value:"arrow-left"
						},{
							label:"Download",
							value:"download"
						}],
						value:attributes.icon
					}),
					el(CheckboxControl,{
						label:'Small',
						checked:attributes.var_min=="true",
						onChange:function(_min){
							props.setAttributes({
								var_min:_min?"true":""
							})
						},
						value:true
					}),
					el(CheckboxControl,{
						label:'Borderless',
						checked:attributes.var_text=="true",
						onChange:function(_text){
							props.setAttributes({
								var_text:_text?"true":""
							})
						},
						value:true
					}),
					el(CheckboxControl,{
						label:'Opens New Window',
						checked:attributes.target,
						onChange:function(_blank){
							props.setAttributes({
								target:_blank?"_blank":""
							})
						},
						value:true
					}),
					el(TextControl,{
						type:'url',
						label:'Link',
						value:attributes.link,
						onChange:function(_link){
							props.setAttributes({
								link:_link
							})
						}
					}),
					el(TextControl,{
						type:'string',
						label:'Label',
						value:attributes.label,
						onChange:function(_label){
							props.setAttributes({
								label:_label
							})
						}
					}),
					el('button',{
						className:'btn-get-html',
						onClick:function(){
							var _html = "<a"+(attributes.link?" href=\""+attributes.link+"\"":"")+" class=\""+("btn"+(attributes.type!=="1"?" btn-"+attributes.type:"")+(attributes.icon?" btn-"+attributes.icon:"")+(attributes.var_text=="true"?" btn-text":"")+(attributes.var_min=="true"?" btn-min":""))+"\""+(attributes.target?" target=\"_blank\"":"")+">"+attributes.label+"</a>";
							if(prompt("Copy HTML to clipboard?",_html)){
								if(!$("#block-copy-html").length) $("body").prepend("<input id=\"block-copy-html\" style=\"position:absolute;top:-200%;\"></input>");
								$("#block-copy-html").val(_html).select();
								document.execCommand("copy");
							}
						}
					},"Copy HTML")
				)
			),

			el('div',{
					className:props.className+" tcc-btn"
				},
				el('div',{
						className:'_left'
					},
					el('div',{
							className:'btn-wrap'
						},
						el('a',{
								className:"btn"+(attributes.type!=="1"?" btn-"+attributes.type:"")+(attributes.icon?" btn-"+attributes.icon:"")+(attributes.var_text=="true"?" btn-text":"")+(attributes.var_min=="true"?" btn-min":"")
							},
							attributes.label
						)
					),
				),
				el('div',{
						className:'_right'
					},
					el(PlainText,{
						key:'editable',
						className:'nonh3',
						placeholder:'Button Label',
						keepPlaceholderOnFocus:true,
						value:attributes.label,
						onChange:function(_label){
							props.setAttributes({
								label:_label
							})
						}
					}),
					el(TextControl,{
						key:'editable',
						placeholder:'Button Link',
						keepPlaceholderOnFocus:true,
						value:attributes.link,
						onChange:function(_link){
							props.setAttributes({
								link:_link
							})
						}
					}),
				)
			)

		]
	},

	save:function(props){
		var attributes = props.attributes

		return (
			el('div',{},
				el('a',{
						className:"btn"+(attributes.type!=="1"?" btn-"+attributes.type:"")+(attributes.icon?" btn-"+attributes.icon:"")+(attributes.var_min=="true"?" btn-min":"")+(attributes.var_text=="true"?" btn-text":""),
						'data-type':attributes.type,
						'data-icon':attributes.icon,
						'data-min':attributes.var_min,
						'data-text':attributes.var_text,
						href:attributes.link,
						target:attributes.target,
						rel:"noopener noreferrer"
					},
					attributes.label
				)
			)
		)
	}
})