var el = wp.element.createElement;
var BlockControls = wp.editor.BlockControls;
var MediaUpload = wp.editor.MediaUpload;
var Button = wp.components.Button;

wp.blocks.registerBlockType('tcc-edu/layout-image',{
	title:'TCC Layout Image',
	icon:'format-image',
	category:'tcc-edu',
	attributes:{
		mediaID:{
			type:'number'
		},
		mediaURL:{
			type:'string'
		}
	},

	edit:function(props){
		var attributes = props.attributes

		var onSelectImage = function(media){
			return props.setAttributes({
				mediaURL:media.sizes.large.url,
				mediaID:media.id
			})
		}

		return [
			el(BlockControls,{
					key:'controls'
				},
				el('div',{
						className:'components-toolbar'
					},
					el(MediaUpload,{
						onSelect:onSelectImage,
						type:'image',
						render:function(obj){
							return el(Button,{
									className:'components-icon-button components-toolbar__control',
									onClick:obj.open
								},
								el('svg',{
										className:'dashicon dashicons-edit',
										width:'20',
										height:'20'
									},
									el('path',{
										d:'M2.25 1h15.5c.69 0 1.25.56 1.25 1.25v15.5c0 .69-.56 1.25-1.25 1.25H2.25C1.56 19 1 18.44 1 17.75V2.25C1 1.56 1.56 1 2.25 1zM17 17V3H3v14h14zM10 6c0-1.1-.9-2-2-2s-2 .9-2 2 .9 2 2 2 2-.9 2-2zm3 5s0-6 3-6v10c0 .55-.45 1-1 1H5c-.55 0-1-.45-1-1V8c2 0 3 4 3 4s1-3 3-3 3 2 3 2z'
									})
								))
						}
					})
				)
			),
			el('div',{
					className:props.className
				},
				el('div',{
						className:attributes.mediaID?'tccedu-layout-image image-active':'tccedu-layout-image image-inactive',
						style:attributes.mediaID?{
							backgroundImage:'url('+attributes.mediaURL+')'
						}:{}
					},
					el(MediaUpload,{
						onSelect:onSelectImage,
						type:'image',
						value:attributes.mediaID,
						render:function(obj){
							return el(Button,{
									className:attributes.mediaID?'image-button':'button button-large',
									onClick:obj.open
								},
								'Choose Image'
							)
						}
					})
				)
			)
		]
	},

	save:function(props){
		var attributes = props.attributes

		return (
			el('div',{
					className:props.className
				},
				el('div',{
					className:'tccedu-layout-image',
					style:{
						backgroundImage:'url('+attributes.mediaURL+')'
					}
				})
			)
		)
	}
})