var el = wp.element.createElement;
var InnerBlocks = wp.editor.InnerBlocks;

wp.blocks.registerBlockType('tcc-edu/accordion-wrap',{
	title:'TCC Accordion',
	icon:'menu',
	category:'tcc-edu',
	edit:function(props){
		return (
			el('div',{
					className:"accordion"
				},
				el(InnerBlocks,{
					template:[
						['tcc-edu/accordion-item',{}]
					],
					templateLock:false,
					allowedBlocks:['tcc-edu/accordion-item']
				})
			)
		)
	},
	save:function(props){
		return (
			el('div',{
					className:"_accordion",
				},
				el('div',{
						className:"items",
					},
					el(InnerBlocks.Content,{})
				)
			)
		)
	},
});