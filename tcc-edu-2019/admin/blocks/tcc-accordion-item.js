var el = wp.element.createElement;
var InspectorControls = wp.editor.InspectorControls;
var InnerBlocks = wp.editor.InnerBlocks;
var PlainText = wp.editor.PlainText;
var PanelBody = wp.components.PanelBody;
var TextControl = wp.components.TextControl;

function parseSlug(_str){
	_str = _str.replace(/\s/g,"-").replace(/[^\w\-]/gi,"").toLowerCase();
	while(_str.substr(0,1)=="-") _str = _str.slice(1);
	while(_str.substr(_str.length-1,1)=="-") _str = _str.slice(0,_str.length-1);
	return _str;
}

wp.blocks.registerBlockType('tcc-edu/accordion-item',{
	title:'TCC Accordion Item',
	icon:'minus',
	category:'tcc-edu',
	attributes:{
		title:{
			type:'array',
			source:'children',
			selector:'label.nonh3'
		},
		slug:{
			type:'string',
			source:'attribute',
			attribute:'data-id',
			selector:'.item'
		},
		autoslug:{
			type:'string'
		}
	},

	edit:function(props){
		return [

			el(InspectorControls,{
					key:'inspector'
				},
				el(PanelBody,{
						title:'Accordion Item Settings',
						initialOpen:true
					},
					el(TextControl,{
						type:'string',
						label:'Title',
						value:props.attributes.title,
						onChange:function(_title){
							props.setAttributes({
								title:_title,
								autoslug:parseSlug(_title)
							})
						}
					}),
					el(TextControl,{
						type:'string',
						label:'Slug (Used in url)',
						placeholder:props.attributes.autoslug,
						value:props.attributes.slug,
						onChange:function(_slug){
							props.setAttributes({
								slug:_slug
							})
						}
					})
				)
			),

			el('div',{
					className:props.className+" accordion-item"
				},
				el('div',{
						className:"title"
					},
					el(PlainText,{
						key:'editable',
						className:'nonh3',
						placeholder:'Accordion Item Title',
						keepPlaceholderOnFocus:true,
						value:props.attributes.title,
						onChange:function(_title){
							props.setAttributes({
								title:_title,
								autoslug:parseSlug(_title)
							})
						}
					}),
				),
				el(InnerBlocks,{
					template:[],
					templateLock:false
				})
			)
		]
	},
	save:function(props){
		return (
			el('div',{
					'data-id':(props.attributes.slug?props.attributes.slug:props.attributes.autoslug),
					className:"item"
				},
				el('div',{
						className:"title",
					},
					el('div',{},
						el('label',{
								className:"nonh3"
							},
							props.attributes.title
						)
					)
				),
				el('div',{
						className:"copy",
					},
					el(InnerBlocks.Content,{})
				)
			)
		)


	},
});