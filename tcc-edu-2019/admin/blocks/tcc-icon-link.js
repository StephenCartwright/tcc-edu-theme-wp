var el = wp.element.createElement;
var RichText = wp.editor.RichText;
var BlockControls = wp.editor.BlockControls;
var MediaUpload = wp.editor.MediaUpload;
var InspectorControls = wp.editor.InspectorControls;
var PanelBody = wp.components.PanelBody;
var TextControl = wp.components.TextControl;
var Button = wp.components.Button;

wp.blocks.registerBlockType('tcc-edu/icon-link',{
	title:'TCC Icon Link',
	icon:'align-center',
	category:'tcc-edu',
	attributes:{
		mediaID:{
			type:'number'
		},
		mediaURL:{
			type:'string'
		},
		title:{
			type:'array',
			source:'children',
			selector:'label'
		},
		copy:{
			type:'array',
			source:'children',
			selector:'p'
		},
		link:{
			type:'url'
		}
	},

	edit:function(props){
		var attributes = props.attributes
		var link = props.attributes.link

		var onSelectImage = function(media){
			return props.setAttributes({
				mediaURL:media.url,
				mediaID:media.id
			})
		}

		return [
			el(BlockControls,{
					key:'controls'
				},
				el('div',{
						className:'components-toolbar'
					},
					el(MediaUpload,{
						onSelect:onSelectImage,
						type:'image',
						render:function(obj){
							return el(Button,{
									className:'components-icon-button components-toolbar__control',
									onClick:obj.open
								},
								el('svg',{
										className:'dashicon dashicons-edit',
										width:'20',
										height:'20'
									},
									el('path',{
										d:'M2.25 1h15.5c.69 0 1.25.56 1.25 1.25v15.5c0 .69-.56 1.25-1.25 1.25H2.25C1.56 19 1 18.44 1 17.75V2.25C1 1.56 1.56 1 2.25 1zM17 17V3H3v14h14zM10 6c0-1.1-.9-2-2-2s-2 .9-2 2 .9 2 2 2 2-.9 2-2zm3 5s0-6 3-6v10c0 .55-.45 1-1 1H5c-.55 0-1-.45-1-1V8c2 0 3 4 3 4s1-3 3-3 3 2 3 2z'
									})
								))
						}
					})
				)
			),

			el(InspectorControls,{
					key:'inspector'
				},
				el(PanelBody,{
						title:'Icon Link Settings',
						className:'block-icon-link',
						initialOpen:true
					},
					el(TextControl,{
						type:'url',
						label:'Page Link',
						value:link,
						onChange:function(newLink){
							props.setAttributes({
								link:newLink
							})
						}
					}))
			),

			el('div',{
					className:props.className+" icon-link"
				},
				el('div',{
						className:attributes.mediaID?'icon':'icon _empty',
						style:attributes.mediaID?{
							backgroundImage:'url('+attributes.mediaURL+')'
						}:{}
					},
					el(MediaUpload,{
						onSelect:onSelectImage,
						type:'div',
						value:attributes.mediaID,
						render:function(obj){
							return el(Button,{
									className:attributes.mediaID?'image-button':'button button-large',
									onClick:obj.open
								},
								'Choose Image'
							)
						}
					})
				),
				el(RichText,{
					key:'editable',
					tagName:'label',
					className:'nonh3',
					placeholder:'Link Title',
					keepPlaceholderOnFocus:true,
					value:attributes.title,
					onChange:function(newTitle){
						props.setAttributes({
							title:newTitle
						})
					}
				}),
				el(RichText,{
					key:'editable',
					tagName:'p',
					placeholder:'Link copy',
					keepPlaceholderOnFocus:true,
					value:attributes.copy,
					onChange:function(newCopy){
						props.setAttributes({
							copy:newCopy
						})
					}
				}),
				el(TextControl,{
					key:'editable',
					tagName:'a',
					placeholder:'Link URL',
					keepPlaceholderOnFocus:true,
					value:attributes.link,
					onChange:function(newLink){
						props.setAttributes({
							link:newLink
						})
					}
				}),

			)

		]
	},

	save:function(props){
		var attributes = props.attributes
		var link = props.attributes.link

		return (
			el('div',{
					className:"icon-link"+(attributes.link?" item":"")
				},
				attributes.link && el('a',{
					className:attributes.link?'hit':'hit _empty',
					href:attributes.link
				}),
				el('div',{
					className:'icon',
					style:{
						backgroundImage:'url('+attributes.mediaURL+')'
					}
				}),
				el(RichText.Content,{
					tagName:'label',
					className:'nonh3',
					value:attributes.title
				}),
				attributes.copy[0] && el(RichText.Content,{
					tagName:'p',
					value:attributes.copy
				}),
			)
		)
	}
})