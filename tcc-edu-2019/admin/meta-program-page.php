<div id="page_settings" class="meta-settings">

<table id="page_main" class="group">
	<tr><td>
		
		<table class="control solo" name="page_options">
			<tr>
				<td><label for="program_video_src">Video Source</label><sub>Format should be h264 encoded mp4</sub></td>
				<td>
					<div class="editor">
						<div class="editor-control">
							<input type="button" class="button _media-video-select" data-id="#program_video_id" value="Choose" />
							<input type="button" class="button _media-clear" data-id="#program_video_id, #program_video_src" value="Clear" />
							<input type="text" id="program_video_id" class="vid entry _vid-previews" data-pre="#program_video_src_pre" data-simple-src="#program_video_src" name="program_video_id" value="<?php echo esc_attr($page_meta['program_video_id'][0]); ?>" />
							<input type="text" id="program_video_src" class="vid _vid-simple-previews" data-pre="#program_video_src_pre" data-id-src="#program_video_id" name="program_video_src" value="<?php echo esc_attr($page_meta['program_video_src'][0]); ?>" />
						</div>
						<div class="summary video-preview" id="program_video_src_pre">
							<div class="info"></div>
							<div class="video empty"></div>
						</div>
					</div>
				</td>
			</tr>
			<tr>
				<td><label for="program_video_poster">Poster Image</label><sub>Override the default first video frame. Should be 16:9 aspect ratio.</sub></td>
				<td>
					<div class="editor">
						<div class="editor-control">
							<input type="button" class="button _media-select" data-id="#program_video_poster" value="Choose" />
							<input type="button" class="button _media-clear" data-id="#program_video_poster" value="Clear" />
							<input type="text" id="program_video_poster" class="_previews" data-pre="#program_video_poster_pre" name="program_video_poster" value="<?php echo esc_attr($page_meta['program_video_poster'][0]); ?>" />
						</div>
						<div class="summary image-preview empty" id="program_video_poster_pre"></div>
					</div>
				</td>
			</tr>
			<tr>
				<td><label for="program_video_captions">Video Captions</label><sub>A WebVTT (.vtt) captions file</sub></td>
				<td>
					<div class="editor">
						<div class="editor-control">
							<input type="button" class="button _media-vtt-select" data-id="#program_video_captions" value="Choose" />
							<input type="button" class="button _media-clear" data-id="#program_video_captions" value="Clear" />
							<input type="text" id="program_video_captions" name="program_video_captions" value="<?php echo esc_attr($page_meta['program_video_captions'][0]); ?>" />
						</div>
					</div>
				</td>
			</tr>
			<tr>
				<td><label for="program_guide">Program Guide</label><sub></sub></td>
				<td>
					<select id="program_guide" name="program_guide"><?php
						echo "<option value=\"\"".(!$page_meta['program_guide'][0]?" selected":"")."></option>";
						foreach(get_posts(array('showposts'=>-1,'post_type'=>'program-guide','order'=>'ASC','orderby'=>'title')) as $program_guide){
							$guide_meta = get_post_meta($program_guide->ID);
							$guide_id = $guide_meta['guide_id'][0]?$guide_meta['guide_id'][0]:sanitize_title($program_guide->post_title);
							echo "<option value=\"".$guide_id."\"".($page_meta['program_guide'][0]==$guide_id?" selected":"").">".$program_guide->post_title."</option>";
						}
					?></select>
				</td>
			</tr>
			<tr>
				<td><label for="program_is_this_1">Is This Program For You?</label><sub><em>Yes, if you:</em></sub></td>
				<td>
					<input type="text" id="program_is_this_1" name="program_is_this_1" value="<?php echo esc_attr($page_meta['program_is_this_1'][0]); ?>" />
					<input type="text" id="program_is_this_2" name="program_is_this_2" value="<?php echo esc_attr($page_meta['program_is_this_2'][0]); ?>" />
					<input type="text" id="program_is_this_3" name="program_is_this_3" value="<?php echo esc_attr($page_meta['program_is_this_3'][0]); ?>" />
				</td>
			</tr>
			<!--tr>
				<td><label for="program_explore_link">Explore Careers and Salaries link</label></td>
				<td><input type="text" id="program_explore_link" name="program_explore_link" value="<?php echo esc_attr($page_meta['program_explore_link'][0]); ?>" /></td>
			</tr-->
			<tr>
				<td><label>Career Coach Values</label></td>
				<td>
					<table class="group">
						<tr>
							<td><label for="program_career_coach_slug">Slug</label></td>
							<td><input type="text" id="program_career_coach_slug" name="program_career_coach_slug" value="<?php echo esc_attr($page_meta['program_career_coach_slug'][0]); ?>" /></td>
						</tr>
						<tr>
							<td><label for="program_career_coach">IDs</label></td>
							<td><textarea id="program_career_coach" name="program_career_coach" maxlength="1250"><?php echo stripslashes(esc_attr($page_meta['program_career_coach'][0])); ?></textarea></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td><label for="page_keywords">Page Keywords</label><sub>Additional search terms that don't already exist in the page content, repeated terms will increase relevance</sub></td>
				<td>
					<textarea id="page_keywords" name="page_keywords" maxlength="1250"><?php echo stripslashes(esc_attr($page_meta['page_keywords'][0])); ?></textarea>
				</td>
			</tr>
		</table>
		
		<?php include('inc/meta-content-feed.php'); ?>
		
	</td></tr>
</table>

</div>