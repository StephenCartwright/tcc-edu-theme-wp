<?php if(!$tccedu_meta) return; ?>

<div class="wrap">
<h2>TCC.edu Theme Settings</h2>
<?php if($meta_state) echo '<div id="setting-error-settings_updated" class="updated settings-error"><p><strong>'.$meta_state.'</strong></p></div>'; ?>
<form method="post" action="admin.php?page=site-settings-forms">
<?php wp_nonce_field('tccedu_meta','tccedu_meta_nonce'); ?>

<div id="tccedu_settings" class="meta-settings">
	<table class="form-table">
		<tr>
			<td>
			
		<p>These JSON objects control the forms on the site. Be careful!</p>
		
		<table class="section"><tr><td>
			<h2>Quick TCC Form</h2>
			<div id="quicktcc_control" class="control-wrap">
				<table class="group">
					<tr>
						<td><textarea id="quicktcc_form_stages" class="very-long" name="quicktcc_form_stages" placeholder="{}"><?php echo stripslashes(get_option('quicktcc_form_stages')); ?></textarea></td>
					</tr>
				</table>
			</div>
		</td></tr></table>
		
		<table class="section"><tr><td>
			<h2>Program Sidebar Form</h2>
			<div id="sidebar_control" class="control-wrap">
				<table class="group">
					<tr>
						<td><textarea id="sidebar_form_stages" class="very-long" name="sidebar_form_stages" placeholder="{}"><?php echo stripslashes(get_option('sidebar_form_stages')); ?></textarea></td>
					</tr>
				</table>
			</div>
		</td></tr></table>
		
			</td>
		</tr>
	</table>
</div>

<?php submit_button('Save Changes','primary','submit-form'); ?>
	
</form>
</div>