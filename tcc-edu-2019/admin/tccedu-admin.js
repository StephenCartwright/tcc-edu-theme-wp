jQuery(document).ready(function(){
	window.$ = jQuery;
	initControls($(".meta-settings"));
});

var initControls = function(_el){
	
	_el.find("input._media-select").on("click",function(){
		window.media_target = $(this).data("id");
		if(window.meta_image_frame){
			window.meta_image_frame.open();
			return;
		}
		window.meta_image_frame = wp.media.frames.meta_image_frame = wp.media({
			title:"Choose or Upload an Image",
			button:{text:"Use this image"},
			library:{type:"image"}
		});
		window.meta_image_frame.on("select", function(){
			var _att = window.meta_image_frame.state().get("selection").first().toJSON();
			$(window.media_target).val(_att.url).trigger("change");
		});
		window.meta_image_frame.open();
	});
	
	_el.find("input._media-clear").on("click",function(){
		$($(this).data("id")).val("").trigger("change");
	});
	
	_el.find("input._previews").each(function(){
		if($(this).data("pre")){
			$(this).on("change",function(){
				//console.log($(this).val());
				$($(this).data("pre")).toggleClass("empty",($(this).val()=="")).css({"background-image":"url("+$(this).val()+")"});
			}).trigger("change");
		}
	});
	
	_el.find("textarea._svg-previews").each(function(){
		if($(this).data("pre")){
			$(this).on("change",function(){
				$($(this).data("pre")).toggleClass("empty",($(this).val()=="")).html("<div class=\"icon icon-large\">"+$(this).val()+"</div>");
			}).trigger("change");
		}
	});
	
	_el.find("input._toggler").on("change",function(){
		//console.log("toggler change");
		var _on = $(this).prop("checked");
		$($(this).data("toggles")).toggleClass("toggle-off",!_on);
		$($(this).data("toggles")).find("input, textarea").attr("readonly",_on?false:true);
	}).trigger("change");
	
	
	_el.find("select._tabber").on("change",function(){
		var _tab = $(this).val();
		var _el = $($(this).data("tabs"));
		_el.find(".tab").each(function(){
			var _ids = $(this).data("tab-id").split?$(this).data("tab-id").split(","):[$(this).data("tab-id")];
			if(_ids.indexOf(_tab)>=0){
				$(this).addClass("on").show();
			} else {
				$(this).removeClass("on").hide();
			}
		});
	}).trigger("change");
	
	_el.find("._ajaxer").on("click",function(){
		//console.log("_ajaxer click");
		var _path = $(this).data("path")?$(this).data("path"):ajaxurl;
		var _key = $(this).data("key");//.toLowerCase();//"force_trending_links"
		var _val = $(this).data("val");
		var _data = {};
		_data[_key] = _val;
		
		$.ajax({url:_path,type:"POST",data:_data,dataType:"json"}).done(function(_res){ console.log(_res); }).fail(function(_res){ console.log("fail"); console.log(_res); });
		
		return;
		
		if(_key&&_val){
			$.ajax({url:_path,type:"POST",data:{_key:_val},dataType:"json"}).done(function(_res){ console.log(_res); }).fail(function(_res){ console.log("fail!"); console.log(_res); });
		}
	});
	
	
	_el.find("._copy_counts").on("keyup change",function(){
		var _count = $($(this).data("counter"));
		_count.html($(this).val().length);
	}).trigger("change");
	
	if(_el.find("input._date").length&&_el.find("input._date").datepicker) _el.find("input._date").datepicker({dateFormat:"mm/dd/yy"});
	
	_el.find("input._autoslug").each(function(){
		var _slug = $(this).data("slugs");
		var _out = $(this);
		var _checks = 0;
		var _check_tmr = setInterval(function(){
			if($(_slug).length){
				var _in = $(_slug);
				_in.on("keyup change",function(){
					_out.attr("placeholder",parseSlug($(this).val()));
				}).trigger("change");
				return clearInterval(_check_tmr);
			}
			_checks++;
			if(_checks>=20) clearInterval(_check_tmr);
		},200);
	});
	
	_el.find("input._format-slug").on("change",function(){
		//console.log("format slug: "+$(this).val());
		$(this).val(parseSlug($(this).val()));
	});
	
	
	_el.find("input._media-pdf-select").on("click",function(){
		window.media_target = $(this).data("id");
		if(window.meta_pdf_frame){
			window.meta_pdf_frame.open();
			return;
		}
		window.meta_pdf_frame = wp.media.frames.meta_pdf_frame = wp.media({
			title:"Choose or Upload a PDF",
			button:{text:"Use this PDF"},
			library:{type:"application/pdf"}
		});
		window.meta_pdf_frame.on("select", function(){
			var _att = window.meta_pdf_frame.state().get("selection").first().toJSON();
			$(window.media_target).val(_att.id).trigger("change");
			if($(window.media_target).data("simple-src")){
				$($(window.media_target).data("simple-src")).val(_att.url).trigger("change");
			}
		});
		window.meta_pdf_frame.open();
	});
	
	_el.find("input._pdf-previews").on("change",parsePDFEntry).trigger("change");
	
	_el.find("input._pdf-simple-previews").each(function(){
		if($(this).data("pre")){
			$(this).on("change",function(){
				if($(this).val()){
					$($(this).data("pre")).find(".link").removeClass("empty").html("<a href=\""+$(this).val()+"\" target=\"_blank\">Preview</a>");
					if($(this).data("id-src")&&!$($(this).data("id-src")).val()) $($(this).data("pre")).find(".info").empty().addClass("empty");
				} else {
					$($(this).data("pre")).find(".link").empty().addClass("empty");
					if($(this).data("id-src")&&!$($(this).data("id-src")).val()) $($(this).data("pre")).find(".info").removeClass("empty").html("No PDF Selected");
				}
			}).trigger("change");
		}
	});
	
	_el.find("input._media-vtt-select").on("click",function(){
		window.media_target = $(this).data("id");
		if(window.meta_vtt_frame){
			window.meta_vtt_frame.open();
			return;
		}
		window.meta_vtt_frame = wp.media.frames.meta_vtt_frame = wp.media({
			title:"Choose or Upload a WebVTT (.vtt) File",
			button:{text:"Use this WebVTT File"},
			library:{type:"text/vtt"}
		});
		window.meta_vtt_frame.on("select", function(){
			var _att = window.meta_vtt_frame.state().get("selection").first().toJSON();
			$(window.media_target).val(_att.url).trigger("change");
			if($(window.media_target).data("simple-src")){
				$($(window.media_target).data("simple-src")).val(_att.url).trigger("change");
			}
		});
		window.meta_vtt_frame.open();
	});
	
	_el.find("input._media-video-select").on("click",function(){
		window.media_target = $(this).data("id");
		if(window.meta_video_frame){
			window.meta_video_frame.open();
			return;
		}
		window.meta_video_frame = wp.media.frames.meta_video_frame = wp.media({
			title:"Choose or Upload a Video",
			button:{text:"Use this Video"},
			library:{type:"video"}
		});
		window.meta_video_frame.on("select", function(){
			var _att = window.meta_video_frame.state().get("selection").first().toJSON();
			$(window.media_target).val(_att.id).trigger("change");
			if($(window.media_target).data("simple-src")){
				$($(window.media_target).data("simple-src")).val(_att.url).trigger("change");
			}
			
		});
		window.meta_video_frame.open();
	});
	
	_el.find("input._vid-previews").on("change",parseVidEntry).trigger("change");
	
	_el.find("input._vid-simple-previews").each(function(){
		if($(this).data("pre")){
			$(this).on("change",function(){
				if($(this).val()){
					$($(this).data("pre")).find(".video").removeClass("empty").html("<video width=\"100%\" height=\"auto\" controls><source src=\""+$(this).val()+"\" type=\"video/mp4\"></video>");
					if($(this).data("id-src")&&!$($(this).data("id-src")).val()) $($(this).data("pre")).find(".info").empty().addClass("empty");
				} else {
					$($(this).data("pre")).find(".video").empty().addClass("empty");
					if($(this).data("id-src")&&!$($(this).data("id-src")).val()) $($(this).data("pre")).find(".info").removeClass("empty").html("No Video Selected");
				}
			}).trigger("change");
		}
	});
	
	_el.find("input._media-select._ratios").each(function(){
		var _ratio_input = $(this).data("id")+"_ratio";
		var _width = $(this).data("id")+"_width";
		var _height = $(this).data("id")+"_height";
		$(_width+", "+_height).on("keyup change",function(){
			$(_ratio_input).val(($(_width).val()/$(_height).val()).toFixed(2));
		});
	});
	_el.find("input._media-select._ratios").off("click").on("click",function(){
		window.media_target = $(this).data("id");
		if(window.meta_image_ratio_frame){
			window.meta_image_ratio_frame.open();
			return;
		}
		window.meta_image_ratio_frame = wp.media.frames.meta_image_ratio_frame = wp.media({
			title:"Choose or Upload an Image",
			button:{text:"Use this image"},
			library:{type:"image"}
		});
		window.meta_image_ratio_frame.on("select", function(){
			var _att = window.meta_image_ratio_frame.state().get("selection").first().toJSON();
			$(window.media_target).val(_att.url).trigger("change");
			$(window.media_target+"_width").val(_att.width);
			$(window.media_target+"_height").val(_att.height);
			$(window.media_target+"_ratio").val((_att.width/_att.height).toFixed(2));
		});
		window.meta_image_ratio_frame.open();
	});
	
};

var parseSlug = function(_str){
	_str = _str.replace(/\s/g,"-").replace(/[^\w\-]/gi,"").toLowerCase();
	while(_str.substr(0,1)=="-") _str = _str.slice(1);
	while(_str.substr(_str.length-1,1)=="-") _str = _str.slice(0,_str.length-1);
	return _str;
}

var parsePDFEntry = function(){
	//console.log("parsePDFEntry"+$(this).val());
	var _pre = $($(this).attr("data-pre")).find(".info");
	if($(this).val()){
		_pre.html("<em>Loading...</em>");
		$.ajax({
			url:ajaxurl,
			type:"POST",
			dataType:"json",
			data:{"action":"get_att_meta",att:$(this).val()}
		}).done(function(_res){
			if(_res.title){
				_pre.html("<strong>"+_res.title+"</strong><br />"+(_res.desc?"<br /><p>"+_res.desc+"</p>":""));
			} else {
				_pre.html("No PDF Selected");
			}
		});
	} else {
		if(!$($(this).data("simple-src")).val()) $($(this).data("simple-src")).trigger("change");
	}
};

var parseVidEntry = function(){
	//console.log("parseVidEntry: "+$(this).val());
	var _pre = $($(this).attr("data-pre")).find(".info");
	if($(this).val()){
		_pre.html("<em>Loading...</em>");
		$.ajax({
			url:ajaxurl,
			type:"POST",
			dataType:"json",
			data:{"action":"get_att_meta",att:$(this).val()}
		}).done(function(_res){
			if(_res.title){
				_pre.html("<strong>"+_res.title+"</strong><br />Length: "+_res.length+", Resolution: "+_res.width+"x"+_res.height+(_res.desc?"<p>"+_res.desc+"</p>":"")+"</div>");
			} else {
				_pre.html("No Video Selected");
			}
		});
	} else {
		if(!$($(this).data("simple-src")).val()) $($(this).data("simple-src")).trigger("change");
	}
};