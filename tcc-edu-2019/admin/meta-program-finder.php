<div id="page_settings" class="meta-settings">

<table id="page_main" class="group">
	<tr><td>
		
		<table class="control solo" name="page_options">
			<tr>
				<td><label for="page_shy">Hide in Section Nav</label></td>
				<td><input type="checkbox" name="page_shy" id="page_shy" value="1"<?php echo $page_meta['page_shy'][0]=="1"?" checked=\"checked\"":""; ?>/></td>
			</tr>
			<tr>
				<td><label for="page_keywords">Page Keywords</label><sub>Additional search terms that don't already exist in the page content, repeated terms will increase relevance</sub></td>
				<td>
				<textarea id="page_keywords" name="page_keywords" maxlength="1250"><?php echo stripslashes(esc_attr($page_meta['page_keywords'][0])); ?></textarea>
				</td>
			</tr>
		</table>
		
		<table class="section"><tr><td>
			<h2>Program Finder</h2>
			
			<div id="program_finder_options" class="control-wrap">
			
				<table class="control" name="program_finder_mod_options">
					<tr>
						<td><label for="program_finder_maxpage">Trim Results</label><sub>Show this many results before "Load More". Always show all results with <em>"-1"</em></sub></td>
						<td><input type="number" id="program_finder_maxpage" name="program_finder_maxpage" placeholder="12" value="<?php echo esc_attr($page_meta['program_finder_maxpage'][0]); ?>" /></td>
					</tr>
					<tr>
						<td><label for="program_finder_locked">Disable Controls</label><sub>Hide search and sort controls</sub></td>
						<td><input type="checkbox" name="program_finder_locked" id="program_finder_locked" value="1"<?php echo $page_meta['program_finder_locked'][0]=="1"?" checked=\"checked\"":""; ?>/></td>
					</tr>
					<tr>
						<td><label for="program_finder_set_career">Set Career Pathway</label></td>
						<td>
							<select id="program_finder_set_career" name="program_finder_set_career"><?php
								echo "<option value=\"\"".(!$page_meta['program_finder_set_career'][0]?" selected":"")."></option>";
								foreach(json_decode(get_option('career_options_list')) as $option){
									echo "<option value=\"".$option->id."\"".($page_meta['program_finder_set_career'][0]==$option->id?" selected":"").">".$option->name."</option>";
								}
							?></select>
						</td>
					</tr>
					<tr>
						<td><label for="program_finder_set_degree">Set Degree Type</label></td>
						<td>
							<select id="program_finder_set_degree" name="program_finder_set_degree"><?php
								echo "<option value=\"\"".(!$page_meta['program_finder_set_degree'][0]?" selected":"")."></option>";
								foreach(json_decode(get_option('degree_options_list')) as $option){
									echo "<option value=\"".$option->id."\"".($page_meta['program_finder_set_degree'][0]==$option->id?" selected":"").">".$option->name."</option>";
								}
							?></select>
						</td>
					</tr>
					<tr>
						<td><label for="program_finder_set_type">Set Program Type</label></td>
						<td>
							<select id="program_finder_set_type" name="program_finder_set_type"><?php
								echo "<option value=\"\"".(!$page_meta['program_finder_set_type'][0]?" selected":"")."></option>";
								foreach(json_decode(get_option('program_type_options_list')) as $option){
									echo "<option value=\"".$option->id."\"".($page_meta['program_finder_set_type'][0]==$option->id?" selected":"").">".$option->name."</option>";
								}
							?></select>
						</td>
					</tr>
					<tr>
						<td><label for="program_finder_set_location">Set Location</label></td>
						<td>
							<select id="program_finder_set_location" name="program_finder_set_location"><?php
								echo "<option value=\"\"".(!$page_meta['program_finder_set_location'][0]?" selected":"")."></option>";
								for($c=1;$c<=15;$c++){
									if(get_option('campus_'.$c.'_show')){
										$campus_id = get_option('campus_'.$c.'_id')?get_option('campus_'.$c.'_id'):sanitize_title(get_option('campus_'.$c.'_title'));
										echo "<option value=\"".$campus_id."\"".($page_meta['program_finder_set_location'][0]==$campus_id?" selected":"").">".get_option('campus_'.$c.'_title')."</option>";
									}
								}
							?></select>
						</td>
					</tr>
					<tr>
						<td><label for="program_finder_set_interest">Set Interest Type</label></td>
						<td>
							<select id="program_finder_set_interest" name="program_finder_set_interest"><?php
								echo "<option value=\"\"".(!$page_meta['program_finder_set_interest'][0]?" selected":"")."></option>";
								foreach(json_decode(get_option('interest_options_list')) as $option){
									echo "<option value=\"".$option->id."\"".($page_meta['program_finder_set_interest'][0]==$option->id?" selected":"").">".$option->name."</option>";
								}
							?></select>
						</td>
					</tr>
				</table>
				
			</div>
			
		</td></tr></table>
		
		<?php include('inc/meta-events-module.php'); ?>
		
		<?php include('inc/meta-content-feed.php'); ?>
		
	</td></tr>
</table>

</div>