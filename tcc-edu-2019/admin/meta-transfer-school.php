<div id="page_settings" class="meta-settings">

<table id="page_main" class="group">
	<tr><td>
		
		<table class="control solo" name="school_options">
			<tr>
				<td><label for="school_type">School Type</label></td>
				<td>
					<select id="school_type" name="school_type">
						<option value=""<?php if(!$school_meta['school_type'][0]) echo " selected"; ?>></option>
						<option value="Public"<?php if($school_meta['school_type'][0]=="Public") echo " selected"; ?>>Public</option>
						<option value="Private"<?php if($school_meta['school_type'][0]=="Private") echo " selected"; ?>>Private</option>
					</select>
				</td>
			</tr>
			<tr>
				<td><label for="school_location">Location</label></td>
				<td><input type="text" id="school_location" name="school_location" value="<?php echo esc_attr($school_meta['school_location'][0]); ?>" /></td>
			</tr>
			<tr>
				<td><label for="school_gpa">Minimum GPA</label></td>
				<td><input type="text" id="school_gpa" name="school_gpa" value="<?php echo esc_attr($school_meta['school_gpa'][0]); ?>" /></td>
			</tr>
			<tr>
				<td><label for="school_url">Page URL</label></td>
				<td><input type="text" id="school_url" name="school_url" value="<?php echo esc_attr($school_meta['school_url'][0]); ?>" /></td>
			</tr>
			<tr>
				<td><label for="school_agreements">Transfer Agreements</label></td>
				<td>
					<div class="checkbox-group"><?php
						foreach(get_posts(array('showposts'=>-1,'post_type'=>'program','order'=>'ASC','orderby'=>'title')) as $option){
							echo "<div class=\"option\"><label><input type=\"checkbox\" name=\"school_agreements[]\" value=\"".$option->post_name."\"".(in_array($option->post_name,unserialize($school_meta['school_agreements'][0]))?" checked=\"checked\"":"")." />".$option->post_title."</label></div>";
						}
					?></div>
				</td>
			</tr>
		</table>
		
	</td></tr>
</table>

</div>