<?php if(!$tccedu_meta) return; ?>

<div class="wrap">
<h2>TCC.edu Theme Settings</h2>
<?php if($meta_state) echo '<div id="setting-error-settings_updated" class="updated settings-error"><p><strong>'.$meta_state.'</strong></p></div>'; ?>
<form method="post" action="admin.php?page=site-settings-programs">
<?php wp_nonce_field('tccedu_meta','tccedu_meta_nonce'); ?>

<div id="tccedu_settings" class="meta-settings">
	<table class="form-table">
		<tr>
			<td>
			
		<p>These values control options related to programs.</p>
			
		<table class="section"><tr><td>
			<h2>Career Pathways</h2>
			<p>Career pathways by which programs are tagged and searched. Add each item on a new line.</p>
			<div id="career_options" class="control-wrap">
				<table class="control">
					<tr>
						<td><?php
							foreach(json_decode(get_option('career_options_list')) as $option) $career_names[] = $option->name;
							echo "<textarea id=\"career_options_list\" class=\"long\" name=\"career_options_list\">".stripslashes(implode("\r\n",$career_names))."</textarea>";
						?></td>
					</tr>
				</table>
			</div>
		</td></tr></table>
		
		
		<table class="section"><tr><td>
			<h2>Program Types</h2>
			<p>Attributes by which programs are tagged and searched. Add each item on a new line.</p>
			<div id="program_type_options" class="control-wrap">
				<table class="control">
					<tr>
						<td><?php
							foreach(json_decode(get_option('program_type_options_list')) as $option) $program_type_names[] = $option->name;
							echo "<textarea id=\"program_type_options_list\" class=\"long\" name=\"program_type_options_list\">".stripslashes(implode("\r\n",$program_type_names))."</textarea>";
						?></td>
					</tr>
				</table>
			</div>
		</td></tr></table>
		
		
		<table class="section"><tr><td>
			<h2>Degree Types</h2>
			<p>Degree types by which programs are tagged and searched. Add each item on a new line.</p>
			<div id="degree_options" class="control-wrap">
				<table class="control">
					<tr>
						<td><?php
							foreach(json_decode(get_option('degree_options_list')) as $option) $degree_names[] = $option->name;
							echo "<textarea id=\"degree_options_list\" class=\"long\" name=\"degree_options_list\">".stripslashes(implode("\r\n",$degree_names))."</textarea>";
						?></td>
					</tr>
				</table>
			</div>
		</td></tr></table>
		
		
		<table class="section"><tr><td>
			<h2>Interest Types</h2>
			<p>Interests by which programs are tagged and searched. Add each item on a new line.</p>
			<div id="interest_options" class="control-wrap">
				<table class="control">
					<tr>
						<td><?php
							foreach(json_decode(get_option('interest_options_list')) as $option) $interest_names[] = $option->name;
							echo "<textarea id=\"interest_options_list\" class=\"long\" name=\"interest_options_list\">".stripslashes(implode("\r\n",$interest_names))."</textarea>";
						?></td>
					</tr>
				</table>
			</div>
		</td></tr></table>
		
		
			</td>
		</tr>
	</table>
</div>

<?php submit_button('Save Changes','primary','submit-form'); ?>
	
</form>
</div>