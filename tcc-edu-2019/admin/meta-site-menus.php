<?php if(!$tccedu_meta) return; ?>

<div class="wrap">
<h2>TCC.edu Theme Settings</h2>
<?php if($meta_state) echo '<div id="setting-error-settings_updated" class="updated settings-error"><p><strong>'.$meta_state.'</strong></p></div>'; ?>
<form method="post" action="admin.php?page=site-settings-menus">
<?php wp_nonce_field('tccedu_meta','tccedu_meta_nonce'); ?>

<div id="tccedu_settings" class="meta-settings">
	<table class="form-table">
		<tr>
			<td>
		
		<table class="section"><tr><td>
			<h2>Menu Features</h2>
			<p>These pieces of content appear in the main menu expansion area.</p>
			<div id="menu_features_options" class="control-wrap"><?php
			for($f=1;$f<=6;$f++){
				echo "
				<table class=\"control-group\" name=\"menu_feature_".$f."_options\">
					<tr>
						<td><label for=\"menu_feature_".$f."_title\">Title</label></td>
						<td><input type=\"text\" class=\"large-text\" id=\"menu_feature_".$f."_title\" name=\"menu_feature_".$f."_title\" value=\"".get_option('menu_feature_'.$f.'_title')."\" /></td>
					</tr>
					<tr>
						<td><label for=\"menu_feature_".$f."_copy\">Copy</label></td>
						<td><textarea id=\"menu_feature_".$f."_copy\" name=\"menu_feature_".$f."_copy\">".get_option('menu_feature_'.$f.'_copy')."</textarea></td>
					</tr>
					<tr>
						<td><label for=\"menu_feature_".$f."_target\">Target Item Label</label><sub>The parent menu item's text label, e.g. <em>\"About TCC\"</em></sub></td>
						<td><input type=\"text\" id=\"menu_feature_".$f."_target\" name=\"menu_feature_".$f."_target\" value=\"".get_option('menu_feature_'.$f.'_target')."\" /></td>
					</tr>
					<tr>
						<td><label for=\"menu_feature_".$f."_cta\">Button CTA</label></td>
						<td><input type=\"text\" id=\"menu_feature_".$f."_cta\" name=\"menu_feature_".$f."_cta\" placeholder=\"Learn More\" value=\"".get_option('menu_feature_'.$f.'_cta')."\" /></td>
					</tr>
					<tr>
						<td><label for=\"menu_feature_".$f."_link\">Button Link</label></td>
						<td><input type=\"text\" id=\"menu_feature_".$f."_link\" name=\"menu_feature_".$f."_link\" value=\"".get_option('menu_feature_'.$f.'_link')."\" /></td>
					</tr>
				</table>
				";
			}
			?></div>
			
		</td></tr></table>
		
			</td>
		</tr>
	</table>
</div>

<?php submit_button('Save Changes','primary','submit-form'); ?>
	
</form>
</div>