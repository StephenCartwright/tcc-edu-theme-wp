<div id="page_settings" class="meta-settings">

<table id="page_main" class="group">
	<tr><td>
		
		<table class="control solo" name="guide_options">
			<tr>
				<td><label for="guide_id">ID</label></td>
				<td><input type="text" class="_autoslug _format-slug" data-slugs="#title" id="guide_id" name="guide_id" value="<?php echo esc_attr($guide_meta['guide_id'][0]); ?>" /></td>
			</tr>
			<!--tr>
				<td><label for="guide_pdf">Guide PDF</label></td>
				<td>
					<div class="editor">
						<div class="editor-control">
							<input type="button" class="button _media-pdf-select" data-id="#guide_pdf_id" value="Choose PDF" />
							<input type="button" class="button _media-clear" data-id="#guide_pdf_id, #guide_pdf_src" value="Clear" />
							<input type="text" id="guide_pdf_id" class="entry _pdf-previews" data-pre="#guide_pdf_pre" data-simple-src="#guide_pdf_src" name="guide_pdf_id" value="<?php echo esc_attr($guide_meta['guide_pdf_id'][0]); ?>" />
							<input type="text" id="guide_pdf_src" class="_pdf-simple-previews" data-pre="#guide_pdf_pre" data-id-src="#guide_pdf_id" name="guide_pdf_src" value="<?php echo esc_attr($guide_meta['guide_pdf_src'][0]); ?>" />
						</div>
						<div class="summary" id="guide_pdf_pre">
							<div class="info"></div>
							<div class="link empty"></div>
						</div>
					</div>
				</td>
			</tr-->
			<tr>
				<td><label for="guide_sf_pdf">Salesforce PDF</label><sub>ID for download in Salesforce, e.g. <em>"0691R00000DyeXvQAJ"</em></sub></td>
				<td><input type="text" id="guide_sf_pdf" name="guide_sf_pdf" value="<?php echo esc_attr($guide_meta['guide_sf_pdf'][0]); ?>" /></td>
			</tr>
			<tr>
				<td><label for="guide_email_template">Email Template</label><sub>ID of auto-response template in Salesforce, e.g. <em>"13966"</em></sub></td>
				<td><input type="text" id="guide_email_template" name="guide_email_template" value="<?php echo esc_attr($guide_meta['guide_email_template'][0]); ?>" /></td>
			</tr>
			<tr>
				<td><label for="guide_specific_interest">Program</label><sub><strong>Specific_Program_Interest</strong> value in Salesforce, e.g. <em>"Graphic Design"</em></sub></td>
				<td>
					<select id="guide_specific_interest" name="guide_specific_interest"><?php
						echo "<option value=\"\"".(!$guide_meta['guide_specific_interest'][0]?" selected":"")."></option>";
						foreach(get_posts(array('showposts'=>-1,'post_type'=>'program','order'=>'ASC','orderby'=>'title')) as $option){
							$program_meta = get_post_meta($option->ID);
							$program_id = $program_meta['program_id'][0]?$program_meta['program_id'][0]:sanitize_title($option->post_title);
							echo "<option value=\"".$program_id."\"".($guide_meta['guide_specific_interest'][0]==$program_id?" selected":"").">".$option->post_title."</option>";
						}
					?></select>
					<input type="text" id="guide_specific_interest_alt" name="guide_specific_interest_alt" placeholder="Custom value" value="<?php echo esc_attr($guide_meta['guide_specific_interest_alt'][0]); ?>" />
				</td>
			</tr>
			<tr>
				<td><label for="guide_program_interest">Career</label><sub><strong>Program_of_Interest</strong> value for Salesforce, e.g. <em>"Arts & Humanities"</em></sub></td>
				<td>
					<select id="guide_program_interest" name="guide_program_interest"><?php
						echo "<option value=\"\"".(!$guide_meta['guide_program_interest'][0]?" selected":"")."></option>";
						foreach(json_decode(get_option('career_options_list')) as $option){
							echo "<option value=\"".$option->id."\"".($guide_meta['guide_program_interest'][0]==$option->id?" selected":"").">".$option->name."</option>";
						}
					?></select>
					<input type="text" id="guide_program_interest_alt" name="guide_program_interest_alt" placeholder="Custom value" value="<?php echo esc_attr($guide_meta['guide_program_interest_alt'][0]); ?>" />
				</td>
			</tr>
			<tr>
				<td><label for="guide_transfer_interest">Transfer</label><sub><strong>Transfer_Interest</strong> value in Salesforce</sub></td>
				<td>
					<select id="guide_transfer_interest" name="guide_transfer_interest">
						<option value=""<?php if(!$guide_meta['guide_transfer_interest'][0]) echo " selected"; ?>></option>
						<option value="yes"<?php if($guide_meta['guide_transfer_interest'][0]=="yes") echo " selected"; ?>>Yes</option>
						<option value="no"<?php if($guide_meta['guide_transfer_interest'][0]=="no") echo " selected"; ?>>No</option>
					</select>
				</td>
			</tr>
			<tr>
				<td><label for="guide_military_interest">Military</label><sub><strong>Prospect_Military_Status</strong> value in Salesforce</sub></td>
				<td>
					<select id="guide_military_interest" name="guide_military_interest">
						<option value=""<?php if(!$guide_meta['guide_military_interest'][0]) echo " selected"; ?>></option>
						<option value="Affiliated"<?php if($guide_meta['guide_military_interest'][0]=="Affiliated") echo " selected"; ?>>Affiliated</option>
					</select>
				</td>
			</tr>
		</table>
		
	</td></tr>
</table>

</div>