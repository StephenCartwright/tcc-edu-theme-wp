<div id="page_settings" class="meta-settings">

<table id="page_main" class="group">
	<tr><td>
		
		<table class="control solo" name="feeditem_options">
			
			<tr>
				<td><label for="feeditem_title">Title</label></td>
				<td><input type="text" class="large-text" id="feeditem_title" name="feeditem_title" placeholder="<?php echo $post->post_title; ?>" value="<?php echo esc_attr($feeditem_meta['feeditem_title'][0]); ?>" /></td>
			</tr>
			<tr>
				<td><label for="feeditem_slug">ID</label><sub>This slug is used in the feed item url</sub></td>
				<td><input type="text" id="feeditem_slug" class="_format-slug" name="feeditem_slug" placeholder="<?php echo $post->post_name; ?>" value="<?php echo esc_attr($feeditem_meta['feeditem_slug'][0]); ?>" /></td>
			</tr>
			<tr>
				<td><label for="feeditem_copy">Copy</label><sub>Trimmed after about 75 characters in feed views.</sub><sub class="_copy_count">Characters: <span id="feeditem_copy_count"></span></sub></td>
				<td><textarea id="feeditem_copy" class="_copy_counts" data-counter="#feeditem_copy_count" name="feeditem_copy" maxlength="250"><?php echo esc_attr($feeditem_meta['feeditem_copy'][0]); ?></textarea></td>
			</tr>
			<tr>
				<td class="labels"><label for="feeditem_img">Image</label><sub>Displayed as the background in feed views, and as the subject in lightbox views</sub></td>
				<td>
					<div class="editor">
						<div class="editor-control">
							<input type="button" class="button _media-select" data-id="#feeditem_img" value="Choose" />
							<input type="button" class="button _media-clear" data-id="#feeditem_img" value="Clear" />
							<input type="text" id="feeditem_img" class="_previews" data-pre="#feeditem_img_pre" name="feeditem_img" value="<?php echo esc_attr($feeditem_meta['feeditem_img'][0]); ?>" />
						</div>
						<div class="summary image-preview empty" id="feeditem_img_pre"></div>
					</div>
				</td>
			</tr>
			<tr>
				<td><label for="feeditem_weighted">Weight in Results</label><sub>Display this item before others in the otherwise random shuffle of results</sub></td>
				<td><input type="checkbox" name="feeditem_weighted" id="feeditem_weighted" value="1"<?php echo $feeditem_meta['feeditem_weighted'][0]=="1"?" checked=\"checked\"":""; ?>/></td>
			</tr>
			<tr>
				<td><label for="feeditem_type">Item Type</label></td>
				<td>
					<select id="feeditem_type" class="_tabber" data-tabs="#feeditem_types" name="feeditem_type">
						<option value="image"<?php if(!$feeditem_meta['feeditem_type'][0]||$feeditem_meta['feeditem_type'][0]=="image") echo " selected"; ?>>Image</option>
						<option value="video"<?php if($feeditem_meta['feeditem_type'][0]=="video") echo " selected"; ?>>Video</option>
						<option value="article"<?php if($feeditem_meta['feeditem_type'][0]=="article") echo " selected"; ?>>Article</option>
						<option value="link"<?php if($feeditem_meta['feeditem_type'][0]=="link") echo " selected"; ?>>Link</option>
					</select>
				</td>
			</tr>
			<tr>
				<td colspan="2" id="feeditem_types" class="_tabbed">
					
					<div class="tab" data-tab-id="video">
						<h2>Video Settings</h2>
						
						<div id="feeditem_video_options" class="control-wrap">
							<table class="control wide" name="feeditem_video_options">
								<tr>
									<td><label for="feeditem_video_src">Video Source</label><sub>Format should be h264 encoded mp4</sub></td>
									<td>
										<div class="editor">
											<div class="editor-control">
												<input type="button" class="button _media-video-select" data-id="#feeditem_video_id" value="Choose" />
												<input type="button" class="button _media-clear" data-id="#feeditem_video_id, #feeditem_video_src" value="Clear" />
												<input type="text" id="feeditem_video_id" class="vid entry _vid-previews" data-pre="#feeditem_video_src_pre" data-simple-src="#feeditem_video_src" name="feeditem_video_id" value="<?php echo esc_attr($feeditem_meta['feeditem_video_id'][0]); ?>" />
												<input type="text" id="feeditem_video_src" class="vid _vid-simple-previews" data-pre="#feeditem_video_src_pre" data-id-src="#feeditem_video_id" name="feeditem_video_src" value="<?php echo esc_attr($feeditem_meta['feeditem_video_src'][0]); ?>" />
											</div>
											<div class="summary video-preview" id="feeditem_video_src_pre">
												<div class="info"></div>
												<div class="video empty"></div>
											</div>
										</div>
									</td>
								</tr>
								<tr>
									<td><label for="feeditem_video_poster">Poster Image</label><sub>Override the default image used in feed views. Should be 16:9 aspect ratio.</sub></td>
									<td>
										<div class="editor">
											<div class="editor-control">
												<input type="button" class="button _media-select" data-id="#feeditem_video_poster" value="Choose" />
												<input type="button" class="button _media-clear" data-id="#feeditem_video_poster" value="Clear" />
												<input type="text" id="feeditem_video_poster" class="_previews" data-pre="#feeditem_video_poster_pre" name="feeditem_video_poster" value="<?php echo esc_attr($feeditem_meta['feeditem_video_poster'][0]); ?>" />
											</div>
											<div class="summary image-preview empty" id="feeditem_video_poster_pre"></div>
										</div>
									</td>
								</tr>
								<tr>
									<td><label for="feeditem_video_captions">Video Captions</label><sub>A WebVTT (.vtt) captions file</sub></td>
									<td>
										<div class="editor">
											<div class="editor-control">
												<input type="button" class="button _media-vtt-select" data-id="#feeditem_video_captions" value="Choose" />
												<input type="button" class="button _media-clear" data-id="#feeditem_video_captions" value="Clear" />
												<input type="text" id="feeditem_video_captions" name="feeditem_video_captions" value="<?php echo esc_attr($feeditem_meta['feeditem_video_captions'][0]); ?>" />
											</div>
										</div>
									</td>
								</tr>
							</table>
						</div>
					</div>
					<div class="tab" data-tab-id="article">
						<h2>Article Settings</h2>
						
						<div id="feeditem_article_options" class="control-wrap">
							<table class="control wide" name="feeditem_article_options">
								<tr>
									<td><label for="feeditem_article_author">Author</label></td>
									<td><input type="text" id="feeditem_article_author" name="feeditem_article_author" value="<?php echo esc_attr($feeditem_meta['feeditem_article_author'][0]); ?>" /></td>
								</tr>
								<tr>
									<td><label for="feeditem_article_date">Date</label></td>
									<td><input type="text" id="feeditem_article_date" class="_date" name="feeditem_article_date" placeholder="Select date" value="<?php echo esc_attr($feeditem_meta['feeditem_article_date'][0]); ?>" /></td>
								</tr>
								<tr>
									<td><label for="feeditem_article_excerpt">Article Excerpt</label><sub>Article preview displayed when this item is in lightbox views</sub></td>
									<td><textarea id="feeditem_article_excerpt" class="long" name="feeditem_article_excerpt" maxlength="1200"><?php echo esc_attr($feeditem_meta['feeditem_article_excerpt'][0]); ?></textarea></td>
								</tr>
								<tr>
									<td><label for="feeditem_article_cta">Button CTA</label></td>
									<td><input type="text" id="feeditem_article_cta" name="feeditem_article_cta" placeholder="Read More" value="<?php echo esc_attr($feeditem_meta['feeditem_article_cta'][0]); ?>" /></td>
								</tr>
								<tr>
									<td><label for="feeditem_article_link">Link</label></td>
									<td><input type="text" id="feeditem_article_link" name="feeditem_article_link" value="<?php echo esc_attr($feeditem_meta['feeditem_article_link'][0]); ?>" /></td>
								</tr>
							</table>
						</div>
						
					</div>
					<div class="tab" data-tab-id="link">
						<h2>Link Settings</h2>
						
						<div id="feeditem_link_options" class="control-wrap">
							<table class="control wide" name="feeditem_link_options">
								<tr>
									<td><label for="feeditem_link_cta">Button CTA</label><sub>Button is displayed only in lightbox views</sub></td>
									<td><input type="text" id="feeditem_link_cta" name="feeditem_link_cta" placeholder="Learn More" value="<?php echo esc_attr($feeditem_meta['feeditem_link_cta'][0]); ?>" /></td>
								</tr>
								<tr>
									<td><label for="feeditem_link_link">Link</label></td>
									<td><input type="text" id="feeditem_link_link" name="feeditem_link_link" value="<?php echo esc_attr($feeditem_meta['feeditem_link_link'][0]); ?>" /></td>
								</tr>
							</table>
						</div>
						
					</div>
				</td>
			</tr>
		</table>
		
	</td></tr>
</table>

</div>